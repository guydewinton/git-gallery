export default (x1, y1, x2, y2, x3, y3, bezels = null) => {

    const paths = [[Math.random() * 10, Math.random() * 10], [Math.random() * 10, Math.random() * 10]];

    // calculate angle given three points

    const findAngle = (A, B, C) => {
        const AB = Math.sqrt(Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2));
        const BC = Math.sqrt(Math.pow(B.x - C.x, 2) + Math.pow(B.y - C.y, 2));
        const AC = Math.sqrt(Math.pow(C.x - A.x, 2) + Math.pow(C.y - A.y, 2));
        return (Math.acos((BC * BC + AB * AB - AC * AC) / (2 * BC * AB)) * 180) / Math.PI;
    };

    // calculate hypotenuse (to divided by 1/3)

    const pythagorean = (sideA, sideB) => {
        return Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2));
    };

    // calculate angle from X: 0, y: 0 & 0 degrees positive x

    function calcAngleDegrees(x, y) {
        return Math.atan2(y, x) * 180 / Math.PI;
    }

    function findNewPoint(x, y, angle, distance) {
        const result = {};
        result.x = Math.cos(angle * Math.PI / 180) * distance + x;
        result.y = Math.sin(angle * Math.PI / 180) * distance + y;
        return result;
    }
    //
    // console.log('findNewPoint test: 5, 5, 0, 3: ', findNewPoint(5,5,0,3));
    // console.log('findNewPoint test: 5, 5, 90, 3: ', findNewPoint(5,5,90,3));

    const drawSVG = () => {

        // const aPoint = {x: paths[paths.length - 2][0], y: paths[paths.length - 2][1]};
        // const bPoint = {x: paths[paths.length - 1][0], y: paths[paths.length - 1][1]};
        // const cPoint = {x: Math.random() * 10, y: Math.random() * 10};

        const aPoint = {x: x1, y: y1};
        const bPoint = {x: x2, y: y2};
        const cPoint = {x: x3, y: y3};

        const bezierGenerator = (aPoint, bPoint, cPoint) => {

            const aAngle = calcAngleDegrees(aPoint.x - bPoint.x, aPoint.y - bPoint.y);
            const cAngle = calcAngleDegrees(cPoint.x - bPoint.x, cPoint.y - bPoint.y);

            const refAngleA = findAngle(aPoint, bPoint, {x: bPoint.x + 100, y: bPoint.y});
            const refAngleC = findAngle(cPoint, bPoint, {x: bPoint.x + 100, y: bPoint.y});

            const abcAngle = findAngle(aPoint, bPoint, cPoint);

            const abHyponuse = pythagorean(Math.abs(aPoint.x - bPoint.x), Math.abs(aPoint.y - bPoint.y));
            const bcHyponuse = pythagorean(Math.abs(cPoint.x - bPoint.x), Math.abs(cPoint.y - bPoint.y));

            let bezelDif = (360 - 180 - abcAngle) / 2;

            // console.log('=== VARIABLES ===');
            //
            // console.log(`abcAngle: ${abcAngle}`);
            //
            // console.log(`aAngle: ${aAngle}`);
            // console.log(`cAngle: ${cAngle}`);
            //
            // console.log('abHyp', abHyponuse);
            // console.log('bcHyp', bcHyponuse);
            //
            // console.log(`bezelDif: ${bezelDif}`);

            let abBezAngle = 0;
            let bcBezAngle = 0;

            if (refAngleA + refAngleC > 180) {
                if (aAngle < cAngle) {
                    abBezAngle = aAngle + bezelDif;
                    bcBezAngle = cAngle - bezelDif;
                } else {
                    abBezAngle = aAngle - bezelDif;
                    bcBezAngle = cAngle + bezelDif;
                }
            } else {
                if (aAngle < cAngle) {
                    abBezAngle = aAngle - bezelDif;
                    bcBezAngle = cAngle + bezelDif;
                } else {
                    abBezAngle = aAngle + bezelDif;
                    bcBezAngle = cAngle - bezelDif;
                }
            }
            //
            // console.log(`abBezAngle = ${abBezAngle}`);
            // console.log(`bcBezAngle = ${bcBezAngle}`);

            let abBezel = 0;
            let bcBezel = 0;

            if (bezels === null) {
                abBezel = abHyponuse / 3;
                bcBezel = bcHyponuse / 3;
            } else {
                abBezel = bezels;
                bcBezel = bezels;
            }

            const abBezier = findNewPoint(bPoint.x, bPoint.y, abBezAngle, abBezel);
            const bcBezier = findNewPoint(bPoint.x, bPoint.y, bcBezAngle, bcBezel);
            //
            // console.log('=== GRAND FINALE ===');
            // console.log('aPoint', aPoint);
            // console.log('bPoint', bPoint);
            // console.log('cPoint', cPoint);

            return [abBezier, bcBezier];

        };

        return bezierGenerator(aPoint, bPoint, cPoint);

    };

    return drawSVG()
}