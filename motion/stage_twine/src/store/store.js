import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";

const initialState = {
    introMap: {},
    initProcess: {}
}

const reducer = (state, action) => {
    switch (action.type){
        case 'INTRO_MAP_FETCH':
            return {...state, introMap: action.payload};
        case 'INIT_DATA_PROCESS_END': {
            return {...state, initData: 'PROCESSED'}
        }
        case 'DOM_CORD':
            console.log('DOM_CORD reducer', action.payload);
            return state;
        case 'SCROLL_PROGRESS':
            console.log('SCROLL_PROGRESS', action.payload);
            return {...state, scrollY: action.payload}
        default:
            return state
    }
    return state
};

const store = createStore(reducer, applyMiddleware(thunk));

export default store