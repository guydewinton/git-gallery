import React from 'react';

import {coordStore} from "../../../store/stageStore";
import {Controller, Scene} from "react-scrollmagic";

import setup from '../../../scenes/_1_Intro/setup'

import ScrollLine from "../../../scenes/_1_Intro/parts/timelines/ScrollLine";


const ListenerScroll = (props) => {

    const scrollPosition = (progress) => {
        ScrollLine
            .progress(progress);

        coordStore.dispatch({type: 'COORD_SCROLL', payload: progress})
    };

    return (
        <div>
            <Controller globalSceneOptions={{triggerHook:0.7, indicators: true}}>
                <Scene duration={setup.page.height} triggerElement={'#scrollSpace'} indicators={false}>
                    {(progress, event) => {
                        {scrollPosition(progress)}
                        return (
                            <div id={'#scrollSpace'}>
                                {/*<ScrollLine scrollTween={progress}/>*/}

                            </div>
                        )
                    }}
                </Scene>
            </Controller>
        </div>
    )
};

export default ListenerScroll