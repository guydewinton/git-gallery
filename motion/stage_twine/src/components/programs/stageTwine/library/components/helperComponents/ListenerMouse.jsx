import React from 'react';
import {coordStore} from "../../../store/stageStore";
import Styled from 'styled-components'

const WindowBox = Styled.div`
position: fixed;
height: 100vh;
width: 100vw;
`;

const ListenerMouse = (props) => {

    const mousePositions = (event) => {
        console.log('event');
        coordStore.dispatch({type: 'COORD_MOUSE', payload:{x: event.clientX, y: event.clientY}})
    };

    return (
        <>
            <WindowBox id={'listener'} onMouseMove={(e) => mousePositions(e)}/>
        </>
    )
};

export default ListenerMouse