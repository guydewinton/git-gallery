
export const filterAppend = (svg) => {
// SVG filter for the gooey effect
// Code taken from http://tympanus.net/codrops/2015/03/10/creative-gooey-effects/
    const defs = svg.append("defs");
    const filter = defs.append("filter").attr("id", "gooeyCodeFilter");
    filter.append("feGaussianBlur")
        .attr('id', 'blurryFilter')
        .attr("in", "SourceGraphic")
        .attr("stdDeviation", "8")
        //to fix safari: http://stackoverflow.com/questions/24295043/svg-gaussian-blur-in-safari-unexpectedly-lightens-image
        .attr("color-interpolation-filters", "sRGB")
        .attr("result", "blur");
// filter.append("feColorMatrix")
//     .attr("class", "blurValues")
//     .attr("in", "blur")
//     .attr("mode", "matrix")
//     .attr("values", "1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 0 0")
//     .attr("result", "gooey");
// filter.append("feBlend")
//     .attr("in", "SourceGraphic")
//     .attr("in2", "gooey")
//     .attr("operator", "atop");



//    <filter id="groupborder" filterUnits="userSpaceOnUse"
//            x="0" y="0" width="250" height="250">
//        <feMorphology operator="dilate" in="SourceAlpha"
//                      radius="8" result="e1" />
//        <feMorphology operator="dilate" in="SourceAlpha"
//                      radius="10" result="e2" />
//        <feComposite in="e1" in2="e2" operator="xor"
//                     result="outline"/>
//        <feColorMatrix type="matrix" in="outline"
//                       values="1 0 0 0 0
// 0 1 0 0 0
// 0 0 1 0 0
// 0 0 0 .3 0" result="outline2"/>
//        <feComposite in="outline2" in2="SourceGraphic"
//                     operator="over" result="output"/>
//    </filter>

}