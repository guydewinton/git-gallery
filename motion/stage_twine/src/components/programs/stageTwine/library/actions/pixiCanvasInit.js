import * as PIXI from 'pixi.js'
import * as d3 from 'd3'

let width = 960, height = 600;



export const initPixiCanvas = () => {
    //
    // let element = document.getElementById('svg');
    // let positionInfo = element.getBoundingClientRect();
    // let height = positionInfo.height;
    // let width = positionInfo.width;

    console.log('pixi height width', height, width)

    let stage = new PIXI.Container();
    let renderer = PIXI.autoDetectRenderer({width, height, antialias: true, transparent: true, resolution: 5});

    document.getElementById('canvasDiv').appendChild(renderer.view);

    let graphics = new PIXI.Graphics();

    return {stage, graphics, renderer}
}

export const pixiPolyDrawer = ({canvas, projection, pointsArray, color}) => {

    let graphics = new PIXI.Graphics();

    graphics.beginFill(0xFFFFFF); // Yellow

// Draw a polygon to look like a star
    graphics.drawPolygon(pointsArray);
    graphics.alpha = .1
    // projection(pointsArray)
    graphics.endFill();

    canvas.addChild(graphics)

}

export const pixiMapDrawer = ({canvas, projection, pointsArray, color, pixiObject}) => {

    let graphics = new PIXI.Graphics();

    let path = d3.geoPath()
        .projection(projection)
        .context(graphics);

    graphics.beginFill(0xffffff);
    graphics.lineStyle(1, 0xcccccc, 1)
    path(pointsArray)
    graphics.alpha = .3

    graphics.endFill();

    canvas.addChild(graphics)

}

export const pixiCircleDrawer = ({canvas, projection, pointsArray, color, pixiObject, i}) => {

    let graphics = new PIXI.Graphics();

    let path = d3.geoPath()
        .projection(projection)
        .context(graphics);

    graphics.beginFill(color);

    graphics.drawCircle(pointsArray.x, pointsArray.y, 1);
    graphics.alpha = 1
    // graphics.lineStyle(.1, 0xffffff, 0.1)
    graphics.endFill();

    canvas.addChild(graphics)

}


export const initPixiArray = (arrayValues) => {

    const {graphics, canvas, pointsArray, color} = arrayValues

    pointsArray.forEach((array, i) => {

        pixiPolyDrawer({graphics, canvas, array, color: 0xf1c40f});

    });
}
