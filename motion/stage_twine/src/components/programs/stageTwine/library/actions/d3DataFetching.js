import * as d3 from "d3";


export const csvFetch = (url, name, {callbackFunction = null, datumFunction = null}) => {
    d3.csv(url, datum => {
        // console.log('csvFetchDatum', datum)
        datumFunction && datumFunction(datum, name);
        return datum
    }).then(data => {
        // console.log('csvFetch', data)
        callbackFunction && callbackFunction(data, name);
        return data
    })
};
