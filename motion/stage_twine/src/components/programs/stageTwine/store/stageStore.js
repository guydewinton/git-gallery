import { createStore, combineReducers} from "redux";

const initialState = {}

const coordReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'COORD_MOUSE': {
            return {...state, mouseXY: action.payload}
        }
        case 'COORD_SCROLL': {
            return {...state, scrollY: action.payload}
        }
        default:
            return state
    }
};

const domReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SELECT_SVG': {
            return {...state, svg: action.payload}
        }
        default:
            return state
    }
};

export const coordStore = createStore(coordReducer);

export const domStore = createStore(domReducer);

export default combineReducers(coordStore, domStore)