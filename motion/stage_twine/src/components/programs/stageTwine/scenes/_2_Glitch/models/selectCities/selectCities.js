const selectCitiesOverlay = {
    name: 'selectCitiesOverlay',
    group: 'selectCities',
    data: 'stageData.getState().scene.selectCities',
    values: {
        initValues: {
            type: 'D3_ARRAY_INIT',
            options: {
                scales: 'stageData.getState().scene.selectCities.scales.init'
            },
            values: {
                attr: [
                    ['r', (d) => d.data, 'callBack?']
                    // but where are the scales imported from...
                    // the object is stored in the state...
                    // the the object is initialised with the scales
                    // object.getState().scales.initRScale
                    // ['r', (d) => (d.data), callBack()]
                    // at the iteration point everything is unpacked
                    // these are the hard values
                    // builder(object) :: inside the wiring (in groupings) :: all necessary information is on the object
                ],
                style: [

                ],
                event: [

                ]
            }
        },
        trans1: {
            type: 'TYPE',
            options: {

            },
            values: {
                attr: [

                ],
                style: [

                ],
                event: [

                ]
            }
        }
    }
}

const pretendIterator = (object, state) => {

    const data = object.data;

    const {options, values} = object[state];

    const scales = options.scales;

    const returnObject = [];

    data.forEach((d, i) => {
        // maps each object and appends it ?? there and then? each one??
        // no there and then... one iteration
        // d3.attr function call for each element... pass in the datum
        // builder(object)
        // all is on the object... helpers and scales imported into object script and attactched to the obejct
        // no need for them to be attached to the object... they are just used on the object
    })

};