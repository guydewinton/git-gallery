// dataFetch(process)


export const initFetches = {
    cities: {
        url: './cities.csv',
        datumFunction: '',
        callbackFunction: ''
    },
    votes: {
        url: './votes.csv',
        datumFunction: '',
        callbackFunction: ''
    },
    startupsPart1: {
        url: './startups_accelerator_partipants.csv',
        datumFunction: '',
        callbackFunction: '',
    },
    startupsPart2: {
        url: './startups_accelerator_part_data.csv',
        datumFunction: '',
        callbackFunction: ''
    },
    citiesHighlights: {
        url: './citiesHighlight.csv',
        datumFunction: '',
        callbackFunction: ''
    },
    startupsFunding: {
        url: './startups_accelerator_funding.csv',
        datumFunction: '',
        callbackFunction: ''
    },
    noDevelopers: {
        url: './NoDevelopers.csv',
        datumFunction: '',
        callbackFunction: ''
    },
    developerSalary: {
        url: './medianSalaries.csv',
        datumFunction: '',
        callbackFunction: ''
    }
}
