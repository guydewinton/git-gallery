import * as d3 from "d3";
import populations from "../data/data/populations";
import countriesMap from "../data/data/countriesMap";
import store from "../../../../../../store/store";
import dataStore from "../data/dataStore";


///////////////////////////////////////////////////////////////////////////
//////////////////////////// Set-up Map /////////////////////////////////
///////////////////////////////////////////////////////////////////////////


//Variables for the map

// const introInitScale = () => {
//     const projection = d3.geoMercator()
//         .mathematics(1200)
//         .center([0, 57])
//         .translate([480, 250]);
//
//     console.log(projection([0, 1]))
//
//     const path = d3.geoPath()
//         .projection(projection);
//
//     const map = svg.append("g")
//         .attr("class", "map");

///////////////////////////////////////////////////////////////////////////
//////////////////////////////// Cities ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////

//Radius mathematics
// const rScale = d3.scaleSqrt()
//     .range([0, 14])
//     .domain([0, d3.max(populations, function (d) {
//         return d.population;
//     })]);

//Put the city locations into the data itself
//     populations.forEach(function (d, i) {
//         d.radius = 5;
//         d.x = projection([d.long, d.lat])[0];
//         d.y = projection([d.long, d.lat])[1];
//     });
//
// }

    export const pageMargin = {
            top: 100,
            right: 0,
            bottom: 0,
            left: 0
    };
    export const pageWidth = '100vw' - pageMargin.left - pageMargin.right;
    export const pageHeight = '100vh' - pageMargin.top - pageMargin.bottom;

// //Variables for the map
//



export const projection = d3.geoMercator()
    .scale(800)
    .center([0, 52])
    .translate([480, 250]);

export const path = d3.geoPath()
    .projection(projection);



export const radiusScale = (data) => {

    return d3.scaleLog()
        .range([5, 15])
        .domain([1, d3.max(data, function (d) {
            return d.startupCount;
        })]);
}

export const bezelGenerator = (links) => {
    return d3.scaleLog()
        .range([0, 12])
        .domain([1, d3.max(links, function (d) {
            return d.cityIndex;
        })]);
}

export const rScale = (data) => {

    const {cities} = dataStore.getState().initProcess;

    const scale = radiusScale(cities)

    return scale(data);
};

export const bezelScale = (data) => {

    const {links} = dataStore.getState().initProcess;

    const bezelScale = bezelGenerator(links)

    return bezelScale(data)
}

export const scatterScales = (svg, extent) => {

    const x = d3.scaleLinear()
        .domain([1, d3.max(extent, d => {
            return d.salary
        })])
        .range([600,30]);

    // Add Y axis
    const y = d3.scaleLinear()
        .domain([1, d3.max(extent, d => {
            return d.devs;
        })])
        .range([10,900]);

    return [x, y]

}