import {csvFetch} from "./dataActions/dataFetches/dataFetches";
import {cityStartupCounter, initDataToState, processCities, startupArrayInit, initVoteLinks} from "./dataActions/dataActions";
import {initFetches} from "./dataObjects";
import dataStore from "./dataStore";
import store from "../../../../../../store/store";


export const initData = () => {
    csvFetch(initFetches.cities.url, 'initCities', {callbackFunction: initDataToState})
    csvFetch(initFetches.votes.url, 'initVotes', {callbackFunction: initDataToState})
    csvFetch(initFetches.startupsPart1.url, 'initStartup1', {callbackFunction: initDataToState})
    csvFetch(initFetches.startupsPart2.url, 'initStartup2', {callbackFunction: initDataToState})
    csvFetch(initFetches.startupsFunding.url, 'initStartup3', {callbackFunction: initDataToState})
    csvFetch(initFetches.citiesHighlights.url, 'initCitiesHightlights', {callbackFunction: initDataToState})
    csvFetch(initFetches.noDevelopers.url, 'noDevelopers', {callbackFunction: initDataToState})
    csvFetch(initFetches.developerSalary.url, 'medianSalary', {callbackFunction: initDataToState})
};

const initDataProcess = () => {
    const {
        initCities,
        initVotes,
        initStartup1,
        initStartup2,
        initStartup3,
        initCitiesHightlights,
        noDevelopers,
        medianSalary
    } = dataStore.getState().initDump

    const startups = startupArrayInit(initStartup1, initStartup2, initStartup3)
    const {
        cities,
        cityHighlights
    } = cityStartupCounter(
        processCities(initCities),
        processCities(initCitiesHightlights),
        startups,
        medianSalary,
        noDevelopers
    )
    const links = initVoteLinks(initVotes, cities)

    dataStore.dispatch({type: 'INIT_DATA_PROCESS_END', payload: {cities, cityHighlights, startups, links}})
    store.dispatch({type: 'INIT_DATA_PROCESS_END', payload: 'PROCESSED'})

}


// best listener would be automated to respeond only when the whole lot are done, rather than listing them
const initListener = () => {
    const dataState = dataStore.getState().initDump;
    const initData = dataStore.getState().initData;
    if (
        dataState.initCities &&
        dataState.initVotes &&
        dataState.initStartup1 &&
        dataState.initStartup2 &&
        dataState.initStartup3 &&
        dataState.initCitiesHightlights &&
        initData === 'FETCHED'
    ) {
        console.log('time to process!!', dataState);
        initDataProcess();
        dataStore.dispatch({type: 'INIT_DATA_PROCESS_START', payload: 'PROCESSING'})
    }

}

dataStore.subscribe(() => initListener())