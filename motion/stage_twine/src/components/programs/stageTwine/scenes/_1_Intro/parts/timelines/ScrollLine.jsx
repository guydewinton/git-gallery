import React from "react";
import gsap, {Power0} from 'gsap';
import setup from "../../setup";
import store from "../../../../../../../store/store";

const duration = setup.page.height

const ScrollLine = new gsap.timeline({ease: Power0.easeNone, repeat: -1, yoyo: true, paused: true})
    .duration(1)

const newTimeline = gsap.to('body', 5000, {}, 0)

ScrollLine.add(newTimeline)

export default ScrollLine