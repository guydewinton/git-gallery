import { createStore} from "redux";

const initialState = {
    initDump: {},
    initProcess: {}
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'INIT_DATA_DUMP': {
            return {...state, initDump: {...state.initDump, ...action.payload}, initData: 'FETCHED'}
        }
        case 'INIT_DATA_PROCESS_START': {
            return {...state, initData: 'PROCESSING'}
        }
        case 'INIT_DATA_PROCESS_END': {
            return {...state, initProcess: {...state.initProcess, ...action.payload}, initData: 'PROCESSED'}
        }
        default:
            return state
    }
};

export default createStore(reducer);