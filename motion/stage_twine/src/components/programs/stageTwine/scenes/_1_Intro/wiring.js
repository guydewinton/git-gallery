import store from "../../../../../store/store";
import {filterAppend} from "../../library/actions/redundantFilterAppend"
// import timelines from "../../library/redundant/calls/timelines";

import newTween from "./parts/timelines/experiment";
import {initD3SVGArray, initD3SVGCanvas} from "../../library/actions/d3ElementInit"
import {
    mapCountries,
    svg as rootSVG,
    selectCitiesOverlay,
    selectCitiesGlow,
    selectCitiesShadow,
    cityNodes,
    cityLinks, initSVG, cityLinksPixi, keyframe2, keyframe3, keyframe4
} from "./parts/nodes/citiesSelectSVG/init";
import {initData} from "./data/dataWiring";
import dataStore from "./data/dataStore";
import {domStore} from "../../store/stageStore";
import {timelineReducer} from "./parts/timelines/timelineReducer";
import {initPixiCanvas} from '../../library/actions/pixiCanvasInit';

import gsap from 'gsap';

import * as PIXI from 'pixi.js'
import { PixiPlugin } from "gsap/PixiPlugin";

PixiPlugin.registerPIXI(PIXI);
gsap.registerPlugin(PixiPlugin);


const initPage = () => {

};

const initSelectCities = () => {

};

const initMap = (props) => {

    let pixiObject = initPixiCanvas()

    console.log('in da select', domStore.getState());

    const chartSVG = initSVG('chartSVG').svgG
    const mapSVG = initSVG('mapSVG').svgG

    // initD3SVGArray(mapSVG, mapCountries('init'))

    filterAppend(chartSVG);
    // initD3SVGArray(chartSVG, cityLinks('init'))
    // console.log('here in the init again', pixiObject)

    mapCountries('init', pixiObject)
    cityLinksPixi('init', pixiObject)

    cityNodes('init', pixiObject)





    // dotInit(svg, cityHighlight, nodes);

    // initD3SVGArray(chartSVG, selectCitiesGlow('init'))
    initD3SVGArray(chartSVG, selectCitiesShadow('init'))
    initD3SVGArray(chartSVG, selectCitiesOverlay('init'))

    // timelineReducer(cityNodes('keyframe#2'))
    cityLinksPixi('keyframe#2', pixiObject)


    newTween()

    keyframe2()
    keyframe3()
    keyframe4()

    let cities = dataStore.getState().initProcess.cities



};
//
// const dataObject = () => {
//     const name = 'string';
//     const group = 'array';
//     const data = 'function';
//     const steps = {
//         init: {
//             call: 'function',
//             options: {
//                 an: {},
//                 object: {},
//                 of: {},
//                 objects: []
//             },
//             values: {
//                 attr: [
//                     ['array', 'of', 'arrays']
//                 ],
//                 style: [
//                     ['selection', 'values', 'callback']
//                 ],
//                 events: [
//                     ['SVG_G_ELEMENT', someFunc(data), 'blah']
//                 ]
//             },
//         },
//         trans1: {
//             timeline: '',
//
//         },
//         trans2: {
//
//         },
//         trans3: {
//
//         },
//         exit: {
//
//         }
//     }
// }
//
// dataObject.name;
//
// const objectFunction = () => {
//
// };
//
// const objectClass = {
//     constructor(values){
//     }
// }

export default initMap

