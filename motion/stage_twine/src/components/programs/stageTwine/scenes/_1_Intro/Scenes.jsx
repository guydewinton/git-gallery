import React, {useEffect} from 'react';
import {connect} from "react-redux";

// import ScrollMagic from 'scrollmagic'

// import {scrollListener} from './Listener'
// import addIndicators from 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators'

import Styled from 'styled-components';
import {Controller} from "react-scrollmagic";

import setup from "./setup";
import ScrollLine from "./parts/timelines/ScrollLine";


const WrapperDiv = Styled.div`
    height: ${setup.page.height};
    width: 100vw;
    background-color: black;
`;

const TriggerDiv = Styled.div`
    // width: 100vw;
    // height: 60px;
    // position: relative;
    // top: 100vh;
    // background-color: aqua;
`;

const Scenes = (props) => {

    useEffect(() => {
        // here initize state
        // append svg? => root svg for all svg work of subcomponent
        // map will be one component
        // links and all cities will be another
        // and key cities will be another

        // const newScene = new ScrollMagic.Scene({
        //     duration: 100,
        //     offset: 200,
        //     triggerElement: '#triggerDiv',
        //     triggerHook: "onEnter",
        //     reverse: false
        // })
            // .addIndicators();

        // scrollListener.addScene(newScene)
        //

        // ScrollLine.scrollLine()
    });





    // initialise / manage / update controller

    // receive state

    // parent linksCityVote (ie: scrollmagic) and trigger linksCityVote

    // nested children citiesSelectSVG => with nested children linksCityVote

    // sets internal time




    // scroll down past some graphs and stuff

    /// down ot aw snap

    return (
        <WrapperDiv>
            <TriggerDiv id={'triggerDiv'}/>
            <div id={'Presentation'}>
                <div id="mapSVG"></div>
                <div id="canvasDiv"></div>
                <div id="chartSVG"></div>
            </div>
        </WrapperDiv>
    )
};

const mapStateToProps = (state) => {
    return state
};

export default connect()(Scenes);

// scatterforce map of cities in europe
// primary/secondary selection
// x/y to geocoords
// size by starups count
// links stength (opacity) by average counter
// onclick handler (pri/sec selection => primary toggles both // secondary toggles on/off and between)
// pure d3

// citiesSelectSVG three states // all europe // single country // country comparison => comparison matrix browser, for later
// click on start up link sidebar for more info // filter by industry

// start with this


/// top nav with sections... app shell... semi working... glittish on clicking the last one... pres trigger...

/// goes to next section => glitch


////////////////////////
////////////////////////
////////////////////////


