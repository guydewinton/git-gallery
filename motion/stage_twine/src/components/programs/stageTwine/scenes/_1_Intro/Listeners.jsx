import React, { useEffect } from 'react';

import ListenerMouse from "../../library/components/helperComponents/ListenerMouse";
import ListenerScroll from "../../library/components/helperComponents/ListenerScroll";

const Listeners = (props) => {

    return (
        <>
            <ListenerMouse/>
            <ListenerScroll/>
        </>
    )
};

export default Listeners