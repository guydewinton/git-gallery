import * as d3 from 'd3';

import gsap from 'gsap';

import * as PIXI from 'pixi.js'
import { PixiPlugin } from "gsap/PixiPlugin";

import {
    bezelGenerator,
    pageHeight,
    pageMargin,
    pageWidth,
    path,
    projection,
    radiusScale,
    rScale, bezelScale, scatterScales
} from '../../../scales/scales';

import trig from "../../../../../../../../library/helpers/calcs/trig";
import countriesMap from "../../../data/data/countriesMap";
import dataStore from "../../../data/dataStore";
import {initD3SVGCanvas} from "../../../../../library/actions/d3ElementInit";

import {domStore} from "../../../../../store/stageStore";
import ScrollLine from "../../timelines/ScrollLine";
import {initPixiCanvas, pixiPolyDrawer, pixiMapDrawer, pixiCircleDrawer} from "../../../../../library/actions/pixiCanvasInit";
import {timelineReducer} from "../../timelines/timelineReducer";

// import scrollLine from 'sc'

const margin = pageMargin;
const width = pageWidth;
const height = pageHeight;

export const initSVG = (name) => {
    const svgInit = {
        select: `#${name}`,
        append: 'svg',
        className: `${name}Container`,
        svgValues: {
            attrArray: [
                ['width', 960],
                ['height', 600],
                ['viewBox', '0 0 960 600']
            ]
        }
    };

    return initD3SVGCanvas(svgInit.select, svgInit)

};

export const mapCountries = (type, pixiObject) => {

    let {stage, graphics, renderer} = pixiObject

    const arrayHeader = {
        append: 'path',
        id: 'map',
        className: 'map',
        groupName: 'mapVotes',
        gValues: {}
    };

    const data = countriesMap[0].features;

    let arrayValues = [];

    switch (type) {
        case 'init':
            arrayValues = data.map((d, i) => {
                pixiMapDrawer({
                    pointsArray: d,
                    color: 0xffffff,
                    graphics: graphics,
                    canvas: stage,
                    projection: projection,
                    i

                })
                // stage.addChild(graphics)
                // return {
                //     attrArray: [
                //         ['d', path(d)],
                //         ['stroke', 'white'],
                //         ['fill', 'black'],
                //     ],
                //     styleArray: [
                //         ["fill-opacity", 0.5],
                //         ["stroke-opacity", .5]
                //     ]
                // }

            });
            break;
    }

   return { ...arrayHeader, arrayValues }

};

export const selectCitiesGlow = (type) => {

    const arrayHeader = {
        append: 'circle',
        id: 'selectCitiesGlow',
        className: 'selectCities',
        groupName: 'featureElements',
        gValues: {
            attrArray: [
                ['filter', "url(#gooeyCodeFilter)"],
                ['z-index', 3]
            ]
        }
    };

    const data = dataStore.getState().initProcess.cityHighlights;

    let arrayValues = [];

    switch (type) {
        case 'init': {
            arrayValues = data.map((d) => {
                return {
                    attrArray: [
                        ['r', rScale(d.startupCount)],
                        ['cx', projection([d.long, d.lat])[0]],
                        ['cy', projection([d.long, d.lat])[1]],
                        ['stroke', 'none'],
                        ['fill', 'white'],
                    ],
                    styleArray: [
                        ["fill-opacity", 0.3],
                        ["stroke-opacity", 0]
                    ]
                }
            });
            break;
        }
    }

    return { ...arrayHeader, arrayValues }


};

export const selectCitiesShadow = (type) => {

    const arrayHeader = {
        append: 'circle',
        id: 'selectCitiesShadow',
        className: 'selectCities',
        groupName: 'featureElements',
        gValues: {
            attrArray: [
                ['filter', "url(#gooeyCodeFilter)"],
                ['z-index', 4]
            ]
        }
    };


    const data = dataStore.getState().initProcess.cityHighlights;
    console.log('highlightcities in da init', data)

    let arrayValues = [];

    switch (type) {
        case 'init': {
            arrayValues = data.map((d) => {
                return {
                    attrArray: [
                        ['r', rScale(d.startupCount)],
                        ['cx', projection([d.long, d.lat])[0]],
                        ['cy', projection([d.long, d.lat])[1]],
                        ['stroke', 'none'],
                        ['fill', 'black'],
                    ],
                    styleArray: [
                        ["fill-opacity", 0.6],
                        ["stroke-opacity", 0]
                    ]
                }
            });
            break;
        }
    }

    return { ...arrayHeader, arrayValues }

};

export const selectCitiesOverlay = (type) => {

    const arrayHeader = {
        append: 'circle',
        id: 'selectCitiesOverlay',
        className: 'selectCities',
        groupName: 'featureElements',
        gValues: {
            attrArray: [
                ['z-index', 5]
            ]
        }
    };

    const data = dataStore.getState().initProcess.cityHighlights;
    console.log('selectCitiesOverlay', data)

    let arrayValues = [];

    switch (type) {
        case 'init': {
            arrayValues = data.map((d) => {
                return {
                    attrArray: [
                        ['r', rScale(d.startupCount)],
                        ['cx', projection([d.long, d.lat])[0]],
                        ['cy', projection([d.long, d.lat])[1]],
                        ['stroke', 'black'],
                        ['fill', 'white'],
                    ],
                    styleArray: [
                        ["fill-opacity", 1],
                        ["stroke-opacity", 0.1]
                    ],
                    eventArray: [
                        ['click', () => alert(`name: ${d.cities}, developers: ${d.devs}, salary: ${d.salary}`)]
                    ]
                }
            });
            break;
        }
        case 'slide2': {
            break;
        }
    }

    return { ...arrayHeader, arrayValues }

};


export const cityNodes = (type, pixiObject) => {

    const arrayHeader = {
        append: 'circle',
        id: 'mapVotes_LinksCities',
        className: 'mapVotes_links',
        groupName: 'mapVotes',
        gValues: {}
    };
console.log('pixinodes', pixiObject)
    let {stage, graphics, renderer} = pixiObject

    const data = dataStore.getState().initProcess.cities;


    switch (type) {
        case 'init':{
                data.forEach((d, i) => {
                    pixiCircleDrawer({
                        pointsArray: {
                            x: projection([d.long, d.lat])[0],
                            y: projection([d.long, d.lat])[1],
                        },
                        color: 0xffffff,
                        graphics: graphics,
                        canvas: stage,
                        projection: projection,
                        i,

                    })
                    // stage.addChild(graphics);
                //
                // return {
                //     attrArray: [
                //         ['r', 1],
                //         ['cx', projection([d.long, d.lat])[0]],
                //         ['cy', projection([d.long, d.lat])[1]],
                //         ['stroke', 'none'],
                //         ['fill', 'white'],
                //     ],
                //     styleArray: [
                //         ["fill-opacity", 1],
                //         ["stroke-opacity", 0.1]
                //     ]
                // }
            });
            break;
        }
        case 'keyframe#2': {
                return {
                    type: 'TWEEN_TO',
                    timeline: ScrollLine,
                    select: `.${arrayHeader.className}`,
                    start: 50,
                    end: 100,
                    values: {
                        opacity: 0
                    }
                }
        }
    }

    // return { ...arrayHeader, arrayValues }

};

export const cityLinks = (type) => {

    const arrayHeader = {
        append: 'path',
        id: 'mapVotes_LinksVotes',
        className: 'mapVotes_links',
        groupName: 'mapVotes',
        gValues: {}
    };

    const data = dataStore.getState().initProcess.links;

    let arrayValues = [];

    switch (type) {
        case 'init': {
            arrayValues = data.map((d) => {

                let trigReturn = trig(projection([d.voteLong, d.voteLat])[0], projection([d.voteLong, d.voteLat])[1], projection([d.cityLong, d.cityLat])[0], projection([d.cityLong, d.cityLat])[1], projection([d.voteLong, d.voteLat])[0], projection([d.voteLong, d.voteLat])[1], bezelScale(d.cityIndex))

                return {
                    attrArray: [
                        ['d', `
                    M ${projection([d.voteLong, d.voteLat])[0]}, ${projection([d.voteLong, d.voteLat])[1]}
                    L ${projection([d.voteLong, d.voteLat])[0]}, ${projection([d.voteLong, d.voteLat])[1]}
                    L ${trigReturn[0].x}, ${trigReturn[0].y}

                    L ${trigReturn[1].x}, ${trigReturn[1].y}
                    L ${projection([d.voteLong, d.voteLat])[0]}, ${projection([d.voteLong, d.voteLat])[1]}
                    Z

                    `],
                        ['stroke', 'white'],
                        ['fill', 'white'],
                    ],
                    styleArray: [
                        ["opacity", .08],
                        ["stroke-opacity", 0]
                    ]
                }
            });
            break;
        }

    }

    return { ...arrayHeader, arrayValues }


};

export const cityLinksPixi = (type, pixiObject) => {

    let {stage, graphics, renderer} = pixiObject

    const arrayHeader = {
        append: 'path',
        id: 'mapVotes_LinksVotes',
        className: 'mapVotes_links',
        groupName: 'mapVotes',
        gValues: {},

    };

    const data = dataStore.getState().initProcess.links;

    let arrayValues = [];

    switch (type) {
        case 'init': {
            data.forEach((d, i) => {

                let trigReturn = trig(projection([d.voteLong, d.voteLat])[0], projection([d.voteLong, d.voteLat])[1], projection([d.cityLong, d.cityLat])[0], projection([d.cityLong, d.cityLat])[1], projection([d.voteLong, d.voteLat])[0], projection([d.voteLong, d.voteLat])[1], bezelScale(d.cityIndex))

                pixiPolyDrawer({
                    pointsArray: [
                        projection([d.voteLong, d.voteLat])[0], projection([d.voteLong, d.voteLat])[1],
                        trigReturn[0].x, trigReturn[0].y,
                        trigReturn[1].x, trigReturn[1].y,
                        projection([d.voteLong, d.voteLat])[0], projection([d.voteLong, d.voteLat])[1]
                    ],
                    color: 0xf1c40f,
                    graphics: graphics,
                    canvas: stage,
                    projection: path,
                    i

                })
            });




            break;
        }
        case 'keyframe#2': {
            // timelineReducer ({
            //     type: 'TWEEN_TO',
            //     timeline: ScrollLine,
            //     select: graphics,
            //     start: 0,
            //     end: 50,
            //     values: {
            //         pixi: {
            //             alpha: 0,
            //             scaleX: 200,
            //         }
            //     }
            // })
            // stage.addChild(graphics);
            renderer.backgroundColor = 0x000000;
            renderer.render(stage);



            ScrollLine.to('canvas', 100, {opacity: 0}, 0)
            // renderer.render(stage);
            console.log('here')

        }

    }
};


export const keyframe2 = () => {

    const data = dataStore.getState().initProcess.cityHighlights;

    const [xScale, yScale] = scatterScales('#chartSVG', data)

    data.forEach((d, i) => {
        if (d.devs) {
            console.log(d, i)
            const tweenParams = {
                type: 'TWEEN_TO',
                timeline: ScrollLine,
                select: `#selectCitiesOverlay_${i}`,
                start: 30,
                end: 300,
                values: {
                    attr: {
                        cx: yScale([d.devs]),
                        cy: xScale([d.salary]),
                    }
                }
            }
            timelineReducer(tweenParams)
        }
        if (!d.devs) {
            console.log(d, i)
            const tweenParams = {
                type: 'TWEEN_TO',
                timeline: ScrollLine,
                select: `#selectCitiesOverlay_${i}`,
                start: 50,
                end: 100,
                values: {
                        opacity: 0,
                }
            }
            timelineReducer(tweenParams)
        }
    })

}


export const keyframe3 = () => {

    const data = dataStore.getState().initProcess.cityHighlights;

    const [xScale, yScale] = scatterScales('#chartSVG', data)

    data.forEach((d, i) => {

        const tweenParams = {
            type: 'TWEEN_TO',
            timeline: ScrollLine,
            select: `#selectCitiesGlow`,
            start: 25,
            end: 25,
            values: {
                display:"none",
            }
        }
        timelineReducer(tweenParams)

    })

}

export const keyframe4 = () => {

    const data = dataStore.getState().initProcess.cityHighlights;

    const [xScale, yScale] = scatterScales('#chartSVG', data)

    data.forEach((d, i) => {

        const tweenParams = {
            type: 'TWEEN_TO',
            timeline: ScrollLine,
            select: `#selectCitiesShadow`,
            start: 25,
            end: 25,
            values: {
                display:"none",
            }
        }
        timelineReducer(tweenParams)

    })

}
