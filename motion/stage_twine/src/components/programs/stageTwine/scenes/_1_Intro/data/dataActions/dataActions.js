import dataStore from "../dataStore";


export const processCities = (cities) => {
    const nodes = [];
    cities.forEach((city) => {
        if (city.region !== 'Non-EU') {
            nodes.push({
                cities: city.cities,
                lat: city.lat,
                long: city.long,
                country: city.country,
                region: city.region
            })
        }
    });
    return nodes
};

export const initVoteLinks = (votes, cities) => {

    const links = [{city: '', vote: ''}]

    votes.forEach(vote => {
    // console.log('links',links)

        let unique = true;

        links.forEach((link, i) => {

            if (link.city === vote.city && link.vote === vote.vote) {
                if (link.cityIndex) {
                    link.cityIndex++
                } else {
                    link.cityIndex = 1
                }
                unique = false
            }
        });
        if (unique) {
            let voteObject = {}
            let doPush = [false, false]
            cities.forEach(node => {
                if (vote.city === node.cities) {
                    voteObject.voteLat = node.lat
                    voteObject.voteLong = node.long
                    doPush[0] = true
                }

                if (vote.vote === node.cities) {
                    voteObject.cityLat = node.lat
                    voteObject.cityLong = node.long
                    doPush[1] = true
                }
            });
            if (doPush[0] && doPush[1]) {
                links.push({...vote, ...voteObject, cityIndex: 1})
            }

        }
    });
    return links
};

export const startupArrayInit = (startups1, startups2, startups3) => {

    const startupState = [];

    startups1.forEach((startup) => {

        let unique = true;

        startupState.forEach((startupArr) => {

            if (startupArr.startup === startup.startup) {
                if (!startupArr.industry && startupArr.industry) startupArr.industry = startup.industry;
                if (!startupArr.description && startupArr.description) startupArr.description = startup.description;
                if (!startupArr.founding && startupArr.founding) startupArr.founding = startup.founding;
                if (!startupArr.website && startupArr.website) startupArr.website = startup.website.toLowerCase();
                if (!startupArr.website && startupArr.website) startupArr.website = startup.website2.toLowerCase();
                if (!startupArr.city && startupArr.city) startupArr.city = startup.city;
                unique = false;
            }

        });
        if (unique) {
            startupState.push(startup)
        }
    });

    startups2.forEach((startup) => {

        let unique = true;

        startupState.forEach((startupArr) => {
            if (startupArr.startup === startup.startup) {
                unique = false;
                if (!startupArr.industry && startupArr.industry) startupArr.industry = startup.industry;
                if (!startupArr.description && startupArr.description) startupArr.description = startup.description;
                if (!startupArr.founding && startupArr.founding) startupArr.founding = startup.founding;
                if (!startupArr.website && startupArr.website) startupArr.website = startup.website.toLowerCase();
                if (!startupArr.website && startupArr.employees) startupArr.employees = (startup.minEmp + startup.maxEmp) / 2;
                if (!startupArr.city && startupArr.city) startupArr.city = startup.city;
            }
        });

        if (unique) {
            startupState.push(startup)
        }
    });

    startups3.forEach((startup) => {

        let unique = true;

        startupState.forEach((startupArr) => {
            if (startupArr.startup === startup.startup) {
                unique = false;
                if (!startupArr.industry && startup.industry) startupArr.industry = startup.industry;
                if (!startupArr.description && startup.description) startupArr.description = startup.description;
                if (!startupArr.founding && startup.founding) startupArr.founding = startup.founding;
                if (!startupArr.website && startup.website) startupArr.website = startup.website.toLowerCase();
                if (!startupArr.city && startup.city) startupArr.city = startup.city;
                if (!startupArr.website && startup.minEmp) startupArr.employees = startup.minEmp;

            }

        });
        if (unique) {
            startupState.push(startup)
        }
    })
    return startupState
};

export const cityStartupCounter = (cities, cityHighlights, startupState, medianSalary, noDevelopers) => {

    cities.forEach((node) => {
        node.startupCount = 0
        startupState.forEach((startup) => {
            if (node.cities === startup.city) {
                node.startupCount++
            }
        })

        cityHighlights.forEach((cityHL) => {

            if (cityHL.cities === node.cities) {
                cityHL.startupCount = node.startupCount
            }

            noDevelopers.forEach((devCity) => {
                if (cityHL.cities === devCity.city) {
                    cityHL.devs = +devCity.developers
                }
            })
            medianSalary.forEach((salCity) => {
                if (cityHL.cities === salCity.city) {
                    cityHL.salary = +salCity.salary
                }
            })

        })
    })
    return {cities, cityHighlights}
};

export const initDataToState = (payload, name) => {
    console.log('initDataToState', payload)
    let payloadObject = {
            [name]: payload,
        };
    dataStore.dispatch({type: 'INIT_DATA_DUMP', payload: payloadObject})
};


