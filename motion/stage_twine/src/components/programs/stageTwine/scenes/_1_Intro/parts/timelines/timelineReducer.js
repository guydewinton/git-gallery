import gsap from 'gsap';
import * as PIXI from 'pixi.js'
import { PixiPlugin } from "gsap/PixiPlugin";


PixiPlugin.registerPIXI(PIXI);
gsap.registerPlugin(PixiPlugin);




export const timelineReducer = (keyframeObject) => {
    const {type, timeline, select, start, end, values} = keyframeObject
    switch (type) {
        case 'TWEEN_TO': {
            console.log('TWEEN_TO', keyframeObject)
            timeline.to(select, end - start, {...values}, start)
        }
    }
};