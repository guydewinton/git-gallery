// this is an object
// it is passed to an updater function
// which updates its values

// can I tween from to d3 state?

const object = {
    a: (d) => {console.log(d.name)},

}

object.a({name: 'bob'})

// an array of objects

const keyframe = {
    select: 'object',  /// pass it the object to which it will perform the actions
    // it could all be passed into greensock as objects {attr: {c: 4, }

}