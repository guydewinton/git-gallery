import React, {memo, useEffect} from "react";
import Listeners from "./Listeners";
import Receiver from "./Receiver";
import Scenes from "./Scenes";

import Styled from 'styled-components';

import initMap from "./wiring";
import {connect} from "react-redux";
import {initData} from "./data/dataWiring";
import {initSVG} from "./parts/nodes/citiesSelectSVG/init";


const StageDiv = Styled.div`
    height: 100vh;
    width: 100vw;
    // position: fixed;
    // overflow: scroll;
`;

initData();
initSVG();

const Stage = (props) => {

    useEffect(() => {

        if (props.initData === 'PROCESSED') {
            console.log('Presentation', props);
            initMap()
        }
        console.log('render')
    }, [props.initData]);

console.log('render');

    return (
        <div id={'StageDiv'} >
            <Listeners/>
            <Receiver/>
            <Scenes/>
        </div>
    )
};


const mapStateToProps = (state) => {
    return {
        ...state
    }
};

export default memo(connect(mapStateToProps)(Stage));