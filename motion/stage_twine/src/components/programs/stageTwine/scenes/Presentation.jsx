import React, { memo, useEffect } from 'react';
import {connect} from "react-redux";
import * as d3 from 'd3'
import gsap from 'gsap';




// import worldViz from "../library/redundant/sourceFiles/worldViz";
// import worldViz_v2 from "../library/redundant/sourceFiles/worldViz_v2";
// import timeline from "../library/redundant/calls/selectAppend";
import populations from "./_1_Intro/data/data/populations";
// import selection from "../library/redundant/calls/selectAppend";
// import dataFetch from "../library/redundant/fetch";
import initMap from "./_1_Intro/wiring";

import Stage from "./_1_Intro/Component";


// dataFetch.WrappedComponent()


const Presentation = (props) => {

    const beat = {};



    // this is kind of the master file for the presentation instance of the stageTween
    //  ...maybe a class extending stageTween??


    useEffect(() => {
        if (props.introMap){
        console.log('Presentation', props)
        // initMap()
}


        // getdata
        // setScales(state)
        // select/append/update
        // timeline(helpers)
        // forEach attributes
        // play / display


        // console.log(gsap.fromTo(".geo-path", 10, {x:-200, y: -200}, {x:1000, y:1000}))

        // if data is loaded render component and set everything aside to not be touched again until triggered
    }, [props.introMap])

const programBeat = setTimeout(() => {
    // beat updated a mutable int time object in state
    // synced with app every second also... ref time... a single int => number of seconds
    // sets global program time

}, 1000);


    return <Stage/>

};

const mapStateToProps = (state) => {
    return {
        ...state
    }
};

export default memo(connect(mapStateToProps)(Presentation));