import React, {useEffect} from 'react';
import { Router, Switch, Route } from "react-router";
import { ThemeProvider } from "styled-components";
import { Provider } from 'react-redux';

import './style.css'

import history from "./history";
import store from "./store/store";

import Presentation from "./components/programs/stageTwine/scenes/Presentation";

function App() {

    // useEffect(() => {
    //     ticker(props)
    //     return tickerSweep()
    // }, []);


  return (
    <div className="App">
      <Provider store={store}>
          <Router history={history}>
              <Switch>
                <Presentation/>
                <h1>hello?????</h1>
              </Switch>
          </Router>
      </Provider>
    </div>
  );
}

export default App;
