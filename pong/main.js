
const canvas = document.querySelector('#canvas');

const ctx = canvas.getContext('2d');






    let leftScore = 0;
    let rightScore = 0;



function renderField() {
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, 600, 600);
    ctx.beginPath();
}

function renderLeftGoalFlashRed() {

        ctx.fillStyle = 'red';
        ctx.fillRect(0, 0, 300, 600);
        ctx.beginPath();

}

function renderRightGoalFlashRed() {

    ctx.fillStyle = 'red';
    ctx.fillRect(300, 0, 300, 600);
    ctx.beginPath();

}

function text() {

    ctx.fillStyle = 'white';
    ctx.font = '48px Arial';
    ctx.textAlign = 'center';
    ctx.fillText('Hello world', 300, 300);

}

function renderLeftGoalFlashBlack() {

    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, 300, 600);
    ctx.beginPath();


}


function renderBall(x = 10, y = 10) {
    ctx.arc(x, y, 10, 0, 2 * Math.PI, false);
    ctx.fillStyle = '#fff';
    ctx.fill();
}

function renderLeftPaddle(x=10, y=10){
    ctx.fillStyle = 'white';
    ctx.fillRect(x, y, 20,80)
}

function renderRightPaddle(x=580, y=10){
    ctx.fillStyle = 'white';
    ctx.fillRect(x, y, 20,80)
}

//renderBall(10, 20)
//renderBall(30, 40)
//renderBall(120, 150)
//renderBall(60, 90)


let plx = 10;
let ply = 500;

let prx = 570;
let pry = 10;


let vply = 0;
let vpry = 0;






let x = 10;
let y = 10;

let vx = 6;
let vy = 4;



let px = vx;
let py = vy;


function initGame() {


    vx = 6;
    vy = 4;


    game()


    function game() {
        setInterval(() => {
            renderField();
            renderLeftPaddle(plx, ply)
            renderRightPaddle(prx, pry)
            renderBall(x, y);
            //


            x += vx;
            y += vy;

            ply += vply;
            pry += vpry;



            let lScore = leftScore
            let rScore = rightScore

            // // ball left/right
            // if (x > 600 || x < 0) {
            //     vx = -vx;
            // }
            if (x < -5) {
                rScore++
                renderLeftGoalFlashRed();
                //text()

            }
            // ball left/right
            if (x > 605) {

                lScore++
                renderRightGoalFlashRed();
                //text()


            }
            if (y > 590 || y < 8) {
                vy = -vy;
            }
            // left paddle limiter top
            if (ply < 20 && vply === -10) {
                vply = 0
            }
            // left paddle limiter top
            if (ply > 500 && vply === 10) {
                vply = 0
            }
            // right paddle limiter top
            if (pry < 20 && vpry === -10) {
                vpry = 0
            }
            // right paddle limiter bottom
            if (pry > 500 && vpry === 10) {
                vpry = 0
            }


            // ball left & right/right paddle interaction
            if (x + 10 > prx && y + 10 > pry && y - 10 < pry + 80 && vx > 0) {
                vx = -vx
            }

            // // ball top of right paddle interaction
            // if (x > prx && y +10 > pry && vy > 0 ) {
            //     vy = - vy
            // }
            //
            // // ball top of left paddle interaction
            // if (x > prx && y - 10 < pry + 80 && vy < 0) {
            //     vy = - vy
            // }




            // ball/left paddle interaction
            if (x - 10 < plx + 20 && y + 10 > ply && y - 10 < ply + 80) {
                vx = -vx
            }

            if (leftScore !== lScore || rightScore !== rScore) {
                // while (document.getElementById('score').lastElementChild) {
                //     document.getElementById('score').removeChild(document.getElementById('score'));
                //         }

                let scoreHTML = document.getElementById('score');
                //

                let child = scoreHTML.lastElementChild;
                while (child) {
                    scoreHTML.removeChild(child);
                    child = scoreHTML.lastElementChild;
                }

                scoreHTML.insertAdjacentHTML('beforeend', '<p>' + lScore + ' :: ' + rScore + '</p>')
                leftScore = lScore;
                rightScore = rScore;


                x = 10;
                y = 10;

                vx = 6;
                vy = 4;


                plx = 10;
                ply = 500;

                prx = 570;
                pry = 10;

                vply = 0;
                vpry = 0;


            }

        }, 32); //60fps (1000/60)

    }
}


const QKeyCode = 81;
const AKeyCode = 65;

const ArrowDownKeyCode = 40;
const ArrowUpKeyCode = 38;


document.addEventListener('keydown', (event) => {

    if (ply > 20 && event.keyCode === QKeyCode) {
        vply = -10
    }
    if (ply < 500 && event.keyCode === AKeyCode) {
        vply = 10
    }
    if (pry > 20 && event.keyCode === ArrowUpKeyCode) {
        vpry = -10
    }
    if (pry < 500 && event.keyCode === ArrowDownKeyCode) {
        vpry = 10
    }

})

document.addEventListener('keyup',(event)=>{
    if (event.keyCode === QKeyCode || event.keyCode === AKeyCode) {
        vply = 0
    }
    // if (event.keyCode === AKeyCode) {
    //     vply = 0
    // }
    if (event.keyCode === ArrowUpKeyCode || event.keyCode === ArrowDownKeyCode) {
        vpry = 0
    }
    // if (event.keyCode === ArrowDownKeyCode) {
    //     vpry = 0
    // }
})

document.addEventListener('keydown',(event) => {
    if(event.keyCode === 32) {
        console.log(`x: ${x}, y:${y}::: plx:${plx}, ply: ${ply}::: prx: ${prx}, pry:${pry}`)
    }
})

// TODO: left paddle
// TODO: Enhance bouncing to check edge of ball instead of centre
// TODO: check no only for equality so speed

let startButton = document.getElementById('startButton');
let pauseButton = document.getElementById('pauseButton');

startButton.addEventListener('click', () => {

    initGame()
})


pauseButton.addEventListener('click', () => {

if(vy){
    px = vx;
    py = vy;

    vx = 0;
    vy = 0;
} else {
    vx = px;
    vy = py;
}

})












class Canvas {
    conskdnfkjg() {

    }
}



class Ball {
    constructor(ctx, radius, xPosition, yPosition, color, random){

    }
}



const jsonObject = {

}
