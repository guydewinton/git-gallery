import React from 'react';
import DOM from "./components/views/DOM";
import {ThemeProvider} from "styled-components";
import {GlobalStyle, theme} from "./library/constants/theme";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {Provider} from "react-redux";

import './App.css'

// import history from "./history";
import store from "./store/store";

import Sampler from "./library/Sampler";
import AuthSignIn from "./components/views/pages/AuthSignIn";
import AuthSignUp from "./components/views/pages/AuthSignUp";
import AuthValidation from "./components/views/pages/AuthValidation";
import AuthRegistration from "./components/views/pages/AuthRegistration";
import Home from "./components/views/pages/Home";
import SearchRestaurants from "./components/views/pages/SearchRestaurants";
import UserProfile from "./components/views/pages/UserProfile";
import SwitchBoard from "./SwitchBoard";
import Search from "./components/views/pages/Search";
import RestaurantProfile from "./components/views/pages/RestaurantProfile";
import CreateNewRestaurant from "./components/views/pages/CreateNewRestaurant";

import { createBrowserHistory} from 'history'

const history = createBrowserHistory()

history.listen((location, action) => {
    window.scrollTo(0, 0)
})



function App() {
  return (
    <div className="App">
        <Provider store={store}>
            <Router history={history} basename='/luna'>
                <ThemeProvider theme={theme}>
                    <GlobalStyle/>
                    <Switch>
                        <DOM>
                            <Route exact path={'/signin'} component={AuthSignIn}/>
                            <Route exact path={'/signup'} component={AuthSignUp}/>
                            <Route path={'/validation'} component={AuthValidation}/>
                            <Route exact path={'/registration'} component={AuthRegistration}/>
                            <Route
                                exact path='/'
                                render={(props) => <SwitchBoard {...props} component={Home} switchType={'HOME'} />}
                            />
                            <Route
                                path='/search'
                                render={(props) => <SwitchBoard {...props} component={Search} switchType={'SEARCH'} />}
                            />
                            <Route
                                exact path='/profile'
                                render={(props) => <SwitchBoard {...props} component={UserProfile} switchType={'PROFILE'} />}
                            />
                            <Route
                                exact path='/restaurant'
                                render={(props) => <SwitchBoard {...props} component={RestaurantProfile} switchType={'RESTAURANT'} />}
                            />
                            <Route exact path={'/sampler'} component={Sampler}/>
                            <Route exact path={'/createnewrestaurant'} component={CreateNewRestaurant}/>
                        </DOM>
                    </Switch>
                </ThemeProvider>
            </Router>
        </Provider>
    </div>
  );
}

export default App;
