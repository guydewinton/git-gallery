import React from "react";
import styled from "styled-components";

const WrapperDiv = styled.div`
height: 80vh;
width: 100vw;
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
`

const LoaderFont = styled.h3`
font-size: 20rem;
`

const Loader = () => {

    return (
        <WrapperDiv>
            <LoaderFont>loading...</LoaderFont>
        </WrapperDiv>
    )
}

export default Loader