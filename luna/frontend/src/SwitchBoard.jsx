import React, {useEffect, useState} from 'react'
import {connect} from 'react-redux'

import Loader from "./Loader";
import {makeFetch} from "./store/actions/fetchActions";
import Home from "./components/views/pages/Home";
import {withRouter} from "react-router";

const SwitchBoard = (props) => {

    window.scrollTo(0, 0);

    console.log('here top')

    const type = props.switchType;

    const [isReady, setIsReady] = useState(false);
    const [Component, setComponent] = useState(props.component);

    useEffect (() => {
        console.log('x', type, Component, props.rest_id === null)
        const switchBoarder = async () => {

            console.log('here middle');

            switch (type) {
                case 'HOME': {
                    setIsReady( true);
                    // does fetches and awaits until confirmation of success
                    //             makeFetch({type: 'GET_RESTAURANTS'})
                    //                 .then((data) => {
                    //                 console.log('almost ready erstaurrants', data);
                    //                 if (data[0]) {
                    //                     setIsReady( true);
                    //                     console.log('ready!!!')
                    //                 }
                    //             props.history.push('/')
                    //             });
                    break
                }
                case 'SEARCH': {
                    setIsReady( true);
                    // does fetches and awaits until confirmation of success
                    // console.log('test')
                    // const restaurantsResult = await await makeFetch({type: 'GET_RESTAURANTS'})[0];
                    //
                    //                 makeFetch({type: 'GET_RESTAURANTS'})
                    //                     .then((data) => {
                    //                         console.log('almost ready', data);
                    //                         if (data[0]) {
                    //                             setIsReady( true);
                    //                             console.log('ready!!!')
                    //                         }
                    //                     })
                    // console.log('almost ready');
                    // console.log(restaurantsResult);
                    // if (restaurantsResult) {
                    //     setIsReady( true);
                    //     console.log('ready!!!')
                    // }
                    break
                }
                case 'PROFILE': {
                    // does fetches and awaits until confirmation of success
                    setIsReady( true);
                    // const userResult = await makeFetch({type: 'GET_PROFILE'})[0];
                    // console.log('almost ready');
                    // console.log(userResult);
                    // if (userResult === 'OK') {
                    //     setIsReady( true);
                    //     console.log('ready!!!')
                    // }
                    break
                }
                case 'RESTAURANT':

                    if (props.rest_id===null) {
                        console.log('xx', Component);
                        setComponent(Home);
                        setIsReady(true);
                        props.history.push('/')
                    } else {
                        setIsReady(true)
                    }
                case 'REVIEWS':
                    setIsReady( true);
                    // makeFetch({type: 'GET_REST_REVIEWS'})
                    //     .then((data) => {
                    //         console.log('almost ready', data);
                    //         if (data[0]) {
                    //             setIsReady( true);
                    //             console.log('ready!!!')
                    //         }
                    //     })


                    break;

                default: {
                    return null
                }
            }

            return () => {
                console.log('here bottom')
                window.scrollTo(0, 0)
            };

        };
        switchBoarder()

    }, [type]);


    return (
        <>
            {/*<Component {...props} />*/}
            {console.log('isREADy,', isReady, Component)}
            {isReady ? <Component {...props} /> : <Loader className={'LOADER'}/>}
            {console.log('in da switch renderer', props)}
        </>
        )

};

const mapStateToProps = (state) => {
    return {
        token: null,
        isReady: null,
        rest_id: state.ui.restaurant
    }
}

export default connect(mapStateToProps)(withRouter(SwitchBoard))