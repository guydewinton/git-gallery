import React from "react"

function Phone(props) {
    return (
        <svg width={'18rem'} height={'26rem'} viewBox="0 0 18 26" {...props}>
            <title>{"phone"}</title>
            <path
                d="M3.6 26h10.8c1.989 0 3.6-1.454 3.6-3.25V3.25C18 1.454 16.389 0 14.4 0H3.6C1.611 0 0 1.454 0 3.25v19.5C0 24.546 1.611 26 3.6 26zM1.8 3.25h14.4v19.5H1.8V3.25z"
                fill="#4C4C4C"
                fillRule="nonzero"
            />
        </svg>
    )
}

export default Phone
