import React from "react";

import Star from "./Star";
import styled from "styled-components";

const StarsDiv = styled.div`
  position: relative;
`;

const BackgroundStars = styled.div`
  width: 125rem;
  height: 25rem;
  position: absolute;
  display: flex;
  flex-direction: row;
  //background-color: orange;
`;

const RatingStars = styled(BackgroundStars)`

`;

export default (props) => {

    const ratedStars = ((props) => {

        console.log(props)

        const wholeRating = Math.floor(props.rating);
        const decimalRating = Math.floor((props.rating - wholeRating) * 100);

        console.log(wholeRating, decimalRating)

        const starArray = [];

        let index = 0

        for (let i = 0; i < wholeRating; i++) {
            index++
            starArray.push(<Star key={'star_' + index} color={'#f8e71c'} index={index}/>)
        }

        starArray.push(<div key={'star_' + index + 1} style={{width: decimalRating * 0.25 + 'rem', overflow: 'hidden'}}><Star color={'#f8e71c'} index={index+1}/></div>);

        return starArray

    });

    return (
        <StarsDiv>
            <BackgroundStars>
                {Array(5).fill(1).map((a, i) => {
                    return <Star
                        color={'#ebebeb'}
                        key={'rating_' + i}
                    />
                })}
            </BackgroundStars>
            <RatingStars>
                {ratedStars(props).map((star) => {
                    return star
                })}
            </RatingStars>
        </StarsDiv>
    )
}