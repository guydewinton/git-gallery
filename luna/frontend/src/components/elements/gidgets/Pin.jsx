import React from "react"

function Pin(props) {
    return (
        <svg width={'19rem'} height={'26rem'} viewBox="0 0 19 26" {...props}>
            <title>{"pin"}</title>
            <g fill="#4C4C4C" fillRule="nonzero">
                <path d="M9.5 26l-.489-.648C8.644 24.863 0 13.337 0 8.71 0 3.908 4.262 0 9.5 0S19 3.908 19 8.71c0 4.627-8.644 16.153-9.011 16.642L9.5 26zm0-24.911c-4.584 0-8.313 3.419-8.313 7.621 0 3.695 6.463 12.824 8.313 15.36 1.85-2.536 8.313-11.665 8.313-15.36 0-4.202-3.73-7.621-8.313-7.621z" />
                <path d="M9 13c-2.205 0-4-1.794-4-4s1.795-4 4-4 4 1.794 4 4-1.795 4-4 4zm0-6.857A2.86 2.86 0 006.143 9 2.86 2.86 0 009 11.857 2.86 2.86 0 0011.857 9 2.86 2.86 0 009 6.143z" />
            </g>
        </svg>
    )
}

export default Pin
