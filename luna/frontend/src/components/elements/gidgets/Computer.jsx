import React from "react"

function Computer(props) {
    return (
        <svg width={'32rem'} height={'26rem'} viewBox="0 0 32 26" {...props}>
            <title>{"web"}</title>
            <path
                d="M30 2.167C30 .97 29.104 0 28 0H4C2.896 0 2 .97 2 2.167v19.5H0C0 24.06 1.79 26 4 26h24c2.21 0 4-1.94 4-4.333h-2v-19.5zm-2 19.5H4v-19.5h24v19.5z"
                fill="#4C4C4C"
                fillRule="nonzero"
            />
        </svg>
    )
}

export default Computer