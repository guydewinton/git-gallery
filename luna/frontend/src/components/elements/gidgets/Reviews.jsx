import React from "react"

function Reviews(props) {
    return (
        <svg width={'31rem'} height={'30rem'} viewBox="0 0 36 35" {...props}>
            <title>{"Star"}</title>
            <path
                d="M18 27.875L8.595 33l1.797-10.854-7.609-7.687 10.515-1.584L18 3l4.702 9.875 10.515 1.584-7.609 7.687L27.405 33z"
                fill="none"
                stroke="#303030"
                strokeWidth={2}
                fillRule="evenodd"
            />
        </svg>
    )
}

export default Reviews
