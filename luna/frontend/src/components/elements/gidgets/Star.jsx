import React from "react"
import styled from "styled-components";

const WrapperDiv = styled.div`
  overflow: hidden;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

export default (props) => {



    return (

        <WrapperDiv style={{height: '25rem', width: '25rem', margin: 'margin: 0 1rem'}}>
            <svg width={'25rem'} height={'25rem'} viewBox="0 0 36 35" {...props}>
                <title>{"Star"}</title>
                <path
                    d="M18 27.875L8.595 33l1.797-10.854-7.609-7.687 10.515-1.584L18 3l4.702 9.875 10.515 1.584-7.609 7.687L27.405 33z"
                    fill={props.color}
                    stroke="none"
                    strokeWidth={0}
                    fillRule="evenodd"
                />
            </svg>
        </WrapperDiv>
    )
}