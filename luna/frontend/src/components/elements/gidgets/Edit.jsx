import React from "react"

function Edit(props) {
    return (
        <svg width={'28rem'} height={'28rem'} viewBox="0 0 30 30" {...props}>
            <title>{"Shape"}</title>
            <path
                d="M18.655 5.007l6.102 6.132L9.31 26.662l-6.1-6.132L18.656 5.007zm10.733-1.48L26.667.794a2.691 2.691 0 00-3.815 0l-2.607 2.62 6.102 6.132 3.041-3.056a2.098 2.098 0 000-2.961zM.017 29.15a.696.696 0 00.84.83l6.8-1.657-6.099-6.133-1.541 6.96z"
                fill="#303030"
                fillRule="nonzero"
            />
        </svg>
    )
}

export default Edit
