import React from 'react';
import styled from "styled-components";

import './App.css'

import Masonry from 'react-masonry-css'

const WrapperDiv = styled.div`
  width: 100%;
`;

const ResponsiveColumns = (props) => {

    const breakpointColumnsSideOn = {
        default: 3,
        1800: 2,
        1200: 1
    };

    // const breakpointColumnsSideOff = {
    //     default: 4,
    //     1300: 3,
    //     1100: 2,
    //     900: 1
    // };

    return (
        <WrapperDiv key={props.index}>
                <Masonry
                    breakpointCols={4}
                    className="my-masonry-grid"
                    columnClassName="my-masonry-grid_column">
                    {props.contentArray}
                </Masonry>
        </WrapperDiv>
    )
};

export default ResponsiveColumns;