import styled from "styled-components";

const ResponsiveContentBox = styled.div`

    width: 1200px;
    @media (max-width: 1800px) {
    width: 800rem;
    }
    @media (max-width: 1200px) {
    width: 400px;
    }
`;

export default ResponsiveContentBox