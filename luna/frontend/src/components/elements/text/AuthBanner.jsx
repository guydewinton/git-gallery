import React from 'react';
import styled from "styled-components";

const LoginBannerDiv = styled.div`
  width: 100rem;
  height: 45rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-bottom: 3rem solid ${props => props.theme.colors.primaryOrange};
  margin-bottom: 69rem;
  
      p {
      font-size: 24rem;
      font-weight: bold;
      text-align: center;
      color: #4c4c4c;
      }
`;

export default (props) => {
    return (
        <LoginBannerDiv style={{...props.style}}> 
            <p>{props.text}</p>
        </LoginBannerDiv>
    )
}

//on line 24, you are acceprting dynamic styling via props on Create New Restaurant  
