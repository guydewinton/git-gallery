import React from "react";
import styled from "styled-components";

const WrapperDiv = styled.div`
  width: 450rem;
  p {
  font-size: 20rem;
  font-weight: normal;
  text-align: center;
  color: ${props => props.theme.colors.paragraphText};
  }
`;

export default (props) => {
    return (
            <WrapperDiv>
                <p>{props.texthead}</p>
                <p>{props.textbody}</p>
            </WrapperDiv>
    )
}