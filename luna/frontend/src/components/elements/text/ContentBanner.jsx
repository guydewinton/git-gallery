import React from 'react';
import styled from "styled-components";

const ContentBannerDiv = styled.div`
  height: 40rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 42rem;
  
      p {
      font-size: 24rem;
      font-weight: bold;
      text-align: center;
      color: #4c4c4c;
      }
`;

//265

const BottomDiv = styled.div`
  width: 265rem;
  height: 3rem;
    border-bottom: 3rem solid ${props => props.theme.colors.primaryOrange};

`;

export default (props) => {
    return (
        <ContentBannerDiv>
            <p>{props.text}</p>
            <BottomDiv/>
        </ContentBannerDiv>
    )
}
