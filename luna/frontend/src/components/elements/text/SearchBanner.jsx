import React from 'react';
import styled from "styled-components";
import {Link} from "react-router-dom";

// const Text = styled.p`
//       font-size: 24rem;
//       font-weight: bold;
//       text-align: center;
//       color: #878787;
//
// `

const ContentBannerDiv = styled(Link)`
width: 205rem;
  height: 40rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  text-decoration: none;
  
            ${props => props.active && ` 
            p {
        color: #4c4c4c;
        }

    `}

  
      p {
      font-size: 24rem;
      font-weight: bold;
      text-align: center;
      color: #878787;

     ${props => props.active && ` 
        color: #4c4c4c;;
    `}

      }
      
      // :hover {
      // background-color: ${props => props.theme.colors.lightGrey};
      // }
      
     :hover .inactive {
    border-bottom: 3.5rem solid ${props => props.theme.colors.primaryOrange};
    width: 180rem;
    }
    
     :hover p {
      color: #4c4c4c;
      }
`;

//265

const BottomDiv = styled.div`
  width: 205rem;
  height: 3rem;

    ${props => props.active && ` 
        border-bottom: 3.5rem solid ${props.theme.colors.primaryOrange};
    `}

`;

export default (props) => {
    let active = null;
    if(props.active) {active = 'active'} else {active = 'inactive'}
    return (
        <ContentBannerDiv to={props.link} active={props.active}>
            <p>{props.text}</p>
            <BottomDiv className={'bottomDiv ' + active} active={props.active}/>
        </ContentBannerDiv>
    )
}
