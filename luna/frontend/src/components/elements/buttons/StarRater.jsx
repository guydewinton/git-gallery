import React, {useState} from "react";

// import Star from "./Star";
import styled from "styled-components";
import StarRating from "../gidgets/StarRating";

const StarsDiv = styled.div`
  position: relative;
  //width: 235rem;
  //height: 25rem;
  //margin: 11rem 19rem 20rem 16rem;
`;

const BackgroundStars = styled.div`
  width: 135rem;
  height: 25rem;
  position: absolute;
  display: flex;
  flex-direction: row;
  //background-color: orange;
  z-index: 0;
`;

const RatingButtonDiv = styled(BackgroundStars)`
  z-index: 5;
`;

const RatingButton = styled.div`
height: 25rem;
width: 25rem;
cursor: pointer;
z-index: 10;

`;

const ZeroRating = styled.div`
position: absolute;
left: -8rem;
height: 25rem;
width: 8rem;
cursor: pointer;
`

export default (props) => {

    const [rating, setRating] = useState(props.rating)

    const mouseOverHandler = (i) => {
        setRating(i)
        console.log('over!!')
    };

    const mouseOutHandler = () => {
        setRating(props.rating)
        console.log('out!!!')
    };

    const clickHandler = (i) => {
        console.log(i)
    };



    return (
        <StarsDiv>
            <ZeroRating
                onMouseOver={() => mouseOverHandler(0)}
                onMouseOut={mouseOutHandler}
                onClick={() => clickHandler(0)}/>
            <RatingButtonDiv>
                {Array(5).fill(1).map((a, i) => {
                    return <RatingButton
                        key={'rater_' + i}
                        onMouseOver={() => mouseOverHandler(i+1)}
                        onMouseOut={mouseOutHandler}
                        onClick={() => clickHandler(i+1)}
                    />
                })}
            </RatingButtonDiv>
            <BackgroundStars>
                <StarRating rating={rating}/>
            </BackgroundStars>

        </StarsDiv>
    )
}