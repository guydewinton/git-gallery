import React from 'react';
import styled from "styled-components";

const WrapperDiv = styled.button`
  height: 40rem;
  width: 120rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.theme.buttons.small.bgColorUp};
  border-radius: ${props => props.theme.buttons.small.borderRadius};
  outline: none;
  cursor: pointer;
  border: none;
  p {
    font-size: ${props => props.theme.buttons.small.fontSize};
    font-weight: ${props => props.theme.buttons.small.fontWeight};
    color: ${props => props.theme.buttons.small.fontColor};
  }
    :hover {
  background-color: ${props => props.theme.buttons.small.bgColorOver}
  }
    :active {
  background-color: ${props => props.theme.buttons.small.bgColorDown}
  }
  

`



export default (props) => {
    return (
        <WrapperDiv>
            <p>{props.text}</p>
        </WrapperDiv>
    );
}
