import React from "react";

export default () => {
    return (
        <svg width={'40rem'} height={'40rem'} viewBox="0 0 40 40">
            <title>{"facebook"}</title>
            <g fill="none" fillRule="evenodd">
                <circle stroke="#979797" strokeWidth={2} cx={20} cy={20} r={19} />
                <path
                    d="M21.816 29.682h-4.545V20.34H15v-3.22h2.271v-1.932c0-2.626 1.282-4.188 4.925-4.188h3.033v3.22h-1.895c-1.419 0-1.512.45-1.512 1.29l-.007 1.61h3.435l-.402 3.22h-3.033v9.342h.001z"
                    fill="#979797"
                />
            </g>
        </svg>
    )
}