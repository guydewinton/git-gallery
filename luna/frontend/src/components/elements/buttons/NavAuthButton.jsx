import React from 'react';
import styled from "styled-components";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

const WrapperDiv = styled.div`
  height: 41rem;
  display: flex;
  flex-direction: row;
  
`

const LeftDiv = styled(Link)`
  height: 41rem;
  width: 100rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: #e47d31;
  border-radius: 28rem 0 0 28rem;
  margin-left: 22rem;
  padding-left: 6rem;
  text-decoration: none;
  
  &:hover {

  }
  
  p {
  font-size: 16rem;
  color: #ffffff;
  }
   :hover {
  background-color: ${props => props.theme.buttons.big.bgColorOver}
  }
  :active {
  background-color: ${props => props.theme.buttons.big.bgColorDown}
  }
`

const RightDiv = styled(Link)`
  height: 41rem;
  width: 100rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: #e47d31;
  border-radius:  0 28rem 28rem 0;
  padding-right: 9rem;
    text-decoration: none;
  
  
  :hover {
  background-color: ${props => props.theme.buttons.big.bgColorOver}
  }
  :active {
  background-color: ${props => props.theme.buttons.big.bgColorDown}
  }
  
   
  
  p {
  font-size: 16rem;
  color: #ffffff;
  }
`

const CenterLineDiv = styled.div`
  height: 41rem;
  width: 1rem;
  background-color: #ffffff;
`

const AuthNav = (props) => {
    return (
        <WrapperDiv>
            <LeftDiv to={'/signup'}>
                <p>SIGNUP</p>
            </LeftDiv>
            <CenterLineDiv/>
            <RightDiv to={'/signin'}>
                <p>{!props.auth?'LOGIN':'LOGOUT'}</p>
            </RightDiv>
        </WrapperDiv>
    );
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth.token
    }
}

export default connect(mapStateToProps)(AuthNav)