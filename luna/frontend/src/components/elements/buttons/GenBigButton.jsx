import React from 'react';
import styled from "styled-components";

const WrapperDiv = styled.button`
  height: ${props => props.theme.buttons.big.height};
  width: ${props => props.theme.buttons.big.width};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.theme.buttons.big.bgColorUp};
  border-radius: ${props => props.theme.buttons.big.borderRadius};
  outline: none;
  cursor: pointer;
  border: none;
  p {
    font-size: ${props => props.theme.buttons.big.fontSize};
    font-weight: ${props => props.theme.buttons.big.fontWeight};
    color: ${props => props.theme.buttons.big.fontColor};
  }
  :hover {
  background-color: ${props => props.theme.buttons.big.bgColorOver}
  }
    :active {
  background-color: ${props => props.theme.buttons.big.bgColorDown}
  }
  

`



export default (props) => {

    return (
        <WrapperDiv onClick={props.onClick}>
            <p>{props.text}</p>
        </WrapperDiv>
    );
}
