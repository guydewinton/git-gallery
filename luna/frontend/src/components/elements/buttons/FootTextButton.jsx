import React from 'react'
import styled from "styled-components";

const WrapperDiv = styled.div`
  margin: 0 28rem;
  cursor: pointer;
  
  p {
  font-size: 20rem;
  font-weight: normal;
  text-align: center;
  color: #646363;
  }

`;

export default (props) => {
    return (
        <WrapperDiv>
            <p>{props.text}</p>
        </WrapperDiv>
    )
}
