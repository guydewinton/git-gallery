import React from 'react';
import styled from "styled-components";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import ThumbsUp from "../gidgets/ThumbsUp";

const WrapperDiv = styled.div`
  //position: relative;
  height: 33rem;
  width: 249rem;
  display: flex;
  flex-direction: row;
  background-color: #ffffff;
`;

const ButtonLeft = styled.div`
  height: 33rem;
  width: 124rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: rgba(145, 145, 145, 0.6);
  border-radius: 28rem 0 0 28rem;
  padding-left: 6rem;
  text-decoration: none;
  cursor: pointer;
  
  &:hover {

  }
  
  p {
  font-size: 16rem;
  color: #ffffff;
  font-weight: 300;
  }
   :hover {
  background-color: rgba(145, 145, 145, 0.9);  }
  :active {
  background-color: rgba(145, 145, 145, 0.7);  }
  
  .commentLike {
  margin: 0 10rem 0 10rem;
  }
  
  
`;

const ButtonRight = styled.div`
  height: 33rem;
  width: 124rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: rgba(145, 145, 145, 0.6);
  border-radius:  0 28rem 28rem 0;
  padding-right: 9rem;
    text-decoration: none;
    cursor: pointer;
  
  
  :hover {
  background-color: rgba(145, 145, 145, 0.9);
  }
  :active {
  background-color: rgba(145, 145, 145, 0.7);
  }
  
   
  
  p {
  font-size: 16rem;
  font-weight: 300;
  color: #ffffff;
  margin: 0 3rem 0 3rem;
  }
`;

const Like = styled(ThumbsUp)`
height: 21rem;
width:21rem;
`

const ButtonDivider = styled.div`
  height: 33rem;
  width: 1rem;
  background-color: #ffffff;
`;

const RevCommentLikeButton = (props) => {

    const comments = props.comments.filter((comment) => props.review.id === comment.review)

    return (
        <WrapperDiv>

                        <ButtonLeft>
                            <Like/>
                            <p className={'commentLike'}>Like</p>
                            <p className={'commentLikeCount'}>21</p>
                        </ButtonLeft>
                        <ButtonDivider/>
                        <ButtonRight>
                            <p>Comment</p>
                            <p>{comments.length}</p>
                        </ButtonRight>

        </WrapperDiv>
    )
};


const mapStateToProps = (state) => {
    return {
        comments: state.fetch.comments.results,


    }
};

export default connect(mapStateToProps)(withRouter(RevCommentLikeButton))