import React from 'react';
import styled from "styled-components";
import {Link} from "react-router-dom";

const WrapperDiv = styled(Link)`
  width: 100rem;
  height: 71rem;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  text-decoration: none;
  

    :hover .bottomDiv{
    border-bottom: 3px solid ${props => props.theme.colors.primaryOrange};
    }
    :hover .text {
    font-weight: bold;
    letter-spacing: -0.5rem;
    }
    &:active {

  }
`

const UpperDiv = styled.div`
  width: 100rem;
  height: 23rem;
  margin: 0 17rem;
`

const TextDiv = styled.div`
  width: 100rem;
  height: 24rem;
  margin: 0 17rem;
  text-align: center;
  
  .text {
  font-size: 20rem;
  }
  
  
  
  

`

const LowerDiv = styled.div`
  width: 45rem;
  height: 24rem;
  
  
    &:hover {

  }
    &:active {

  }
`

export default (props) => {
    return (
        <WrapperDiv to={props.link}>
            <UpperDiv/>
            <TextDiv>
                {props.active ? <p className={'text'} style={{color: 'black', fontWeight: 'bold', letterSpacing: '-0.5rem'}}>{props.text}</p> : <p className={'text'} style={{color: 'black'}}>{props.text}</p>}
            </TextDiv>
            {props.active ? <LowerDiv className={'bottomDiv'} style={{borderBottom: '3px solid #e47d31'}}/> : <LowerDiv className={'bottomDiv'}/>}

        </WrapperDiv>
    );
}
