import styled from "styled-components";

const InputField = styled.input`
    
    
    width: 340rem;
    height: 52rem;
    border-radius: 3rem;
    border: solid 1rem #ebebeb;
    background-color: #ffffff;
    margin: 10rem 0;
    padding: 0 0 0 23rem;
    color: #979797;
    font-size: 20rem;
    
    ::placeholder {
  font-weight: bold;
  color: #979797;
}

`

export default InputField;

// export default () => {
//     return (
//         <InputField/>
//     )
// }