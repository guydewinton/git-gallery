import React from "react";
import styled from "styled-components";

const WrapperDiv = styled.div`
width: 619rem;
//height: 113rem;
display: flex;
flex-direction: column;
margin: 0;
h3 {
  font-size: 20rem;
  font-weight: bold;
  color: #979797;
  margin-bottom: 10rem;
}
`

const InputField = styled.text`
    width: 582rem;
    height: 120rem;
    border-radius: 3rem;
    border: solid 1rem #ebebeb;
    background-color: #ffffff;
    margin: 10rem 0;
    padding: 0 0 0 23rem;
    color: #979797;
    font-size: 20rem;
    margin-bottom: 27rem;
    
    ::placeholder {
  font-weight: bold;
  color: #979797;
}

`

export default (props) => {
    return (
        <WrapperDiv>
            <h3>{props.text}</h3>
            <InputField placeholder={props.placeholder} onChange={props.onChange}/>
        </WrapperDiv>
    )
}