import React from 'react'
import styled from 'styled-components';
import {connect} from "react-redux";

const WrapperDiv = styled.div`
padding: 0 11rem;
p {
font-size: 20rem;
}
`;

const HeaderDiv = styled.div`
margin-top: 17rem;
margin-bottom: 15rem;
h3 {
  font-size: 20rem;
  font-weight: 300;
  color: #000000;
}
`;

const BodyDiv = styled.div`
padding-bottom: 15rem;
h4 {
  font-size: 14rem;
  font-weight: bold;
  color: #e47d31;
}

p {
  font-size: 14rem;
  font-weight: 300;
  color: #000000;
}
`;


const SearchReviewComments = (props) => {

    const comments = props.comments.filter((comment) => comment.review === props.review.id);


    return (
        <WrapperDiv>
            <HeaderDiv>
                <h3>COMMENTS</h3>
            </HeaderDiv>
            {comments.map((comment) => {

            const user = props.users.filter((user) => user.id === comment.user);
            console.log('SearchReviewComments', user)

                return (
                    <BodyDiv>
                        <h4>{user[0].first_name}</h4>
                        <p>{comment.content}</p>
                    </BodyDiv>
                )
            })}
        </WrapperDiv>
    )

};

const mapStateToProps = (state) => {
    return {
        comments: state.fetch.comments.results,
        users: state.fetch.users.results
    }
}

export default connect(mapStateToProps)(SearchReviewComments)