import React from 'react';
import styled from "styled-components";
import StarRater from "../elements/buttons/StarRater";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import SearchUserArrayBody from "./SearchUserArrayBody";
import SearchReviewUserArrayHeader from "./SearchReviewUserArrayHeader";

const WrapperDiv = styled.div`
  //position: relative;
  
  width: 270rem;
  display: flex;
  flex-direction: column;
  outline: solid 1rem #ebebeb;
  cursor: pointer;
  border-top: 8rem solid ${props => props.theme.colors.primaryOrange};
  margin: 0 15rem 30rem 15rem;

  //border: solid 1rem #ebebeb;
  background-color: #ffffff;
`;

const SearchUserArray = (props) => {

    const user = props.users.filter((user) => user.id === props.user.id)[0];
    const reviews = props.reviews.filter((review) => review.user === user.id)

    return (
        <WrapperDiv>
            <SearchReviewUserArrayHeader user={user}/>
            <SearchUserArrayBody user={user}/>
        </WrapperDiv>

    )

};

const mapStateToProps = (state) => {
    return {
        users: state.fetch.users.results,
        reviews: state.fetch.reviews.results

    }
};

export default connect(mapStateToProps)(SearchUserArray)
