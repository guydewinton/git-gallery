import React, {useEffect, useState} from 'react';
import styled from "styled-components";
import {connect} from "react-redux";
import Stars from "./Stars";
import Pin from "../elements/gidgets/Pin";
import Phone from "../elements/gidgets/Phone";
import Computer from "../elements/gidgets/Computer";
import TextInputField from "../elements/input/TextInputField";
import InlineButton from "../elements/buttons/InlineButton";
import RestaurantReviewArray from "./RestaurantReviewArray";
import Clock from "../elements/gidgets/Clock";
import Money from "../elements/gidgets/Money";
import GenSmallButton from "../elements/buttons/GenSmallButton";

const WrapperDiv = styled.div`
  width: 100vw;
`;

const BannerDiv = styled.div`
width: 100vw;
height: 496rem;
position: relative;
background-image: url("${props => props.image}");
background-size: cover;
background-position: center;
`;

const BannerHeaderDiv = styled.div`
width: 100vw;
height: 204rem;
position: absolute;
background-color: rgba(0, 0, 0, 0.5);
`;

const BannerTitleDiv = styled.div`
display: flex;
flex-direction: column;
position: relative;
top: 33rem;
left: 130rem;

    .name {
    font-size: 30rem;
    font-weight: bold;
    color: #ffffff;
    margin-bottom: 11rem;
    }
    
    .category {
    font-size: 24rem;
    font-weight: 300;
    color: #ffffff;
    }
    
`;

const InfoDiv = styled.div`
min-height: 100rem;
width: 360rem;
position: absolute;
top: 30rem;
right: 130rem;
background-color: #ffffff;
display: flex;
flex-direction: column;
`;

const MapDiv = styled.div`
height: 167rem;
width: 360rem;
background-image: url("https://newinzurich.com/wp-content/uploads/2018/12/Screenshot-2018-12-30-at-20-compressed.jpg");
background-position: center;
background-size: cover;
`;

const InfoBit = styled.div`
display: flex;
flex-direction: row;
width: 360rem;
height: 47rem;
align-items: center;
p {
  font-size: 20rem;
  color: #4c4c4c;
}
`;

const InfoWidgetDiv = styled.div`
width: 84rem;
height: 47rem;
display: flex;
justify-content: center;
align-items: center;
`

const YetAnotherDiv = styled.div`
margin: 14rem 0;
`;

const BaseDiv = styled.div`
height: 100%;
padding: 0 130rem;
display: flex;
flex-direction: row;
`;

const BaseLeft = styled.div`
width: 650rem;
display: flex;
flex-direction: column;
margin-bottom: 30rem;
`;

const FilterDiv = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
`;

const FilterBar = styled.div`
width: 650rem;
height: 40rem;
display: flex;
flex-direction: row;
justify-content: space-between;
align-items: center;
margin: 20rem 0 27rem 0;
`;

const FilterField = styled(TextInputField)`
height: 40rem;
width: 510rem;
margin: 0;
`;

const ReviewDiv = styled.div`
//height: 188rem;
width: 650rem;
margin-bottom: 15rem;
background-color: orange;
`;

const BaseRight = styled.div`
height: 100%;
width: 508rem;
display: flex;
flex-direction: column;
padding: 18rem;
margin-left: 22rem;
background-color: #f5f5f5;
`;

const BaseRightTime = styled.div`
display: flex;
flex-direction: row;
justify-content: flex-start;
align-items: center;
padding-bottom: 13rem;
border-bottom: solid 1rem #d8d8d8;
p {
 font-size: 20rem;
  font-weight: 300;
  color: #000000;
  margin-left: 21rem;
}
`;

const BaseRightPrice = styled.div`
margin-top: 13rem;
display: flex;
flex-direction: row;
justify-content: flex-start;
align-items: center;
margin-bottom: 31rem;

p {
 font-size: 20rem;
  font-weight: 300;
  color: #000000;
  margin-left: 21rem;
}
`;

const BaseRightButtons = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
align-items: center;
`;

const RestaurantProfilePage = (props) => {

    const restaurant = props.restaurants.filter((restaurant) => restaurant.id === props.rest_id)[0];

    const reviewArray = props.reviews.filter((review) => review.restaurant === restaurant.id);

    let priceLevel = '';

    Array(restaurant.price_level).fill(0).forEach((buck) => {
        priceLevel += '$'
    });



    return(
        <WrapperDiv>
            <BannerDiv image={restaurant.image}>
                <BannerHeaderDiv>
                    <BannerTitleDiv>
                        <h3 className={'name'}>{restaurant.name}</h3>
                        <p className={'category'}>{restaurant.category}</p>
                        <Stars rating={restaurant.average_rating} counter={restaurant.review_counter} color={'#ffffff'}/>
                    </BannerTitleDiv>
                </BannerHeaderDiv>
                <InfoDiv>
                    <MapDiv/>
                    <YetAnotherDiv>
                        <InfoBit key={1}><InfoWidgetDiv><Pin/></InfoWidgetDiv><p>{restaurant.street}</p></InfoBit>,
                        <InfoBit key={2}><InfoWidgetDiv><Phone/></InfoWidgetDiv><p>{restaurant.phone}</p></InfoBit>,
                        {restaurant.website && <InfoBit key={3}><InfoWidgetDiv><Computer/></InfoWidgetDiv><p>{restaurant.website}</p></InfoBit>}
                    </YetAnotherDiv>
                </InfoDiv>
            </BannerDiv>
            <BaseDiv>
                <BaseLeft>
                <FilterDiv>
                    <FilterBar>
                        <FilterField/>
                        <InlineButton text={'FILTER'}/>
                    </FilterBar>
                </FilterDiv>
                    {reviewArray.map((review, i)=> {
                        return (
                        <ReviewDiv
                            key={'review_' + i}
                        >
                            <RestaurantReviewArray
                                review_id={review.id}
                            />
                        </ReviewDiv>
                        )
                    })}
                </BaseLeft>
                <BaseRight>
                    <BaseRightTime>
                        <Clock/>
                        <p>{restaurant.opening_hours}</p>
                    </BaseRightTime>
                    <BaseRightPrice>
                        <Money/>
                        <p>Price level: {priceLevel}</p>
                    </BaseRightPrice>
                    <BaseRightButtons>
                        <GenSmallButton text={'WRITE A REVIEW'}/>
                        <GenSmallButton text={'EDIT DATA'}/>
                    </BaseRightButtons>
                </BaseRight>
            </BaseDiv>
        </WrapperDiv>
    )
};

const mapStateToProps = (state) => {
    return {
        restaurants: state.fetch.restaurants.results,
        reviews: state.fetch.reviews.results
    }
};

export default connect(mapStateToProps)(RestaurantProfilePage)