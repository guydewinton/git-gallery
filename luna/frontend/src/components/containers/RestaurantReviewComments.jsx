import React from 'react';
import styled from "styled-components";
import RestaurantReviewCommentsArray from "./RestaurantReviewCommentsArray";
import {connect} from "react-redux";
import TextInputField from "../elements/input/TextInputField";
import PostCommentButton from "../elements/buttons/PostCommentButton";

const WrapperDiv = styled.div`
margin-top: 10rem;
width: 650rem;
display: flex;
flex-direction: column;
`;

const AddCommentDiv = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
width: 620rem;
height: 31rem;
margin-bottom: 9rem;

`;

const AddCommentLeftDiv = styled.div`
display: flex;
flex-direction: row;
align-items: center;
`;

const AddCommentRightDiv = styled.div`
display: flex;
flex-direction: row;
align-items: center;
cursor: pointer;
p {
  font-size: 16rem;
  color: #e47d31;
}
`;

const AddCommentField = styled(TextInputField)`
height: 31rem;
width: 414rem;
margin-right: 9rem;
`;

const AddCommentButton = styled(PostCommentButton)`
height: 31rem;
width: 120rem;
`;

const CommentsDiv = styled.div`
width: 650rem;
display: flex;
flex-direction: column;
`;

const RestaurantReviewComments = (props) => {

    console.log('RestaurantReviewComments', props.comments)

    const comments = props.comments.filter((comment) => comment.review === props.review.id)

    return (
        <WrapperDiv>
            <AddCommentDiv>
                <AddCommentLeftDiv>
                    <AddCommentField/>
                    <AddCommentButton text={'POST'}/>
                </AddCommentLeftDiv>
                <AddCommentRightDiv onClick={() => props.hide()}>
                    <p>Hide</p>
                </AddCommentRightDiv>
            </AddCommentDiv>
            <CommentsDiv>
                {comments.map((comment, i) => {
                    return <RestaurantReviewCommentsArray id={comment.id} key={'commentlala_' + i}/>
                })}

            </CommentsDiv>
        </WrapperDiv>
    )

};

const mapStateToProps = (state) => {
    console.log('mapStateToProps', state)
    return {
        comments: state.fetch.comments.results
    }
}

export default connect(mapStateToProps)(RestaurantReviewComments)