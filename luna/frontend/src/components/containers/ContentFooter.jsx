import React from 'react';
import styled from "styled-components";

import FootTextButton from "../elements/buttons/FootTextButton";
import FootFacebookButton from "../elements/buttons/FootFacebookButton";
import FootInstgramButton from "../elements/buttons/FootInstgramButton";
import FooterGoogleButton from "../elements/buttons/FooterGoogleButton";
import FootTwitterButton from "../elements/buttons/FootTwitterButton";


const FooterDiv = styled.div`
  height: 56em;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-left: 2em;
  padding-right: 7em;
  background-color: #ffffff;
`;

const SubFootDiv = styled.div`
  height: 36em;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  border-top: 1em solid #ebebeb;
  background-color: #ffffff;
  padding: 0 29em;
    
    p {
      font-size: 12em;
      font-weight: normal;
      color: #646363;
    }
`;

const ButtonsDiv = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

const SocialButtonDiv = styled.div`
  margin: 0 11em;
`


export default (props) => {
    return (
        <>
            <FooterDiv>
                <ButtonsDiv>
                    <FootTextButton text={'About Us'}/>
                    <FootTextButton text={'Press'}/>
                    <FootTextButton text={'Blog'}/>
                    <FootTextButton text={'iOS'}/>
                    <FootTextButton text={'Android'}/>
                </ButtonsDiv>
                <ButtonsDiv>
                    <SocialButtonDiv>
                        <FootFacebookButton/>
                    </SocialButtonDiv>
                    <SocialButtonDiv>
                        <FootTwitterButton/>
                    </SocialButtonDiv>
                    <SocialButtonDiv>
                        <FooterGoogleButton/>
                    </SocialButtonDiv>
                    <SocialButtonDiv>
                        <FootInstgramButton/>
                    </SocialButtonDiv>
                </ButtonsDiv>
            </FooterDiv>
            <SubFootDiv>
                <p>© Copyright Luna 2020</p>
            </SubFootDiv>

        </>


    )
}