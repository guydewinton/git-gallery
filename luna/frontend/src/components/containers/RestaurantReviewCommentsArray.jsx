import React from 'react';
import styled from "styled-components";
import {connect} from "react-redux";

const WrapperDiv = styled.div`
position: relative;
right: 15rem;
width: 650rem;
min-height: 48rem;
display: flex;
flex-direction: row;
border-top: solid 1px #ebebeb;
padding: 0 15rem;


p{
font-size: 16rem;
margin: 1rem;
}
`;

const CommentLeftDiv = styled.div`
display: flex;
flex-direction: column;
padding: 6rem 0;
`;

const NameText = styled.p`
  font-size: 14px;
  font-weight: bold;
  color: #e47d31;
  margin: 1rem 0;

`;

const CommentText = styled.p`
 font-size: 14px;
  font-weight: 300;
  color: #000000;

`;

const CommentRightDiv = styled.div`

`

const RestaurantReviewCommentsArray = (props) => {

    const comment = props.comments.filter((comment) => {
        return comment.id === props.id
    })[0];

    const user = props.users.filter((user) => {
        return user.id === comment.user
    })[0];

    console.log('RestaurantReviewCommentsArray', comment)

    return (
        <WrapperDiv>
            <CommentLeftDiv>
                <NameText>{user.first_name} {user.last_name}</NameText>
                <CommentText>{comment.content}</CommentText>
            </CommentLeftDiv>
            <CommentRightDiv/>
        </WrapperDiv>
    )

};

const mapStateToProps = (state) => {
    console.log('mapStateToProps', state)
    return {
        comments: state.fetch.comments.results,
        users: state.fetch.users.results,
    }
};

export default connect(mapStateToProps)(RestaurantReviewCommentsArray)