import React, {useState} from 'react';
import styled from "styled-components";
import Star from "../elements/gidgets/Star";
import StarRating from "../elements/gidgets/StarRating";
import StarRater from "../elements/buttons/StarRater";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import restaurants from "../../library/initials/restaurants";
import RevCommentLikeButton from "../elements/buttons/RevCommentLikeButton";
import {Link} from "react-router-dom";
import RestaurantReviewComments from "./RestaurantReviewComments";

const WrapperDiv = styled.div`
  //position: relative;
  //height: 188rem;
  width: 650rem;
  display: flex;
  flex-direction: column;
  background-color: #ffffff;
`;

const HeaderDiv = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  border-bottom: solid 1rem #ebebeb;
  
  .name {
      font-size: 20rem;
  font-weight: bold;
  color: #e47d31;
  }
  
    .ratings {
 font-size: 14rem;
  font-weight: bold;
  color: #4c4c4c;
  margin-top: 4rem;
  }
`

const HeaderLeftDiv = styled.div`
height: 68rem;
    display: flex;
  flex-direction: row;
  justify-content: space-between;
  
`;

const HeaderLeftDivA = styled.div`
height: 68rem;
margin: 0 7rem 0 0rem;
`;

const HeaderLeftDivB = styled.div`
height: 68rem;
width: 150rem;
margin: 0 7rem 0 0;
    display: flex;
  flex-direction: column;
  justify-content: center;
`;

const HeaderLeftDivC = styled.div`
height: 68rem;
width: 125rem;
    display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const HeaderRightDiv = styled.div`
  height: 68rem;
    flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 10rem 13rem 9rem 0;
  p {
  font-size: 14rem;
  font-weight: 300;
  color: #000000;
  }

`;

const HeaderRightDivA = styled.div`

`


const ImageDiv = styled.div`
  height: 68rem;
  width: 66rem;
      background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
`;

const StarsDiv = styled.div`
  width: 243rem;
  height: 25rem;
  //margin: 12rem 12rem 21rem 10rem;

`;

const BodyDiv = styled.div`
width: 650rem;
//height: 66rem;
display: flex;
flex-direction: column;
justify-content: center;
 padding: 15rem 15rem 11rem 15rem;

`;

const CommentDiv = styled.div`
//height: 66rem;
 width: 620rem;


 .reviewTextProfile {

 //display: -webkit-box;
 //   -webkit-line-clamp: 3;
 //   -webkit-box-orient: vertical;
 //   overflow: hidden;
 //   text-overflow: ellipsis;

 width: 620rem;
 //max-height: 68rem;
   font-size: 16rem;
  font-weight: 300;
  color: #000000;
 }
`;

const BottomDiv = styled.div`
width: 620rem;
display: flex;
flex-direction: row;
justify-content: space-between;
padding: 15rem 0rem 3rem 0rem;
`;

const BottomDivLeft = styled.div`

`;

const BottomDivRight = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
p{
 font-size: 16rem;
  color: #e47d31;
}
`;

const ViewCommentsButton = styled.div`
  text-decoration: none;
  cursor: pointer;
`

const RestaurantReviewArray = (props) => {

    const [toggleComments, setToggleComments] = useState(false)

    const review = props.reviews.filter((review) => review.id === props.review_id)[0];

    const user = props.users.filter((user) => user.id === review.owner)[0];

    const userReviewTotal = props.reviews.filter((review) => review.owner === user.id);

    const handleViewCommentsButton = () => {
        setToggleComments(!toggleComments)
    }

    return (
        <WrapperDiv>
            <HeaderDiv>
                <HeaderLeftDiv>
                    <HeaderLeftDivA>
                        <ImageDiv style={{backgroundImage: `url(${user.profile_picture})`}}/>
                    </HeaderLeftDivA>
                    <HeaderLeftDivB>
                        <p className={'name'}>{user.first_name} {user.last_name.charAt(0)}.</p>
                        <p className={'ratings'}>{userReviewTotal.length} reviews in total</p>
                    </HeaderLeftDivB>
                    <HeaderLeftDivC>
                        <StarsDiv>
                        <StarRating rating={review.rating}/>
                        </StarsDiv>
                    </HeaderLeftDivC>
                </HeaderLeftDiv>
                <HeaderRightDiv>
                    <p>{review.date_created}</p>
                </HeaderRightDiv>
            </HeaderDiv>
            <BodyDiv>
                <CommentDiv>
                    <p className={'reviewTextProfile'}>{review.content}</p>
                </CommentDiv>

                {!toggleComments && <BottomDiv>
                    <BottomDivLeft>
                        <RevCommentLikeButton review={review}/>
                    </BottomDivLeft>
                    <BottomDivRight>
                        <ViewCommentsButton onClick={handleViewCommentsButton}>
                            <p>View all comments</p>
                        </ViewCommentsButton>
                    </BottomDivRight>
                </BottomDiv>}
                {toggleComments && <RestaurantReviewComments review={review} hide={handleViewCommentsButton}/>}
            </BodyDiv>
        </WrapperDiv>
    )
};


const mapStateToProps = (state) => {
    return {
        restaurants: state.fetch.restaurants.results,
        reviews: state.fetch.reviews.results,
        users: state.fetch.users.results,

    }
};

export default connect(mapStateToProps)(withRouter(RestaurantReviewArray))