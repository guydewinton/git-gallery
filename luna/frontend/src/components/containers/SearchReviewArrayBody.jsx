import React from 'react';
import styled from "styled-components";
import StarRater from "../elements/buttons/StarRater";
import {connect} from "react-redux";
import {withRouter} from "react-router";

const WrapperDiv = styled.div`
  //position: relative;
  
  width: 270rem;
  display: flex;
  flex-direction: column;

`;

const BodyDiv = styled.div`
padding: 11rem 11rem;
//height: 112rem;



p {
  font-size: 14rem;
  font-weight: bold;
  color: #4c4c4c;
   display: -webkit-box;
    -webkit-line-clamp: 5;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
}
`

const SearchReviewArrayBody = (props) => {

    return (
        <WrapperDiv>
            <BodyDiv>
                <p>{props.review.content}</p>
            </BodyDiv>
        </WrapperDiv>

    )

};

const mapStateToProps = (state) => {
    return {
        users: state.fetch.users.results,
        reviews: state.fetch.reviews.results

    }
};

export default connect(mapStateToProps)(SearchReviewArrayBody)
