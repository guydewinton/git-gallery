import React from 'react';
import styled from "styled-components";
import StarRater from "../elements/buttons/StarRater";
import {connect} from "react-redux";
import {withRouter} from "react-router";

const WrapperDiv = styled.div`
  //position: relative;
  
  width: 270rem;
  display: flex;
  flex-direction: column;
  cursor: pointer;

`;

const HeaderDiv = styled.div`
height: 75rem;
display: flex;
flex-direction: row;
justify-content: flex-start;
`;

const ImgDiv = styled.div`
height: 68rem;
width: 66rem;
background-image: url("${props => props.image}");
background-size: cover;
background-position: center;
`;

const TitleDiv = styled.div`
padding-left: 7rem;
display: flex;
flex-direction: column;
justify-content: center;

h3 {
  font-size: 20rem;
  font-weight: bold;
  color: #e47d31;
  margin-bottom: 4rem;
}

p {
  font-size: 14rem;
  font-weight: bold;
  color: #4c4c4c;
}
`;

const SearchReviewUserArrayHeader = (props) => {

    console.log('SearchReviewUserArrayHeader', props.user)

    const user = props.user
    const reviews = props.reviews.filter((review) => review.owner === user.id)

    const goToUser = () => {

        props.dispatch({type: 'NAV_USER_PROFILE', payload:{user: user.id}})
        props.history.push('/profile')

    }

    return (
        <WrapperDiv onClick={goToUser}>
            <HeaderDiv>
                <ImgDiv image={user.profile_picture}/>
                <TitleDiv>
                    <h3>{user.first_name}. {user.last_name.charAt(0).toUpperCase()}</h3>
                    <p>{reviews.length} Reviews in total</p>
                </TitleDiv>
            </HeaderDiv>
        </WrapperDiv>

    )

};

const mapStateToProps = (state) => {
    return {
        users: state.fetch.users.results,
        reviews: state.fetch.reviews.results

    }
};

export default connect(mapStateToProps)(withRouter(SearchReviewUserArrayHeader))
