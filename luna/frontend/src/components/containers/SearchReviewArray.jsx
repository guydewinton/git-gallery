import React from 'react';
import styled from "styled-components";
import StarRater from "../elements/buttons/StarRater";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import SearchUserArrayBody from "./SearchUserArrayBody";
import SearchReviewArrayBody from "./SearchReviewArrayBody";
import SearchReviewUserArrayHeader from "./SearchReviewUserArrayHeader";
import RevCommentLikeButton from "../elements/buttons/RevCommentLikeButton";
import SearchReviewComments from "./SearchReviewComments";

const WrapperDiv = styled.div`
  //position: relative;
  
  width: 270rem;
  display: flex;
  flex-direction: column;
  outline: solid 1rem #ebebeb;
  border-top: 8rem solid ${props => props.theme.colors.primaryOrange};
  margin: 0 15rem 30rem 15rem;

  //border: solid 1rem #ebebeb;
  background-color: #ffffff;
`;

const ButtonDiv = styled.div`
display: flex;
flex-direction: row;
justify-content: center;
`


const SearchReviewArray = (props) => {

    const review = props.review

    const user = props.users.filter((user) => user.id === review.owner)[0];

    return (
        <WrapperDiv>
            <SearchReviewUserArrayHeader user={user}/>
            <SearchReviewArrayBody review={review}/>
            <ButtonDiv>
            <RevCommentLikeButton  review={review}/>
            </ButtonDiv>
            <SearchReviewComments review={review}/>
        </WrapperDiv>

    )

};

const mapStateToProps = (state) => {
    return {
        users: state.fetch.users.results,
        reviews: state.fetch.reviews.results

    }
};

export default connect(mapStateToProps)(SearchReviewArray)
