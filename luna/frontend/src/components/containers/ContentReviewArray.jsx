import React from 'react';
import styled from "styled-components";
import StarRater from "../elements/buttons/StarRater";
import {connect} from "react-redux";
import {withRouter} from "react-router";

const WrapperDiv = styled.div`
  //position: relative;
  height: 410rem;
  width: 270rem;
  display: flex;
  flex-direction: column;
  outline: solid 1rem #ebebeb;
  cursor: pointer;


  border-top: 8rem solid ${props => props.theme.colors.primaryOrange};
  margin: 0 15rem 30rem 15rem;
  //border: solid 1px #ebebeb;
  background-color: #ffffff;
`;