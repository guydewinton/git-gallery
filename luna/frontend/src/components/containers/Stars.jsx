import React from 'react';
import styled from "styled-components";
import StarRater from "../elements/buttons/StarRater";

const StarsDiv = styled.div`
  position: relative;
  width: 240rem;
  height: 25rem;
  margin: 12rem 12rem 21rem 0;
    p {
    position: absolute;
    right: 0;
    bottom: 1.3rem;
  font-size: 20rem;
  font-weight: 300;
  color: ${props => props.color}
  }
`;

const RatingStars = styled.div`
  width: 125rem;
  height: 25rem;
  position: absolute;
    display: flex;
  flex-direction: row;
  

  //background-color: green;
  z-index: 0;

`;

const ContentRestaurantArray = (props) => {

    return (
            <StarsDiv color={props.color}>
                <RatingStars>
                    <StarRater rating={props.rating}/>
                </RatingStars>
                <p>{props.counter} reviews</p>
            </StarsDiv>

    )
}


export default ContentRestaurantArray