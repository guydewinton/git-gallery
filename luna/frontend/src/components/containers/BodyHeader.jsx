import React from 'react';
import styled from "styled-components";

import NavLunaButton from "../elements/buttons/NavLunaButton";
import FootFacebookButton from "../elements/buttons/FootFacebookButton";
import NavNavButton from "../elements/buttons/NavNavButton";
import GenSmallButton from "../elements/buttons/GenSmallButton";
import NavAuthButton from "../elements/buttons/NavAuthButton";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

const HeaderDiv = styled.div`
  height: 71em;
  width: 100vw;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 30em;
  background-color: white;
  z-index: 9999;
`;

const ButtonsDiv = styled.div`
display: flex;
flex-direction: row;
align-items: center;
`;

const NavButtonsDiv = styled.div`
display: flex;
flex-direction: row;
`;

const AuthButtonsDiv = styled.div`
height: 40em;
display: flex;
flex-direction: row;
`;


const BodyHeader = (props) => {
    console.log(props.ui)
    return (
        <>
            <HeaderDiv>
                <NavLunaButton />
                <ButtonsDiv>
                    <NavButtonsDiv>
                        <NavNavButton text={'Home'} active={props.ui === 'HOME'} link={'/'}/>
                        <NavNavButton text={'Search'} active={props.ui === 'SEARCH'} link={'/search/restaurants'}/>
                        <NavNavButton text={'Profile'} active={props.ui === 'PROFILE'} link={'/profile'}/>
                    </NavButtonsDiv>
                    <AuthButtonsDiv>
                        <NavAuthButton/>
                    </AuthButtonsDiv>
                </ButtonsDiv>
            </HeaderDiv>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        ui: state.ui.ui.primary
    }
};

export default connect(mapStateToProps)(BodyHeader)