import React from 'react';
import {connect} from "react-redux";
import styled from "styled-components";
import SearchBanner from "../elements/text/SearchBanner";
import TextInputField from "../elements/input/TextInputField";


const WrapperDiv = styled.div`
  width: 100vw;
    display: flex;
  flex-direction: column;
  align-items: center;

`;

const SearchDiv = styled.div`
  width: 100vw;
  height: 50rem;
  background-color: #ffffff;
  border-top: solid 1rem #d8d8d8;
  border-bottom: solid 1rem #ebebeb;
`;

const BrowserDiv = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 19rem;
  border-bottom: solid 1rem #d8d8d8;
      margin-bottom: 57rem;
  
`;

const SearchField = styled(TextInputField)`
width: 1149rem;
position: relative;
top: -11rem;
height: 48rem;
border-radius: 0;
outline:none;
border: none;
background: none;
`;

const SearchHeader = (props) => {

    console.log('header', props)

    return(
        <WrapperDiv>
            <SearchDiv>
                <SearchField placeholder={'Search...'}/>
            </SearchDiv>
            <BrowserDiv>
                <SearchBanner text={'RESTAURANTS'} link={'/search/restaurants'} active={props.ui === 'RESTAURANTS'}/>
                <SearchBanner text={'REVIEWS'} link={'/search/reviews'} active={props.ui === 'REVIEWS'}/>
                <SearchBanner text={'USERS'} link={'/search/users'} active={props.ui === 'USERS'}/>
            </BrowserDiv>
        </WrapperDiv>
            )
};

const mapStateToProps = (state) => {

    return {
        restaurants: state.fetch.restaurants.results,
        ui: state.ui.ui.secondary
    }
};

export default connect(mapStateToProps)(SearchHeader);