import React from 'react';
import styled from "styled-components";
import StarRater from "../elements/buttons/StarRater";
import {connect} from "react-redux";
import {withRouter} from "react-router";

const WrapperDiv = styled.div`
  //position: relative;
  height: 410rem;
  width: 270rem;
  display: flex;
  flex-direction: column;
  outline: solid 1rem #ebebeb;
  cursor: pointer;


  border-top: 8rem solid ${props => props.theme.colors.primaryOrange};
  margin: 0 15rem 30rem 15rem;
  //border: solid 1px #ebebeb;
  background-color: #ffffff;
`;

const TitleDiv = styled.div`
  margin: 11rem 0 3rem 13rem;
  .name {
    font-size: 20rem;
  }
`

const LocationDiv = styled.div`
  margin: 5rem 0 0 13rem;
    .location {
    font-size: 18rem;
  }
`;

const ImageDiv = styled.div`
  height: 284rem;
  width: 270rem;
      background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
`;

const StarsDiv = styled.div`
  position: relative;
  width: 243rem;
  height: 25rem;
  margin: 12rem 12rem 21rem 10rem;
    p {
    position: absolute;
    right: 0;
    bottom: 1.3rem;
  font-size: 16rem;
  font-weight: 300;
  color: #4c4c4c;
  }
`;

const RatingStars = styled.div`
  width: 125rem;
  height: 25rem;
  position: absolute;
    display: flex;
  flex-direction: row;

  //background-color: green;
  z-index: 0;

`;

const ContentRestaurantArray = (props) => {

    const restaurant = props.restaurants.filter((restaurant) => restaurant.id === props.rest_id)[0]

    const clickHandler = () => {
        props.dispatch({type: 'UI_NAV', payload: {primary: 'SEARCH', secondary: 'RESTAURANTS'}});
        props.dispatch({type: 'UI_RESTAURANT', payload: props.rest_id});
        props.history.push('/restaurant')
    };

    console.log('rest_array_div', restaurant, props.rest_id)

    return (
        <WrapperDiv onClick={clickHandler}>
            <TitleDiv>
                <p className={'name'}>{restaurant.name}</p>
            </TitleDiv>
            <LocationDiv>
                <p className={'location'}>{restaurant.street + ', ' + restaurant.city}</p>
            </LocationDiv>
            <StarsDiv>
                <RatingStars>
                    <StarRater rating={restaurant.average_rating}/>
                </RatingStars>
                <p>{restaurant.review_counter}</p>
            </StarsDiv>
            <ImageDiv style={{backgroundImage: `url(${restaurant.image})`}}/>
        </WrapperDiv>
    )
}


const mapStateToProps = (state) => {
    return {
        restaurants: state.fetch.restaurants.results
    }
};

export default connect(mapStateToProps)(withRouter(ContentRestaurantArray))