import React, {useEffect} from 'react';
import styled from "styled-components";
import BodyContent from "./divs/BodyContent";
import BodyHeader from "../containers/BodyHeader";

const DOMDiv = styled.div`
  height: 100%;
  width: 100%;
`;

const HeaderDiv = styled.div`
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    z-index:9999;
    
`;

const BodyDiv = styled.div`
    height: 100%;
    width: 100%;
    padding-top: 71rem;
    display: flex;
    justify-content: stretch;
    flex-direction: row;
    overflow: hidden;
`;

export default (props) => {

    return (
        <>
            <DOMDiv>

                <HeaderDiv>
                    <BodyHeader/>
                </HeaderDiv>
                <BodyDiv>
                    <BodyContent>
                        {props.children}
                    </BodyContent>
                </BodyDiv>
            </DOMDiv>
        </>
    )
}