import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import styled from "styled-components";

import star from '../../../assets/svgs/star.svg'
import comment from '../../../assets/svgs/comment.svg'
import restaurant from '../../../assets/svgs/restaurant.svg'
import edit from '../../../assets/svgs/edit.svg'
import Star from "../../elements/gidgets/Star";
import Reviews from "../../elements/gidgets/Reviews";
import Comments from "../../elements/gidgets/Comments";
import Restaurant from "../../elements/gidgets/Restaurants";
import Edit from "../../elements/gidgets/Edit";
import UserProfileComments from "./UserProfileComments";
import UserProfileRestaurants from "./UserProfileRestaurants";
import UserProfileReviews from "./UserProfileReviews";
import UserProfileEdit from "./UserProfileEdit";

const WrapperDiv = styled.div`
width: 100vw;
display: flex;
flex-direction: column;
align-items: center;
`;

const HeaderDiv = styled.div`
width: 100vw;
position: relative;
`;

const HeaderImageDiv = styled.div`
width: 100vw;
height: 152rem;
position: absolute;
background-image: url("${'https://www.chinadiscovery.com/assets/images/travel-guide/yangshuo/yangshuo-travel-guide-1920.jpg'}");
background-size: cover;
background-position: center;
z-index: 3;
`;

const HeaderOverlayDiv = styled.div`
width: 100vw;
height: 152rem;
position: absolute;
background-color: rgba(0, 0, 0, 0.5);
z-index: 5;
`;

const HeaderInfoDiv = styled.div`
position: relative;
top: -2rem;
height: 108rem;
width: 300rem;
z-index: 10;
padding-left: 16rem;


h3 {
  font-size: 24rem;
  font-weight: bold;
  color: #ffffff;
  margin-bottom: 7rem;
}

p {
  font-size: 18rem;
  font-weight: 300;
  color: #ffffff;
  margin-bottom: 3rem;
}
`;

const BodyDiv = styled.div`
width: 1180rem;
margin-top: 35rem;
display: flex;
flex-direction: row;
padding-top: 9rem;
`;

const ContentDiv = styled.div`
width: 1072rem;
display: flex;
flex-direction: column;
`;

const ContentMain = styled.div`
width: 1072rem;
display: flex;
flex-direction: row;
`;


const ContentMainLeft = styled.div`
width: 619rem;
min-height: 50vh;
`;

const ContentMainRight = styled.div`
width: 329rem;
min-height: 50vh;
padding-top: 24rem;
padding-left: 15rem
`;

const ProfileMenuWrapperDiv = styled.div`
width: 232rem;
`;

const ProfileMenuDiv = styled.div`
width: 232rem;
display: flex;
flex-direction: column;
border-bottom: solid 1rem #979797;
`;

const ProfileMenuImageDiv = styled.div`
width: 232rem;
height: 234rem;
background-image: url("${props => props.image}");
background-size: cover;
background-position: center;
z-index: 10;
`;

const ProfileMenuNameDiv = styled.div`
width: 232rem;
height: 70rem;
display: flex;
flex-direction: column;
justify-content: center;
p {
  font-size: 18rem;
  font-weight: bold;
  color: #4c4c4c;
}

`;

const ProfileMenuItemsDiv = styled.div`
width: 232rem;
height: 46rem;
position: relative;
display: flex;
flex-direction: row;
border-top: solid 1rem #979797;
cursor: pointer;
 .selectorDiv {
 position: absolute;

 height: 46rem;
 width: 5rem;
 }
 :hover {
 background-color: rgba(0, 0, 0, 0.08);
 
 .selectorDiv {
  background-color: #e47d31;
 }
 }
 ${props => props.active && `
 background-color: rgba(0, 0, 0, 0.08);
 
 .selectorDiv {
  background-color: #e47d31;
 }
 
 `}
`;

const ProfileMenuIconDiv = styled.div`
width: 58rem;
height: 46rem;
display: flex;
justify-content: center;
align-items: center;
padding-bottom: 1rem;
`;

const ProfileMenuTextDiv = styled.div`
width: 174rem;
height: 46rem;
padding-top: 2rem;
display: flex;
flex-direction: row;
justify-content: flex-start;
align-items: center;

p {
  height: 22rem;
  font-size: 18rem;
  font-weight: 300;
  color: #303030;
}
`;

const ProfileContentDiv = styled.div`
//component
`;

const ProfileAboutWrapperDiv = styled.div`

`;

const ProfileAboutDiv = styled.div`
h2 {
  font-size: 20rem;
  font-weight: bold;
  color: #303030;
  margin-bottom: 26rem;
}

h3 {
  font-size: 20rem;
  font-weight: bold;
  color: #000000;
  margin-bottom: 6rem;
}

p {
  font-size: 20rem;
  font-weight: 300;
  color: #000000;
  margin-bottom: 24rem;
}
`;

const ProfileAboutText = styled.div`
//component
`;


const UserProfile = (props) => {

    const [ui, setUi] = useState('REVIEWS')

    useEffect(() => {
        return (() => props.dispatch({type: 'NAV_USER_PROFILE', payload: null}))
    })

    // const user = props.users.filter((user) => user.id === props.session.user.id);

    let user = ''

    if (props.user) {
        user = props.users.filter((user) => user.id === props.user.user)[0];
        console.log('hahaha', user, props.user)
    } else {
        user = props.users[0]
        console.log('lalala', user)
    }



    const uiStateHandler = (uiState) => {
        setUi(uiState)
    }

    const reviews = props.reviews.filter((review) => review.user === user.id);

    const comments = props.reviews.filter((comment) => comment.user === user.id);

    props.dispatch({type: 'UI_NAV', payload: {primary: 'PROFILE', secondary: null}});
    return(
        <WrapperDiv>
            <HeaderDiv>
                <HeaderImageDiv/>
                <HeaderOverlayDiv/>
            </HeaderDiv>
            <BodyDiv>
                <ProfileMenuWrapperDiv>
                    <ProfileMenuDiv>
                        <ProfileMenuImageDiv image={user.profile_picture}/>
                        <ProfileMenuNameDiv>
                            <p>{user.first_name} {user.last_name}</p>
                        </ProfileMenuNameDiv>
                        <ProfileMenuItemsDiv onClick={() => uiStateHandler('REVIEWS')} active={ui === 'REVIEWS'}>
                            <div className={'selectorDiv'}/>
                            <ProfileMenuIconDiv>
                                <Reviews/>
                            </ProfileMenuIconDiv>
                            <ProfileMenuTextDiv>
                                <p>Reviews</p>
                            </ProfileMenuTextDiv>
                        </ProfileMenuItemsDiv>
                        <ProfileMenuItemsDiv onClick={() => uiStateHandler('COMMENTS')} active={ui === 'COMMENTS'}>
                            <div className={'selectorDiv'}/>
                            <ProfileMenuIconDiv>
                                <Comments/>
                            </ProfileMenuIconDiv>
                            <ProfileMenuTextDiv>
                                <p>Comments</p>
                            </ProfileMenuTextDiv>
                        </ProfileMenuItemsDiv>
                        <ProfileMenuItemsDiv onClick={() => uiStateHandler('RESTAURANTS')} active={ui === 'RESTAURANTS'}>
                            <div className={'selectorDiv'}/>
                            <ProfileMenuIconDiv>
                                <Restaurant/>
                            </ProfileMenuIconDiv>
                            <ProfileMenuTextDiv>
                                <p>Restaurants</p>
                            </ProfileMenuTextDiv>
                        </ProfileMenuItemsDiv>
                        <ProfileMenuItemsDiv onClick={() => uiStateHandler('EDIT')} active={ui === 'EDIT'}>
                            <div className={'selectorDiv'}/>
                            <ProfileMenuIconDiv>
                                <Edit/>
                            </ProfileMenuIconDiv>
                            <ProfileMenuTextDiv>
                                <p>Edit Profile</p>
                            </ProfileMenuTextDiv>
                        </ProfileMenuItemsDiv>
                    </ProfileMenuDiv>
                </ProfileMenuWrapperDiv>
                <ContentDiv>
                    <HeaderInfoDiv>
                        <h3>{user.first_name} {user.last_name}</h3>
                        <p>{user.location}</p>
                        <p>{reviews.length} reviews</p>
                        <p>{comments.length} comments</p>
                    </HeaderInfoDiv>
                    <ContentMain>
                        <ContentMainLeft>
                            {ui === 'COMMENTS' && <UserProfileComments user={user}/>}
                            {ui === 'REVIEWS' && <UserProfileReviews user={user}/>}
                            {ui === 'RESTAURANTS' && <UserProfileRestaurants user={user}/>}
                            {ui === 'EDIT' && <UserProfileEdit user={user}/>}
                        </ContentMainLeft>
                        <ContentMainRight>
                            <ProfileAboutWrapperDiv>
                                <ProfileAboutDiv>
                                    {console.log('userProfile', user)}
                                    <h2>ABOUT {user.first_name.toUpperCase()}</h2>
                                    <h3>Location</h3>
                                    <p>{user.location}</p>
                                    <h3>Luna member since</h3>
                                    <p>{user.date_joined}</p>
                                    <h3>Things I love</h3>
                                    <p>{user.things_i_love}</p>
                                    <h3>Description</h3>
                                    <p>{user.description}</p>
                                </ProfileAboutDiv>
                            </ProfileAboutWrapperDiv>
                        </ContentMainRight>
                    </ContentMain>
                </ContentDiv>
            </BodyDiv>
        </WrapperDiv>
    )
}

const mapStateToProps = (state) => {
    return {
        users: state.fetch.users.results,
        reviews: state.fetch.reviews.results,
        comments: state.fetch.comments.results,
        user: state.ui.user

    }
};

export default connect(mapStateToProps)(UserProfile)