import React from 'react';
import styled from "styled-components";
import {connect} from 'react-redux'

import GenBigButton from "../../elements/buttons/GenBigButton";
import TextInputField from "../../elements/input/TextInputField";

import food from '../../../assets/img/food.png'
import ContentBanner from "../../elements/text/ContentBanner";
import ContentRestaurantArray from "../../containers/ContentRestaurantArray";
import store from "../../../store/store";


const WrapperDiv = styled.div`
    width: 100%;
    padding: 0 0;
`;

const HeaderDiv = styled.div`
    width: 100%;
    height: 350rem;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-image: url(${food});
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
`;

const SearchDiv = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

const SearchField = styled(TextInputField)`
    height: 55rem;
    margin-right: 25rem;
`;

const ContentDiv = styled.div`
    width: 100%;
    margin: 29rem 0;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

const ArrayDiv = styled.div`
    width: 100%;
    margin: 0 0;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    p {
    font-size: 16rem;
    }
`

const Home = (props) => {

    props.dispatch({type: 'UI_NAV', payload: {primary: 'HOME', secondary: null}});

    const restaurantsAll = props.restaurants.sort((a, b)=> b.average_rating - a.average_rating)

    const restaurantsReduced = restaurantsAll.slice(0, 4);

    console.log(restaurantsReduced);

    window.scrollTo(0, 0)

    return (
        <WrapperDiv>
            <HeaderDiv>
                <SearchDiv>
                    <SearchField placeholder={'Search...'}/>
                    <GenBigButton text={'Search'}/>
                </SearchDiv>
            </HeaderDiv>
            <ContentDiv>
                <ContentBanner text={'BEST RATED RESTAURANTS'}/>
                <ArrayDiv>
                    {restaurantsReduced.map((restaurant, i) => {
                        console.log("Differentdophinhey", props);
                        return <ContentRestaurantArray
                            rest_id={restaurant.id}
                            key={'restaurant_' + i}
                            />;
                    })}
                </ArrayDiv>
            </ContentDiv>
        </WrapperDiv>
    )

}

const mapStateToProps = (state) => {
    return {
        restaurants: state.fetch.restaurants.results
    }
};

export default connect(mapStateToProps)(Home)