import React, {useEffect} from 'react';
import styled from "styled-components";
import {connect} from "react-redux";
import RestaurantProfilePage from "../../containers/RestaurantProfilePage";
import {withRouter} from "react-router";

const WrapperDiv = styled.div`
  width: 100vw;
`;

const RestaurantProfile = (props) => {

    console.log('restypesty',props.rest_id)
    // if (props.rest_id===null) {props.history.push('/')}


    useEffect(() => {
        // if (props.rest_id===null) {props.history.push('/')}
        return () => {
            props.dispatch({type: 'UI_RESTAURANT', payload: null});
        }
    }, []);

    return(
        <WrapperDiv>
            <RestaurantProfilePage rest_id={props.rest_id}/>
        </WrapperDiv>
)
}

const mapStateToProps = (state) => {
    return {
        restaurants: state.fetch.restaurants.results,
        rest_id: state.ui.restaurant
    }
};

export default connect(mapStateToProps)(withRouter(RestaurantProfile))