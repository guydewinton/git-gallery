import React from 'react';
import {connect} from "react-redux";
import styled from "styled-components";
import ContentRestaurantArray from "../../containers/ContentRestaurantArray";
import ReviewUserArrayHeader from "../../containers/SearchReviewUserArrayHeader";
import SearchReviewArray from "../../containers/SearchReviewArray";
import ResponsiveColumns from "../../elements/divs/ResponsiveColumns";
import ResponsiveContentBox from "../../elements/divs/ResponsiveContentBox";

const WrapperDiv = styled.div`
  width: 100vw;
    display: flex;
  flex-direction: column;
  align-items: center;
`;


const ContentDiv = styled.div`
  width: 1200rem;
  display: flex;
  flex-direction: row;
      flex-wrap: wrap;
  justify-content: space-between;
  margin-bottom: 30rem;
`;

const SearchReviews = (props) => {

    props.dispatch({type: 'UI_NAV', payload: {primary: 'SEARCH', secondary: 'REVIEWS'}});
    //
    // console.log('in da search restaurants', props.restaurants)
    //
    // const restaurants = props.restaurants.sort((a, b)=> b.average_rating - a.average_rating);
    // //
    // const goToTop = () => {
    //     window.scrollTo(0, 0)
    //     {console.log(window)}
    // }

    const reviewsArray = props.reviews.map((review, i) => {
        return <SearchReviewArray
            review={review}
            // title={restaurant.name}
            // city={restaurant.address}
            // street={restaurant.address}
            // image={restaurant.image}
            // average_rating={restaurant.rating}
            // review_counter={restaurant.reviews}
            key={'restaurant_' + i}
        />
    })


    return(
        <>
            <WrapperDiv>
                <ContentDiv>

                    <ResponsiveColumns contentArray={reviewsArray}/>


                </ContentDiv>
            </WrapperDiv>
        </>
    )
};

const mapStateToProps = (state) => {
    return {
        reviews: state.fetch.reviews.results,

    }
};

export default connect(mapStateToProps)(SearchReviews)