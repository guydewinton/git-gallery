import React from 'react';
import {connect} from "react-redux";
import styled from "styled-components";
import ContentBanner from "../../elements/text/ContentBanner";
import ContentRestaurantArray from "../../containers/ContentRestaurantArray";
import SearchHeader from "../../containers/SearchHeader";
import {Route} from "react-router";
import SearchRestaurants from "./SearchRestaurants";
import SearchReviews from "./SearchReviews";
import SearchUsers from "./SearchUsers";
import SwitchBoard from "../../../SwitchBoard";
import RestaurantProfile from "./RestaurantProfile";
import DOM from "../DOM";

const WrapperDiv = styled.div`
  width: 100vw;
    display: flex;
  flex-direction: column;
  align-items: center;
`;


const Search = (props) => {

    return(
        <>
            <SearchHeader/>
            <WrapperDiv>

                <Route exact path={'/search/restaurants'} component={SearchRestaurants}/>
                {/*<Route exact path={'/search/reviews'} component={SearchReviews}/>*/}
                <Route exact path={'/search/users'} component={SearchUsers}/>
                <Route
                    exact path={'/search/reviews'}
                    render={(props) => <SwitchBoard {...props} component={SearchReviews} switchType={'REVIEWS'} />}
                />
            </WrapperDiv>
        </>
    )
};

const mapStateToProps = (state) => {
    return {
        restaurants: state.fetch.restaurants.results
    }
};

export default connect(mapStateToProps)(Search)