import React from 'react';
import styled from 'styled-components'
import {connect} from "react-redux";

const WrapperDiv = styled.div`
width: 619rem;
background-color: #ffffff;
margin-bottom: 30rem;

`;

const BannerDiv = styled.div`
border-bottom: solid 1px #ebebeb;
padding-left: 15rem;
h2 {
  font-size: 20px;
  font-weight: bold;
  color: #303030;
padding-top: 25rem;
margin-bottom: 13rem;
}
`;

const CommentWrapper = styled.div`
padding: 0 15rem;
`;

const CommentDiv = styled.div`
min-height: 80rem;
display: flex;
flex-direction: column;
justify-content: center;
border-bottom: solid 1px #ebebeb;

h5 {
width: 480rem;
  font-size: 20px;
  color: #4c4c4c;
    font-weight: normal;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin-bottom: 7rem;
}

p {
  font-size: 16px;
  font-weight: 300;
  color: #303030;
}
`

const UserProfileComments = (props) => {

    const userComments = props.comments.filter((comment) => comment.user === props.user.id);

    const comments = userComments.map((comment) => {

        const review = props.reviews.filter((review) => comment.review === review.id)[0];

        console.log(review, props.reviews)

        return (
            <CommentWrapper>
                <CommentDiv>
                    <h5>{review.content}lala</h5>
                    <p>{comment.content}</p>
                </CommentDiv>
            </CommentWrapper>
        )
    })

    console.log('UserProfileComments', comments, userComments, props.user)

    return(
        <WrapperDiv>
            <BannerDiv>
                <h2>COMMENTS</h2>
            </BannerDiv>
            {comments.map((comment) => {
                return comment
            })}
        </WrapperDiv>
)
}

const mapStateToProps = (state) => {
    return {
        comments: state.fetch.comments.results,
        reviews: state.fetch.reviews.results
    }
};

export default connect(mapStateToProps)(UserProfileComments)