import React, {useState} from 'react';
import styled from "styled-components";
import GenBigButton from "../../elements/buttons/GenBigButton";

import InputField from "../../elements/input/TextInputField";
import AuthBanner from "../../elements/text/AuthBanner";
import AuthRegTextBlock from "../../elements/text/AuthRegTextBlock";
import {makeFetch} from "../../../store/actions/fetchActions";

const WrapperDiv = styled.div`
    width: 100vw;
    padding: 49rem 0;
      border-top: 1rem solid #d8d8d8;

`

const LoginDiv = styled.div`
  //height: 365rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const InputDiv = styled.div`
    display: grid;
    grid-template-columns: 370rem 370rem;
    grid-template-rows: 67rem 67rem 67rem 67rem;
    margin-bottom: 40rem;
    
    .email {
      grid-column: 1 / span 1;
      grid-row: 1 / span 1;
    }
    
    .verification {
      grid-column: 2 / span 1;
      grid-row: 1 / span 1;
    }
    
    .username {
      grid-column: 1 / span 1;
      grid-row: 2 / span 1;
    }
    
    .location {
      grid-column: 2 / span 1;
      grid-row: 2 / span 1;
    }
    
    .first {
      grid-column: 1 / span 1;
      grid-row: 3 / span 1;
    }
    
    .last {
      grid-column: 2 / span 1;
      grid-row: 3 / span 1;
    }
    
    .password {
      grid-column: 1 / span 1;
      grid-row: 4 / span 1;
    }
    
    .password_confirm {
      grid-column: 2 / span 1;
      grid-row: 4 / span 1;
    }
`;

const InputFieldDiv = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  
`;




export default (props) => {



    const [emailField, setEmailField] = useState('');
    const [codeField, setCodeField] = useState('');
    const [userNameField, setUserNameField] = useState('');
    const [locationField, setLocationField] = useState('');
    const [firstNameField, setFirstNameField] = useState('');
    const [lastNameField, setLastNameField] = useState('');
    const [passwordField, setPasswordField] = useState('');
    const [confirmPasswordField, setConfirmPasswordField] = useState('');


    const emailHandler = (event) => {
        setEmailField(event.target.value);
        console.log(emailField);
    }

    const codeHandler = (event) => {
        setCodeField(event.target.value)
    }

    const userNameHandler = (event) => {
        setUserNameField(event.target.value);
    }
    const locationHandler = (event) => {
        setLocationField(event.target.value);
    }
    
    const firstNameHandler = (event) => {
        setFirstNameField(event.target.value);
    }

    const lastNameHandler = (event) => {
        setLastNameField(event.target.value);
    }

    const passwordHandler = (event) => {
        setPasswordField(event.target.value);
    }
    
    const confirmPasswordHandler = (event) => {
        setConfirmPasswordField(event.target.value);
    }
    
    const handleSubmit = (event) => {
        if(passwordField !== confirmPasswordField){
            window.alert("Hey man, they dont match!")
        } else  {
            makeFetch({
                type: 'AUTH_VALIDATE',
                payload: { code: codeField, email: emailField, username: userNameField, location: locationField, first_name: firstNameField, last_name: lastNameField, password: passwordField, password_repeat:confirmPasswordField}
            });
            props.history.push('/')
        }
    }
    
    ;

    return (
        <WrapperDiv>
            <LoginDiv>
                <AuthBanner text={'REGISTRATION'}/>
                <InputDiv>
                    <InputFieldDiv className={'email'}>
                        <InputField placeholder={'E-Mail Address'} onChange={emailHandler} type={'email'}/>
                    </InputFieldDiv>
                    <InputFieldDiv className={'verification'}>
                        <InputField placeholder={'Verification'} onChange={codeHandler} type={'text'}/>
                    </InputFieldDiv>
                    <InputFieldDiv className={'username'}>
                        <InputField placeholder={'Username'} onChange={userNameHandler} type={'text'}/>
                    </InputFieldDiv>
                    <InputFieldDiv className={'location'}>
                        <InputField placeholder={'Location'} onChange={locationHandler} type={'text'}/>
                    </InputFieldDiv>
                    <InputFieldDiv className={'first'}>
                        <InputField placeholder={'First Name'} onChange={firstNameHandler} type={'text'}/>
                    </InputFieldDiv>
                    <InputFieldDiv className={'last'}>
                        <InputField placeholder={'Last Name'} onChange={lastNameHandler} type={'text'}/>
                    </InputFieldDiv>
                    <InputFieldDiv className={'password'}>
                        <InputField placeholder={'Password'} onChange={passwordHandler} type={'password'}/>
                    </InputFieldDiv>
                    <InputFieldDiv className={'password_confirm'}>
                        <InputField placeholder={'Confirm Password'} onChange={confirmPasswordHandler} type={'password'}/>
                    </InputFieldDiv>
                </InputDiv>
                <GenBigButton onClick={()=> handleSubmit()} text={'Finish registration'}/>
            </LoginDiv>
        </WrapperDiv>
    )

}