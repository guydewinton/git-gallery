import React from 'react';
import styled from "styled-components";

import AuthBanner from "../../elements/text/AuthBanner";
import AuthRegTextBlock from "../../elements/text/AuthRegTextBlock";

const WrapperDiv = styled.div`
    width: 100vw;
    padding: 49rem 0;
  border-top: 1rem solid #d8d8d8;
`;

const LoginDiv = styled.div`
  height: 365rem;
  display: flex;

  flex-direction: column;
  align-items: center;
`;


export default (props) => {
    return (
        <WrapperDiv>
            <LoginDiv>
                <AuthBanner text={'REGISTRATION'}/>
                <AuthRegTextBlock
                    texthead={'Thanks for your registration.'}
                    textbody={'Our hard working monkeys are preparing a digital message called E-Mail that will be sent to you soon. Since monkeys arent good in writing the message could end up in you junk folder. Our apologies for any inconvienience.'}
                />
            </LoginDiv>
        </WrapperDiv>
    )

}