import React, {useState} from 'react';
import styled from "styled-components";
import GenBigButton from "../../elements/buttons/GenBigButton";

// import history from "../../../history";
import {makeFetch} from "../../../store/actions/fetchActions";

import InputField from "../../elements/input/TextInputField";
import AuthBanner from "../../elements/text/AuthBanner";
import {withRouter} from "react-router";
import {connect} from "react-redux";


const WrapperDiv = styled.div`
    width: 100vw;
    padding: 49rem 0;
border-top: 1rem solid #d8d8d8;

`;

const LoginDiv = styled.div`
  height: 365rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const InputDiv = styled.div`
display: flex;
flex-direction: column;
justify-content: flex-start;
align-items: center;
    margin-bottom: 40rem;
`;

const AuthSignIn = (props) => {

    const [emailField, setEmailField] = useState('');
    const [passwordField, setPasswordField] = useState('');

    const handleEmail = (event) => {
        setEmailField(event.target.value)
    };

    const handlePassword = (event) => {
        setPasswordField(event.target.value)
    };


    const handleSubmit = async (event) => {
        event.preventDefault();
        const status = makeFetch({type:'AUTH_SIGNIN', payload: {email: emailField, password: passwordField}});
        props.history.push('/')
    };

    return (
        <WrapperDiv>
            <LoginDiv>
            <AuthBanner text={'LOGIN'}/>
            <InputDiv>
                <InputField type='email' placeholder={'E-Mail'} onChange={handleEmail}/>
                <InputField type={'password'} placeholder={'Password'} onChange={handlePassword}/>
            </InputDiv>
                <GenBigButton text={'Login'} type={'submit'} onClick={handleSubmit}/>
            </LoginDiv>
        </WrapperDiv>
    )

}

export default connect()(withRouter(AuthSignIn))