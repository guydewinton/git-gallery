import React from 'react';
import styled from 'styled-components'
import EditProfileInput from "../../elements/input/EditProfileInput";
import EditProfileTextField from "../../elements/input/EditProfileTextField";
import GenBigButton from "../../elements/buttons/GenBigButton";

const WrapperDiv = styled.div`
//width: 100vw;
padding: 0 25rem 30rem 20rem;

`;

const HeaderDiv = styled.div`
display: flex;
flex-direction: column;
height: 62rem;
justify-content: center;
 h3 {
   font-size: 20rem;
  font-weight: bold;
  color: #303030;
 }
`

export default (props) => {
    return(
        <WrapperDiv>
            <HeaderDiv>
                <h3>EDIT USER PROFILE</h3>
            </HeaderDiv>
                <EditProfileInput text={'Username'}/>
                <EditProfileInput text={'First Name'}/>
                <EditProfileInput text={'Last Name'}/>
                <EditProfileInput text={'E-Mail'}/>
                <EditProfileInput text={'Location'}/>
                <EditProfileInput text={'Phone'}/>
            <EditProfileTextField text={'Things I love'}/>
            <EditProfileTextField text={'Description'}/>
            <GenBigButton text={'Submit'}/>


        </WrapperDiv>
)
}