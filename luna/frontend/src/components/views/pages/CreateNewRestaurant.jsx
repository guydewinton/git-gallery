// import React from 'react';

// export default (props) => {
//     return(
//         <div> Create New Restaurant</div>
// )
// }

import React, {useState} from 'react';
import styled from "styled-components";
import GenBigButton from "../../elements/buttons/GenBigButton";
import GenSmallButton from "../../elements/buttons/GenSmallButton";

import InputField from "../../elements/input/TextInputField";
import AuthBanner from "../../elements/text/AuthBanner";
import AuthRegTextBlock from "../../elements/text/AuthRegTextBlock";
import {makeFetch} from "../../../store/actions/fetchActions";
import arrowdown from '../../../assets/svgs/arrowdown.svg';
// import Button from 'react-bootstrap/But 




const WrapperDiv = styled.div`
    width: 100vw;
    padding: 49rem 0;
      border-top: 1rem solid #d8d8d8;
`

const LoginDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const InputDiv = styled.div`
    display: grid;
    grid-template-columns: 1110 rem ;
    grid-template-rows: 30rem 130rem 30rem 130rem 30rem 130rem 30rem 130rem 130rem;
    margin-bottom: 40rem;
`;

const InputFieldNew = styled(InputField) `
border: none;
width:250rem;
height: 50rem;
`
;




const Row1Div = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 5rem;

  display: grid;
  grid-template-columns: 370rem 370rem 370rem;
  grid-template-rows: rem ;
  margin-bottom: 40rem;
  
  .row1col1 {
   
    grid-row: 1 / span 1;
    display: flex;
    flex-direction: column;
  }
`;



const Row2Div = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

    display: grid;
    grid-template-columns: 370rem 370rem 370rem;
    grid-template-rows: ;
    margin-bottom: 40rem;
  

    .row2col1 {
        grid-column: 1 / span 1;
        grid-row: 2 / span 1;
        display: flex;
        flex-direction: column;
      }
      
      .row2col2 {
        grid-column: 2 / span 1;
        grid-row: 2 / span 1;
        display: flex;
        flex-direction: column;
        
      }
      
      .row2col3 {
        grid-column: 3 / span 1;
        grid-row: 2 / span 1;
        display: flex;
        flex-direction: column;
      }
`;

const Row3Div = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 25rem;

  display: grid;
  grid-template-columns: 370rem 370rem 370rem;
  grid-template-rows:rem ;
  margin-bottom: 40rem;
  
  .row3col1 {
    grid-column: 1 / span 1;
    grid-row: 3 / span 1;
    display: flex;
    flex-direction: column;
  }
`;

const Row4Div = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    display: grid;
    grid-template-columns: 370rem 370rem 370rem;
    grid-template-rows: rem ;
    margin-bottom: 40rem;
  
    .row4col1 {
        grid-column: 1 / span 1;
        grid-row: 4 / span 1;
        display: flex;
        flex-direction: column;
  
      }
      
      .row4col2 {
        grid-column: 2 / span 1;
        grid-row: 4 / span 1;
        display: flex;
        flex-direction: column;
      }
      
      .row4col3 {
        grid-column: 3 / span 1;
        grid-row: 4 / span 1;
        display: flex;
        flex-direction: column;
      }
`;

const Row5Div = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 25rem;

  display: grid;
  grid-template-columns: 370rem 370rem 370rem;
  grid-template-rows:rem ;
  margin-bottom: 40rem;
  
  .row3col1 {
    grid-column: 1 / span 1;
    grid-row: 5 / span 1;
    display: flex;
    flex-direction: column;
  }
`;

const Row6Div = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    display: grid;
    grid-template-columns: 370rem 370rem 370rem;
    grid-template-rows: rem ;


    .row6col1 {
        grid-column: 1 / span 1;
        grid-row: 6 / span 1;
        display: flex;
        flex-direction: column;
  
      }
      
      .row6col2 {
        grid-column: 2 / span 1;
        grid-row: 6 / span 1;
        display: flex;
        flex-direction: column;
      }
      
      .row6col3 {
        grid-column: 3 / span 1;
        grid-row: 6 / span 1;
        display: flex;
        flex-direction: column;
      }
`;


const Row7Div = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 25rem;

  display: grid;
  grid-template-columns: 370rem 370rem 370rem;
  grid-template-rows:rem ;
  margin-bottom: 40rem;
  
  .row3col1 {
    grid-column: 1 / span 1;
    grid-row: 7 / span 1;
    display: flex;
    flex-direction: column;
  }
`;

const Row8Div = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    display: grid;
    grid-template-columns: 370rem 370rem 370rem;
    grid-template-rows: rem ;
    margin-bottom: 40rem;
  
    .row8col1 {
        grid-column: 1 / span 1;
        grid-row: 8 / span 1;
        display: flex;
        flex-direction: column;
  
      }
      
      .row8col2 {
        grid-column: 2 / span 1;
        grid-row: 8 / span 1;
        display: flex;
        flex-direction: column;
      }
      
      .row8col3 {
        grid-column: 3 / span 1;
        grid-row: 8 / span 1;
        display: flex;
        flex-direction: column;
        height: 100%;
        justify-content: space-between;
      }
`;


const Row9Div = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  display: grid;
  grid-template-columns: 1110;
  grid-template-rows:rem ;
  margin-bottom: 40rem;
  
  .row3col1 {
    grid-column: 1 / span 1;
    grid-row: 9 / span 1;
    display: flex;
    flex-direction: column;
  }
`;

const SpaceDiv = styled.div`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
justify-content: space-between;
background-color: white;
padding: 0 0 0 1px;
width: 340rem;
height: 54rem;
border-radius: 3rem;
border: solid 1rem #ebebeb;
background-color: #ffffff;
margin: 10rem 0;
color: #979797;
`;



const InputFieldDiv = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border: none; 
  outline: none;
`;



const Label = styled.label`
    font-size: 20rem;
    width: 100%;
    padding: 0 0 0 23rem;
    color: #979797;
    font-weight: bold;
    font-family: Helvetica;

`;

const Alert = styled.p`
    font-size: 16rem;
    width: 100%;
    padding: 0 0 0 23rem;
    color: #b00000;
    
`;

const Title = styled.p`
    font-size: 20rem;
    width: 100%;
    color: #4c4c4c;
    font-weight: bold;
    font-family: Helvetica;
    padding: 0 0 0 23rem;
    
`;


const Filebutton = styled.div`
height: 100%;
width: 100%;
justify-content:flex-start;
display: flex;
align-items: center;
padding: 0 0 0 20rem;
`;

export default (props) => {



    const [nameField, setNameField] = useState('');
    const [categoryField, setCategoryField] = useState('American');
    const [countryField, setCountryField] = useState('');
    const [streetField, setStreetField] = useState('');
    const [cityField, setCityField] = useState('');
    const [zipField, setZipField] = useState('');
    const [websiteField, setWebsiteField] = useState('');
    const [phoneField, setPhoneField] = useState('');
    const [emailField, setEmailField] = useState('');
    const [openingField, setOpeningField] = useState('');
    const [priceField, setPriceField] = useState('');
    const searchNames = ['American', 'Asian', 'Chinese', 'Chocolatier', 'Continental', 'Fast Food','French', 'Greek', 'Italian', 'Mexican', 'Middel Eastern', 'Swiss', 'Vietnamese', 'Other'];


    const nameHandler = (event) => {
        setNameField(event.target.value);
        console.log(nameField);
    }

    const categoryHandler = (event) => {
        setCategoryField(event.target.value);


    }
    const countryHandler = (event) => {
        setCountryField(event.target.value);
    }
    
    const streetHandler = (event) => {
        setStreetField(event.target.value);
    }

    const cityHandler = (event) => {
        setCityField(event.target.value);
    }

    const zipHandler = (event) => {
        setZipField(event.target.value);
    }
    
    const websiteHandler = (event) => {
        setWebsiteField(event.target.value);
    }

    const phoneHandler = (event) => {
        setPhoneField(event.target.value);
    }

    const emailHandler = (event) => {
        setEmailField(event.target.value);
    }

    const openingHandler = (event) => {
        setOpeningField(event.target.value);
    }

    const priceHandler = (event) => {
        setPriceField(event.target.value);

        // (1, 'Low'),
        // (2, 'Medium'),
        // (3, 'High'),
    }
    
    const handleFile = (event) => {
        
    }

   

    const handleCreate = (event) => {
        makeFetch({
            type: 'CREATE_RESTAURANT',
            payload: { name: nameField, category: categoryField, country: countryField, street: streetField, city: cityField, zip: zipField, website: websiteField, phone: phoneField, email: emailField, opening: openingField, price: priceField}
        })
    }
    ;

    return (
        <WrapperDiv>
            <LoginDiv>
                <AuthBanner style={{width:'500rem'}} text={'CREATE NEW RESTAURANT'}/>
                
                
                <InputDiv>
                    <Row1Div className={'row1col1'}>
                            
                                <Title >Basic</Title>
                            
                    </Row1Div>
                        
                    <Row2Div>  
                            <InputFieldDiv className={'row2col1'}>
                                <Label>Name *</Label>
                                <InputField  onChange={nameHandler} type={'text'}/>
                                <Alert>This field is required</Alert>
                            </InputFieldDiv>

                            <InputFieldDiv className={'row2col2'}>
                                <Label>Category *</Label>
                                    <SpaceDiv>
                                        <InputFieldNew placeholder={'Select a value...'} onClick={categoryHandler} type={'text'}/>
                                        <img src={arrowdown} height= '20px' width='50px' />
                                            {/* <Dropdown>
                                                <Dropdown.Toggle variant="success" id="dropdown-basic">
                                                    Dropdown Button
                                                </Dropdown.Toggle>

                                                <Dropdown.Menu>
                                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown> */}
                                    </SpaceDiv>
                                <Alert>This field is required</Alert>
                            </InputFieldDiv>

                            <InputFieldDiv className={'row2col3'}>
                                    <Label>Country *</Label>
                                    <SpaceDiv>
                                        <InputFieldNew  
                                                        placeholder={'Select a value...'} 
                                                        onChange={countryHandler} type={'text'}/>
                                        <img src={arrowdown} height= '20px' width='50px'/>
                                    </SpaceDiv>
                                <Alert>.</Alert>
                            </InputFieldDiv>

                    </Row2Div>   

                    <Row3Div className={'row3col1'}>
                            
                                <Title >Address</Title>
                            
                    </Row3Div>
                        
                    <Row4Div>  
                            <InputFieldDiv className={'row4col1'}>
                                <Label>Street *</Label>
                                <InputField  onChange={streetHandler} type={'text'}/>
                            </InputFieldDiv>

                            <InputFieldDiv className={'row4col2'}>
                                <Label>City *</Label>
                                <InputField ponChange={cityHandler} type={'text'}/>
                            </InputFieldDiv>

                            <InputFieldDiv className={'row4col3'}>
                                <Label>Zip </Label>
                                <InputField onChange={zipHandler} type={'text'}/>
                            </InputFieldDiv>

                    </Row4Div>   
                        
                    <Row5Div className={'row5col1'}>
                            
                            <Title >Contact</Title>
                        
                    </Row5Div>
                        
                    <Row6Div>  
                            <InputFieldDiv className={'row6col1'}>
                                <Label>Website</Label>
                                <InputField  onChange={websiteHandler} type={'text'}/>
                            </InputFieldDiv>

                            <InputFieldDiv className={'row6col2'}>
                                <Label>Phone</Label>
                                <InputField onChange={phoneHandler} type={'text'}/>
                            </InputFieldDiv>

                            <InputFieldDiv className={'row6col3'}>
                                <Label>Email </Label>
                                <InputField onChange={emailHandler} type={'email'}/>
                            </InputFieldDiv>

                    </Row6Div> 
                    
                    <Row7Div className={'row5col1'}>
                            
                            <Title >Details</Title>
                        
                    </Row7Div>
                        
                    <Row8Div>  
                            <InputFieldDiv className={'row8col1'}>
                                <Label>Opening hours *</Label>
                                <InputField  onChange={openingHandler} type={'text'}/>
                            </InputFieldDiv>

                            <InputFieldDiv className={'row8col2'}>
                                <Label>Price Level</Label>
                                <SpaceDiv>
                                        <InputFieldNew placeholder={'Select a value...'} onChange={categoryHandler} type={'text'}/>
                                        <img src={arrowdown} height= '20px' width='50px'/>
                                </SpaceDiv>
                            </InputFieldDiv>

                            <InputFieldDiv className={'row8col3'}>
                                <Label>Image</Label>
                                    <Filebutton>
                                        <GenSmallButton 
                                        onClick={()=> handleFile()} text={'CHOOSE A FILE'}>
                                        </GenSmallButton>
                                    </Filebutton>
                            </InputFieldDiv>

                    </Row8Div> 

                    <Row9Div>
                        <GenBigButton 
                            onClick={()=> handleCreate()} text={'Create'}>
                        </GenBigButton>
                    </Row9Div>
                    
                </InputDiv>

                

            </LoginDiv>
        </WrapperDiv>
    )

}