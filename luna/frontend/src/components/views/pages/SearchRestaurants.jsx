import React from 'react';
import {connect} from "react-redux";
import styled from "styled-components";
import ContentRestaurantArray from "../../containers/ContentRestaurantArray";

const WrapperDiv = styled.div`
  width: 100vw;
    display: flex;
  flex-direction: column;
  align-items: center;
`;


const ContentDiv = styled.div`
  width: 1200rem;
  display: flex;
  flex-direction: row;
      flex-wrap: wrap;
  justify-content: flex-start;
  margin-bottom: 30rem;
`;

const SearchRestaurants = (props) => {

    props.dispatch({type: 'UI_NAV', payload: {primary: 'SEARCH', secondary: 'RESTAURANTS'}});

    console.log('in da search restaurants', props.restaurants)

    const restaurants = props.restaurants.sort((a, b)=> b.average_rating - a.average_rating);
    //
    // const goToTop = () => {
    //     window.scrollTo(0, 0)
    //     {console.log(window)}
    // }

    return(
        <>
            <WrapperDiv>
                <ContentDiv>
                        {restaurants.map((restaurant, i) => {
                            return <ContentRestaurantArray
                                rest_id={restaurant.id}
                                // title={restaurant.name}
                                // city={restaurant.address}
                                // street={restaurant.address}
                                // image={restaurant.image}
                                // average_rating={restaurant.rating}
                                // review_counter={restaurant.reviews}
                                key={'restaurant_' + i}
                            />
                        })}
                </ContentDiv>
            </WrapperDiv>
        </>
)
};

const mapStateToProps = (state) => {
    return {
        restaurants: state.fetch.restaurants.results
    }
};

export default connect(mapStateToProps)(SearchRestaurants)