import React from 'react';
import styled from 'styled-components'
import {connect} from "react-redux";
import StarRating from "../../elements/gidgets/StarRating";

const WrapperDiv = styled.div`
width: 619rem;
background-color: #ffffff;
margin-bottom: 30rem;

`;

const BannerDiv = styled.div`
border-bottom: solid 1px #ebebeb;
padding-left: 15rem;
h2 {
  font-size: 20px;
  font-weight: bold;
  color: #303030;
padding-top: 25rem;
margin-bottom: 13rem;
}
`;

const CommentWrapper = styled.div`
padding: 0 15rem;
`;

const CommentDiv = styled.div`
min-height: 80rem;
display: flex;
flex-direction: column;
justify-content: center;
border-bottom: solid 1px #ebebeb;
padding: 12rem 0 10rem 0;

h5 {
width: 480rem;
  font-size: 20px;
  color: #4c4c4c;
    font-weight: normal;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin-bottom: 13rem;
}

p {
  font-size: 16px;
  font-weight: 300;
  color: #303030;
      margin-top: 40rem;
}
`

const UserProfileReviews = (props) => {

    const userReviews = props.reviews.filter((review) => review.owner === props.user.id);

    const reviews = userReviews.map((review) => {

        const restaurant = props.restaurants.filter((restaurant) => restaurant.id === review.restaurant)[0];

        return (
            <CommentWrapper>
                <CommentDiv>
                    <h5>{restaurant.name}</h5>
                    <StarRating rating={review.rating}/>}
                    <p>{review.content}</p>
                </CommentDiv>
            </CommentWrapper>
        )
    })

    return(
        <WrapperDiv>
            <BannerDiv>
                <h2>REVIEWS</h2>

            </BannerDiv>
            {reviews.map((comment) => {
                return comment
            })}
        </WrapperDiv>
    )
}

const mapStateToProps = (state) => {
    return {
        restaurants: state.fetch.restaurants.results,
        reviews: state.fetch.reviews.results
    }
};

export default connect(mapStateToProps)(UserProfileReviews)