import React, {useState} from 'react';
import styled from "styled-components";
import {connect} from "react-redux";

import GenBigButton from "../../elements/buttons/GenBigButton";

import InputField from "../../elements/input/TextInputField";
import AuthBanner from "../../elements/text/AuthBanner";
import {makeFetch} from "../../../store/actions/fetchActions";
import {withRouter} from "react-router";


const WrapperDiv = styled.div`
    width: 100vw;
    padding: 49rem 0;
  border-top: 1rem solid #d8d8d8;

`;

const LoginDiv = styled.div`
  height: 365rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const InputDiv = styled.div`
display: flex;
flex-direction: column;
justify-content: flex-start;
align-items: center;
    margin-bottom: 40rem;
`;


const AuthSignUp = (props) => {

    const [emailField, setEmailField] = useState('');

    const handleEmail = (event) => {
        setEmailField(event.target.value);
        console.log(emailField)
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(emailField)
        makeFetch({type:'AUTH_SIGNUP', payload: {email: emailField}});
        props.history.push('/validation')
    };

    return (
        <WrapperDiv>
            <LoginDiv>
                <AuthBanner text={'REGISTRATION'}/>
                <InputDiv>
                    <InputField type={'email'} placeholder={'E-Mail address'} onChange={handleEmail}/>
                </InputDiv>
                <GenBigButton text={'Login'} onClick={handleSubmit}/>
            </LoginDiv>
        </WrapperDiv >
    )

}

export default connect()(withRouter(AuthSignUp))