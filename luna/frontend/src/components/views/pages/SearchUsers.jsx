import React from 'react';
import {connect} from "react-redux";
import styled from "styled-components";
import ReviewUserArrayHeader from "../../containers/SearchReviewUserArrayHeader";
import SearchUserArray from "../../containers/SearchUserArray";
import ResponsiveColumns from "../../elements/divs/ResponsiveColumns";

const WrapperDiv = styled.div`
  width: 100vw;
    display: flex;
  flex-direction: column;
  align-items: center;
`;


const ContentDiv = styled.div`
  width: 1200rem;
  display: flex;
  flex-direction: row;
      flex-wrap: wrap;
  justify-content: space-between;
  margin-bottom: 30rem;
`;

const SearchUsers = (props) => {

    props.dispatch({type: 'UI_NAV', payload: {primary: 'SEARCH', secondary: 'USERS'}});

    // console.log('in da search restaurants', props.restaurants)
    //
    // const restaurants = props.restaurants.sort((a, b)=> b.average_rating - a.average_rating);
    // //
    // const goToTop = () => {
    //     window.scrollTo(0, 0)
    //     {console.log(window)}
    // }

    const usersArray = props.users.map((user, i) => {
            return <SearchUserArray
                user={user}
                // title={restaurant.name}
                // city={restaurant.address}
                // street={restaurant.address}
                // image={restaurant.image}
                // average_rating={restaurant.rating}
                // review_counter={restaurant.reviews}
                key={'restaurant_' + i}
            />
        })

    return(
        <>
            <WrapperDiv>
                <ContentDiv>
                    <ResponsiveColumns contentArray={usersArray}/>
                </ContentDiv>
            </WrapperDiv>
        </>
    )
};

const mapStateToProps = (state) => {
    return {
        users: state.fetch.users.results
    }
};

export default connect(mapStateToProps)(SearchUsers)