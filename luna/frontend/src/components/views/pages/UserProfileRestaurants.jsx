import React from 'react';
import styled from 'styled-components'
import {connect} from "react-redux";
import StarRating from "../../elements/gidgets/StarRating";
import ReviewInput from "./ReviewInput";

const WrapperDiv = styled.div`
width: 619rem;
background-color: #ffffff;
margin-bottom: 30rem;

`;

const BannerDiv = styled.div`
border-bottom: solid 1px #ebebeb;
padding-left: 15rem;
h2 {
  font-size: 20px;
  font-weight: bold;
  color: #303030;
padding-top: 25rem;
margin-bottom: 13rem;
}
`;

const RestaurantWrapper = styled.div`
padding: 0 15rem;
margin-bottom: 15rem;
`;

const RestaurantDiv = styled.div`
min-height: 80rem;
display: flex;
flex-direction: column;
justify-content: center;
border-bottom: solid 1px #ebebeb;
padding: 12rem 0 10rem 0;
margin-bottom: 30rem;


h5 {
width: 480rem;
  font-size: 20px;
  color: #4c4c4c;
    font-weight: normal;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin-bottom: 30rem;
}

p {
  font-size: 16px;
  font-weight: 300;
  color: #303030;
      margin-top: 40rem;
}
`;

const StarDivUp = styled.div`
margin: 20rem 0;
position: relative;
top: -30rem;
`

const StarDivDown = styled.div`
margin: 20rem 0;
position: relative;
top: -20rem;
`

const ReviewDiv = styled.div`
border-top: solid 1px #ebebeb;
padding: 15rem 20rem;
margin-bottom: 10rem;
`


const UserProfileRestaurants = (props) => {

    const userRestaurants = props.restaurants.filter((restaurant) => restaurant.owner === props.user.id);

    const restaurants = userRestaurants.map((restaurant) => {

        const reviews = props.reviews.filter((review) => restaurant.id === review.restaurant);

        return (
            <RestaurantWrapper>
                <RestaurantDiv>
                    <h5>{restaurant.name}</h5>
                    <StarDivUp>
                        <StarRating rating={restaurant.average_rating}/>}
                    </StarDivUp>
                    {reviews.map((review) => {
                        return(
                            <ReviewDiv>
                                <StarDivDown>
                                <StarRating rating={review.rating}/>
                                </StarDivDown>
                            <p>{review.content}</p>
                            </ReviewDiv>
                        )
                    })}
                </RestaurantDiv>
            </RestaurantWrapper>
        )
    })

    return(
        <WrapperDiv>
            <BannerDiv>
                <h2>RESTAURANTS</h2>

            </BannerDiv>
            {restaurants.map((comment) => {
                return comment
            })}
        </WrapperDiv>
    )
}

const mapStateToProps = (state) => {
    return {
        restaurants: state.fetch.restaurants.results,
        reviews: state.fetch.reviews.results
    }
};

export default connect(mapStateToProps)(UserProfileRestaurants)