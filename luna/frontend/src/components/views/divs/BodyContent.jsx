import React, {useEffect} from 'react';
import styled from "styled-components";

import ContentFooter from "../../containers/ContentFooter";

const WrapperDiv = styled.div`
    width: 100vw;
    background-color: ${props => props.theme.divs.content.backgroundColor};

`;

const ContentDiv = styled.div`
    width: 100%;
  min-height: calc(100vh - 164rem);
`;

const FooterDiv = styled.div`;
  width: 100%;
`;

export default (props) => {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <>
            <WrapperDiv className={'BodyContent'}>
                <ContentDiv>
                    {props.children}
                </ContentDiv>
                <FooterDiv>
                    <ContentFooter/>
                </FooterDiv>
            </WrapperDiv>
        </>
    )
}