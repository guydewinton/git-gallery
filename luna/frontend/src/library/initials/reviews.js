const reviews = {
    results: [
        {
            id: 1,
            content: 'The best salmonella in town! Dont miss the scramble eggs!Delicious food, and service was excellent. Ambience was lovely. Must visit.Dont want to be snobby or anything but coming from the Bay Area I was dreading going to a Chinese restaurant in Zurich. Then I read about Achi and I thought this is probably good. And ta-da yes I was pretty happy to finally eating real Chinese again, the congintsai (Thai spinach) so delicious, although I dont think it should have had hot peppers in it, garlic only would be more authentic, Ill ask for it next time.   ',
            rating: 4,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 2,
            restaurant: 9827349,
        },

        {
            id: 2,
            content: 'Super delicious food. Ramen is great. Its meant to be cheap and filling, but with plenty of opportunity to experiment on the basics. This is the only Japanese restaurant in Zürich that hits the mark on everything. Service is friendly too, and the mix of drinks (non alcohol and with) are great. Not quite a 5 for me, but as the restaurant settles in and develops, I hope it will go that way! ',
            rating: 4,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 9827349,
        },

        {
            id: 3,
            content: 'The restaurant is located in an alley with no windows. Worst carbonara Ive had in my life. Real carbonara doesnt use cream, but I decided to give it the benefit of doubt, which I regretted. The entire dish was an overly rich but tasteless glob of carb. The sauce was a thick, goopy mess, its taste reminding me of canned cream soup except its even more bland. There is no hint of yolk or cheese in the sauce. I barely finished it and wasted the CHF 24. ',
            rating: 4,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 9827349,
        },

        {
            id: 4,
            content: 'Amazing!! Nobody screamed at me this time. The burgers are just excellent, service is top-notch, atmosphere is great, the bite is a shining example of what a burger restaurant can be.',
            rating: 4,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 1,
            restaurant: 9827349,
        },

        {
            id: 5,
            content: 'I saw a rat. It really is unfortunate that the experience of enjoying good tasting good food can be thoroughly wrecked by factors like price, etc. ',
            rating: 2,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 4,
            restaurant: 9234536,
        },

        {
            id: 6,
            content: 'I rather chop my arm off and eat it, before returning to this nightmare. What a genuine Northern specialized Chinese restaurant this place is. Located near An Hua, Id much hands down come here as An Hua is a nice mixed Thai-Chinese... But I dont like mixed. I like going to somewhere for specialities; and Northern Chinese food specialities you will certainly get. Their Cantonese dishes are good too. To top it off, even though I dont mind paying extra...this place is reasonably priced for Zurich standards and the flavoring/portions are also very reasonable. ',
            rating: 1,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 9234536,
        },

        {
            id: 7,
            content: 'It is a very special and beautifull place. After checking in at a nearby hotel we stumbled across this cute little restaurant and we were so happy we did!  It is small and charming - everything from the decor to the old school meat slicer to the clock that rang an old bell.  The service was wonderful and everyone was very friendly to our two children.  ',
            rating: 4,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 1,
            restaurant: 9234536,
        },

        {
            id: 8,
            content: 'The restaurant is nice. After checking in at a nearby hotel we stumbled across this cute little restaurant and we were so happy we did!  It is small and charming - everything from the decor to the old school meat slicer to the clock that rang an old bell.  The service was wonderful and everyone was very friendly to our two children.  ',
            rating: 3,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 4,
            restaurant: 29834,
        },

        {
            id: 9,
            content: 'TThe rice is white and tasty. A nice restaurant thats all about Indian food. Youll find many of the traditional dishes here, all of them prepared with great the required expertise.  ',
            rating: 3,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 29834,
        },

        {
            id: 10,
            content: 'Dont miss the scramble eggs! Good decor and ambience, in general a nice Mexican restaurant, one of favorites in Zürich. Although the soup I started with did not create the best first impression, the Super Papitas are excellent as an appetizer although super rich. ',
            rating: 4,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 2,
            restaurant: 9234823,
        },

        {
            id: 11,
            content: 'Super delicious food. Good decor and ambience, in general a nice Mexican restaurant, one of favorites in Zürich. Although the soup I started with did not create the best first impression, the Super Papitas are excellent as an appetizer although super rich. ',
            rating: 4,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 9234823,
        },

        {
            id: 12,
            content: 'The restaurant is located in an alley with no windows. Its a combination of some asian foods, not of the north-eastern Chinese food. The speed they surve food is really slow. And the pigs feet are really bad!! There are still fur on it and not well cooked! And the use normal napkins to put fried dumplings on, so they stick together. The oily smell all around the restaurant and the environment is really noisy. Its like the small streetside restaurants in China. The taste of food is at average level. But the environment is too bad and I dont think its clean. ',
            rating: 2,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 2,
            restaurant: 9234823,
        },

        {
            id: 13,
            content: 'It really is unfortunate that the experience of enjoying good tasting good food can be thoroughly wrecked by factors like price, etc.',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 9,
            restaurant: 9234823,
        },

        {
            id: 14,
            content: 'I saw a snake! For the spendiness of this place the service was marginal. The food was really good, but not superb. My steak was awesome.  ',
            rating: 1,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 1,
            restaurant: 923487,
        },

        {
            id: 15,
            content: 'I will never return to this nightmare. For the spendiness of this place the service was marginal. The food was really good, but not superb. My steak was awesome.  ',
            rating: 1,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 923487,
        },

        {
            id: 16,
            content: 'it is a very special place. I have eaten at so many Indian restaurants, never should you HAVE to order one thing!  And I had to choose between rice and naan not both! I have eaten Indian all over the world and this was not up to par! Have a mix of flavors to try,  not one thing! ',
            rating: 0,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 2,
            restaurant: 923487,
        },

        {
            id: 17,
            content: 'Very nice rice pudding. We were seated close to the outdoor seating area, and I was inhaling someones cigarette smokes throughout the meal. Smoking seems so prevalent in the countries that we visited in Europe, and there is lack of separated areas for smokers and non-smokers. ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 9,
            restaurant: 923487,
        },

        {
            id: 18,
            content: 'The napkins are white and the food is tasty. Delicious food, and service was excellent. Ambience was lovely. Must visit  ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 2,
            restaurant: 923487,
        },

        {
            id: 19,
            content: 'The only thing saving it from a one-star rating is the spaghetti ragu, which meets the standard of being a classic staple pasta, if not a little too salty. ',
            rating: 2,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 2,
            restaurant: 923487,
        },

        {
            id: 20,
            content: 'Super delicious food. Delicious food, and service was excellent. Ambience was lovely. Must visit.Dont want to be snobby or anything but coming from the Bay Area I was dreading going to a Chinese restaurant in Zurich. Then I read about Achi and I thought this is probably good. And ta-da yes I was pretty happy to finally eating real Chinese again, the congintsai (Thai spinach) so delicious, although I dont think it should have had hot peppers in it, garlic only would be more authentic, Ill ask for it next time.  ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 92833,
        },

        {
            id: 21,
            content: 'The restaurant is located in an alley with no windows. I have eaten at so many Indian restaurants, never should you HAVE to order one thing!  And I had to choose between rice and naan not both! I have eaten Indian all over the world and this was not up to par! Have a mix of flavors to try, not one thing! ',
            rating: 3,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 7,
            restaurant: 92833,
        },

        {
            id: 22,
            content: 'I effing hate this place! Nobody cares about the customers. Horrible food, and service was Bad!!. Ambience was scary. DONT visit ',
            rating: 2,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 92833,
        },

        {
            id: 23,
            content: 'I saw a rat. Overpriced and shitty food (I\'m Dutch - we dont pay $35 for terrible hamburgers in the Amsterdam). My wife was served a chicken sandwich that was undercooked (raw in the middle) and they still charged us for it. AVOID AVOID AVOID!!!! ',
            rating: 1,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 6,
            restaurant: 29373484,
        },

        {
            id: 24,
            content: 'I rather chop my leg off and eat it, before returning to this nightmare. It really is unfortunate that the experience of enjoying good tasting good food can be thoroughly wrecked by factors like price, etc. ',
            rating: 1,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 29373484,
        },

        {
            id: 25,
            content: 'it is a very special place. Once a friend told me: the burger is only as good as the bread. The Bite actually masters the art of the burger bread. Of course, everything else they also master. Top 3 Burger in town. ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 9,
            restaurant: 29373484,
        },

        {
            id: 26,
            content: 'The restaurant is nice. Once a friend told me: the burger is only as good as the bread. The Bite actually masters the art of the burger bread. Of course, everything else they also master. Top 3 Burger in town. ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 29373484,
        },

        {
            id: 27,
            content: 'My favorite ramen is the Original Yume creamy chicken broth with chicken Chashu. The taste is exquisite. The combination of ingredients truly compliment each other. Most satisfying, hits the spot every time! ',
            rating: 3,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 2,
            restaurant: 29373484,
        },

        {
            id: 28,
            content: 'Dont miss the pasta bolognesa! Delicious food, and service was excellent. Ambience was lovely. Must visit.',
            rating: 4,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 4,
            restaurant: 29373484,
        },

        {
            id: 29,
            content: 'Super delicious food. My favorite ramen is the Original Yume creamy chicken broth with chicken Chashu. The taste is exquisite. The combination of ingredients truly compliment each other. Most satisfying, hits the spot every time! ',
            rating: 4,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 4,
            restaurant: 29373484,
        },

        {
            id: 30,
            content: 'The restaurant is located in an alley with no windows. It really is unfortunate that the experience of enjoying good tasting good food can be thoroughly wrecked by factors like price, etc. ',
            rating: 2,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 1,
            restaurant: 29373484,
        },

        {
            id: 31,
            content: 'Amazing!! Delicious food, and service was excellent. Ambience was lovely. Must visit. I ordered a vegetarian thali from the lunch menu. I liked their pappadam and roti, as well as the mango lassi. Will be back for sure. They didnt charge me for ice water. ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 4,
            restaurant: 29373484,
        },

        {
            id: 32,
            content: 'I saw insects all over the place! Last night we went to Masala, Nans were chewy, kulfi was pathetic. Place was very noisy. Curries were fine but quantities were very less for the price.',
            rating: 1,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 7,
            restaurant: 29373484,
        },

        {
            id: 33,
            content: 'I saw the chef sneezing on the food. Horrible! The music is too loud!!Im American and Ive had In-N-Out Burger so of course Im a little skeptical when I see animal style French fries.  ',
            rating: 1,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 4,
            restaurant: 29373484,
        },

        {
            id: 34,
            content: 'It is a very loud place.The only thing saving it from a one-star rating is the spaghetti ragu, which meets the standard of being a classic staple pasta, if not a little too salty. ',
            rating: 0,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 3,
            restaurant: 29373484,
        },

        {
            id: 35,
            content: 'The fish is nice and cheap.Good decor and ambience, in general a nice Mexican restaurant, one of favorites in Zürich. Although the soup I started with did not create the best first impression, the Super Papitas are excellent as an appetizer although super rich. ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 4,
            restaurant: 9234536,
        },

        {
            id: 36,
            content: 'Only Gangsters go to this joint. It really is unfortunate that the experience of enjoying good tasting good food can be thoroughly wrecked by factors like price, etc. ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 6,
            restaurant: 29373484,
        },

        {
            id: 37,
            content: 'The bar is nice and the food is cheap. Decent place especially if youre craving Mexican. I especially liked their black beans, which were very flavorful. I also liked the pollo verde, but overall the portions seemed a bit small. ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 9,
            restaurant: 9234536,
        },

        {
            id: 38,
            content: 'Only Celebrities go to this joint. Decent place especially if youre craving Mexican. I especially liked their black beans, which were very flavorful. I also liked the pollo verde, but overall the portions seemed a bit small. ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 5,
            restaurant: 9827349,
        },

        {
            id: 39,
            content: 'The food is cheap, Only Celebrities go to this joint ',
            rating: 5,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 7,
            restaurant: 9827349,
        },

        {
            id: 40,
            content: 'I prefer McDonalds, Only Celebrities go to this joint. I have to say: I used to live in California where the Mexicans know how to cook authentic food. This place is pretty awesome: They dont serve Fajitas (which is an European version of Mexican food).',
            rating: 2,
            date_created: '2020-01-01',
            date_modified: '2020-01-02',
            owner: 4,
            restaurant: 9827349,
        },
    ],
};

export default reviews;