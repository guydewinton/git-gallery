import React from 'react';
import styled from "styled-components";
import InputField from "../components/elements/input/TextInputField";

const SamplerDiv = styled.div`
  min-height: calc(100vh - 164em);
  width: 100vw;
`;

export default (props) => {
    return (
        <>
            <SamplerDiv>
                <InputField/>
            </SamplerDiv>
        </>
    )
}