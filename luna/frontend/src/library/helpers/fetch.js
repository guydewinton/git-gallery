import store from "../../store/store";

const doFetch = async ({type, apiBaseUrl, requestUrl, method, headers, body, payload}) => {
    console.log('FETCHER', type, apiBaseUrl, requestUrl, method, headers, body)
    try {
        const response = await fetch(`${apiBaseUrl}${requestUrl}`, {method, headers, body});
        const data = await response.json();
        console.log('doFetch', data, response);
        store.dispatch({type: type, payload: [data, response, payload]})
        // store.dispatch({}) // log transaction
        return ['OK', response, data]
    }
    catch (error){
        store.dispatch({type: type, payload: {result: error, ...payload}})
        console.log(error)
        return ['error', error]
    }

};

export default doFetch
