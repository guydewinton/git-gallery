const urls = {
    apiBaseUrl: 'https://teambravo.propulsion-learn.ch/',
    auth: {
        signup: 'api/registration/',
        validate: 'api/registration/validate/',
        signin: 'api/auth/token/',
        signout: '',
        refresh: 'api/auth/token/refresh/'
    },
    // restaurants: {
    //     getAll: 'api/restaurants/',
    //     create: 'api/restaurants/new/',
    //     like: '' // add post ID

    // },
  
    users: {
        getAll: 'api/users/list/',
        search: 'api/users/'
        // getUser: '/api/users/'
    },
    me: {
        getMe: 'api/users/me/',
    },

    updateUser: {
        updateMe: 'api/users/me/',
    },

    categories: {
        getCategories: 'api/category/list/',
    },

    comments: {
        getComments: 'api/review/comment/', //Get all the comments from a single user
        postComment: 'api/review/comment/new/',
    },


    reviews: {
        postReviews: 'api/reviews/new/', //Create new review for a restaurant
        getRestReviews: 'api/reviews/', //Get the list of the reviews for a single restaurant
        getUserReviews: 'api/reviews/user/', //Get the list of the reviews by a single user
        getIdReviewss: 'api/reviews/',//Get a specific review by ID and display all the information
        updateReviews: 'api/reviews/',//Update a specific review (only by owner)
        deleteReviews: 'api/reviews/',//Delete a specific review (only by owner)
        postToggleReviews: 'api/reviews/toggle-like/',
        getLikedReviews: 'api/reviews/likes/', //Get the list of the reviews the currently logged in user liked
        getCommentedReviews: 'api/reviews/comments/',//Get the list of the reviews the current user commented.
    },

    restaurants: {
        getRestaurants: 'api/restaurants/', //Get the list of all the restaurant
        postRestaurants: 'api/restaurants/new/', 
        getCatRestaurants: 'api/restaurants/category/', 
        getCronoRestaurants: 'api/restaurants/user/',//Get the all the restaurants created by a specific user in chronological order
        getIdRestaurants: 'api/restaurants/details/', //Get the details of a restaurant by providing the id of the restaurant.
        patchRestaurants: 'api/restaurants/', // Update a restaurant by id (only by owner or restaurant admin)
        deleteRestaurants: 'api/restaurants/', // Delete a restaurant by id (only by owner or restaurant admin)

    },

    search: {
        getSearch: 'api/search/', //Search for ‘restaurants’, ‘reviews’ or ‘users’. {type: ‘restaurants’, ‘search_string’: ‘Pub’}   
    
    },

    registration: {
        postRegistration: 'api/registration/', //Register new user by asking for email (a validation code will be send to given email)  
        postUserValidation: 'api/registration/validate/', //Validate the creation of new user with the code sent by email
    },

    authentication: {
        postJwtWithUser: 'api/auth/token/', //Get a new JWT by passing username and password.
        postJwtWithOldJwt: 'api/auth/token/refresh/', //Get a new JWT by passing an old still valid JWT.
        postVerifyToken: ' api/auth/token/verify/', //Verify a token by passing the token in the body
    
    },

};

export default urls