import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-size: 0.07vw; 
        font-family: Helvetica, Arial, sans-serif;
        color: #000000;
        -webkit-font-smoothing: antialiased;
    }
`;

export const theme = {
    colors: {
        primaryOrange: '#e47d31',
        secondaryOrange: '#D67443',
        lightOrange: '#e29d7a',
        darkGrey: '#878787',
        lightGrey: '#cccccc',
        paragraphText: '#4c4c4c',

    },
    buttons: {
        big: {
            bgColorUp: '#e47d31',
            bgColorOver: '#D67443',
            bgColorDown: '#d46e28',
            fontColor: '#ffffff',
            height: '55rem',
            width: '200rem',
            fontSize: '20em',
            borderRadius: '28em',
        },
        small: {
            bgColorUp: '#e47d31',
            bgColorOver: '#D67443',
            bgColorDown: '',
            fontColor: '#ffffff',
            height: '40rem',
            width: '200rem',
            fontSize: '16em',
            fontWeight: 'normal',
            borderRadius: '28em',
        },
        commentLike: {
            bgColorUp: 'rgba(145, 145, 145, 0.6)',
            bgColorOver: '',
            bgColorDown: '',
            fontColor: '#ffffff',
            height: '33em',
            width: '250em',
            fontSize: '16em',
            borderRadius: '28em',
        },
        social: {

        },
        profile: {}
    },
    divs: {
        content: {
            backgroundColor: '#f8f8f8',
        },
    },
    accentColor: '#845EC2',
    primaryColor: '#e47d31',
    secondaryColor: '#0081CF',
    borderRadius: '6px'
}
