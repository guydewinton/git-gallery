import store from "../store";

import urls from "../../library/constants/urls";
import fetchObject from "../../library/constants/fetchObject";
import doFetch from "../../library/helpers/fetch";
import { bindActionCreators } from "redux";

export const makeFetch = async ({type, payload = null}) => {
    console.log('makefetch in da')

    const object = {...fetchObject};

    object.type = type;

    switch (type) {
        // AUTH
        case 'AUTH_SIGNIN': {
            object.method = 'POST';
            object.body = JSON.stringify({
                username: payload.username,
                password: payload.password,
            });
            object.requestUrl = urls.auth.signin;
            console.log('prefetch', object);
            const fetchObject = await doFetch(object);
            return [await makeFetch('USER_ID'), fetchObject]
        }
        case 'AUTH_USER_ID': {
            object.requestUrl = urls.me.getMe;
            return await doFetch(object)
        }
        // SIGNUP
        case 'AUTH_SIGNUP': {
            object.method = 'POST';
            object.body = JSON.stringify({
                email: payload.email,
            });
            object.requestUrl = urls.auth.signup;
            console.log('signup', object)
            return await doFetch(object)
        }
        case 'AUTH_VALIDATE': {
            object.method = 'PATCH';
            object.body = JSON.stringify({
                ...payload
            });
            object.requestUrl = urls.auth.validate;
            return await doFetch(object)
        }
        case 'AUTH_REFRESH_TOKEN': {
            object.method = 'POST';
            object.requestUrl = urls.auth.refresh;
            return await doFetch(object)
        }
        case 'AUTH_SIGNOUT': {
            return null
        }
     
        // USERS
        case 'GET_USERS': {
            object.method = 'GET';
            object.requestUrl = urls.users.getAll;
            return await doFetch(object)
        }
        // case 'GET_USER': {
        //     object.method = 'GET';
        //     object.requestUrl = urls.users.getUser;
        //     return await doFetch(object)
        // }
        case 'SEARCH_USER': {
            object.method = 'GET';
            object.requestUrl = urls.users.search + payload;
            return await doFetch(object)
        }
        // ME
        case 'GET_PROFILE': {
            object.requestUrl = urls.me.getMe + payload;
            return await doFetch(object)
        }
        case 'UPDATE_PROFILE': {
            object.requestUrl = urls.updateUser.me;
            return await doFetch(object)
        }

        //CATEGORIES

        case 'GET_CATEGORIES': {
            object.requestUrl = urls.categories.getCategories;
            return await doFetch(object)
        }

        //COMMENTS

        case 'GET_COMMENTS': {
            object.requestUrl = urls.comments.getComments + payload;
            return await doFetch(object)
        }

        case 'POST_COMMENTS': {
            object.requestUrl = urls.comments.postComment + payload;
            return await doFetch(object)
        }


        //REVIEWS

        case 'POST_REVIEWS': {
            object.requestUrl = urls.reviews.postReviews;
            return await doFetch(object)
        }

        case 'GET_REST_REVIEWS': {
            object.requestUrl = urls.reviews.getRestReviews;
            return await doFetch(object)
        }

        case 'GET_USER_REVIEWS': {
            object.requestUrl = urls.reviews.getRestReviews + payload;
            return await doFetch(object)
        }

        case 'GET_ID_REVIEWS': {
            object.requestUrl = urls.reviews.getIdReviews + payload;
            return await doFetch(object)
        }

        case 'UPDATE_REVIEWS': {
            object.requestUrl = urls.reviews.updateReviews + payload;
            return await doFetch(object)
        }

        case 'DELETE_REVIEWS': {
            object.requestUrl = urls.reviews.deleteReviews + payload;
            return await doFetch(object)
        }

        case 'POST_TOGGLE_REVIEWS': {
            object.requestUrl = urls.reviews.postToggleReviews + payload;
            return await doFetch(object)
        }

        case 'GET_LIKE_REVIEWS': {
            object.requestUrl = urls.reviews.getLikedReviews;
            return await doFetch(object)
        }

        case 'GET_COMMENTED_REVIEWS': {
            object.requestUrl = urls.reviews.getCommentedReviews;
            return await doFetch(object)
        }



        //RESTAURANTS

        case 'GET_RESTAURANTS': {
            object.requestUrl = urls.restaurants.getRestaurants;
            return await doFetch(object)
        }

        case 'POST_RESTAURANTS': {
            object.requestUrl = urls.restaurants.postRestaurants;
            return await doFetch(object)
        }

        case 'GET_CAT_RESTAURANTS': {
            object.requestUrl = urls.restaurants.getCatRestaurants + payload;
            return await doFetch(object)
        }

        case 'GET_CRONO_RESTAURANTS': {
            object.requestUrl = urls.restaurants.getCronoRestaurants + payload;
            return await doFetch(object)
        }

        case 'GET_ID_RESTAURANTS': {
            object.requestUrl = urls.restaurants.getIdRestaurants + payload;
            return await doFetch(object)
        }

        case 'PATCH_RESTAURANTS': {
            object.requestUrl = urls.restaurants.patchRestaurants;
            return await doFetch(object)
        }

        case 'DELETE_RESTAURANTS': {
            object.requestUrl = urls.restaurants.deleteRestaurants;
            return await doFetch(object)
        }


        //SEARCH

        case 'GET_SEARCH': {
            object.requestUrl = urls.restaurants.getSearch;
            return await doFetch(object)
        }

        //REGISTRATION

        case 'POST_REGISTRATION': {
            object.requestUrl = urls.restaurants.postRegistration;
            return await doFetch(object)
        }

        case 'POST_USER_VALIDATION': {
            object.requestUrl = urls.restaurants.postUserValidation;
            return await doFetch(object)
        }

        //AUTHENTICATION

        case 'POST_JWT_WITH_USER': {
            object.requestUrl = urls.restaurants.postJwtWithUser;
            return await doFetch(object)
        }

        case 'POST_JWT_WITH_OLD_JWT': {
            object.requestUrl = urls.restaurants.postJwtWithOldJwt;
            return await doFetch(object)
        }

        case 'POST_VERIFY_TOKEN': {
            object.requestUrl = urls.restaurants.postVerifyToken;
            return await doFetch(object)
        }


        // DEFAULT
        default:
            return null
    }

};
