import store from "../store";

import urls from "../../library/constants/urls";
import fetchObject from "../../library/constants/fetchObject";
import doFetch from "../../library/helpers/fetch";

export const makeFetch = async ({type, payload = null}) => {
    console.log('makefetch in da')

    const object = {...fetchObject};

    object.type = type;

    switch (type) {
        // AUTH
        case 'AUTH_SIGNIN': {
            object.method = 'POST';
            object.body = JSON.stringify({
                username: payload.username,
                password: payload.password,
            });
            object.requestUrl = urls.auth.signin;
            console.log('prefetch', object);
            const fetchObject = await doFetch(object);
            return [await makeFetch('USER_ID'), fetchObject]
        }
        case 'AUTH_USER_ID': {
            object.requestUrl = urls.me.getMe;
            return await doFetch(object)
        }
        // SIGNUP
        case 'AUTH_SIGNUP': {
            object.method = 'POST';
            object.body = JSON.stringify({
                email: payload.email,
            });
            object.requestUrl = urls.auth.signup;
            console.log('signup', object);
            return await doFetch(object)
        }
        case 'AUTH_VALIDATE': {
            object.method = 'PATCH';
            object.body = JSON.stringify({
                ...payload
            });
            object.requestUrl = urls.auth.validate;
            return await doFetch(object)
        }
        case 'AUTH_REFRESH_TOKEN': {
            object.method = 'POST';
            object.requestUrl = urls.auth.refresh;
            return await doFetch(object)
        }
        case 'AUTH_SIGNOUT': {
            return null
        }
        // DEFAULT
        default:
            return null
    }

};
