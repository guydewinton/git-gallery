import {applyMiddleware, createStore, combineReducers} from "redux";
import thunk from "redux-thunk";

import authReducer from "./reducers/authReducer";
import fetchReducer from "./reducers/fetchReducer";
import uiReducer from "./reducers/uiReducer";


const reducer = combineReducers({auth: authReducer, fetch: fetchReducer, ui: uiReducer})

const store = createStore(reducer, applyMiddleware(thunk));

// store.subscribe(() => {window.localStorage.setItem('luna', JSON.stringify(store.getState().auth))});

console.log(store.getState())

export default store;