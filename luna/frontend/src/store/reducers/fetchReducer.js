import comments from '../../library/initials/comments'
import restaurants from '../../library/initials/restaurants'
import reviews from '../../library/initials/reviews'
import users from '../../library/initials/users'
import likes from "../../library/initials/likes";

const initialState = {
    users: users,
    restaurants: restaurants,
    reviews: reviews,
    comments: comments,
    likes: likes
};

export default (state = initialState, action) => {

    switch (action.type) {
       
        //USERS

        case "GET_USERS": {
          
          return {
            ...state,
              users: {results: [...action.payload[0]]}
          };
        }


        case "SEARCH_USER": {
            return {
                ...state,
                  users: {results: [...action.payload[0]]}
              };
        }

        case "GET_PROFILE": {
            return {
                ...state,
                  users: {results:[...action.payload[0]]}
              };
        }

        case "UPDATE_PROFILE": {
            return state
          // return {
          //   ...state,
          //     users: {...state.fetch.updateUser.updateMe, action.payload}
          // };
        }

        //CATEGORIES

        case "GET_CATEGORIES": {
            return {
                ...state,
                  categories: {results: [...action.payload[0]]}
              };
        }

        //COMMENTS

        case "GET_COMMENTS": {
            return {
                ...state,
                  comments: {results: [...action.payload[0]]}
              };
        }

        case "POST_COMMENT": {
            return state
          // return {
          //   ...state,
          //     comments: {...state.fetch.comments.postComment, action.payload}
          // };
        }

        //REVIEWS

        case "POST_REVIEWS": {
            return state
            // return {
            //     ...state,
            //       reviews: {...action.payload}
            //   };
        }

        case "GET_REST_REVIEWS": {
            console.log('GET_REST_REVIEWS', action.payload)
            let newState = [...state.reviews.results]
            action.payload[0].forEach((reviewA) => {
                let isPresent = false;
                newState.forEach((reviewB) => {
                    if (reviewA.id === reviewB.id) {
                        isPresent = true;
                    }
                })
                if (!isPresent){
                    newState.push(reviewA)
                }
            });
            return {
                ...state,
                reviews: {results: newState}
            };
        }


        case "GET_USER_REVIEWS": {
            return {
                ...state,
                  reviews: {results: [...action.payload[0]]}
              };
        }

        case "GET_ID_REVIEWS": {
            return {
                ...state,
                  reviews: {results: [...action.payload[0]]}
              };
        }

        case "UPDATE_REVIEWS": {
            return state
            // return {
            //     ...state,
            //       reviews: {...action.payload}
            //   };
        }

        case "DELETE_REVIEWS": {
            return state
            // return {
            //     ...state,
            //       reviews: {...action.payload}
            //   };
        }

        case "POST_TOGGLE_REVIEWS": {
            return state
            // return {
            //     ...state,
            //       reviews: {...action.payload}
            //   };
        }

        case "GET_LIKED_REVIEWS": {
            return {
                ...state,
                  reviews: {results: [...action.payload[0]]}
              };
        }

        case "GET_COMMENTED_REVIEWS": {
            return {
                ...state,
                  reviews: {results: [...action.payload[0]]}
              };
        }
        case "GET_REVIEWS_REST": {
            let newState = [...state.reviews.results]
            action.payload[0].forEach((reviewA) => {
                let isPresent = false;
                let newReview = reviewA
                newState.forEach((reviewB) => {
                    if (reviewA.id === reviewB.id) {
                        isPresent = true;
                    }
                })
                if (!isPresent){
                    newState.push(newReview)
                }
            });
            return {
                ...state,
                reviews: {results: newState}
            };
        }
    
        //RESTAURANTS

        case "GET_RESTAURANTS": {
            let newState = [...state.restaurants.results]
            action.payload[0].forEach((restaurantA) => {
                let isPresent = false;
                let newRestaurant = restaurantA
                newState.forEach((restaurantB) => {
                    if (restaurantA.id === restaurantB.id) {
                        isPresent = true;
                    }
                })
                if (!isPresent){
                    newState.push(newRestaurant)
                }
            });


            return {
                ...state,
                restaurants: {results: newState}
                
            };
        }

        case "POST_RESTAURANTS": {
            return state
            // return {
            //     ...state,
            //       restaurants: {...action.payload}
            //   };
        }

        case "GET_CAT_RESTAURANTS": {
            return {
                ...state,
                restaurants: {results: [...action.payload[0]]}
            };
        }

        case "GET_CRONO_RESTAURANTS": {
            return {
                ...state,
                restaurants: {results: [...action.payload[0]]}
            };
        }

        case "GET_ID_RESTAURANTS": {
            return {
                ...state,
                restaurants: {results: [...action.payload[0]]}
            };
        }

        case "PATCH_RESTAURANTS": {
            return state
            // return {
            //     ...state,
            //       restaurants: {...action.payload}
            //   };
        }

        case "DELETE_RESTAURANTS": {
            return state
            // return {
            //     ...state,
            //       restaurants: {...action.payload}
            //   };
        }


        //SEARCH

        case "GET_SEARCH": {
            return {
                ...state,
                search: {results: [...action.payload[0]]}
            };
        }

        //REGISTRATION

        case "POST_REGISTRATION": {
            return state
            // return {
            //     ...state,
            //       registration: {...action.payload}
            //   };
        }

        case "POST_USER_VALIDATION": {
            return state
            // return {
            //     ...state,
            //       registration: {...action.payload}
            //   };
        }

        //AUTHENTICATION

        case "POST_JWT_WITH_USER": {
            return state
            // return {
            //     ...state,
            //       authentication: {...action.payload}
            //   };
        }

        case "POST_VERIFY_TOKEN": {
            return state
            // return {
            //     ...state,
            //       authentication: {...action.payload}
            //   };
        }




        default:
          return state;
      }

}