const initialState = () => {

    // let initState = {token: {access: null}};
    // const localState = JSON.parse(window.localStorage.getItem('luna'));
    // console.log(localState);
    // if (localState) {
    //     initState = localState
    // }
    // return initState
    return {}
}

export default (state = initialState(), action) => {
    switch (action.type) {
        case 'AUTH_SIGNUP': {
            return {...state, token: action.payload}
        }
        case 'AUTH_VALIDATE': {
            return {...state, token: action.payload}
        }
        case 'AUTH_SIGNIN': {
            console.log('LOGGED IN')
            return {...state, token: action.payload[0]}
        }
        default:
            return state
    }

}