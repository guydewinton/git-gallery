const initialState = {
    ui: {
        primary: '',
        secondary: ''
    },
    restaurant: null,
    profile: null,
    user: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'UI_NAV':
            return {...state, ui: {...action.payload}};
        case 'UI_RESTAURANT':
            return {...state, restaurant: action.payload};
        case 'UI_PROFILE':
            return {...state, user: action.payload};
        case 'NAV_USER_PROFILE':
            return {...state, user: action.payload};
        default:
            return state
    }
}