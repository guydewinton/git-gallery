
import urlsObject from "../../../../../library/constants/urlsObject";
import fetchObject from "../../../../../library/constants/fetchObject";
import makeFetch from "../../../../../library/helpers/functions/fetch";

export default async ({type = null, payload = null}) => {
    console.log('makefetch in da')

    const object = {...fetchObject};

    object.type = type;

    switch (type) {
        // AUTH
        case 'AUTH_SIGNIN': {
            object.method = 'POST';
            object.body = JSON.stringify({
                username: payload.username,
                password: payload.password,
            });
            object.requestUrl = urlsObject.auth.signin;
            console.log('prefetch', object);
            const fetchObject = await makeFetch(object);
            return [await makeFetch('USER_ID'), fetchObject]
        }
        case 'AUTH_USER_ID': {
            object.requestUrl = urlsObject.me.getMe;
            return await makeFetch(object)
        }
        // SIGNUP
        case 'AUTH_SIGNUP': {
            object.method = 'POST';
            object.body = JSON.stringify({
                email: payload.email,
            });
            object.requestUrl = urlsObject.auth.signup;
            console.log('signup', object);
            return await makeFetch(object)
        }
        case 'AUTH_VALIDATE': {
            object.method = 'PATCH';
            object.body = JSON.stringify({
                ...payload
            });
            object.requestUrl = urlsObject.auth.validate;
            return await makeFetch(object)
        }
        case 'AUTH_REFRESH_TOKEN': {
            object.method = 'POST';
            object.requestUrl = urlsObject.auth.refresh;
            return await makeFetch(object)
        }
        case 'AUTH_SIGNOUT': {
            return null
        }
        // DEFAULT
        default:
            return null
    }

};
