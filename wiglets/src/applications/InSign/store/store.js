// import {createConnect, createHook} from "overmind-react";
// import jobsInitial from "../library/initials/data/jobsInitial";
//
// export const store = {
//     state: {
//         jobs: jobsInitial.map((job, i) => {
//             return {
//                 i,
//                 id: job.id,
//                 jobName: job.jobName,
//                 jobNumber: job.jobNumber,
//             }
//         }),
//     },
//     actions: {},
//     effects: {},
// };
//
// export const useStore = createHook();
// export const connect = createConnect();

import {createStore, combineReducers, applyMiddleware} from "redux";
import thunk from "redux-thunk";

import authReducer from "./reducers/system/authReducer";
import navReducer from "./reducers/system/navReducer";
import uiReducer from "./reducers/system/uiReducer";

import contactReducer from "./reducers/application/contactReducer";
import invoiceReducer from "./reducers/application/invoiceReducer";
import jobReducer from "./reducers/application/jobReducer";
import orderReducer from "./reducers/application/orderReducer";
import todoReducer from "./reducers/application/todoReducer";
import companyReducer from "./reducers/application/companyReducer";
import personReducer from "./reducers/application/personReducer";
import supplierReducer from "./reducers/application/supplierReducer";
import employeeReducer from "./reducers/application/employeeReducer";
import clientReducer from "./reducers/application/clientReducer";

const reducer = combineReducers({
    // auth: authReducer,
    nav: navReducer,
    ui: uiReducer,
    companies: companyReducer,
    contacts: contactReducer,
    invoices: invoiceReducer,
    jobs: jobReducer,
    orders: orderReducer,
    people: personReducer,
    todos: todoReducer,
    suppliers: supplierReducer,
    employees: employeeReducer,
    clients: clientReducer
});

const store = createStore(reducer, applyMiddleware(thunk));

export default store;
