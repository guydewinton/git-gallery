import React, {useContext, useEffect} from "react";
import {Route, withRouter} from "react-router";
import AppFrame from "./components/frames/AppFrame";
import store from "./store/store";
import {Provider} from "react-redux";

const InSign = (props) => {

    useEffect(() => {
        props.history.push('/main/insign/dash')
    }, []);

    return (
        <Provider store={store}>
            <Route
                path={'/main/insign'}
                render={(props) => <AppFrame {...props} />}
            />
        </Provider>
    )
};

export default withRouter(InSign);