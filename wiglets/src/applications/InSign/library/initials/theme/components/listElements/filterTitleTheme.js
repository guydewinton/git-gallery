import colors from "../../../../../../../library/theme/definitions/colors";
import sizes from "../../../../../../../library/theme/definitions/sizes";

export default `

.FilterTitleWrapper {
    padding-top: 4.5rem;
    margin-right: 10rem;
    p {
       font-size: ${sizes.mainPane[1].text[1].contentTextLarge[0]};
       color: ${colors.mainPane[1].text[1].contentText[0]};
       cursor: default;
       white-space: nowrap;
    }
}
  
`;
