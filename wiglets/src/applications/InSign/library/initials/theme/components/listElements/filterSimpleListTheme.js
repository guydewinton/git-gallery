import colors from "../../../../../../../library/theme/definitions/colors";

export default `


.SimpleList {
    padding: 6rem 10rem;
    p {
            font-size: 18rem;
            font-family: Heebo;
            color: ${colors.mainPane[1].text[1].contentListItemUp[0]};
            font-weight: bold;
            cursor: default;
    }
    
}

    .SimpleListItem-HIGHLIGHTED_OPTION {
background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
    & p {
        color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
    }
}
    
    
.SimpleListItem-SELECTED_OPTION {
background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
    & p {
        color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
    }
    }
    
`