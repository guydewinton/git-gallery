import colors from "../../../../../../../library/theme/definitions/colors";
import filterDropdownTheme from "./filterDropdownTheme";

export default `


.SearchDropdownWrapper {
    width: 100%;
}

.BannerDropdownWrapper {
    width: 100%;
    position: relative;
}

${filterDropdownTheme}

`