import colors from "../../../../../../../library/theme/definitions/colors";
import sizes from "../../../../../../../library/theme/definitions/sizes";


export default `
    ::-webkit-scrollbar {
    width: ${sizes.mainPane[1].scroll[1].bigScrollWidth[0]};
    }
::-webkit-scrollbar-track {
    background-color: ${colors.mainPane[1].frames[1].scrollTrack[0]};
    }
        ::-webkit-scrollbar-thumb {
    background-color: ${colors.mainPane[1].frames[1].scrollThumb[0]};
    border-radius: ${sizes.mainPane[1].scroll[1].bigScrollBorderRadius[0]};
    }


`