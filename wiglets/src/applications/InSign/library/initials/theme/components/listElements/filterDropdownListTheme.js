import colors from "../../../../../../../library/theme/definitions/colors";
import effects from "../../../../../../../library/theme/definitions/effects";
import sizes from "../../../../../../../library/theme/definitions/sizes";

export default `

.DropdownOuterWrapper {
    position: absolute;
width: 100%;
    margin-top: 10rem;
    border: ${effects.mainPane[1].widgets[1].dropdownBoxBorder[0]};
    border-radius: 5rem;
    padding: 5rem;
    // width: 100%;
    box-shadow: ${effects.mainPane[1].widgets[1].dropdownShadow[0]};
    background-color: white;

}

.DropdownOuterWrapper {
overflow: hidden;
padding: 0;

}

.DropdownInnerWrapper {
max-height: 310rem;
width: calc(100% + 1rem);
overflow-y: scroll;
overflow-x: hidden;
}

.DropdownElementWrapper {
    padding: 6rem 10rem;
    p {
            font-size: ${sizes.mainPane[1].text[1].contentListTextLarge[0]};
            font-family: Heebo;
            color: ${colors.mainPane[1].text[1].contentListItemUp[0]};
            font-weight: bold;
            cursor: default;
    }
    
}
    
    
.SimpleListItem-SELECTED_OPTION {
background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
    & p {
        color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
    }
}
`