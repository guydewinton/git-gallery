import colors from "../../../../../../../library/theme/definitions/colors";

import inputFieldTheme from "./filterInputFieldTheme";
import effects from "../../../../../../../library/theme/definitions/effects";

export default `

${inputFieldTheme}

.ClearableInput {
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
}

.ClearButton {
    margin-top: 9rem;
    margin-right: 6rem;
    height: 18rem;
    width: 18rem;
       svg {
            fill: ${colors.mainPane[1].buttons[1].widgetButtonUp[0]};
        }
}

  .MarginSpacer {
    width: 8rem;
    min-width: 8rem;
}


.BannerDropdownWrapper {
    width: 100%;
}


.BannerInnerWrapper {
    width: 100%;
    border-bottom: ${effects.mainPane[1].widgets[1].contentInputUnderline[0]};
    padding: 0rem 10rem;
    display: flex;
    flex-direction: row;
}
    
    
.BannerInputWrapper {
    width:100%;
}

.InnerElementWrapper {
    width: 100%;
}

  
  
`