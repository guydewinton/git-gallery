import colors from "../../../../../../../library/theme/definitions/colors";

export default `

.DropdownButtonWrapper {
    margin-top: 7rem;
    height: 20rem;
    width: 20rem;
       svg {
            fill: ${colors.mainPane[1].buttons[1].widgetButtonUp[0]};
        }
}


`