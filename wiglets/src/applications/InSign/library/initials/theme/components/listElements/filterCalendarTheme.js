import colors from "../../../../../../../library/theme/definitions/colors";
import effects from "../../../../../../../library/theme/definitions/effects";



export default `

    .react-calendar-wrapper {
        position: relative;
        right: 65rem;
    }
   
    
    .react-calendar {
        position: absolute;
        border: ${effects.mainPane[1].widgets[1].dropdownBoxBorder[0]};
        font-family: Arial, Helvetica, sans-serif;
        box-shadow: ${effects.mainPane[1].widgets[1].dropdownShadow[0]};
        background: white;
        padding: 5rem;
        border-radius: 3rem;
        width: 250rem;
        margin-top: 10rem;
        z-Index: 100;
        
    }
    .react-calendar--doubleView {
        width: 700rem;
    }
    .react-calendar--doubleView .react-calendar__viewContainer {
        display: flex;
        margin: -0.5rem;
    }
    .react-calendar--doubleView .react-calendar__viewContainer > * {
        width: 50%;
        margin: 0.5rem;
    }
    .react-calendar,
    .react-calendar *,
    .react-calendar *:before,
    .react-calendar *:after {
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }
    .react-calendar button {
        margin: 0;
        border: 0;
        outline: 0;
    }
    .react-calendar button:enabled:hover {
        cursor: pointer;
    }
    .react-calendar__navigation {
        height: 30rem;
        margin-bottom: 1rem;
    }
    .react-calendar__navigation button {
        background: none;
        font-size: 15rem;
    }
    .react-calendar__navigation__arrow {
        width: 20rem;
    }

    .react-calendar__navigation__label {
        width: 100rem;
        span {
                font-size: 15rem;
                }

    }

    .react-calendar__navigation button:enabled:hover {
        background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
        color: ${colors.mainPane[1].text[1].contentListItemOver[0]}
     
    }
    .react-calendar__navigation button[disabled] {
        background-color: ${colors.mainPane[1].buttons[1].contentListItemDisabled[0]};
    }
    .react-calendar__month-view__weekdays {
        padding: 2rem;
        text-align: center;
        text-transform: uppercase;
        font-weight: bold;
        height: 25rem;
        //font-size: 0.75rem;
        
    }
    .react-calendar__month-view__weekdays__weekday {
        //padding: 0.5rem;
        abbr {
        font-size: 10rem;
        text-decoration: none;
        }
    }
    .react-calendar__month-view__days {
        // button { 
        // height: 20rem;
        // width: 50rem;
        // outline: 0;
        //
        //       abbr {
        //           font-size: 10rem;
        //       }
        // }
    }
    .react-calendar__month-view__weekNumbers {
        font-weight: bold;
    }
    .react-calendar__month-view__weekNumbers .react-calendar__tile {
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 0.75rem;
        //padding: calc(0.75rem / 0.75) calc(0.5rem / 0.75);
    }
    .react-calendar__month-view__days__day--weekend {
        color: pink;
    }
    .react-calendar__month-view__days__day--neighboringMonth {
        color: pink;
    }
    .react-calendar__year-view .react-calendar__tile,
    .react-calendar__decade-view .react-calendar__tile,
    .react-calendar__century-view .react-calendar__tile {
        padding: 2rem 0.5rem;
    }
    .react-calendar__tile {
        max-width: 100%;
        text-align: center;
        padding: 0.75rem 0.5rem;
        background: none;
           
        height: 30rem;
        width: 50rem;
        outline: 0;
        
              abbr {
                  font-size: 12rem;
              }
        
    }
    .react-calendar__tile:disabled {
        background-color: ${colors.mainPane[1].buttons[1].contentListItemDisabled[0]};
    }
    .react-calendar__tile:enabled:hover,
    .react-calendar__tile:enabled:focus {
        background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
        abbr {
            color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
        }
    }
    .react-calendar__tile--now {
        background: ${colors.mainPane[1].buttons[1].contentListItemHighlighted[0]};
    }
    .react-calendar__tile--now:enabled:hover,
    .react-calendar__tile--now:enabled:focus {
        background: ${colors.mainPane[1].buttons[1].contentListItemDisabled[0]};
    }
    .react-calendar__tile--hasActive {
        background: ${colors.mainPane[1].buttons[1].contentListItemSelected[0]};
           abbr {
            color: ${colors.mainPane[1].text[1].contentListItemSelected[0]};
        }
    }
    .react-calendar__tile--hasActive:enabled:hover,
    .react-calendar__tile--hasActive:enabled:focus {
        background: ${colors.mainPane[1].buttons[1].contentListItemSelected[0]};
           abbr {
            color: ${colors.mainPane[1].text[1].contentListItemSelected[0]};
        }
    }
    .react-calendar__tile--active {
        background: ${colors.mainPane[1].buttons[1].contentListItemHighlighted[0]};
    }
    .react-calendar__tile--active:enabled:hover,
    .react-calendar__tile--active:enabled:focus {
        background: ${colors.mainPane[1].buttons[1].contentListItemSelected[0]};
           abbr {
            color: ${colors.mainPane[1].text[1].contentListItemSelected[0]};
        }
    }
    
        .react-calendar__tile--rangeStart {
        background: ${colors.mainPane[1].buttons[1].contentListItemSelected[0]};
         abbr {
            color: ${colors.mainPane[1].text[1].contentListItemSelected[0]};
        }
    }
    .react-calendar--selectRange .react-calendar__tile--hover {
        background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
        abbr {
            color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
        }
    }


`