import colors from "../../../../../../../library/theme/definitions/colors";
import sizes from "../../../../../../../library/theme/definitions/sizes";

export default `


.DividedList {
    padding: 6rem 10rem;
    p {
            font-size: ${sizes.mainPane[1].text[1].contentListTextLarge[0]};
            font-family: Heebo;
            color: ${colors.mainPane[1].text[1].contentListItemUp[0]};
            font-weight: bold;
            cursor: default;
    }
    
}
    
    
.SimpleListItem-SELECTED_OPTION {
background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
    & p {
        color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
    }
    }
    
    .SimpleListItem-HIGHLIGHTED_OPTION {
background-color: blue;
    & p {
        color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
    }
}
`