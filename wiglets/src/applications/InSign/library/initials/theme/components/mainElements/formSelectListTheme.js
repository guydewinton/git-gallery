import colors from "../../../../../../../library/theme/definitions/colors";

export default `


.SimpleList {
height: 100%;
    // padding: 6rem 10rem;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    // justify-content: space-around;
    border-radius: 5rem 0 0 5rem;
    // overflow: hidden;
    border-right: solid 1rem #AAAAAA;
    p {
            font-size: 18rem;
            font-family: Heebo;
            color: ${colors.mainPane[1].text[1].contentListItemUp[0]};
            font-weight: bold;
            white-space: nowrap;
            cursor: default;
    }

    
}

.ListItemSelect {
height: 100%;
width: 100%;
display: flex;
// justify-content: center;
align-items: center;
padding: 5rem 80rem 5rem 35rem;
// border-radius: 5rem;
border-bottom: solid 1rem #AAAAAA;

        :last-child {
    border-bottom: none;

    }
}

    .ListItemSelect-HIGHLIGHTED_OPTION {
background-color: ${colors.mainPane[1].buttons[1].contentListItemHighlighted[0]};
    & p {
        color: ${colors.mainPane[1].text[1].contentListItemUp[0]};
    }
    

}
    
    
.ListItemSelect-SELECTED_OPTION {
background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
    & p {
        color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
    }
    }
    
    
    .ListItemSelect-SELECTED_OPTION.ListItemSelect-HIGHLIGHTED_OPTION {
p {
        color: ${colors.mainPane[1].text[1].contentListItemUp[0]};
    }
    
`