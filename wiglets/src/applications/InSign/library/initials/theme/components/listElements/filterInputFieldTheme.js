import colors from "../../../../../../../library/theme/definitions/colors";
import sizes from "../../../../../../../library/theme/definitions/sizes";

export default `

.SimpleInput {
    width: 100%;
    input {
        position: relative;
        top: 3rem;
        height: 30rem;
        width: 100%;
        font-size: ${sizes.mainPane[1].text[1].contentTextLarge[0]};
        font-family: Heebo;
        font-weight: bold;
        border: none;
        color: ${colors.mainPane[1].text[1].contentText[0]};
        background-color: transparent;
            :focus {
                outline: 0;
            }
            ::placeholder {
                color: ${colors.mainPane[1].text[1].contentPlaceholderText[0]};
            }
    }
}

  
`