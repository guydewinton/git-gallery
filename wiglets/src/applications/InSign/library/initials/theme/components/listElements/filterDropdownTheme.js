import colors from "../../../../../../../library/theme/definitions/colors";
import effects from "../../../../../../../library/theme/definitions/effects";

export default `

.MarginSpacer {
    width: 8rem;
    min-width: 8rem;
}


.BannerDropdownWrapper {
    width: 100%;
}


.BannerInnerWrapper {
    width: 100%;
    border-bottom: ${effects.mainPane[1].widgets[1].contentInputUnderline[0]};
    padding: 0rem 10rem;
    display: flex;
    flex-direction: row;
}
    
    
.BannerInputWrapper {
    width:100%;
}

.InnerElementWrapper {
    width: 100%;
}


// .DropdownOuterWrapper_VISIBLE{
//     opacity: 1;
// }
// 
// .DropdownOuterWrapper_INVISIBLE{
//     opacity: 0;
// }
// 
 
.DropdownOuterWrapper {
    transition: opacity 120ms ease-in;
}

.DropdownButton {
 svg {
    height: 20rem;
    width: 20rem;
 }
}


`