import colors from "../../../../../../../library/theme/definitions/colors";
import sizes from "../../../../../../../library/theme/definitions/sizes";

export default `

.TableHeaderSimple {
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    padding-right: ${sizes.mainPane[1].scroll[1].bigScrollWidth[0]};
    
}

    .HeaderRow-TextItem {
  
 
    font-size: ${sizes.mainPane[1].text[1].contentTextHeader[0]};
    font-family: Heebo;
    color: ${colors.mainPane[1].text[1].contentListItemUp[0]};
    font-weight: bold;
    cursor: default;
}

.HeaderRow-TableRowTextItem {
    
   .TableRowTextItem-divider {
   height: 20rem;
   width: 40rem;
   padding: 0 10rem;
   border-right: solid 1rem #BCBCBC;
   svg {
   fill: ${colors.mainPane[1].buttons[1].widgetButtonUp[0]};
      height: 20rem;
   width: 20rem;
   // display: none;
   }
   }
   
   :last-child {
      .TableRowTextItem-divider {
border-right: none;   
}
   
   }
}

.TableRowTextItem-head-0.StartButton {
   height: 20rem;
   width: 0;
   padding: 0;
   border-right: solid 1rem #BCBCBC;
}






.SortArrowHidden {
display: none;
}

.SortArrowUp {
display: block;
transform: rotate(180deg);
}

.SortArrowDown {
display: block;
transform: rotate(0deg);
}

`