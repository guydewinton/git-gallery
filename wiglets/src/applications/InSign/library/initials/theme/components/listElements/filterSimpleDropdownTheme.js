import colors from "../../../../../../../library/theme/definitions/colors";
import filterDropdownTheme from "./filterDropdownTheme";

export default `


.SimpleDropdownWrapper {
    width: 100%;
}

.BannerDropdownWrapper {
    width: 100%;
    position: relative;
}

${filterDropdownTheme}

input {
  cursor: default;
}



`