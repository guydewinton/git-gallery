import colors from "../../../../../../../library/theme/definitions/colors";
import listTableHeaderTheme from "./listTableHeaderTheme";
import listTableBodyTheme from "./listTableBodyTheme";

export default `

.HeaderBodyWrapper {
height: 100%;
}

.HeaderWrapper {
height: 35rem;
width: 100%;
border-bottom: solid 1rem #BCBCBC;
display: flex;
flex-direction: row;
align-items: center;

}

.HeaderElementWrapper {
width: 100%;
}



.BodyWrapper {
height: calc(100% - 35rem);
}

.BodyElementWrapper {
height: 100%;
}

.TableBody {
height: 100%;
}

${listTableHeaderTheme}
${listTableBodyTheme}

.TextItem {
padding: 5rem 10rem;

}

.TableRowTextItem {
   width: 180rem;
   display: flex;
   flex-direction: row;
   justify-content: space-between;
   align-items: center;
}



`