import colors from "../../../../../../../library/theme/definitions/colors";

import scrollbarTheme from "../utilityElements/scrollbarTheme";
import sizes from "../../../../../../../library/theme/definitions/sizes";

export default `

.TableBody {
    width: 100%;
    display: flex;
    flex-direction: column;
    overflow-y: scroll;
    overflow-x: hidden;
    ${scrollbarTheme}
    padding: 5rem 0;
}

.TableRowSimple {
    width: 100%;
    display: flex;
    flex-direction: row;
        justify-content: flex-start;
        align-items: center;
        border-bottom: solid .5rem #f0f0f0;
        :last-child {
        border-bottom: none;
        }
    
}

.ButtonComponent {
margin: 0 10rem;
}
       
        .BodyRow-TextItem {
    font-size: ${sizes.mainPane[1].text[1].contentTextLarge[0]};
    color: ${colors.mainPane[1].text[1].contentListItemUp[0]};
    cursor: default;
}



.TableRowSimple-SELECTED_OPTION {
background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
        .BodyRow-TextItem {
    color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
}
}

.TableRowSimple-UNFOCUSED_OPTION {
    :hover {
        background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
        .BodyRow-TextItem {
    color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
}
}

        }
}

.TableRowSimple-INACTIVE_OPTION {
    :hover {
        background-color: ${colors.mainPane[1].buttons[1].contentListItemOver[0]};
        .BodyRow-TextItem {
    color: ${colors.mainPane[1].text[1].contentListItemOver[0]};
}
}

        }
}


`