import colors from "../../../../../../../library/theme/definitions/colors";

export default `

    .DropdownButtonWrapper {

        margin-top: 9rem;
        margin-left: 2rem;
        svg {
            fill: ${colors.mainPane[1].buttons[1].widgetButtonUp[0]};
            height: 14rem;
            width: 14rem;
        }
    }

`