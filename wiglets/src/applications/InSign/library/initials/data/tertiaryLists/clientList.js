export default [
    {
        value: 0,
        label: 'Meriton Ltd',
    },
    {
        value: 1,
        label: 'Multiplex',
    },
    {
        value: 2,
        label: 'Lend Lease',
    },
    {
        value: 3,
        label: 'Builders Consortium',
    },
    {
        value: 4,
        label: 'So-and-so Partners',
    },
    {
        value: 5,
        label: 'Some other builder',
    },
]