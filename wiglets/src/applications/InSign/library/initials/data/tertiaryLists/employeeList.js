export default [
    {
        value: 0,
        label: 'Bob Jones',
    },
    {
        value: 1,
        label: 'Jerry Marshall',
    },
    {
        value: 2,
        label: 'Katherine Turner',
    },
    {
        value: 3,
        label: 'Kerry Poppins',
    },
    {
        value: 4,
        label: 'Mary Franks',
    },
    {
        value: 5,
        label: 'Jenny Rudwick',
    },
]