export default [
    {
        value: 0,
        label: 'RIM',
    },
    {
        value: 1,
        label: 'Blah Fabricators',
    },
    {
        value: 2,
        label: 'Things and Stuff',
    },
    {
        value: 3,
        label: 'Materials Are Us',
    },
    {
        value: 4,
        label: 'Cut, Scrape and Polish',
    },
    {
        value: 5,
        label: 'Sticky Bits',
    },
]