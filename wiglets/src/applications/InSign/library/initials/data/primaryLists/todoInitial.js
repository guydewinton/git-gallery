export default [
    {
        id: '9f68ea4a-46fa-4bf0-85ea-66bb0870ce04',
        description: 'The first todo...',
        refObject: {
            id: 1,
            type: 'Job',
            number: 124,
            name: 'Megatron Housing Estate',
        },
        assignment: [
            {
                id: '9f68ea4a-46fa-4bf0-85ea-66bb0870ce04',
                firstName: 'Guy',
                middleNames: ['Robert', 'Chephren'],
                lastName: 'de Winton',
                nickName: '',
                displayID: 'Guy de Winton'
            },
        ],
        type: '',
        status: '',
        items: [
            {
                // nested todos
            }
        ],
        notes: [
            {

            }
        ],
        added: 0,
        addedBy: {

        },
        modified: 0,
        modifiedBy: {

        },
        log: [
            {

            }
        ]
    }
]