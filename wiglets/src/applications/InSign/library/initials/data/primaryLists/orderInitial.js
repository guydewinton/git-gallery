export default [
    {
        id: '9f68ea4a-46fa-4bf0-85ea-66bb0870ce04',
        job: {
            id: 1,
            jobNumber: 124,
            jobName: 'Megatron Housing Estate',
        },
        supplier: {
            id: '9f68ea4a-46fa-4bf0-85ea-66bb0870ce04',
            companyName: 'RIM Fabrication Services',
            displayID: 'RIM',
        },
        description: 'Brass Plates',
        type: '',
        items: [
            {

            }
        ],
        received: {

        },
        notes: [
            {

            }
        ],
        added: 0,
        addedById: 0,
        modified: 0,
        modifiedById: 0,
        log: [
            {

            }
        ]
    },
]