export default new Proxy([
    {
        id: 0,
        jobNumber: 123,
        jobName: 'Skyrise Apartments',
        address: {
            street: [
                'Level 11, Meriton Tower',
                '528 Kent Street',
            ],
            city: 'Sydney',
            state: 'NSW',
            postCode: '2000',
            country: 'Australia',
        },
        projectManager: {
            id: 0,
            firstName: 'Bob',
            lastName: 'Jones',
        },
        client: {
            id: 0,
            displayID: 'Karimbla',
        },
        contract: {
            id: 0,
        },
        contacts: [
            {
                id: 0,
                displayID: 'Jessica Rose',
                primaryTelephone: '0483 833 836',
                position: 'Foreman'

            },
            {
                id: 0,
                displayID: 'Josh Brown',
                primaryTelephone: '0432 384 283',
                position: 'Site Manager'

            },
            {
                id: 0,
                displayID: 'Chris Chen',
                primaryTelephone: '0467 940 305',
                position: 'Architect'

            },
            {
                id: 0,
                displayID: 'Angela Sirolli',
                primaryTelephone: '0484 479 390',
                position: 'Level 6 Manager'

            },
            {
                id: 0,
                displayID: 'Michael Palmer',
                primaryTelephone: '0412 379 038',
                position: 'Level 5 Manager'

            },
            {
                id: 0,
                displayID: 'Graham Harding',
                primaryTelephone: '0454 348 573',
                position: 'Level 3 Manager'

            },
        ],
        status: '',
        dates: {
            startDate: new Date(2020, 2, 3),
            statutoryDate: new Date(2020, 4, 6),
            completionDate: new Date(2020, 5, 27),
        },
        notes: [
            {
                id: 0,
                note: 'Skyrise Apartments... This is a note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is another note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is a note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is another note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is a note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is another note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is a note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is another note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is a note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is another note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is a note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is another note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
        ],
        todos: [
            {
                id: 0,
                todo: 'Contact foreman regarding installation',
                category: 'Invoices',
                status: 'Complete',
                addedDate: new Date(2020, 7, 16),
                completedDate: new Date(2020, 7, 16),

            },
            {
                id: 0,
                todo: 'Contact foreman regarding installation',
                category: 'Invoices',
                status: 'Complete',
                addedDate: new Date(2020, 7, 16),
                completedDate: new Date(2020, 7, 16),

            },

        ],
        orders: [
            {
                id: 0,
                supplier: 'RIM',
                description: 'Brass Plates',
                date: new Date(2020, 7, 16),
                status: 'Received',
                person: '',
            },
            {
                id: 0,
                supplier: 'RIM',
                description: 'Brass Plates',
                date: new Date(2020, 7, 16),
                status: 'Received',
                person: '',
            },
        ],
        invoices: [
            {
                id: 0,
                description: 'March, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'March, 2020 Variations',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'April, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'May, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'March, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'March, 2020 Variations',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'April, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'May, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'March, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'March, 2020 Variations',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'April, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'May, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
        ],
        files: [
            {
                id: 0,
                name: 'Schedule',
                size: '12MB',
                uploadedBy: 'Jane Goodall',
                uploadDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                name: 'Schedule',
                size: '12MB',
                uploadedBy: 'Jane Goodall',
                uploadDate: new Date(2020, 7, 16)

            },
        ],
        dateAdded: 0,
        addedById: 0,
        dateModified: 0,
        modifiedById: {

        },
        log: [
            {

            }
        ]
    },
    {
        id: 1,
        jobNumber: 124,
        jobName: 'Megatron Housing Estate',
        address: {
            street: [
                'Level 11, Meriton Tower',
                '528 Kent Street',
            ],
            city: 'Sydney',
            state: 'NSW',
            postCode: '2000',
            country: 'Australia',
        },
        projectManager: {
            id: 1,
            firstName: 'Bob',
            lastName: 'Jones',
        },
        client: {
            id: 1,
            displayID: 'Multiplex',
        },
        contract: {
            id: 0,
        },
        contacts: [
            {
                id: 0,
                displayID: 'Jessica Rose',
                primaryTelephone: '0483 833 836',
                position: 'Foreman'

            },
            {
                id: 0,
                displayID: 'Josh Brown',
                primaryTelephone: '0432 384 283',
                position: 'Site Manager'

            },
            {
                id: 0,
                displayID: 'Chris Chen',
                primaryTelephone: '0467 940 305',
                position: 'Architect'

            },
            {
                id: 0,
                displayID: 'Angela Sirolli',
                primaryTelephone: '0484 479 390',
                position: 'Level 6 Manager'

            },
            {
                id: 0,
                displayID: 'Michael Palmer',
                primaryTelephone: '0412 379 038',
                position: 'Level 5 Manager'

            },
            {
                id: 0,
                displayID: 'Graham Harding',
                primaryTelephone: '0454 348 573',
                position: 'Level 3 Manager'

            },
            {
                id: 0,
                displayID: 'Jessica Rose',
                primaryTelephone: '0483 833 836',
                position: 'Foreman'

            },
            {
                id: 0,
                displayID: 'Josh Brown',
                primaryTelephone: '0432 384 283',
                position: 'Site Manager'

            },
            {
                id: 0,
                displayID: 'Chris Chen',
                primaryTelephone: '0467 940 305',
                position: 'Architect'

            },
            {
                id: 0,
                displayID: 'Angela Sirolli',
                primaryTelephone: '0484 479 390',
                position: 'Level 6 Manager'

            },
            {
                id: 0,
                displayID: 'Michael Palmer',
                primaryTelephone: '0412 379 038',
                position: 'Level 5 Manager'

            },
            {
                id: 0,
                displayID: 'Graham Harding',
                primaryTelephone: '0454 348 573',
                position: 'Level 3 Manager'

            },
        ],
        status: '',
        dates: {
            startDate: new Date(2020, 5, 12),
            statutoryDate: new Date(2020, 6, 8),
            completionDate: new Date(2020, 7, 16),
        },
        notes: [
            {
                id: 0,
                note: 'Skyrise Apartments... This is a note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is another note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
        ],
        todos: [
            {
                id: 0,
                todo: 'Contact foreman regarding installation',
                category: 'Invoices',
                status: 'Complete',
                addedDate: new Date(2020, 7, 16),
                completedDate: new Date(2020, 7, 16),

            },
            {
                id: 0,
                todo: 'Contact foreman regarding installation',
                category: 'Invoices',
                status: 'Complete',
                addedDate: new Date(2020, 7, 16),
                completedDate: new Date(2020, 7, 16),

            },

        ],
        orders: [
            {
                id: 0,
                supplier: 'RIM',
                description: 'Brass Plates',
                date: new Date(2020, 7, 16),
                status: 'Received',
                person: '',
            },
            {
                id: 0,
                supplier: 'RIM',
                description: 'Brass Plates',
                date: new Date(2020, 7, 16),
                status: 'Received',
                person: '',
            },
        ],
        invoices: [
            {
                id: 0,
                description: 'March, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'March, 2020 Variations',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'April, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'May, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
        ],
        files: [
            {
                id: 0,
                name: 'Schedule',
                size: '12MB',
                uploadedBy: 'Jane Goodall',
                uploadDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                name: 'Schedule',
                size: '12MB',
                uploadedBy: 'Jane Goodall',
                uploadDate: new Date(2020, 7, 16)

            },
        ],
        dateAdded: 0,
        addedById: 0,
        dateModified: 0,
        modifiedById: 0,
    },
    {
        id: 11,
        jobNumber: 8888,
        jobName: 'Superheap Combo Collection',
        address: {
            street: [
                'Level 11, Meriton Tower',
                '528 Kent Street',
            ],
            city: 'Sydney',
            state: 'NSW',
            postCode: '2000',
            country: 'Australia',
        },
        projectManager: {
            id: 2,
            firstName: 'Bob',
            lastName: 'Jones',
        },
        client: {
            id: 2,
            displayID: 'Lend Lease',
        },
        contract: {
            id: 0,
        },
        contacts: [
            {
                id: 0,
                displayID: 'Jessica Rose',
                primaryTelephone: '0483 833 836',
                position: 'Foreman'

            },
            {
                id: 0,
                displayID: 'Josh Brown',
                primaryTelephone: '0432 384 283',
                position: 'Site Manager'

            },

        ],
        status: '',
        dates: {
            startDate: new Date(2020, 4, 17),
            statutoryDate: new Date(2020, 7, 21),
            completionDate: new Date(2020, 9, 3),
        },
        notes: [
            {
                id: 0,
                note: 'Skyrise Apartments... This is a note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                note: 'Skyrise Apartments... This is another note...',
                category: 'Invoices',
                addedBy: 'Foreman',
                addedDate: new Date(2020, 7, 16)

            },
        ],
        todos: [
            {
                id: 0,
                todo: 'Contact foreman regarding installation',
                category: 'Invoices',
                status: 'Complete',
                addedDate: new Date(2020, 7, 16),
                completedDate: new Date(2020, 7, 16),

            },
            {
                id: 0,
                todo: 'Contact foreman regarding installation',
                category: 'Invoices',
                status: 'Complete',
                addedDate: new Date(2020, 7, 16),
                completedDate: new Date(2020, 7, 16),

            },

        ],
        orders: [
            {
                id: 0,
                supplier: 'RIM',
                description: 'Brass Plates',
                date: new Date(2020, 7, 16),
                status: 'Received',
                person: '',
            },
            {
                id: 0,
                supplier: 'RIM',
                description: 'Brass Plates',
                date: new Date(2020, 7, 16),
                status: 'Received',
                person: '',
            },
        ],
        invoices: [
            {
                id: 0,
                description: 'March, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'March, 2020 Variations',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'April, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
            {
                id: 0,
                description: 'May, 2020 Claim',
                date: new Date(2020, 7, 16),
                status: 'Sent',
                person: 'Bobo Hope',
            },
        ],
        files: [
            {
                id: 0,
                name: 'Schedule',
                size: '12MB',
                uploadedBy: 'Jane Goodall',
                uploadDate: new Date(2020, 7, 16)

            },
            {
                id: 0,
                name: 'Schedule',
                size: '12MB',
                uploadedBy: 'Jane Goodall',
                uploadDate: new Date(2020, 7, 16)

            },
        ],
        dateAdded: 0,
        addedById: 0,
        dateModified: 0,
        modifiedById: 0,
    },
], {
    get: (object, prop) => {
        console.log('PROXY TEST!!!', object, prop)
        return object[prop]
    }
})