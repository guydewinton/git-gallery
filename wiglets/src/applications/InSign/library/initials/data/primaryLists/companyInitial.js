export default [
    {
        id: '9f68ea4a-46fa-4bf0-85ea-66bb0870ce04',
        companyName: 'Karimbla Construction Services Pty Ltd',
        displayID: 'Karimbla',
        isActive: true,
        type: {

        },
        addresses: [
            {
                location: 1,
                street: [
                    'Level 11',
                    'Meriton Tower',
                    '528 Kent Street'
                ],
                city: 'Sydney',
                state: 'NSW',
                postcode: '2000',
                country: 'Australia',
                contacts: {
                    phone: [['Home', '02 9287 2888']],
                    mobile: [['Home', '02 9287 2888']],
                    fax: [['Home', '02 9287 2888']],
                    email: [['Home', 'payable@karimbla.com.au']],
                    website: [['Home', 'www.blah.com']],
                    other: [[]],
                }
            }
        ],
        contacts: {
            phone: [['Home', '02 9287 2888']],
            mobile: [['Home', '02 9287 2888']],
            fax: [['Home', '02 9287 2888']],
            email: [['Home', 'payable@karimbla.com.au']],
            website: [['Home', 'www.blah.com']],
            other: [[]],
        },
        notes: [
            {

            }
        ],
        added: '',
        modified: '2018-10-16T22:17:09.33',
        photoURI: null,
        URI: 'https://ar1.api.myob.com/accountright/0cf47a0a-24d6-49fe-9230-03cabccea94f/Contact/Customer/9f68ea4a-46fa-4bf0-85ea-66bb0870ce04',
        RowVersion: '-1264630786477260800',
        log: [
            {

            }
        ],
    },
]