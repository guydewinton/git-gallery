import React from 'react';
import styled from "styled-components";
import HeaderTitle from "../elements/pureElements/bannerElements/HeaderTitle";

const WrapperDiv = styled.div`
  height: 50rem;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  //border-top: solid #D6D6D6 1rem;
  padding: 0 30rem;
`;

const ContentFooter = (props) => {

    return (
        <WrapperDiv className={`ContentFooter ${props.className}`}>
            <HeaderTitle text={props.text}/>
        </WrapperDiv>
    )
};

export default ContentFooter;