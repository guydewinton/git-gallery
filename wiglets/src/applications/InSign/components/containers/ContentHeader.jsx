import React from 'react';
import styled from "styled-components";
import HeaderTitle from "../elements/pureElements/bannerElements/HeaderTitle";

const WrapperDiv = styled.div`
  height: 50rem;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-bottom: solid #D6D6D6 1rem;
  padding: 0 30rem;
`;


const ContentHeader = (props) => {

    return (
        <WrapperDiv className={`ContentHeader ${props.className}`}>
            <HeaderTitle text={props.title}/>
        </WrapperDiv>
    )
};

export default ContentHeader;