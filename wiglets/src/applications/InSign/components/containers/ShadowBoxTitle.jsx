import React, {useEffect} from "react";
import styled from "styled-components";
import ShadowBox from "./ShadowBox";
import sizes from "../../../../library/theme/definitions/sizes";

const WrapperOuterDiv = styled.div`
height: ${props => props.divHeight ? props.divHeight : '100%'};
${props => props.divWidth && `width: ${props.divWidth};`}
display: flex;
flex-direction: column;
${props => props.flexGrow && `flex: ${props.flexGrow};`};
padding: 0rem 15rem 20rem 15rem;
`;

const TitleWrapper = styled.div`

`;

const TitleElement = styled.h3`
  font-size: ${sizes.mainPane[1].text[1].shadowBoxTitle[0]};
  padding-bottom: 8rem;
  font-weight: 500;
  color: #4C4C4C;
`;

const WrapperInnerDiv = styled.div`
width: 100%;
flex: auto;
`;

const GroupWrapper = styled.div`
  height: 100%;
  width: 100%;
    display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: ${props => props.innerPadding ? '10rem 30rem' : '0 0'};

`;


const ShadowBoxTitle = ({
    className,
    divWidth,
    divHeight,
    flexGrow,
    title,
    innerPadding = true,
    children,

                        }) => {

    return (
        <WrapperOuterDiv
            className={`ShadowBoxTitle ${className}`}
            divHeight={divHeight}
            divWidth={divWidth}
            flexGrow={flexGrow}
        >
            <TitleWrapper>
                <TitleElement>{title}</TitleElement>
            </TitleWrapper>
            <WrapperInnerDiv>
                <ShadowBox>
                    <GroupWrapper innerPadding={innerPadding}>
                        {children}
                    </GroupWrapper>
                </ShadowBox>
            </WrapperInnerDiv>

        </WrapperOuterDiv>
    )

}


export default ShadowBoxTitle