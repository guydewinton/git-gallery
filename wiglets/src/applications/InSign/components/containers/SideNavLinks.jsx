import React, {useEffect, useState} from "react";
import styled from "styled-components";

import colors from "../../../../library/theme/definitions/colors";
import StyledSideNavList from "../elements/styledElements/sideNav/StyledSideNavList";
import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";
import {withRouter} from "react-router";

const BodyDiv = styled.div`
  height: 100vh;
  width: 100vw;
  padding-top: 45rem;
  display: flex;
  flex-direction: column;
`;

const SideNavLinks = (props) => {

    const setHistory = (link) => {
        if (props.history.location.pathname === link) {
            props.history.push(link)
        } else {
            props.history.push(link)
        }
    };

    const navValues = [
        [
            { value: '/main/insign/dash', label: 'DASHBOARD' },
        ],
        [
            { value: '/main/insign/jobsList', label: 'JOBS' },
            { value: '/main/insign/todosList', label: 'TODOS' },
            { value: '/main/insign/ordersList', label: 'ORDERS' },
            { value: '/main/insign/invoicesList', label: 'INVOICES' },
            { value: '/main/insign/contactsList', label: 'CONTACTS' },
        ],
    ];

    return (
        <>
        <BodyDiv>
            <StyledSideNavList
                inputValues={navValues}
                focusIndex={[0,0,0]}
                outputValue={(link) => setHistory(link.value)}
                initValue={[0,0]}
                // focusControl={false}
            />
        </BodyDiv>
            </>
    )

};

export default withRouter(SideNavLinks)