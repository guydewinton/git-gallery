import React from 'react';
import styled from "styled-components";
import HeaderTitle from "../elements/pureElements/bannerElements/HeaderTitle";

const WrapperDiv = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  padding: 20rem 25rem 4rem 25rem;
`;

const ContentBodyInner = (props) => {

    return (
        <WrapperDiv className={`ContentBody ${props.className}`}>
            {props.children}
        </WrapperDiv>
    )
};

export default ContentBodyInner;