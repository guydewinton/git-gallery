import React from 'react';
import styled from "styled-components";
import HeaderTitle from "../elements/pureElements/bannerElements/HeaderTitle";

const WrapperDiv = styled.div`
  height: 50rem;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: solid #D6D6D6 1rem;
  padding: 0 40rem;
`;

const ButtonLeftWrapper = styled.div`
width: 100%;
display: flex;
flex-direction: row;
justify-content: flex-start;
`;

const ButtonRightWrapper = styled.div`
width: 100%;
display: flex;
flex-direction: row;
justify-content: flex-end;
`;


const ContentHeaderNav = ({
    title = '',
    className = '',
    TopLeftElement = null,
    TopRightElement = null,
    elementProps = {},
                          }) => {
    return (
        <WrapperDiv className={`ContentFormBodyHeader ${className}`}>
            <ButtonLeftWrapper className={`ButtonLeftWrapper`}>
                {TopLeftElement && <TopLeftElement.component
                    elementProps={TopLeftElement.props}
                />}
            </ButtonLeftWrapper>
            <HeaderTitle text={title}/>
            <ButtonRightWrapper className={`ButtonRightWrapper`}>
                {TopRightElement && <TopRightElement.component elementProps={TopRightElement.props}/>}
            </ButtonRightWrapper>
        </WrapperDiv>
    )
};

export default ContentHeaderNav;