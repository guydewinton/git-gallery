import React, {useEffect} from "react";
import styled from "styled-components";

const BodyDiv = styled.div`
  height: 100%;
  width: 100%;
  //overflow: hidden;
  border-radius: 5rem;
  background-color: #FFFFFF;
    display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.03), 0 6px 20px 0 rgba(0, 0, 0, 0.03);
  
`;

const ShadowBox = (props) => {

    return (
        <BodyDiv className={'ShadowBox'}>
            {props.children}
        </BodyDiv>
    )

}


export default ShadowBox