import React from "react";
import TableSortSimple from "../../../../../app_modules/FormWiglets/wiglets/TableSortSimple/TableSortSimple";
import TableSortEdit from "../../../../../app_modules/FormWiglets/wiglets/TableSortEdit/TableSortEdit";

const WrappedEditTable = ({

                                  title = '',

                                  className = '',
                                  instanceName = '',
                                  inputValues,
                                  outputValue = (output) => console.log('output not rigged', output),
                                  keyOut = null,
                                  socketIn = '',
                                  socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
                                  focusIndex = null,
                                  focusOut = null,
                                  blurOut = null,
                                  defaultKeyNull = {},

                                  headerValues = [],
                                  filterValue = null,
                                  filterIndex = 0,
                                  sortIndex = 0,

                                  ButtonElementLeft = null,
                                  ButtonElementRight = null,

                                  floatButtons = [],

                              }) => {

    return (
        <div className={`WrappedSortableTable ${className}`}>
            <TableSortEdit
                title={title}
                className={className}
                instanceName={instanceName}
                inputValues={inputValues}
                outputValue={outputValue}
                keyOut={keyOut}
                socketIn={socketIn}
                socketOut={socketOut}
                focusIndex={focusIndex}
                focusOut={focusOut}
                blurOut={blurOut}
                defaultKeyNull={defaultKeyNull}

                headerValues={headerValues}
                filterValue={filterValue}
                filterIndex={filterIndex}
                sortIndex={sortIndex}

                ButtonElementLeft={ButtonElementLeft}
                ButtonElementRight={ButtonElementRight}

                floatButtons={floatButtons}
            />
        </div>
    )

};

export default WrappedEditTable;