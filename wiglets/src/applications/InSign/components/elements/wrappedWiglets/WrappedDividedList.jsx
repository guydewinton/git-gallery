import React from "react";
import ListDivided from "../../../../../app_modules/FormWiglets/wiglets/ListDivided/ListDivided";

const WrappedDividedList = ({

    title = '',

    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    initIndex = [0,0],
    filterValue = null,

                            }) => {

    return (
        <div className={`WrappedDividedList ${className}`}>
            <ListDivided
                title={title}
                className={className}
                instanceName={instanceName}
                inputValues={inputValues}
                outputValue={outputValue}
                keyOut={keyOut}
                socketIn={socketIn}
                socketOut={socketOut}
                focusIndex={focusIndex}
                focusOut={focusOut}
                blurOut={blurOut}
                defaultKeyNull={defaultKeyNull}

                initIndex={initIndex}
                filterValue={filterValue}
            />
        </div>
    )

};

export default WrappedDividedList;