import React from "react";
import TableHeadlessSimple from "../../../../../app_modules/FormWiglets/wiglets/TableHeadlessSimple/TableHeadlessSimple";

const WrappedListTable = ({

                              title = '',

                              className = '',
                              instanceName = '',
                              inputValues,
                              outputValue = (output) => console.log('output not rigged', output),
                              keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
                              socketIn = '',
                              socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
                              focusIndex = null,
                              focusOut = null,
                              blurOut = null,
                              defaultKeyNull = {},

                              headerValues = [],
                              filterArray = [],
                              filterValue = null,
                              filterIndex = 0,
                              sortIndex = 0,

                              ButtonElementLeft = null,
                              ButtonElementRight = null,

                          }) => {

    return (
        <div className={`WrappedListTable ${className}`}>
            <TableHeadlessSimple
                title={title}
                className={className}
                instanceName={instanceName}
                inputValues={inputValues}
                outputValue={outputValue}
                keyOut={keyOut}
                socketIn={socketIn}
                socketOut={socketOut}
                focusIndex={focusIndex}
                focusOut={focusOut}
                blurOut={blurOut}
                defaultKeyNull={defaultKeyNull}

                headerValues={headerValues}
                filterArray={filterArray}
                filterValue={filterValue}
                filterIndex={filterIndex}
                sortIndex={sortIndex}

                ButtonElementLeft={ButtonElementLeft}
                ButtonElementRight={ButtonElementRight}
            />
        </div>
    )

};

export default WrappedListTable;