import React from "react";
import DropdownSimple from "../../../../../app_modules/FormWiglets/wiglets/DropdownSimple/DropdownSimple";

const WrappedSimpleDropdown = ({

    title = '',

    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    initIndex = null,

                               }) => {


    return (
        <div className={`WrappedSimpleDropdown ${className}`}>
            <DropdownSimple
                title={title}
                className={className}
                instanceName={instanceName}
                inputValues={inputValues}
                outputValue={outputValue}
                keyOut={keyOut}
                socketIn={socketIn}
                socketOut={socketOut}
                focusIndex={focusIndex}
                focusOut={focusOut}
                blurOut={blurOut}
                defaultKeyNull={defaultKeyNull}

                initIndex={initIndex}
            />
        </div>
    )

};

export default WrappedSimpleDropdown;