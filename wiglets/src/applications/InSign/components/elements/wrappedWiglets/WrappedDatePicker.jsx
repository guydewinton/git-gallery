import React from "react";
import DatePicker from "../../../../../app_modules/FormWiglets/wiglets/DatePicker/DatePicker";

const WrappedDatePicker = ({

    title = '',

    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    initValue = '',

                           }) => {

    return (
        <div className={`WrappedDatePicker ${className}`}>

            <DatePicker
                title={title}
                className={className}
                instanceName={instanceName}
                inputValues={inputValues}
                outputValue={outputValue}
                keyOut={keyOut}
                socketIn={socketIn}
                socketOut={socketOut}
                focusIndex={focusIndex}
                focusOut={focusOut}
                blurOut={blurOut}
                defaultKeyNull={defaultKeyNull}

                initValue={initValue}
            />
        </div>
    )

};

export default WrappedDatePicker;