import React from "react";
import styled from "styled-components";

import WrappedDividedList from "../../wrappedWiglets/WrappedDividedList";

import colors from "../../../../../../library/theme/definitions/colors";
import sizes from "../../../../../../library/theme/definitions/sizes";
import effects from "../../../../../../library/theme/definitions/effects";



const StyledSideNavList = styled(WrappedDividedList)`

 .ListItemSelect {
   width: 100%;
  padding: 8rem 45rem;
  margin: 10rem 0;
  cursor: default;
  transition: background-color 75ms;
 }
 
 .ListGroup_0 {
.ListItemSelectP {
    font-family: Poppins,sans-serif;
    font-size: ${sizes.sideBar[1].text[1].headerText[0]};
    font-weight: 500;
    color: ${colors.sideBar[1].text[1].navHeaderUp[0]};
    transition: color 75ms, font-weight 0ms;
}

  .ListItemSelect-HIGHLIGHTED_OPTION {
   background-color: #666666;
   box-shadow: 0 4rem 8rem 0 rgba(30, 30, 30, 0.03), 0 6rem 20rem 0 rgba(30, 30, 30, 0.03);
         //border-left: 20rem solid black;


   p {
          font-weight: 600;
          letter-spacing: -.4px;
          color: ${colors.sideBar[1].text[1].navLinksUp[0]};
   }
   
         :active {
        background-color: ${colors.sideBar[1].buttons[1].navButtonDown[0]};
        p {
          color: ${colors.sideBar[1].text[1].navHeaderDown[0]};
        }
 }
}
  .ListItemSelect-SELECTED_OPTION {
   background-color: ${colors.sideBar[1].buttons[1].navButtonActive[0]};
   p {
            color: ${colors.sideBar[1].text[1].navHeaderActive[0]};
          font-weight: 600;
          letter-spacing: -.3px;
   }
   }
 

}

.ListGroup_1 {
.ListItemSelectP {
    font-family: Poppins,sans-serif;
    font-size: ${sizes.sideBar[1].text[1].linkText[0]};
    font-weight: 400;
    color: ${colors.sideBar[1].text[1].navLinksUp[0]};
    transition: color 75ms, font-weight 0ms;
}

  .ListItemSelect-HIGHLIGHTED_OPTION {
   background-color: #666666;
   box-shadow: 0 4rem 8rem 0 rgba(30, 30, 30, 0.03), 0 6rem 20rem 0 rgba(30, 30, 30, 0.03);
   //border-left: 20rem solid black;
   p {
            color: ${colors.sideBar[1].text[1].navLinksUp[0]};
          font-weight: 600;
          letter-spacing: -.4px;
   }
   
         :active {
        background-color: ${colors.sideBar[1].buttons[1].navButtonDown[0]};
        p {
          color: ${colors.sideBar[1].text[1].navLinksDown[0]};
        }
 }
}
  .ListItemSelect-SELECTED_OPTION {
   background-color: ${colors.sideBar[1].buttons[1].navButtonActive[0]};
   p {
            color: ${colors.sideBar[1].text[1].navLinksActive[0]};
          font-weight: 600;
          letter-spacing: -.3px;
   }
   }
 
}

.DividedList :last-child {
.ListGroup-DividerTop {
  height: 0.5rem;
  width: calc(100% - 60rem);
  background-color: ${effects.sideBar[1].nav[1].navBreak[0]};
  margin: 6rem 30rem;
}
}

.ListItemSelect-SELECTED_OPTION.ListItemSelect-HIGHLIGHTED_OPTION {
background-color: ${colors.sideBar[1].buttons[1].navButtonOver[0]};


   p {
            color: ${colors.sideBar[1].text[1].navHeaderUp[0]};
            }
}


`;

export default StyledSideNavList;