import React, {useEffect, useRef} from "react";
import styled from "styled-components";

import WrappedDateRanger from "../../wrappedWiglets/WrappedDateRanger";

import filterTitleTheme from "../../../../library/initials/theme/components/listElements/filterTitleTheme";
import filterSearchDropdownTheme from "../../../../library/initials/theme/components/listElements/filterSearchDropdownTheme";
import filterClearableInputTheme from "../../../../library/initials/theme/components/listElements/filterClearableInputTheme";
import filterCalendarButtonTheme from "../../../../library/initials/theme/components/listElements/filterCalendarButtonTheme";
import filterCalendarTheme from "../../../../library/initials/theme/components/listElements/filterCalendarTheme";

const StyledDateRanger = styled(WrappedDateRanger)`

display: flex;
flex-direction: row;

.BannerOuterWrapper {
    width: 170rem;
    min-width: 170rem;
    }


${filterTitleTheme}

${filterSearchDropdownTheme}

${filterClearableInputTheme}

${filterCalendarButtonTheme}

${filterCalendarTheme}

`;

export default StyledDateRanger;