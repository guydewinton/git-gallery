import React, {useEffect, useRef} from "react";
import styled from "styled-components";

import WrappedBasicInput from "../../wrappedWiglets/WrappedBasicInput";

import filterSimpleDropdownTheme from "../../../../library/initials/theme/components/listElements/filterSimpleDropdownTheme";
import filterTitleTheme from "../../../../library/initials/theme/components/listElements/filterTitleTheme";
import filterClearableInputTheme from "../../../../library/initials/theme/components/listElements/filterClearableInputTheme";


const StyledBasicInput = styled(WrappedBasicInput)`

width: 100%;
display: flex;
flex-direction: row;

.MouseOverBannerWrapper {
width: 100%;
}

${filterTitleTheme}

${filterClearableInputTheme}

.ClearButtonWrapper {
width: 20rem;
}


.BannerOuterWrapper {
    width: 100%;
}

.BannerInnerWrapper {
    width: 100%;
}

`;

export default StyledBasicInput;