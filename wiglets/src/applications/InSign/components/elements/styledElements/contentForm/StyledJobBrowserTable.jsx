import React, {useEffect, useRef} from "react";
import styled from "styled-components";

import WrappedSortableTable from "../../wrappedWiglets/WrappedSortableTable";
import listTableTheme from "../../../../library/initials/theme/components/listElements/listHeadedTableTheme";
import formHeadedTableTheme from "../../../../library/initials/theme/components/mainElements/formHeadedTableTheme";
import WrappedEditTable from "../../wrappedWiglets/WrappedEditTable";

const StyledJobsBrowserTable = styled(WrappedEditTable)`

width: 100%;
height: 100%;
position: relative;
//  //padding: 10rem 10rem;
//


.HeaderBodyWrapper {
width: 100%;
position: absolute;
}


${formHeadedTableTheme}

.notes-TableRowTextItem-0 {
  flex-grow: 8;
}

.notes-TableRowTextItem-1 {
  flex-grow: 1;
}

.notes-TableRowTextItem-2 {
  flex-grow: 1;
}

.notes-TableRowTextItem-3 {
  flex-grow: 1;
}

.todos-TableRowTextItem-0 {
  flex-grow: 4;
}

.todos-TableRowTextItem-1 {
  flex-grow: 1;
}

.todos-TableRowTextItem-2 {
  flex-grow: 1;
}

.todos-TableRowTextItem-3 {
  flex-grow: 1;
}

.orders-TableRowTextItem-0 {
  flex-grow: 3;
}

.orders-TableRowTextItem-1 {
  flex-grow: 1;
}

.orders-TableRowTextItem-2 {
  flex-grow: 1;
}

.invoices-TableRowTextItem-0 {
  flex-grow: 3;
}

.invoices-TableRowTextItem-1 {
  flex-grow: 1;
}


.files-TableRowTextItem-0 {
  flex-grow: 3;
}

.files-TableRowTextItem-1 {
  flex-grow: 1;
}

.files-TableRowTextItem-2 {
  flex-grow: 1;
}

.files-TableRowTextItem-3 {
  flex-grow: 1;
}

.TableRowSimple-SELECTED_OPTION {
.TableItemEdit {
color: white;
}
}




`;

export default StyledJobsBrowserTable;