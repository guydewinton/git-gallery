import React, {useEffect, useRef} from "react";
import styled from "styled-components";

import WrappedSimpleDropdown from "../../wrappedWiglets/WrappedSimpleDropdown";

import filterSimpleDropdownTheme from "../../../../library/initials/theme/components/listElements/filterSimpleDropdownTheme";
import filterDropdownListTheme from "../../../../library/initials/theme/components/listElements/filterDropdownListTheme";
import filterDropdownButtonTheme from "../../../../library/initials/theme/components/listElements/filterDropdownButtonTheme";
import filterTitleTheme from "../../../../library/initials/theme/components/listElements/filterTitleTheme";
import filterInputFieldTheme from "../../../../library/initials/theme/components/listElements/filterInputFieldTheme";


const StyledSimpleDropdown = styled(WrappedSimpleDropdown)`

width: 100%;
display: flex;
flex-direction: row;

${filterTitleTheme}

${filterSimpleDropdownTheme}

${filterInputFieldTheme}

${filterDropdownButtonTheme}
  
${filterDropdownListTheme}




`;

export default StyledSimpleDropdown;