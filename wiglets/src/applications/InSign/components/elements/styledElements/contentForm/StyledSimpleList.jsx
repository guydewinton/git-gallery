import React, {useEffect, useRef} from "react";
import styled from "styled-components";

import WrappedSimpleDropdown from "../../wrappedWiglets/WrappedSimpleDropdown";

import filterSimpleDropdownTheme from "../../../../library/initials/theme/components/listElements/filterSimpleDropdownTheme";
import filterDropdownListTheme from "../../../../library/initials/theme/components/listElements/filterDropdownListTheme";
import filterDropdownButtonTheme from "../../../../library/initials/theme/components/listElements/filterDropdownButtonTheme";
import filterTitleTheme from "../../../../library/initials/theme/components/listElements/filterTitleTheme";
import filterInputFieldTheme from "../../../../library/initials/theme/components/listElements/filterInputFieldTheme";
import WrappedSimpleList from "../../wrappedWiglets/WrappedSimpleList";
import filterSimpleListTheme from "../../../../library/initials/theme/components/listElements/filterSimpleListTheme";
import formSelectListTheme from "../../../../library/initials/theme/components/mainElements/formSelectListTheme";


const StyledSimpleList = styled(WrappedSimpleList)`

//width: 100%;
height: 100%;
//display: flex;
//flex-direction: row;

${filterTitleTheme}

${formSelectListTheme}



`;

export default StyledSimpleList;