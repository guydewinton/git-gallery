import React, {useEffect, useRef} from "react";
import styled from "styled-components";

import WrappedSortableTable from "../../wrappedWiglets/WrappedSortableTable";
import listTableTheme from "../../../../library/initials/theme/components/listElements/listHeadedTableTheme";

const StyledOrdersTable = styled(WrappedSortableTable)`
width: 100%;
height: 100%;
//  //padding: 10rem 10rem;
//


//.WrappedListTable {
//width: height
//}

${listTableTheme}

.TableRowTextItem-1 {
  flex-grow: 3;
}
.TableRowTextItem-2 {
  flex-grow: 2;
}
.TableRowTextItem-3 {
  flex-grow: 1;
}
.TableRowTextItem-4 {
  flex-grow: 2;
}

`;

export default StyledOrdersTable;