import React from "react";
import styled from "styled-components";
import ContentHeader from "../../../containers/ContentHeader";
import sizes from "../../../../../../library/theme/definitions/sizes";

export default styled(ContentHeader)`

.BannerTitle {
h1 {
  cursor: default;
font-size: ${sizes.mainPane[1].text[1].bannerTitle[0]};
color: #4C4C4C;

}
}

    `