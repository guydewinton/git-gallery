import React, {useEffect, useRef} from "react";
import styled from "styled-components";

import sizes from "../../../../../../library/theme/definitions/sizes";
import colors from "../../../../../../library/theme/definitions/colors";

import WrappedSimpleDropdown from "../../wrappedWiglets/WrappedSimpleDropdown";

import filterSimpleDropdownTheme from "../../../../library/initials/theme/components/listElements/filterSimpleDropdownTheme";
import filterDropdownListTheme from "../../../../library/initials/theme/components/listElements/filterDropdownListTheme";
import filterDropdownButtonTheme from "../../../../library/initials/theme/components/listElements/filterDropdownButtonTheme";
import filterTitleTheme from "../../../../library/initials/theme/components/listElements/filterTitleTheme";
import filterInputFieldTheme from "../../../../library/initials/theme/components/listElements/filterInputFieldTheme";


const StyledSimpleDropdown = styled(WrappedSimpleDropdown)`

width: 100%;
display: flex;
flex-direction: row;

${filterTitleTheme}

${filterSimpleDropdownTheme}

${filterInputFieldTheme}

${filterDropdownButtonTheme}
  
${filterDropdownListTheme}

.BannerOuterWrapper {
    border: 1rem solid darkgrey;
    border-radius: 5rem;
        position: relative;
    top: 1rem;
}
.BannerInnerWrapper {
    border: none;
    //position: relative;
    //bottom: 1rem;
}

.SimpleInput {

    width: 100%;
    input {
      position: absolute;
      width: 0;
     
    }
    p {
        position: relative;
        top: 3rem;
        height: 30rem;
        width: 100%;
        font-size: ${sizes.mainPane[1].text[1].contentTextLarge[0]};
        font-family: Heebo;
        font-weight: bold;
        border: none;
        color: ${colors.mainPane[1].text[1].contentText[0]};
        background-color: transparent;
            :focus {
                outline: 0;
            }
            ::placeholder {
                color: ${colors.mainPane[1].text[1].contentPlaceholderText[0]};
            }
    }
}

.DropdownButtonWrapper {
    position: relative;
    bottom: 1rem;
}

`;

export default StyledSimpleDropdown;