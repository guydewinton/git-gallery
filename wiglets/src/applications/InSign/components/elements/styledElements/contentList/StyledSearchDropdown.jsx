import React, {useEffect, useRef} from "react";
import styled from "styled-components";

import WrappedSearchDropdown from "../../wrappedWiglets/WrappedSearchDropdown";

import colors from "../../../../../../library/theme/definitions/colors";
import filterSearchDropdownTheme from "../../../../library/initials/theme/components/listElements/filterSearchDropdownTheme";
import filterClearableInputTheme from "../../../../library/initials/theme/components/listElements/filterClearableInputTheme";
import filterDropdownListTheme from "../../../../library/initials/theme/components/listElements/filterDropdownListTheme";
import filterDropdownButtonTheme from "../../../../library/initials/theme/components/listElements/filterDropdownButtonTheme";
import filterTitleTheme from "../../../../library/initials/theme/components/listElements/filterTitleTheme";

const StyledSearchDropdown = styled(WrappedSearchDropdown)`

width: 100%;
display: flex;
flex-direction: row;

${filterTitleTheme}

${filterSearchDropdownTheme}

${filterClearableInputTheme}

${filterDropdownButtonTheme}
  
${filterDropdownListTheme}


`;

export default StyledSearchDropdown;