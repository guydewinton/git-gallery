import React from "react";
import styled from "styled-components";
import sizes from "../../../../../../library/theme/definitions/sizes";
import colors from "../../../../../../library/theme/definitions/colors";


const WrapperDiv = styled.div`
display: flex;
flex-direction: row;
padding: 5rem 10rem;

`

const TextElement = styled.p`
font-size: ${props => props.fontSize ? props.fontSize : sizes.mainPane[1].text[1].contentTextLarge[0]};
font-weight: ${props => props.fontWeight ? props.fontWeight : `600`};
white-space: nowrap;
color: ${colors.mainPane[1].text[1].contentText[0]};
`;

const KeyTextElement = styled.p`
padding-right: 10rem;
font-size: ${props => props.fontSize ? props.fontSize : sizes.mainPane[1].text[1].contentTextLarge[0]};
font-weight: ${props => props.fontWeight ? props.fontWeight : `400`};
white-space: nowrap;
color: ${colors.mainPane[1].text[1].contentText[0]};
`;

const KeyTextItem = (props) => {

return (
    <WrapperDiv>
        <KeyTextElement fontSize={props.fontSize} fontWeight={props.fontWeight}>{props.keyText}</KeyTextElement>
        <TextElement fontSize={props.fontSize} fontWeight={props.fontWeight}>{props.text}</TextElement>
    </WrapperDiv>

)

};

export default KeyTextItem