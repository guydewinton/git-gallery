import React from "react";
import styled from "styled-components";
import sizes from "../../../../../../library/theme/definitions/sizes";
import colors from "../../../../../../library/theme/definitions/colors";

const WrapperDiv = styled.div`
padding: 5rem 10rem;
`

const TextElement = styled.h1`
font-size: ${props => props.fontSize ? props.fontSize : sizes.mainPane[1].text[1].contentTextHeader[0]};
${props => props.fontWeight && `font-weight: ${props.fontWeight}`};
white-space: nowrap;
color: ${colors.mainPane[1].text[1].contentText[0]};
`;

const TextItemH = (props) => {

    return (
        <WrapperDiv>
            <TextElement fontSize={props.fontSize} fontWeight={props.fontWeight}>{props.text}</TextElement>
        </WrapperDiv>

    )

};

export default TextItemH