import React from 'react';
import styled from "styled-components";
import ArrowLeft from "../../svgElements/ArrowLeft";
import TextButton from "./TextButton";
import ButtonWrapper from "./ButtonWrapper";

const WrapperDiv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const BackButton = ({className = '', elementProps = {}}) => {

    const clickHandler = () => {
        elementProps.output && elementProps.output()
    };

    return (
        <ButtonWrapper
            elementName={'BannerNavBackButton'}
            clickHandler={clickHandler}
        >
            <WrapperDiv className={`BannerNavBackButton ${className}`}>
                <ArrowLeft />
                <TextButton elementProps={elementProps}/>
            </WrapperDiv>
        </ButtonWrapper>
    )

}

export default BackButton;