import React from 'react';
import styled from "styled-components";

const WrapperDiv = styled.div`

`;

const HeaderTitle = ({className = '', text = ''}) => {

    return (
        <WrapperDiv className={`BannerTitle ${className}`}>
            <h1 style={{whiteSpace: 'noWrap'}}>{text}</h1>
        </WrapperDiv>
    )

}

export default HeaderTitle;