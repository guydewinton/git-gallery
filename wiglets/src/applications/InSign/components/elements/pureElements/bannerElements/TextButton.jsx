import React from 'react';
import styled from "styled-components";
import ButtonWrapper from "./ButtonWrapper";

const WrapperDiv = styled.div`
`;

const TextButton = ({className = '', elementProps}) => {

    const clickHandler = () => {
        elementProps.output && elementProps.output()
    };


    return (
        <ButtonWrapper
            elementName={'TextButton'}
            onClick={clickHandler}
        >
            <WrapperDiv className={`BannerTextButton ${className}`}>
                <h2 style={{whiteSpace: 'noWrap'}}>{elementProps.label}</h2>
            </WrapperDiv>
        </ButtonWrapper>
    )

}

export default TextButton;