import React from "react";
import ContentHeader from "../containers/ContentHeader";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";
import StyledBasicInput from "../elements/styledElements/contentList/StyledBasicInput";

const Invoices = (props) => {

    const index = props.location.state.i

    const handleKeyPress = (keyCode) => {
        console.log(keyCode)
        switch(keyCode) {
            case (9): {

                break;
            }
            case (13): {

                break;
            }
            case (27): {
                console.log('here')
                props.history.push('/main/insign/invoicesList')
                break;
            }
            default:

                break;
        }
    };

    return (
        <>
            <FocusController
                defaultArray={[1,0,1]}
            />
            <StyledBasicInput
                focusIndex={[1,0,1]}
                keyOut={handleKeyPress}
                style={{opacity: 0, outline: 0, position: 'absolute'}}
                // output={(output) => handleKeyPress(output)}
            />
            <ContentHeader title={'INVOICES'}/>
            <h1 style={{fontSize: '150rem'}}>{props.invoices[index].job.jobName}</h1>
        </>
    )

}


const mapStateToProps = (state) => {
    return {
        invoices: state.invoices
    }
}

export default withRouter(connect(mapStateToProps)(Invoices))