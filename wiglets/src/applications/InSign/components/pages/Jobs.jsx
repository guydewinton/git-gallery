import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import styled from "styled-components";


import FocusTriggerer from "../../../../app_modules/FormWiglets/wiglets/FocusTriggerer/FocusTriggerer";
import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";


import BackButton from "../elements/pureElements/bannerElements/BackButton";
import TextButton from "../elements/pureElements/bannerElements/TextButton";
import ContentBodyInner from "../containers/ContentBodyInner";
import FlexRowWrapper from "../containers/FlexRowWrapper";
import ShadowBox from "../containers/ShadowBox";
import StyledBasicInput from "../elements/styledElements/contentForm/StyledBasicInput";
import StyledSearchDropdown from "../elements/styledElements/contentForm/StyledSearchDropdown";
import KeyTextItem from "../elements/pureElements/contentForm/KeyTextItem";
import TextItemP from "../elements/pureElements/contentForm/TextItemP";
import FlexColumnWrapper from "../containers/FlexColumnWrapper";
import ShadowBoxTitle from "../containers/ShadowBoxTitle";
import ContentBanners from "../containers/ContentBanners";
import sizes from "../../../../library/theme/definitions/sizes";
import TableHeadlessSimple from "../../../../app_modules/FormWiglets/wiglets/TableHeadlessSimple/TableHeadlessSimple";
import StyledJobsTable from "../elements/styledElements/contentList/StyledJobsTable";
import StyledContactsTable from "../elements/styledElements/contentForm/StyledContactsTable";
import StyledSimpleList from "../elements/styledElements/contentForm/StyledSimpleList";
import StyledJobsBrowserTable from "../elements/styledElements/contentForm/StyledJobBrowserTable";
import parseDate from "../../../../library/helpers/functions/parseDate";


const OuterWrapper = styled.div`
  height: 100%;
  width: 100%;
padding: 20rem 15rem 0rem 15rem;
`

const ElementWrapper = styled.div`
width: 100%;
  padding: 5rem 5rem 10rem 5rem;
 
`;

const headers = [
    {value: 'displayID', label: 'Name', type: 'string', edit: true},
    {value: 'position', label: 'Position', type: 'string'},
    {value: 'primaryTelephone', label: 'Telephone', type: 'string'},
];

const notesHeader = [
    {value: 'note', label: 'Note', type: 'string', edit: true},
    {value: 'category', label: 'Category', type: 'string', edit: true},
    {value: 'addedBy', label: 'Added By', type: 'string'},
    {value: 'addedDate', label: 'Added Date', type: 'string'},
];

const todosHeader = [
    {value: 'todo', label: 'Todo', type: 'string', edit: true},
    {value: 'category', label: 'Category', type: 'string'},
    {value: 'status', label: 'Status', type: 'string'},
    {value: 'completedDate', label: 'Added Date', type: 'string'},
];

const ordersHeader = [
    {value: 'description', label: 'Description', type: 'string'},
    {value: 'supplier', label: 'Supplier', type: 'string', edit: true},
    {value: 'status', label: 'Status', type: 'string'},
];

const invoicesHeader = [
    {value: 'description', label: 'Description', type: 'string'},
    {value: 'status', label: 'Status', type: 'string'},
];

const filesHeader = [
    {value: 'name', label: 'Name', type: 'string', edit: true},
    {value: 'size', label: 'Size', type: 'string'},
    {value: 'uploadedBy', label: 'Uploaded By', type: 'string'},
    {value: 'uploadDate', label: 'Uploaded Date', type: 'string'},
];

const browserList = [
    {value: 'notes', label: 'Notes'},
    {value: 'todos', label: 'Todos'},
    {value: 'orders', label: 'Orders'},
    {value: 'invoices', label: 'Invoices'},
    {value: 'files', label: 'Files'},
]

const Jobs = (props) => {

    const index = props.location.state.i;

    const [output, setOutput] = useState('');
    const [inputValues, setInputValues] = useState([]);
    const [address, setAddress] = useState('');
    const [dates, setDates] = useState([]);
    const [focusUpdate, setFocusUpdate] = useState(null)
    const [browserObject, setBrowserObject] = useState({
        header: notesHeader,
        values: props.jobs[index].notes.map((note) => {
            return {
                note: note.note,
                category: note.category,
                addedBy: note.addedBy,
                addedDate: parseDate(note.addedDate)
            }
        }),
        instanceName: 'notes',
    })






    useEffect(() => {

        setInputValues(() => {
            return props.jobs.map((job) => {
                return {
                    jobName: job.jobName,
                    jobNumber: job.jobNumber,
                    id: job.id
                }
            })
        });

        setAddress(() => {
            return `${props.jobs[index].address.street.join(', ')}, ${props.jobs[index].address.city}, ${props.jobs[index].address.postCode}`
        });

        setDates(() => {
            return [
                parseDate(props.jobs[index].dates.startDate),
                parseDate(props.jobs[index].dates.statutoryDate),
                parseDate(props.jobs[index].dates.completionDate),
            ]
        })
    }, [props.jobs]);

    const returnToList = () => {
        props.history.push('/main/insign/jobsList')
    };

    const handleBrowserSelection = (selection) => {
        switch (selection.value) {
            case 'notes': {
                setBrowserObject({
                    header: notesHeader,
                    values: props.jobs[index].notes.map((note) => {
                        return {
                            note: note.note,
                            category: note.category,
                            addedBy: note.addedBy,
                            addedDate: parseDate(note.addedDate)
                        }
                    }),
                    instanceName: 'notes',
                })
                setFocusUpdate([4,0,1])
                break;
            }
            case 'todos': {
                setBrowserObject({
                    header: todosHeader,
                    values: props.jobs[index].todos.map((todo) => {
                        return {
                            todo: todo.todo,
                            category: todo.category,
                            status: todo.status,
                            completedDate: parseDate(todo.completedDate)
                        }
                    }),
                    instanceName: 'todos',
                })
                setFocusUpdate([4,0,1])
                break;
            }
            case 'orders': {
                setBrowserObject({
                    header: ordersHeader,
                    values: props.jobs[index].orders.map((order) => {
                        return {
                            supplier: order.supplier,
                            description: order.description,
                            status: `${order.status}: ${parseDate(order.date)} by ${order.person}`,
                        }
                    }),
                    instanceName: 'orders',
                })
                setFocusUpdate([4,0,1])
                break;
            }
            case 'invoices': {
                setBrowserObject({
                    header: invoicesHeader,
                    values: props.jobs[index].invoices.map((invoice) => {
                        return {
                            description: invoice.description,
                            status: `${invoice.category}: ${parseDate(invoice.date)} by ${invoice.person}`,
                        }
                    }),
                    instanceName: 'invoices',
                })
                setFocusUpdate([4,0,1])
                break;
            }
            case 'files': {
                setBrowserObject({
                    header: filesHeader,
                    values: props.jobs[index].files.map((file) => {
                        return {
                            name: file.name,
                            size: file.size,
                            uploadedBy: file.uploadedBy,
                            uploadedDate: parseDate(file.uploadDate)
                        }
                    }),
                    instanceName: 'files',
                })
                setFocusUpdate([4,0,1])
                break;
            }
        }
    };

    const handleLoop = (keyCode) => {
        console.log(keyCode)
        if (keyCode === 9 || keyCode === 13) {
            setFocusUpdate([2, 1, 0])
        } else if (keyCode === 27) {

        }
    }

    const handleKeyPress = (keyCode) => {
        switch(keyCode) {
            case (9): {

               break;
            }
            case (13): {

                break;
            }
            case (27): {
                    returnToList();
                break;
            }
            default:

                break;
        }
    };

    return (
        <>
            <ContentBanners
                headerTitle={'JOB'}
                TopLeftElement={{component: BackButton, props: {
                        label: 'Jobs List',
                        output: () => returnToList()
                    }}}
                TopRightElement={{component: TextButton, props: {
                        label: 'View Job Log'
                    }}}
                BottomLeftElement={{component: TextButton, props: {
                        label: 'Delete'
                    }}}
                BottomCenterElement={{component: TextButton, props: {
                        label: 'Mark As Completed'
                    }}}
                BottomRightElement={{component: TextButton, props: {
                        label: 'Print'
                    }}}
                topLeftOut
                topRightOut
                bottomLeftOut
                bottomCenterOut
                bottomRightOut
            >
                <FocusController
                    defaultArray={[2,1,0]}
                    focusUpdate={focusUpdate}
                />
                {/*<FocusTriggerer*/}
                {/*    focusIndex={[1,0,1]}*/}
                {/*    keyOut={handleKeyPress}*/}
                {/*    style={{opacity: 0, outline: 0, position: 'absolute'}}*/}
                {/*/>*/}
                <ContentBodyInner>
                <FlexRowWrapper
                    className={'rowOne'}
                    flexGrow={'0'}
                >
                    <ShadowBoxTitle
                        title={'Job Number'}
                        divWidth={'280rem'}
                    >
                            <div style={{height: '100%', overflow: 'visible'}}>
                                <div style={{height: '0', overflow: 'visible', position: 'relative', bottom: '8rem'}}>
                                    <h1 style={{fontSize: '80rem'}}>{props.jobs[index].jobNumber}</h1>
                                </div>
                            </div>
                    </ShadowBoxTitle>
                    <ShadowBoxTitle title={'Job Information'} flexGrow={'auto'}>
                        <FlexRowWrapper>

                            <FlexColumnWrapper flexGrow={'3'}>
                                <ElementWrapper>
                                    <StyledBasicInput
                                        title={'Job Name:'}
                                        focusIndex={[2,1,0]}
                                        initValue={props.jobs[index].jobName}
                                        keyOut={handleKeyPress}
                                    />
                                </ElementWrapper>
                                <ElementWrapper>
                                    <StyledBasicInput
                                        title={'Address:'}
                                        focusIndex={[2,3,0]}
                                        initValue={address}
                                        // keyOut={handleKeyPress}
                                    />
                                </ElementWrapper>
                            </FlexColumnWrapper>
                            <FlexColumnWrapper flexGrow={'2'} className={'HERE!!!!'}>
                                <ElementWrapper>
                                    <StyledSearchDropdown
                                        title={'Client:'}
                                        focusIndex={[2,2,0]}
                                        inputValues={props.clients}
                                        initValue={props.jobs[index].client.id}
                                        // keyOut={handleKeyPress}
                                    />
                                </ElementWrapper>
                                <ElementWrapper>
                                    <StyledSearchDropdown
                                        title={'Project Manager:'}
                                        focusIndex={[2,4,0]}
                                        inputValues={props.employees}
                                        initValue={props.jobs[index].projectManager.id}
                                        // keyOut={handleKeyPress}
                                    />
                                </ElementWrapper>
                            </FlexColumnWrapper>
                        </FlexRowWrapper>
                    </ShadowBoxTitle>
                </FlexRowWrapper>
                <FlexRowWrapper className={'rowTwo'} flexGrow={'0'}>
                    <FlexColumnWrapper>
                        <ShadowBoxTitle title={'Values'}>
                            <FlexColumnWrapper
                                flexAlignItems={'flex-end'}
                            >
                                <TextItemP text={'Contract Sum'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]}/>
                                <TextItemP text={'Percentage Complete'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]}/>
                                <TextItemP text={'Value Complete'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]}/>
                                <TextItemP text={'Percentage Invoiced'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]}/>
                                <TextItemP text={'Value Invoiced'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]}/>
                                <TextItemP text={'Percentage Remaining'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]}/>
                                <TextItemP text={'Balance Remaining'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]}/>
                            </FlexColumnWrapper>
                            <FlexColumnWrapper
                                flexAlignItems={'flex-start'}
                            >
                                <TextItemP text={'$ 2 000 000'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]} fontWeight={'600'}/>
                                <TextItemP text={'30%'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]} fontWeight={'600'}/>
                                <TextItemP text={'$ 250 000'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]} fontWeight={'600'}/>
                                <TextItemP text={'25%'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]} fontWeight={'600'}/>
                                <TextItemP text={'$ 197 000'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]} fontWeight={'600'}/>
                                <TextItemP text={'75%'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]} fontWeight={'600'}/>
                                <TextItemP text={'$ 1 750 000'} fontSize={sizes.mainPane[1].text[1].contentTextMedium[0]} fontWeight={'600'}/>
                            </FlexColumnWrapper>
                        </ShadowBoxTitle>
                    </FlexColumnWrapper>
                    <FlexColumnWrapper
                        flexGrow={'auto'}
                        divWidth={'100%'}
                        divHeight={'100%'}
                    >
                        <ShadowBoxTitle
                            title={'Dates'}
                            flexGrow={'0'}
                            divWidth={'100%'}
                        >
                            <FlexRowWrapper
                                flexJustifyContent={'space-around'}
                                divWidth={'100%'}
                            >
                                <KeyTextItem keyText={'Scheduled Start:'} text={dates[0]} fontSize={'16rem'}/>
                                <KeyTextItem keyText={'Statutory Signage:'} text={dates[1]} fontSize={'16rem'}/>
                                <KeyTextItem keyText={'Scheduled Completion:'} text={dates[2]} fontSize={'16rem'}/>
                            </FlexRowWrapper>
                        </ShadowBoxTitle>
                        <FlexRowWrapper
                            flexGrow={'auto'}
                            divHeight={'100%'}
                            divWidth={'100%'}
                        >
                            <ShadowBoxTitle title={'Contacts'}
                                divWidth={'100%'}
                                            innerPadding={false}
                            >
                                <StyledContactsTable
                                    inputValues={props.jobs[index].contacts}
                                    headerValues={headers}
                                    instanceName={'lala'}
                                    focusIndex={[2,5,0]}
                                    filterValue={output}
                                    filterIndex={1}
                                />
                            </ShadowBoxTitle>
                        </FlexRowWrapper>
                    </FlexColumnWrapper>
                </FlexRowWrapper>
                <FlexRowWrapper className={'rowThree'} flexGrow={'auto'}>
                    <OuterWrapper>
                        <ShadowBox>
                            <StyledSimpleList
                                focusIndex={[3,0,1]}
                                inputValues={browserList}
                                outputValue={handleBrowserSelection}
                            />
                            <StyledJobsBrowserTable
                                inputValues={browserObject.values}
                                headerValues={browserObject.header}
                                instanceName={browserObject.instanceName}
                                focusIndex={[4,0,1]}
                                filterValue={output}
                                filterIndex={1}
                                keyOut={{9: handleLoop, 13: handleLoop}}
                                floatButtons={''}
                            />
                        </ShadowBox>
                    </OuterWrapper>
                </FlexRowWrapper>
                </ContentBodyInner>
            </ContentBanners>
        </>
    )

}

const mapStateToProps = (state) => {
    return {
        jobs: state.jobs,
        employees: state.employees,
        clients: state.clients
    }
}

export default withRouter(connect(mapStateToProps)(Jobs))
