import React, {Component, useEffect, useState} from "react";
import styled from "styled-components";
import {connect} from "react-redux";
import {withRouter} from "react-router";

import ContentHeader from "../containers/ContentHeader";
import ContentBody from "../frames/ContentBody";
import ShadowBox from "../containers/ShadowBox";

import StyledJobsTable from "../elements/styledElements/contentList/StyledJobsTable";
import StyledBasicInput from "../elements/styledElements/contentList/StyledBasicInput";

import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";
import StyledHeaderWrapper from "../elements/styledElements/contentList/StyledHeaderWrapper";
import StyledTodosTable from "../elements/styledElements/contentList/StyledTodosTable";
import StyledDateRanger from "../elements/styledElements/contentList/StyledDateRanger";
import StyledSimpleDropdown from "../elements/styledElements/contentList/StyledSimpleDropdown";



const WrapperDiv = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const FilterWrapper = styled.div`
  height: 60rem;
  margin: 20rem 0;
`;

const ListWrapper = styled.div`
  height: calc(100% - 180rem);
  width: 100%;
  //margin: 20rem 0;
`;

const FilterDiv = styled.div`
  width: 100%;
  padding: 0 50rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;



const TestBox = styled.div`
background-color: grey;
height: 20rem;
width: 50rem;
`

const headers = [
    {value: 'jobNumber', label: 'Job Number', type: 'number'},
    {value: 'jobName', label: 'Job Name', type: 'string'},
    {value: 'clientName', label: 'Client', type: 'string'},
    {value: 'startDate', label: 'Start Date', type: 'date'},
    {value: 'completionDate', label: 'End Date', type: 'date'},
]

const JobsList = (props) => {

    const [output, setOutput] = useState({name: '', client: '', date: []});
    const [inputValues, setInputValues] = useState([]);

    useEffect(() => {
        setInputValues(() => {
            return props.jobs.map((job) => {
                return {
                    jobName: job.jobName,
                    jobNumber: job.jobNumber,
                    clientName: job.client.displayID,
                    startDate: job.dates.startDate,
                    completionDate: job.dates.completionDate,
                    id: job.id
                }
            })
        })
    }, [props.jobs]);

    const setTableOutput = (output) => {
        props.history.push('/main/insign/jobDetail', output)
    };

    return (
        <WrapperDiv>
            <FocusController
                defaultArray={[1,0,0]}
            />
            <ContentBody>
                <StyledHeaderWrapper title={'JOBS LIST'}/>
                <FilterWrapper>
                    <ShadowBox>
                        <FilterDiv style={{zIndex:500}}>
                            <StyledBasicInput
                                title={'Job: '}
                                focusIndex={[1,0,0]}
                                outputValue={(output) => setOutput(prev => {return {...prev, name: output}})}
                            />
                            <StyledBasicInput
                                title={'Client: '}
                                focusIndex={[1,0,1]}
                                outputValue={(output) => setOutput(prev => {return {...prev, client: output}})}
                            />
                            <StyledDateRanger
                                titleIn={'Date From: '}
                                titleOut={'Date To: '}
                                focusIndex={[1,0,2]}
                                outputValue={(output) => setOutput(prev => {return {...prev, date: output}})}
                            />
                            <StyledSimpleDropdown
                                title={'Status: '}
                                inputValues={[]}
                                focusIndex={[1,0,3]}
                            />
                        </FilterDiv>
                    </ShadowBox>
                </FilterWrapper>
                <ListWrapper>
                    <ShadowBox>
                        <StyledJobsTable
                            inputValues={inputValues}
                            headerValues={headers}
                            instanceName={'lala'}
                            focusIndex={[2,0,1]}
                            filterArray={
                                [
                                    {value: output.name, index: 1},
                                    {value: output.client, index: 2},
                                    {value: output.date, index: [3,4]},
                                ]
                            }
                            filterValue={output}
                            filterIndex={1}
                            ButtonElementLeft={TestBox}
                            ButtonElementRight={TestBox}
                            outputValue={setTableOutput}
                        />
                    </ShadowBox>
                </ListWrapper>
            </ContentBody>
        </WrapperDiv>
    )

};

const mapStateToProps = (state) => {
    return {
        jobs: state.jobs
    }
}

export default withRouter(connect(mapStateToProps)(JobsList))