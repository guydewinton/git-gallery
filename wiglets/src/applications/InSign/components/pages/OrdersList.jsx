import React, {useEffect, useState} from "react";
import styled from "styled-components";
import ContentHeader from "../containers/ContentHeader";
import ContentBody from "../frames/ContentBody";
import ShadowBox from "../containers/ShadowBox";

import Calendar from 'react-calendar';
import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";
import StyledBasicInput from "../elements/styledElements/contentList/StyledBasicInput";
import StyledJobsTable from "../elements/styledElements/contentList/StyledJobsTable";
import {connect} from "react-redux";
import StyledOrdersTable from "../elements/styledElements/contentList/StyledOrdersTable";
import StyledHeaderWrapper from "../elements/styledElements/contentList/StyledHeaderWrapper";
import StyledTodosTable from "../elements/styledElements/contentList/StyledTodosTable";

const WrapperDiv = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const FilterWrapper = styled.div`
  height: 60rem;
  margin: 20rem 0;
`;

const ListWrapper = styled.div`
  height: calc(100% - 180rem);
  width: 100%;
  //margin: 20rem 0;
`;

const FilterDiv = styled.div`
  width: 500rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const headers = [
    {value: 'jobNumber', label: 'Job Number', type: 'number'},
    {value: 'jobName', label: 'Job Name', type: 'string'},
    {value: 'supplier', label: 'Supplier', type: 'string'},
    {value: 'description', label: 'Description', type: 'string'},
    {value: 'id', label: 'ID', type: 'number'}
]

const TestBox = styled.div`
background-color: red;
height: 20rem;
width: 50rem;
`

const OrdersList = (props) => {

    const [output, setOutput] = useState('')
    const [inputValues, setInputValues] = useState([]);

    useEffect(() => {
        setInputValues(() => {
            return props.orders.map((order) => {
                return {
                    jobNumber: order.job.jobNumber,
                    jobName: order.job.jobName,
                    supplier: order.supplier.displayID,
                    description: order.description,
                    id: order.id
                }
            })
        })
    }, [props.orders]);

    const setTableOutput = (output) => {
        props.history.push('/main/insign/orderDetail', output)
    };

    return (
        <WrapperDiv>
            <FocusController
                defaultArray={[1,0,0]}
            />
            <ContentBody>
                <StyledHeaderWrapper title={'ORDERS LIST'}/>
                <FilterWrapper>
                    <ShadowBox>
                        <FilterDiv style={{zIndex:500}}>
                            <StyledBasicInput
                                title={'Job: '}
                                focusIndex={[1,0,0]}
                                outputValue={(output) => setOutput(output)}
                            />
                        </FilterDiv>
                    </ShadowBox>
                </FilterWrapper>
                <ListWrapper>
                    <ShadowBox>
                        <StyledOrdersTable
                            inputValues={inputValues}
                            headerValues={headers}
                            instanceName={'lala'}
                            focusIndex={[2,0,1]}
                            filterValue={output}
                            filterIndex={0}
                            ButtonElementLeft={TestBox}
                            ButtonElementRight={TestBox}
                            outputValue={setTableOutput}
                        />
                    </ShadowBox>
                </ListWrapper>
            </ContentBody>
        </WrapperDiv>
    )

};

const mapStateToProps = (state) => {
    return {
        orders: state.orders
    }
}

export default connect(mapStateToProps)(OrdersList)