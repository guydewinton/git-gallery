import React, {useEffect, useState} from "react";
import ContentHeader from "../containers/ContentHeader";
import ContentBody from "../frames/ContentBody";
import ShadowBox from "../containers/ShadowBox";
import styled from "styled-components";
import PageController from "../../../../app_modules/PageController/PageController";
import ListSimple from "../../../../app_modules/FormWiglets/wiglets/ListSimple/ListSimple";
import StyledSimpleDropdown from "../elements/styledElements/contentList/StyledSimpleDropdown";
import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";
import StyledSimpleList from "../elements/styledElements/contentList/StyledSimpleList";
import StyledSideNavList from "../elements/styledElements/sideNav/StyledSideNavList";
import StyledBasicInput from "../elements/styledElements/contentList/StyledBasicInput";
import StyledJobsTable from "../elements/styledElements/contentList/StyledJobsTable";
import {connect} from "react-redux";
import StyledTodosTable from "../elements/styledElements/contentList/StyledTodosTable";
import StyledHeaderWrapper from "../elements/styledElements/contentList/StyledHeaderWrapper";

const WrapperDiv = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const FilterWrapper = styled.div`
  height: 60rem;
  margin: 20rem 0;
`;

const ListWrapper = styled.div`
  height: calc(100% - 180rem);
  width: 100%;
  //margin: 20rem 0;
`;

const FilterDiv = styled.div`
  width: 500rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const headers = [
    {value: 'description', label: 'Description', type: 'string'},
    {value: 'type', label: 'Type', type: 'string'},
    {value: 'name', label: 'Ref Name', type: 'string'},
    {value: 'assignment', label: 'Assignees', type: 'string'},
]

const TestBox = styled.div`
background-color: red;
height: 20rem;
width: 50rem;
`



const TodosList = (props) => {

    const [output, setOutput] = useState('');
    const [inputValues, setInputValues] = useState([]);

    useEffect(() => {
        const returnArray = props.todos.map((todo) => {
            const assignment = todo.assignment.map((assignment) => {
                return assignment.displayID
            }).join(', ')
            return {
                description: todo.description,
                type: todo.refObject.type,
                name: todo.refObject.name,
                assignment: assignment,
            }
        })
        console.log(returnArray)
        setInputValues(returnArray)
    }, [props.todos]);

    const setTableOutput = (output) => {
        props.history.push('/main/insign/todoDetail', output)
    };

    return (
        <WrapperDiv>
            <FocusController
                defaultArray={[1,0,0]}
            />
            <ContentBody>
                <StyledHeaderWrapper title={'TODOS LIST'}/>
                <FilterWrapper>
                    <ShadowBox>
                        <FilterDiv style={{zIndex:500}}>
                            <StyledBasicInput
                                title={'Job: '}
                                focusIndex={[1,0,0]}
                                outputValue={(output) => setOutput(output)}
                            />
                        </FilterDiv>
                    </ShadowBox>
                </FilterWrapper>
                <ListWrapper>
                    <ShadowBox>
                        <StyledTodosTable
                            inputValues={inputValues}
                            headerValues={headers}
                            instanceName={'lala'}
                            focusIndex={[2,0,1]}
                            filterValue={output}
                            filterIndex={0}
                            ButtonElementLeft={TestBox}
                            ButtonElementRight={TestBox}
                            outputValue={setTableOutput}
                        />
                    </ShadowBox>
                </ListWrapper>
            </ContentBody>
        </WrapperDiv>
    )

};

const mapStateToProps = (state) => {
    return {
        todos: state.todos
    }
}

export default connect(mapStateToProps)(TodosList)