import React, {useCallback, useMemo, useState} from "react";
import ContentHeader from "../containers/ContentHeader";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";
import StyledBasicInput from "../elements/styledElements/contentList/StyledBasicInput";
import TodosProxyTest from "./TodoProxyTest";

import styled from "styled-components";
import store from "../../store/store";


const Button = styled.div`
height: 100rem;
width: 100rem;
background-color: red;
`

const createProxy = (object, handler) => {
    return new Proxy(object, handler)
}

const baseObject = {
        text: {text: 0},
        test: Boolean(true)
    }

const createHandler = (callBack) => {
    return {
        set: (object, prop, value) => {
            console.log('SET!!! => prop: ', prop)
            object.text = value + 1
            callBack()
            return true
        },
        get: (target, property, receiver) => {
            console.log('GET!!!', target, property, receiver)
            return target.text
        }
    }
}

const baseHandler = createHandler(() => console.log('lala'))




const proxyObject = createProxy(baseObject, baseHandler)

const Todos = (props) => {

    const index = props.location.state.i

    const [text, setText] = useState({text: '1'})





    const handleClick = () => {
        proxyObject.text = proxyObject.text.text
        setText({text: 'weee'})
    };

    console.log(proxyObject.text)

    return (
        <>
            <FocusController
                defaultArray={[1,0,1]}
            />
            <ContentHeader title={'TODOS'}/>
            <h1 style={{fontSize: '150rem'}}>{props.todos[index].description}</h1>
            <TodosProxyTest text={proxyObject.text.text} {...{someProps: proxyObject.text.text}}/>
            <Button
                onClick={handleClick}
            />
            <p style={{fontSize: '20rem'}}>{text.text}</p>
            <p style={{fontSize: '20rem'}}>{proxyObject.text.text}</p>
        </>
    )

}

const mapStateToProps = (state) => {
    return {
        todos: state.todos
    }
}

export default withRouter(connect(mapStateToProps)(Todos))