import React, {useState} from "react";



const TodosProxyTest = (props) => {

    console.log(props.text)

    return (
        <>
            <h1 style={{fontSize: '50rem'}}>{props.text}</h1>
            <h1 style={{fontSize: '50rem'}}>{props.someProps}</h1>


        </>
    )

}

const mapStateToProps = (state) => {
    return {
        todos: state.todos
    }
}

export default TodosProxyTest