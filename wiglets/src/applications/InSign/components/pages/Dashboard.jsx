import React, {useState} from "react";
import ShadowBox from "../containers/ShadowBox";
import styled from "styled-components";
import DropdownSearch from "../../../../app_modules/FormWiglets/wiglets/DropdownSearch/DropdownSearch";
import DatePicker from "../../../../app_modules/FormWiglets/wiglets/DatePicker/DatePicker";
import StyledSearchDropdown from "../elements/styledElements/contentList/StyledSearchDropdown";
import StyledDatePicker from "../elements/styledElements/contentList/StyledDatePicker";
import StyledDateRanger from "../elements/styledElements/contentList/StyledDateRanger";
import PageController from "../../../../app_modules/PageController/PageController";
import StyledSimpleDropdown from "../elements/styledElements/contentList/StyledSimpleDropdown";
import StyledBasicInput from "../elements/styledElements/contentList/StyledBasicInput";

import contactInitial from "../../library/initials/data/primaryLists/personInitial";
import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";
import {connect} from "react-redux";

import longListInitial from "../../library/initials/data/primaryLists/longListInitial";
import StyledJobsBrowserTable from "../elements/styledElements/contentForm/StyledJobBrowserTable";
import StyledSimpleList from "../elements/styledElements/contentForm/StyledSimpleList";
import StyledTestSimpleList from "../elements/styledElements/contentForm/StyledTestSimpleList";
import StyledJobsTable from "../elements/styledElements/contentList/StyledJobsTable";

const BodyDiv = styled.div`
  height: 100%;
  width: 100%;
  padding: 100rem;
`;

const headerValues = [
    {value: 'CompanyName', label: 'Description', type: 'string'},
]


const Dashboard = (props) => {

    const inputValues = longListInitial.map((item) => {
        return {
            value: item.CompanyName,
            label: item.CompanyName,
        }
    })

    return (
        <BodyDiv>
            <ShadowBox>
                <FocusController
                    defaultArray={[1,0,3]}
                />
                <div style={{padding: '12rem'}}>
                    <StyledBasicInput
                        title={'Blah: '}
                        focusIndex={[1,0,0]}
                    />
                    <div style={{padding: '12rem'}}/>
                    <StyledSearchDropdown
                        inputValues={inputValues}
                        title={'Job: '}
                        focusIndex={[1,0,1]}
                    />
                    <div style={{padding: '12rem'}}/>
                    <StyledSimpleDropdown
                        title={'Status: '}
                        inputValues={inputValues}
                        focusIndex={[1,0,2]}
                    />
                    <div style={{padding: '12rem'}}/>
                    <StyledDatePicker
                        title={'Date: '}
                        focusIndex={[1,0,3]}
                    />
                    <div style={{padding: '12rem'}}/>
                    <StyledDateRanger
                        titleIn={'Date From: '}
                        titleOut={'Date To: '}
                        focusIndex={[1,0,4]}
                    />
                    <div style={{padding: '12rem'}}/>
                    {/*<StyledJobsTable*/}
                    {/*    inputValues={longListInitial}*/}
                    {/*    focusIndex={[1,0,5]}*/}
                    {/*    filterIndex={0}*/}
                    {/*    headerValues={headerValues}*/}
                    {/*/>*/}
                </div>
            </ShadowBox>
        </BodyDiv>
            )
}

const mapStateToProps = (state) => {
    return {
        jobs: state.jobs
    }
}

export default connect(mapStateToProps)(Dashboard)