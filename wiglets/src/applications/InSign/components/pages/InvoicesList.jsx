import React, {useEffect, useState} from "react";
import styled from "styled-components";

import ContentHeader from "../containers/ContentHeader";
import ContentBody from "../frames/ContentBody";
import ShadowBox from "../containers/ShadowBox";

import PageController from "../../../../app_modules/PageController/PageController";
import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";
import StyledBasicInput from "../elements/styledElements/contentList/StyledBasicInput";
import StyledJobsTable from "../elements/styledElements/contentList/StyledJobsTable";
import {connect} from "react-redux";
import StyledInvoicesTable from "../elements/styledElements/contentList/StyledInvoicesTable";
import StyledHeaderWrapper from "../elements/styledElements/contentList/StyledHeaderWrapper";
import StyledTodosTable from "../elements/styledElements/contentList/StyledTodosTable";

const WrapperDiv = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const FilterWrapper = styled.div`
  height: 60rem;
  margin: 20rem 0;
`;

const ListWrapper = styled.div`
  height: calc(100% - 180rem);
  width: 100%;
  //margin: 20rem 0;
`;

const FilterDiv = styled.div`
  width: 500rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const headers = [
    {value: 'jobNumber', label: 'Job Number', type: 'number'},
    {value: 'jobName', label: 'Job Name', type: 'string'},
    {value: 'client', label: 'Client', type: 'string'},
    {value: 'id', label: 'ID', type: 'number'}
]

const TestBox = styled.div`
background-color: red;
height: 20rem;
width: 50rem;
`


const inputValues = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'children', label: 'Children' },
    { value: 'chow', label: 'Chow' },
    { value: 'chocoholic', label: 'Chocoholic' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'strand', label: 'Strand' },
    { value: 'string', label: 'String' },
    { value: 'vanilla', label: 'Vanilla' },
    { value: 'blue', label: 'Blue' },
];


const InvoicesList = (props) => {

    const [output, setOutput] = useState('');
    const [inputValues, setInputValues] = useState([]);

    useEffect(() => {
        setInputValues(() => {
            return props.invoices.map((invoice) => {
                return {
                    jobNumber: invoice.job.jobNumber,
                    jobName: invoice.job.jobName,
                    client: invoice.customer.displayID,
                    id: invoice.id
                }
            })
        })
    }, [props.invoices]);

    const setTableOutput = (output) => {
        props.history.push('/main/insign/invoiceDetail', output)
    };

    return (
        <WrapperDiv>
            <FocusController
                defaultArray={[1,0,0]}
            />
            <ContentBody>
                <StyledHeaderWrapper title={'INVOICES LIST'}/>
                <FilterWrapper>
                    <ShadowBox>
                        <FilterDiv style={{zIndex:500}}>
                            <StyledBasicInput
                                title={'Job: '}
                                focusIndex={[1,0,0]}
                                outputValue={(output) => setOutput(output)}
                            />
                        </FilterDiv>
                    </ShadowBox>
                </FilterWrapper>
                <ListWrapper>
                    <ShadowBox>
                        <StyledInvoicesTable
                            inputValues={inputValues}
                            headerValues={headers}
                            instanceName={'lala'}
                            focusIndex={[2,0,1]}
                            filterValue={output}
                            filterIndex={1}
                            ButtonElementLeft={TestBox}
                            ButtonElementRight={TestBox}
                            outputValue={setTableOutput}
                        />
                    </ShadowBox>
                </ListWrapper>
            </ContentBody>
        </WrapperDiv>
    )

};

const mapStateToProps = (state) => {
    return {
        invoices: state.invoices
    }
}

export default connect(mapStateToProps)(InvoicesList)