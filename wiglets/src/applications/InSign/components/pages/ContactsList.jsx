import React, {useEffect, useState} from "react";

import styled from "styled-components";
import FocusController from "../../../../app_modules/FormWiglets/wiglets/FocusController/FocusController";
import ContentBody from "../frames/ContentBody";
import ContentHeader from "../containers/ContentHeader";
import ShadowBox from "../containers/ShadowBox";
import StyledBasicInput from "../elements/styledElements/contentList/StyledBasicInput";
import StyledJobsTable from "../elements/styledElements/contentList/StyledJobsTable";
import {connect} from "react-redux";
import StyledContactsTable from "../elements/styledElements/contentList/StyledContactsTable";
import StyledHeaderWrapper from "../elements/styledElements/contentList/StyledHeaderWrapper";
import StyledTodosTable from "../elements/styledElements/contentList/StyledTodosTable";

const WrapperDiv = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const FilterWrapper = styled.div`
  height: 60rem;
  margin: 20rem 0;
`;

const ListWrapper = styled.div`
  height: calc(100% - 180rem);
  width: 100%;
  //margin: 20rem 0;
`;

const FilterDiv = styled.div`
  width: 500rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const headers = [
    {value: 'displayID', label: 'Name', type: 'string'},
]

const TestBox = styled.div`
background-color: red;
height: 20rem;
width: 50rem;
`


const ContactsList = (props) => {

    const [output, setOutput] = useState('')
    const [inputValues, setInputValues] = useState([]);

    useEffect(() => {
        setInputValues(() => {
            return props.contacts.map((contact) => {
                return {
                    displayID: contact.displayID,
                    id: contact.id
                }
            })
        })
    }, [props.contacts]);

    const setTableOutput = (output) => {
        console.log(output)
        props.history.push('/main/insign/contactDetail', output)
    };

    return (
        <WrapperDiv>
            <FocusController
                defaultArray={[1,0,0]}
            />
            <ContentBody>
                <StyledHeaderWrapper title={'CONTACTS LIST'}/>
                <FilterWrapper>
                    <ShadowBox>
                        <FilterDiv style={{zIndex:500}}>
                            <StyledBasicInput
                                title={'Job: '}
                                focusIndex={[1,0,0]}
                                outputValue={(output) => setOutput(output)}
                            />
                        </FilterDiv>
                    </ShadowBox>
                </FilterWrapper>
                <ListWrapper>
                    <ShadowBox>
                        <StyledContactsTable
                            inputValues={inputValues}
                            headerValues={headers}
                            instanceName={'lala'}
                            focusIndex={[2,0,1]}
                            filterValue={output}
                            filterIndex={0}
                            ButtonElementLeft={TestBox}
                            ButtonElementRight={TestBox}
                            outputValue={setTableOutput}
                        />
                    </ShadowBox>
                </ListWrapper>
            </ContentBody>
        </WrapperDiv>
    )

};

const mapStateToProps = (state) => {
    return {
        contacts: state.contacts
    }
}

export default connect(mapStateToProps)(ContactsList)