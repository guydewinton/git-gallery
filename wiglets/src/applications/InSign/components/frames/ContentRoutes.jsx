import React from "react";
import styled from "styled-components";

import colors from "../../../../library/theme/definitions/colors";
import {Route, Switch} from "react-router-dom";
import Dashboard from "../pages/Dashboard";
import JobsList from "../pages/JobsList";
import TodosList from "../pages/TodosList";
import OrdersList from "../pages/OrdersList";
import InvoicesList from "../pages/InvoicesList";
import ContactsList from "../pages/ContactsList";
import Jobs from "../pages/Jobs";
import Todos from "../pages/Todos";
import Orders from "../pages/Orders";
import Invoices from "../pages/Invoices";
import Contacts from "../pages/Contacts";

const BodyDiv = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  background-color: ${colors.mainPane[1].frames[1].contentBackground[0]};
`;

const ContentRoutes = (props) => {

    return (
        <BodyDiv>
            <Route
                exact path={'/main/insign/dash'}
                component={(props) => <Dashboard {...props} />}
            />
            <Route
                exact path={'/main/insign/jobsList'}
                component={(props) => <JobsList {...props} />}
            />
            <Route
                exact path={'/main/insign/jobDetail'}
                component={(props) => <Jobs {...props} />}
            />
            <Route
                exact path={'/main/insign/todosList'}
                component={(props) => <TodosList {...props} />}
            />
                <Route
                    exact path={'/main/insign/todoDetail'}
                    component={(props) => <Todos {...props} />}
                />
            <Route
                exact path={'/main/insign/ordersList'}
                component={(props) => <OrdersList {...props} />}
            />
                <Route
                    exact path={'/main/insign/orderDetail'}
                    component={(props) => <Orders {...props} />}
                />
            <Route
                exact path={'/main/insign/invoicesList'}
                component={(props) => <InvoicesList {...props} />}
            />
                <Route
                    exact path={'/main/insign/invoiceDetail'}
                    component={(props) => <Invoices {...props} />}
                />
            <Route
                exact path={'/main/insign/contactsList'}
                component={(props) => <ContactsList {...props} />}
            />
                <Route
                    exact path={'/main/insign/contactDetail'}
                    component={(props) => <Contacts {...props} />}
                />
        </BodyDiv>
    )
}


export default ContentRoutes