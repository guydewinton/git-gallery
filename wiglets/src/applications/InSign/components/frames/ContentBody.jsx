import React from 'react';
import styled from "styled-components";

const WrapperDiv = styled.div`
  height: 100%;
  width: 100%;
  padding: 0 30rem;`;

const ContentBody = (props) => {

    return <WrapperDiv>
        {props.children}
    </WrapperDiv>

}

export default ContentBody;