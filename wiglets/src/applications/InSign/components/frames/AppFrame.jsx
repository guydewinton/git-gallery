import React from "react";
import styled from "styled-components";
import SideBar from "./SideBar";
import ContentRoutes from "./ContentRoutes";

const BodyDiv = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: row;
  overflow: hidden;
  background-color: aqua;
`;

const AppFrame = (props) => {

    return (
        <BodyDiv>
            <SideBar/>
            <ContentRoutes/>
        </BodyDiv>
    )
};


export default AppFrame