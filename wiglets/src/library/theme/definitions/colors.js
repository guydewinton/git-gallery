// export default {
//     panes: [
//         'Panes',
//         {
//             navBackground: ['#3D3D3D'],
//             content: ['#EFEFEF'],
//         }
//     ],
//     panes2: [
//         'Panes',
//         {
//             nav: [
//                 'Nav',
//                 {
//                     navBackground: ['#3D3D3D'],
//                 },
//             ],
//             content: [
//                 'Content',
//                 {
//                     content: ['#EFEFEF'],
//                 }
//             ]
//         },
//     ],
//     text: [
//         'Text',
//         {
//         navHeaderUp: ['#E8E8E8'],
//         navHeaderOver: ['#565656'],
//         navHeaderDown: ['#666666'],
//         navHeaderActive: ['#565656'],
//         navLinksUp: ['#D6D6D6'],
//         navLinksOver: ['#565656'],
//         navLinksDown: ['#666666'],
//         navLinksActive: ['#565656'],
//         contentHeader: [''],
//         contentTitle: ['#606060'],
//         contentText: ['#606060'],
//         contentInputUp: ['#606060'],
//         contentInputOver: ['#606060'],
//         contentInputActive: ['#606060'],
//         contentInputPlaceholder: ['#606060'],
//         contentDropdownItemUp: ['#606060'],
//         contentDropdownItemOver: ['#FFFFFF'],
//         contentDropdownItemDown: [''],
//         contentDropdownItemSelected: ['#FFFFFF'],
//         contentListHeader: [''],
//         contentListItem: [''],
//         }
//     ],
//     buttons: [
//         'Buttons',
//         {
//         navOver: ['#BCBCBC'],
//         navDown: ['#AAAAAA'],
//         navActive: ['#D8D8D8'],
//         contentDropdownUp: ['#7A7A7A'],
//         contentDropdownOver: ['#575757'],
//         contentDropdownDown: [''],
//         contentDropdownActive: [''],
//         contentDropdownItemUp: [''],
//         contentDropdownItemOver: ['#a1a1a1'],
//         contentDropdownItemDown: [''],
//         contentDropdownItemSelected: ['#575757'],
//         contentDropdownItemHighlighted: ['#ececec'],
//         contentDropdownItemDisabled: ['#f0f0f0'],
//         contentClearInputOver: [''],
//         contentClearInputDown: [''],
//         }
//     ],
//     lines: [
//         'Lines',
//         {
//         navBreak: ['#575757'],
//         contentInputUnderline: ['solid 2rem #AAAAAA'],
//         contentInputUnderlineOver: [''],
//         contentInputUnderlineActive: [''],
//         }
//     ],
//     borders: [
//         'Borders',
//         {
//         dropdownBox: ['solid 1rem #D3D3D3'],
//         }
//     ],
//     shadows: [
//         'Shadows',
//         {
//         defaultShadow: ['0 4rem 8rem 0 rgba(0, 0, 0, 0.03), 0 6rem 20rem 0 rgba(0, 0, 0, 0.03)'],
//         }
//     ],
//     scroll: [
//         'Scroll',
//         {
//         thumb: ['#7A7A7A'],
//         track: ['#f0f0f0']
//         }
//     ]
// }

export default {

    sideBar: [
        'Sidebar',
        {
            buttons: [
                'Buttons',
                {
                    navButtonOver: ['#BCBCBC'],
                    navButtonDown: ['#AAAAAA'],
                    navButtonActive: ['#D8D8D8'],
                }
            ],
            text: [
                'Text',
                {
                    navHeaderUp: ['#E8E8E8'],
                    navHeaderOver: ['#565656'],
                    navHeaderDown: ['#666666'],
                    navHeaderActive: ['#565656'],
                    navLinksUp: ['#D6D6D6'],
                    navLinksOver: ['#565656'],
                    navLinksDown: ['#666666'],
                    navLinksActive: ['#565656'],
                }
            ],
            frames: [
                'Frames',
                {
                    sideBarBackground: ['#3D3D3D'],
                }
            ]

        }
    ],
    mainPane: [
        'Main Pane',
        {
            buttons: [
                'Buttons',
                {
                    bannerButtonUp: '',
                    bannerButtonOver: '',
                    bannerButtonDown: '',
                    bannerButtonActive: '',
                    widgetButtonUp: ['#7A7A7A'],
                    widgetButtonOver: ['#575757'],
                    widgetButtonDown: [''],
                    widgetButtonActive: [''],
                    contentListItemUp: [''],
                    contentListItemOver: ['#a1a1a1'],
                    contentListItemDown: [''],
                    contentListItemSelected: ['#575757'],
                    contentListItemHighlighted: ['#ececec'],
                    contentListItemDisabled: ['#f0f0f0'],
                }
            ],
            text: [
                'Text',
                {
                    bannerHeader: '',
                    bannerButtonUp: '',
                    bannerButtonOver: '',
                    bannerButtonDown: '',
                    bannerButtonActive: '',
                    shadowBoxHeader: [''],
                    contentListItemUp: ['#606060'],
                    contentListItemOver: ['#FFFFFF'],
                    contentListItemDown: [''],
                    contentListItemSelected: ['#FFFFFF'],
                    contentPlaceholderText: ['#606060'],
                    contentKeyText: ['#606060'],
                    contentText: ['#606060'],
                }
            ],
            frames: [
                'Frames',
                {
                    topBannerBackground: '',
                    bottomBannerBackground: '',
                    contentBackground: ['#EFEFEF'],
                    filterBarBackground: '',
                    shadowBoxBackground: '',
                    scrollThumb: ['#7A7A7A'],
                    scrollTrack: ['#f0f0f0']
                }
            ],
        }
    ]


};