const breaks = {
    mobilePortrait: ['300px'],
    mobileLandscape_tabletPortrait: ['600px'],
    tabletLandscape_laptopSmall: ['900px'],
    laptopBig_desktopSmall: ['1100px'],
    desktopBig: ['1700px'],
};

export default {
    mobilePortrait: [`(min-width: ${breaks.mobilePortrait})`],
    mobileLandscape_tabletPortrait: [`(min-width: ${breaks.mobileLandscape_tabletPortrait})`],
    tabletLandscape_laptopSmall: [`(min-width: ${breaks.tabletLandscape_laptopSmall})`],
    laptopBig_desktopSmall: [`(min-width: ${breaks.laptopBig_desktopSmall})`],
    desktopBig: [`(min-width: ${breaks.desktopBig})`],
};

// show also include min heights