export default {
    sideBar: [
        'Sidebar',
        {
            text: [
                'Text',
                {
                    headerText: ['20rem'],
                    linkText: ['18rem'],
                }
            ],
            buttons: [
                'Buttons',
                {

                }
            ],
        },
    ],
    mainPane: [
        'Main Pane',
        {
            text: [
                'Text',
                {
                    contentListTextLarge: ['18rem'],
                    contentListTextSmall: ['16rem'],
                    bannerTitle: ['20rem'],
                    bannerButton: ['16rem'],
                    shadowBoxTitle: ['18rem'],
                    contentTextSmall: ['14rem'],
                    contentTextMedium: ['16rem'],
                    contentTextLarge: ['18rem'],
                    contentTextHeader: ['20rem'],
                }
            ],
            buttons: [
                'Buttons',
                {

                }
            ],
            scroll: [
                'Scroll',
                {
                    bigScrollWidth: ['10rem'],
                    bigScrollBorderRadius: ['5rem'],
                }
            ],
        },
    ],
    frames: [
        'Frames',
        {
        topNavHeight: ['0rem'],
        }
    ],
}