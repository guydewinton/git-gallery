export default {

    sideBar: [
        'Sidebar',
        {
            nav: [
                'Nav',
                {
                    navBreak: '',
                }
            ]
        }

    ],
    mainPane: [
        'Main Pane',
        {
            widgets: [
                'Widgets',
                {
                    contentInputUnderline: ['solid 1rem #AAAAAA'],
                    dropdownBoxBorder: ['solid 1rem #D3D3D3'],
                    dropdownShadow: ['0 4rem 8rem 0 rgba(0, 0, 0, 0.03), 0 6rem 20rem 0 rgba(0, 0, 0, 0.03)'],
                }
            ],
            frames: [
                {
                    shadowBoxShadow: ['0 4rem 8rem 0 rgba(0, 0, 0, 0.03), 0 6rem 20rem 0 rgba(0, 0, 0, 0.03)'],
                }
            ]
        }
    ]


};