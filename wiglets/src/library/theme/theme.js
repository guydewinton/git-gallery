import colors from "./definitions/colors";
import breaks from "./definitions/breaks";
import sizes from "./definitions/sizes";
import effects from "./definitions/effects";

export default {
    colors,
    breaks,
    sizes,
    effects,
};