export default (date) => {

    const getDay = () => {
        if (date.getDate().toString().length === 1) {
            return `0${date.getDate()}`
        } else {
            return date.getDate()
        }
    };

    const getMonth = () => {
        if (date.getMonth().toString().length === 1) {
            return `0${date.getMonth()}`
        } else {
            return date.getMonth()
        }
    };

    return `${getDay()}/${getMonth()}/${date.getFullYear()}`
};