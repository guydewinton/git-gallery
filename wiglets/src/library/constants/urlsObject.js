const urlsObject = {
    apiBaseUrl: '127.0.0.1:8000/',
    auth: {
        signup: 'api/auth/registration/',
        validate: 'api/auth/registration/validate/',
        signin: 'api/auth/token/',
        signout: '',
        refresh: 'api/auth/token/refresh/'
    },
};

export default urlsObject