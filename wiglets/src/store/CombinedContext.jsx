import React from "react";

import {AppProvider} from "./contexts/AppContext";
import {FocusProvider} from "../app_modules/FormWiglets/store/MasterFocusContext";

export default (props) => {
    return (
        <FocusProvider>
        <AppProvider>
            {props.children}
        </AppProvider>
        </FocusProvider>
    )
}