import React, {createContext, useState} from 'react';

export const AppContext = createContext();

export const AppProvider = (props) => {

    const [activeApp, setActiveApp] = useState(0)

    const valueObject = {
        activeApp,
        setActiveApp,
    };

    return <AppContext.Provider value={valueObject}>{props.children}</AppContext.Provider>

};
