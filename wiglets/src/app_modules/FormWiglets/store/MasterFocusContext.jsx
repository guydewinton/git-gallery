import React, {createContext, useEffect, useState} from 'react';

export const MasterFocusContext = createContext();

let pageLoadGetter = false

export const FocusProvider = (props) => {

    const [focusObject, setFocusObject] = useState({});
    const [currentFocus, setCurrentFocus] = useState([0,0,0]);
    const [update, setUpdate] = useState([0,0])
    const [pageLoad, setPageLoad] = useState(false);
    const [keyCode, setKeyCode] = useState([null])

    const pageLoadSetter = (bool) => {
        pageLoadGetter = bool;
        setPageLoad(bool);
    };

    useEffect(() => {
        if (update[0] === update[1] && update[0] !== 0) {
            pageLoadSetter(true)
        }
    }, [update]);

    useEffect(() => {
        if (pageLoad) {
            if (Object.keys(focusObject).length > 0) {
                if (focusObject[currentFocus[0]]) {
                    if (focusObject[currentFocus[0]][currentFocus[1]]) {
                        if (focusObject[currentFocus[0]][currentFocus[1]][currentFocus[2]]) {
                            focusObject[currentFocus[0]][currentFocus[1]][currentFocus[2]].focus()
                        }
                    }
                }
            }
        }
    }, [currentFocus, focusObject, pageLoad]);


    const getRefArray = (pageIndex, clusterIndex) => {

        return Object.keys(focusObject[pageIndex][clusterIndex])
            .sort((a, b) => {
                let higher = parseInt(a);
                let lower = parseInt(b);
                if (lower > higher) return -1;
                if (higher > lower) return 1;
                return 0;
            });
    };

    const getClusterArray = (pageIndex) => {
        return Object.keys(focusObject[pageIndex])
            .sort((a, b) => {
                let higher = parseInt(a);
                let lower = parseInt(b);
                if (lower > higher) return -1;
                if (higher > lower) return 1;
                return 0;
            });
    };

    const getPageArray = () => {
        return Object.keys(focusObject)
            .sort((a, b) => {
                let higher = parseInt(a);
                let lower = parseInt(b);
                if (lower > higher) return -1;
                if (higher > lower) return 1;
                return 0;
            });
    };

    useEffect(() => {
        if (currentFocus.length === 3 && pageLoadGetter) {
            switch (keyCode[0]) {
                case 9: {   //tab
                    // const refArray = getRefArray(currentFocus[0], currentFocus[1]);
                    // const currentRefIndex = refArray.indexOf(currentFocus[2].toString());
                    // if (currentRefIndex === refArray.length - 1) {
                    //     setCurrentFocus([currentFocus[0], currentFocus[1], refArray[0]])
                    // } else {
                    //     setCurrentFocus([currentFocus[0], currentFocus[1], refArray[currentRefIndex + 1]])
                    // }
                    // break;
                    const refArray = getRefArray(currentFocus[0], currentFocus[1]);
                    const currentRefIndex = refArray.indexOf(currentFocus[2].toString());
                    if (currentRefIndex < refArray.length - 1) {
                        setCurrentFocus([currentFocus[0], currentFocus[1], refArray[currentRefIndex + 1]])
                    } else {
                        const clusterArray = getClusterArray(currentFocus[0]);
                        const currentClusterIndex = clusterArray.indexOf(currentFocus[1].toString());
                        if (currentClusterIndex === clusterArray.length - 1) {
                            const pageArray = getPageArray();
                            const currentPageIndex = pageArray.indexOf(currentFocus[0].toString());
                            if (pageArray.length === 1) {
                            } else if (currentPageIndex === pageArray.length - 1) {
                                const nextClusterArray = getClusterArray(pageArray[0]);
                                const nextRefArray = getRefArray(pageArray[0], nextClusterArray[0]);
                                setCurrentFocus([pageArray[0], nextClusterArray[0], nextRefArray[0]])
                            } else {
                                const nextClusterArray = getClusterArray(pageArray[currentPageIndex + 1]);
                                const nextRefArray = getRefArray(pageArray[currentPageIndex + 1], nextClusterArray[0]);
                                setCurrentFocus([pageArray[currentPageIndex + 1], nextClusterArray[0], nextRefArray[0]])
                            }
                        } else {
                            const nextRefArray = getRefArray(currentFocus[0], clusterArray[currentClusterIndex + 1]);
                            setCurrentFocus([currentFocus[0], clusterArray[currentClusterIndex + 1], nextRefArray[0]])
                        }
                    }
                    break;
                }
                case 13: {  //enter
                    const clusterArray = getClusterArray(currentFocus[0]);
                    const currentClusterIndex = clusterArray.indexOf(currentFocus[1].toString());
                    if (currentClusterIndex === clusterArray.length - 1) {
                        const pageArray = getPageArray();
                        const currentPageIndex = pageArray.indexOf(currentFocus[0].toString());
                        if (pageArray.length === 1) {
                        } else if (currentPageIndex === pageArray.length - 1) {
                            const nextClusterArray = getClusterArray(pageArray[0]);
                            const nextRefArray = getRefArray(pageArray[0], nextClusterArray[0]);
                            setCurrentFocus([pageArray[0], nextClusterArray[0], nextRefArray[0]])
                        } else {
                            const nextClusterArray = getClusterArray(pageArray[currentPageIndex + 1]);
                            const nextRefArray = getRefArray(pageArray[currentPageIndex + 1], nextClusterArray[0]);
                            setCurrentFocus([pageArray[currentPageIndex + 1], nextClusterArray[0], nextRefArray[0]])
                        }
                    } else {
                        const nextRefArray = getRefArray(currentFocus[0], clusterArray[currentClusterIndex + 1]);
                        setCurrentFocus([currentFocus[0], clusterArray[currentClusterIndex + 1], nextRefArray[0]])
                    }

                    break;
                }
                case 27: {  // esc
                    const pageArray = getPageArray()
                    const currentPageIndex = pageArray.indexOf(currentFocus[0].toString());
                    const clusterArray = getClusterArray(currentFocus[0]);
                    const currentClusterIndex = clusterArray.indexOf(currentFocus[1].toString());
                    if (currentClusterIndex !== 0) {
                        const previousRefArray = getRefArray(pageArray[currentPageIndex], clusterArray[0]);
                        setCurrentFocus([pageArray[currentPageIndex], clusterArray[0], previousRefArray[0]])
                    } else if (currentPageIndex !== 0) {
                        const nextClusterArray = getClusterArray(pageArray[currentPageIndex - 1]);
                        const nextRefArray = getRefArray(pageArray[currentPageIndex - 1], nextClusterArray[0]);
                        setCurrentFocus([pageArray[currentPageIndex - 1], nextClusterArray[0], nextRefArray[0]])
                    }
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }, [keyCode])

    const updateFocus = (keyCode) => {
        setKeyCode([keyCode])
    };

    const addFocusRef = (focusArray, inputRef) => {

        setFocusObject(prevObject => {

            if (focusArray.length !== 3) {
                throw 'updateFocusObject: incorrect focusArray length';
            }

            const returnFocusObject = {...prevObject};

            if (!returnFocusObject[focusArray[0]]) {
                returnFocusObject[focusArray[0]] = {};
            }

            if (!returnFocusObject[focusArray[0]][focusArray[1]]) {
                returnFocusObject[focusArray[0]][focusArray[1]] = {};
            }

            if (!returnFocusObject[focusArray[0]][focusArray[1]][focusArray[2]]) {
                returnFocusObject[focusArray[0]][focusArray[1]][focusArray[2]] = inputRef;
            } else {
                throw `updateFocusObject: focusArray at index ${focusArray[2]} already contains a reference`;
            }
            return returnFocusObject
        })
    };

    const removeFocusRef = (focusArray) => {
        setFocusObject(prev => {
            const returnFocusObject = {...prev}
            delete returnFocusObject[focusArray[0]][focusArray[1]][focusArray[2]];
            if (Object.keys(returnFocusObject[focusArray[0]][focusArray[1]]).length === 0) {
                delete returnFocusObject[focusArray[0]][focusArray[1]];
                if (Object.keys(returnFocusObject[focusArray[0]]).length === 0) {
                    delete returnFocusObject[focusArray[0]];
                }
            }
            return returnFocusObject
        });
    };

    const valueObject = {
        updateFocus,
        addFocusRef,
        removeFocusRef,
        focusObject,
        setCurrentFocus,
        setPageLoad,
        pageLoad,
        update,
        setUpdate,
        pageLoadSetter
    };


    return <MasterFocusContext.Provider value={valueObject}>{props.children}</MasterFocusContext.Provider>

};
