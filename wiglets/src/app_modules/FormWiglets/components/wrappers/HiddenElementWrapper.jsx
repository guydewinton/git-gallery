import React, {useContext, useEffect, useRef} from 'react';
import InputElementActions from "../../wiglets/ListSimple/store/actions/InputHiddenActions";

const HiddenElementWrapper = ({
                          className = 'defaultClass',
                          instanceName = 'defaultInstance',
                          contextObject,
                          components,
                      }) => {

    const {InnerElement, HiddenElement} = components;

    return (
        <>
            <InnerElement className={className} instanceName={instanceName} contextObject={contextObject} components={components}/>
            <HiddenElement contextObject={contextObject}/>
        </>

    )
};

export default HiddenElementWrapper;