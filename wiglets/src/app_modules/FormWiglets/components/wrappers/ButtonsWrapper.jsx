import React, {useContext, useEffect, useRef} from 'react';
import InputElementActions from "../../wiglets/ListSimple/store/actions/InputHiddenActions";

const ButtonsWrapper = ({
                                  className = 'defaultClass',
                                  instanceName = 'defaultInstance',
                                  contextObject,
                                  components,
                              }) => {

    const {InnerElement, ButtonsArray} = components;

    const ButtonElements = ButtonsArray.map((Button, i) => {
        return (
            <div>
                <Button.component elementProps={Button.props} contextObject={contextObject}/>
            </div>
        )
    });

    return (
        <>
            <InnerElement className={className} instanceName={instanceName} contextObject={contextObject} components={components}/>
            {ButtonElements}
        </>

    )
};

export default ButtonsWrapper;