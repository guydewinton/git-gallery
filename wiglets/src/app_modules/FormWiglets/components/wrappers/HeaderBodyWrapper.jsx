import React, {useContext, useEffect} from 'react';

const HeaderBodyWrapper = ({
                          className = 'defaultClass',
                          instanceName = 'defaultInstance',
                          contextObject,
                          components,
                      }) => {

    const {HeaderElement, BodyElement} = components;

    return (
        <div className={`HeaderBodyWrapper ${className}-HeaderBodyWrapper ${instanceName}-HeaderBodyWrapper`}>
            <div className={`HeaderWrapper ${className}-HeaderWrapper ${instanceName}-HeaderWrapper`}>
                <div className={`HeaderElementWrapper ${className}-HeaderElementWrapper ${instanceName}-HeaderElementWrapper`}>
                    <HeaderElement className={className} instanceName={instanceName} elementName={'HeaderElement'} contextObject={contextObject} components={components}/>
                </div>
            </div>
            <div className={`BodyWrapper ${className}-BodyWrapper ${instanceName}-BodyWrapper`}>
                <div className={`BodyElementWrapper ${className}-BodyElementWrapper ${instanceName}-BodyElementWrapper`}>
                    <BodyElement className={className} instanceName={instanceName} elementName={'BodyElement'} contextObject={contextObject} components={components}/>
                </div>
            </div>
        </div>
    )
};

export default HeaderBodyWrapper;