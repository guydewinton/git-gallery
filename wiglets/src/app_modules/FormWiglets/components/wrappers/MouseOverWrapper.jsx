import React, {useContext} from 'react';

const MouseOverBanner = ({
                            className = 'defaultClass',
                            instanceName = 'defaultInstance',
                            contextObject,
                            components,
                        }) => {

    const {setMouseOverBanner} = useContext(contextObject.FocusContext);
    const {InnerElement} = components;

    const handleMouseOverBanner = () => {
        setMouseOverBanner(true)
    };

    const handleMouseOutBanner = () => {
        setMouseOverBanner(false)
    };


    return (
        <div className={`MouseOverBannerWrapper ${className}-MouseOverBannerWrapper ${instanceName}-MouseOverBannerWrapper`}>
            <div
                className={`BannerOuterWrapper ${className}-BannerOuterWrapper ${instanceName}-BannerOuterWrapper`}
                onMouseEnter={handleMouseOverBanner}
                onMouseLeave={handleMouseOutBanner}
            >
                <div className={`BannerInnerWrapper ${className}-BannerInnerWrapper ${instanceName}-BannerInnerWrapper`}>
                    <div className={`InnerElementWrapper ${className}-InnerElementWrapper ${instanceName}-InnerElementWrapper`}>
                        <InnerElement className={className} instanceName={instanceName} elementName={'InnerElement'} contextObject={contextObject} components={components}/>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default MouseOverBanner;