import React, {useContext, useState, useEffect} from 'react';

const BannerDropdownWrapper = ({
    className = 'defaultClass',
    instanceName = 'defaultInstance',
    contextObject,
    components,
}) => {

    const {setMouseOverBanner, setMouseOverDropdown, showDropdown} = useContext(contextObject.FocusContext);
    const {InnerElement, DropdownButton, DropdownElement} = components;

    const [displayClass, setDisplayClass] = useState('INVISIBLE');
    const [toggleDisplay, setToggleDisplay] = useState(false);

    useEffect(() => {
        if (showDropdown) {
            setDisplayClass('VISIBLE')
                setToggleDisplay(true)
        } else {
            setDisplayClass('INVISIBLE')
            // setTimeout(() => {
                setToggleDisplay(false)
            // }, 150)
        }
    }, [showDropdown])

    const handleMouseOverBanner = () => {
        setMouseOverBanner(true)
    };

    const handleMouseOutBanner = () => {
        setMouseOverBanner(false)
    };

    const handleMouseOverDropdown = () => {
        setMouseOverDropdown(true)
    };

    const handleMouseOutDropdown = () => {
        setMouseOverDropdown(false)
    };

    return (
        <div className={`BannerDropdownWrapper ${className}-BannerDropdownWrapper ${instanceName}-BannerDropdownWrapper`}>
            <div
                className={`BannerOuterWrapper ${className}-BannerOuterWrapper ${instanceName}-BannerOuterWrapper`}
                onMouseEnter={handleMouseOverBanner}
                onMouseLeave={handleMouseOutBanner}
            >
                <div className={`BannerInnerWrapper ${className}-BannerInnerWrapper ${instanceName}-BannerInnerWrapper`}>
                    <div className={`InnerElementWrapper ${className}-InnerElementWrapper ${instanceName}-InnerElementWrapper`}>
                        <InnerElement className={className} instanceName={instanceName} elementName={'InnerElement'} contextObject={contextObject} components={components}/>
                    </div>
                    <div className={`DropdownButtonWrapper ${className}-DropdownButtonWrapper ${instanceName}-DropdownButtonWrapper`}>
                        <DropdownButton className={className} instanceName={instanceName} elementName={'DropdownButton'} contextObject={contextObject} components={components}/>
                    </div>
                </div>
            </div>
            {toggleDisplay &&
            <div
                className={`DropdownOuterWrapper ${className}-DropdownOuterWrapper ${instanceName}-DropdownOuterWrapper DropdownOuterWrapper_${displayClass} ${className}-DropdownOuterWrapper_${displayClass} ${instanceName}-DropdownOuterWrapper_${displayClass}`}
                onMouseEnter={handleMouseOverDropdown}
                onMouseLeave={handleMouseOutDropdown}
                style={{zIndex: 9999}}
            >

                <div
                    className={`DropdownInnerWrapper ${className}-DropdownInnerWrapper ${instanceName}-DropdownInnerWrapper`}

                >
                    <div
                        className={`DropdownElementWrapper ${className}-DropdownElementWrapper ${instanceName}-DropdownElementWrapper`}>
                        <DropdownElement className={className} instanceName={instanceName}
                                         elementName={'DropdownElement'} contextObject={contextObject}
                                         components={components}/>
                    </div>
                </div>

            </div>
            }
        </div>
    )
};

export default BannerDropdownWrapper;