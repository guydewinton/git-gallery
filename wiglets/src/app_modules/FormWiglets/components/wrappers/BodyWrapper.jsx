import React, {useContext, useEffect} from 'react';

const HeaderBodyWrapper = ({
                          className = 'defaultClass',
                          instanceName = 'defaultInstance',
                          contextObject,
                          components,
                      }) => {

    const {BodyElement} = components;

    return (
            <div className={`BodyWrapper ${className}-BodyWrapper ${instanceName}-BodyWrapper`}>
                <div className={`BodyElementWrapper ${className}-BodyElementWrapper ${instanceName}-BodyElementWrapper`}>
                    <BodyElement className={className} instanceName={instanceName} elementName={'BodyElement'} contextObject={contextObject} components={components}/>
                </div>
            </div>
    )
};

export default HeaderBodyWrapper;