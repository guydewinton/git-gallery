import React, {useContext, useEffect, useMemo, useRef, useState} from "react";
import TableItemEditActions from "../../wiglets/TableSortEdit/store/actions/TableItemEditActions";

const TableItemEdit = ({className, instanceName, elementName, contextObject, value, i}) => {

    const [inputValue, setInputValue] = useState('');
    const {FocusContext, TableItemEditActions} = contextObject;

    const {activeFocus, focus} = useContext(FocusContext);

    const {blurHandler, keyDownHandler, registerInput, focusHandler, clickHandler} = TableItemEditActions()

    const inputRef = useRef(null);

    useEffect(() => {
        registerInput([i, inputRef.current])
    }, []);

    useEffect(() => {
        if (value) {
            setInputValue(value)
        }
    }, [value]);

    const handleFocus = () => {
        focusHandler(i)
    };

    const handleBlur = () => {
        blurHandler(i)
    };

    const handleChange = (event) => {
        setInputValue(event.target.value)
    };

    const handleKeyDown = (event) => {
        keyDownHandler(event)
    };

    const handleClick = () => {
        clickHandler(i)
    }


    return useMemo(() => {
        return (
            <input
                className={`TableItemEdit ${className}-TableItemEdit ${instanceName}-TableItemEdit-${i[1]} TableItemEdit-${i} ${className}-TableItemEdit-${i[1]} ${instanceName}-TableItemEdit-${i[1]} ${elementName} ${elementName}-TableItemEdit-${i[1]} ${elementName}-TableItemEdit ${className}-${elementName} ${instanceName}-${elementName}`}
                ref={inputRef}
                value={inputValue}
                onChange={handleChange}
                onFocus={handleFocus}
                onBlur={handleBlur}
                onKeyDown={handleKeyDown}
                onClick={handleClick}
            />
            )
    }, [value, inputValue, activeFocus, focus])

};

export default TableItemEdit