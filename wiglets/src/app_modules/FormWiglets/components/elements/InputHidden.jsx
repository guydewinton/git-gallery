import React, {useContext, useEffect, useRef} from 'react';
import InputElementActions from "../../wiglets/ListSimple/store/actions/InputHiddenActions";

const HiddenInputWrapper = ({
                                className = 'defaultClass',
                                instanceName = 'defaultInstance',
                                contextObject,
                                components,
                            }) => {

    const inputField = useRef(null);

    const {InputHiddenActions, FocusContext} = contextObject;

    const {keyDownHandler, focusHandler, blurHandler} = InputHiddenActions();

    const {setInputRef} = useContext(FocusContext);


    useEffect(() => {
        setInputRef(inputField.current);
    }, []);

    const handleFocus = () => {
        focusHandler()
    };

    const handleBlur = () => {
        blurHandler()
    };

    const handleKeyDown = (event) => {
        keyDownHandler(event)
    };

    return (
       <input
            ref={inputField}
            style={{opacity: 0, outline: 0, position: 'absolute'}}
            onKeyDown={handleKeyDown}
            onFocus={handleFocus}
            onBlur={handleBlur}
            readOnly
       />

    )
};

export default HiddenInputWrapper;