import React, {useContext, useEffect, useRef, useState} from 'react';

const InputReadOnly = ({className, instanceName, elementName, contextObject}) => {

    const {FocusContext, StateContext, ReadOnlyInputActions} = contextObject;

    const {
        changeHandler,
        keyDownHandler,
        clickHandler,
        focusHandler,
        blurHandler,
    } = ReadOnlyInputActions();

    const {setInputRef, mouseOverBanner, focus} = useContext(FocusContext);
    const {input} = useContext(StateContext);

    const inputField = useRef(null);

    useEffect(() => {
        setInputRef(inputField.current);
    }, []);

    const handleClick = (event) => {
        clickHandler(event)
    };

    const handleChange = (event) => {
        changeHandler(event)
    };

    const handleFocus = (event) => {
        focusHandler(event)
    };

    const handleBlur = (event) => {
        blurHandler(event)
    };

    const handleKeyDown = (event) => {
        keyDownHandler(event)
    };

    return (
        <div
            className={`SimpleInput ${className}-SimpleInput ${instanceName}-SimpleInput ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
            style={{display: 'flex'}}
            onClick={handleClick}
        >
            <input
                className={`SimpleInputField ${className}-SimpleInputField ${instanceName}-SimpleInputField ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
                style={{opacity: 0, outline: 0, position: 'absolute'}}
                ref={inputField}
                onFocus={handleFocus}
                onBlur={handleBlur}
                onChange={handleChange}
                onKeyDown={handleKeyDown}
            />
            <p>{input}</p>
        </div>
    )

};

export default InputReadOnly;