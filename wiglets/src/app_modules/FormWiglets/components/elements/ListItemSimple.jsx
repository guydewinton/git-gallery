import React, {memo, useContext, useEffect, useLayoutEffect, useMemo, useRef, useState} from 'react';

const ListItemSimple = ({className, instanceName, elementName, contextObject, value, i}) => {

    const {StateContext, FocusContext, ListItemSimpleActions} = contextObject;

    const {clickHandler, rowFocusHandler} = ListItemSimpleActions();

    const {selection, setSelection} = useContext(StateContext);

    const {focus} = useContext(FocusContext);

    const [selected, setSelected] = useState('INACTIVE_OPTION');

    const rowFocus = useRef(null);

    useEffect(() => {
        if (focus) {
            if (selection === i) {
                setSelected('SELECTED_OPTION');
                rowFocus.current.focus()
            } else {
                setSelected('INACTIVE_OPTION');
            }
        }

    }, [selection, focus]);

    const handleMouseEnter = () => {
        setSelection(i);
    };

    const handleClick = () => {
        clickHandler(value.value, value.label, i)
    };

    const handleFocus = () => {
        rowFocusHandler(i)
    }

    return useMemo(() => {
        return (
            <div
                className={`SimpleListItem SimpleListItem-${selected} ${className}-SimpleListItem ${instanceName}-SimpleListItem ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
                style={{position: 'relative'}}
                onClick={handleClick}
                onMouseEnter={handleMouseEnter}
            >
                <input
                    className={`rowFocus_${i}`}
                    id={`rowFocus_${i}`}
                    ref={rowFocus}
                    style={{height: '100%', width: '0', outline: 'none', background: 'none', opacity: '0', position: 'absolute'}}
                    onFocus={handleFocus}
                    readOnly
                />
                <p
                    className={`SimpleListItemP ${className}-SimpleListItemP ${instanceName}-SimpleListItemP ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
                >{value.label}</p>
            </div>
        )
    }, [value, selected])

};

export default ListItemSimple;