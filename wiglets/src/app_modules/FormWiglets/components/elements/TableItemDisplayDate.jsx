import React, {useEffect, useMemo} from "react";
import parseDate from "../../library/helpers/functions/parseDate";

const TableItemDisplayDate = ({className, instanceName, elementName, contextObject, value, i}) => {


    return useMemo(() => {
        return (
            <p
                className={`TextItem ${className}-TextItem ${instanceName}-TextItem-${i} TextItem-${i} ${className}-TextItem-${i} ${instanceName}-TextItem-${i} ${elementName} ${elementName}-TextItem-${i} ${elementName}-TextItem ${className}-${elementName} ${instanceName}-${elementName}`}
            >
                {parseDate(value)}
            </p>
        )
    }, [value])

};

export default TableItemDisplayDate