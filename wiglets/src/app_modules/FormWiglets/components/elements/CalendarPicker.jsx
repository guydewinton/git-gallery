import React, {useContext, useEffect, useLayoutEffect, useState} from 'react';

import Calendar from "react-calendar";

const CalendarPicker = ({className, instanceName, elementName, contextObject}) => {

    const {StateContext, FocusContext, CalendarPickerActions} = contextObject;

    const {
        dateValue,
        setDateValue,
        changeHandler,
        clickHandler
    } = CalendarPickerActions();

    const {setInputRef, mouseOverBanner} = useContext(FocusContext);
    const {input, startDateIndex} = useContext(StateContext);
    const [activeStartDate, setActiveStartDate] = useState([])

    const handleChange = (date) => {
        changeHandler(date)
    };

    useLayoutEffect(() => {
        if (startDateIndex === 0) {
            setActiveStartDate([dateValue[0], 'month'])
        } else if (startDateIndex === 1) {
            setActiveStartDate([dateValue[1], 'month'])
        } else {
            setActiveStartDate([dateValue, 'month'])
        }

    }, [dateValue])

    const handleActiveStartDateChange = ({activeStartDate, view}) => {
        console.log(activeStartDate, view)
        setActiveStartDate([activeStartDate, view])
    }

    return (
        <div className={'react-calendar-wrapper'}>
            <Calendar
                className={[`${className}-react-calendar`]}
                minDetail={'year'}
                onChange={handleChange}
                value={dateValue}
                view={activeStartDate[1]}
                activeStartDate={activeStartDate[0]}
                onActiveStartDateChange={handleActiveStartDateChange}

            />
        </div>
    )

};

export default CalendarPicker;