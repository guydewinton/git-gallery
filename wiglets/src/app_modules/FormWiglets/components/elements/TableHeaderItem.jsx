import React, {useContext, useEffect, useState} from 'react';
import ButtonDropdown from "./ButtonDropdown";
import SVGDownArrow from "./SVGDownArrow";

const TableHeaderItem = ({className, instanceName, elementName, buttonClasses, contextObject, components, value, i}) => {
    const {StateContext, SortableListRowActions} = contextObject;

    const {TableRowTextItem} = components;

    // const {clickHandler} = SortableListRowActions();

    const {headerValues, listValues, sortColumn, sortIndex, sortDescending} = useContext(StateContext);

    const [sortArrow, setSortArrow] = useState('');

    useEffect(() => {

        if (i === sortIndex) {
            if (sortDescending) {
                setSortArrow('SortArrowDown')
            } else {
                setSortArrow('SortArrowUp')
            }
        } else {
            setSortArrow('SortArrowHidden')
        }
    }, [sortIndex, sortDescending])


    const handleClick = () => {
        sortColumn(i)
    };

    return (
        <>
            <div className={`TableRowTextItem-head-${i} ${buttonClasses}`}/>
            <div
                className={`TableRowTextItem TableRowTextItem-${i} ${instanceName}-TableRowTextItem-${i} ${className} ${buttonClasses} ${instanceName} ${elementName} ${elementName}-TableRowTextItem ${elementName}-TableRowTextItem-${i}`}
                onClick={handleClick}
                key={`${i}_${value.value}`}
            >

                <TableRowTextItem
                    className={className}
                    instanceName={instanceName}
                    elementName={elementName}
                    contextObject={contextObject}
                    value={value.label}
                    i={i}
                />

                <div className={`TableRowTextItem-divider TableRowTextItem-${i}-divider`}>
                    <SVGDownArrow elementName={sortArrow}/>
                </div>
            </div>
        </>
    )


};

export default TableHeaderItem;