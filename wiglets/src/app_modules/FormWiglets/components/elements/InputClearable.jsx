import React from 'react';
import InputSimple from "./InputSimple";
import ButtonClear from "./ButtonClear";

const InputClearable = ({className, instanceName, elementName, contextObject}) => {

    const {ClearableInputActions} = contextObject;

    const {
        changeHandler,
        keyDownHandler,
        clickHandler,
        focusHandler,
        blurHandler,
        clearHandler,
    } = ClearableInputActions();

    const SimpleInputActions = () => {
        return {
            changeHandler,
            keyDownHandler,
            clickHandler,
            focusHandler,
            blurHandler
        }
    };

    const ClearButtonActions = () => {
        return {clearHandler}
    };

    contextObject = {...contextObject, ClearButtonActions, SimpleInputActions};

    return (
        <div className={`ClearableInput ${className}-ClearableInput ${instanceName}-ClearableInput ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}>
            <InputSimple
                className={className}
                instanceName={instanceName}
                elementName={elementName}
                contextObject={contextObject}
            />
            <ButtonClear
                className={className}
                instanceName={instanceName}
                elementName={elementName}
                contextObject={contextObject}
            />
        </div>
    )

};

export default InputClearable;