import React, {useContext, useEffect, useRef} from 'react';

const InputSimple = ({className, instanceName, elementName, contextObject}) => {

    const {FocusContext, StateContext, SimpleInputActions} = contextObject;

    const {
        changeHandler,
        keyDownHandler,
        clickHandler,
        focusHandler,
        blurHandler,
    } = SimpleInputActions();

    const {setInputRef, mouseOverBanner} = useContext(FocusContext);
    const {input} = useContext(StateContext);

    const inputField = useRef(null);

    useEffect(() => {
        setInputRef(inputField.current);
    }, []);

    const handleClick = (event) => {
        clickHandler(event)
    };

    const handleChange = (event) => {
        changeHandler(event)
    };

    const handleFocus = (event) => {
        focusHandler(event)
    };

    const handleBlur = (event) => {
        blurHandler(event)
    };

    const handleKeyDown = (event) => {
        keyDownHandler(event)
    };

    return (
        <div className={`SimpleInput ${className}-SimpleInput ${instanceName}-SimpleInput ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`} >
            <input
                className={`SimpleInputField ${className}-SimpleInputField ${instanceName}-SimpleInputField ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
                ref={inputField}
                value={input}
                onFocus={handleFocus}
                onBlur={handleBlur}
                onChange={handleChange}
                onKeyDown={handleKeyDown}
                onClick={handleClick}
            />
        </div>
    )

};

export default InputSimple;