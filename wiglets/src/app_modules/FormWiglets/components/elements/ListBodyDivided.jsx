import React, {useContext, useMemo} from 'react';

const ListBodyDivided = ({className, instanceName, elementName, contextObject, components}) => {

    const {listValues} = useContext(contextObject.StateContext);
    const {ListItem} = components;

    const ListArray = useMemo(() => {
        return listValues.map((valueArray, i) => {
            return (

                    <div
                        className={`ListGroup ListGroup_${i}`}
                        key={`ListGroup_${i}`}
                    >
                        <div className={`ListGroup-DividerTop ListGroup_${i}-DividerTop`}/>
                            {valueArray.map((value, j) => {
                                return (
                                    <ListItem
                                        key={`${i}_${j}_${value.value}`}
                                        className={className}
                                        instanceName={instanceName}
                                        elementName={elementName}
                                        contextObject={contextObject}
                                        value={value}
                                        i={`${i}_${j}`}
                                    />
                                )
                            })}
                        <div className={`ListGroup-DividerBottom ListGroup_${i}-DividerBottom`}/>
                    </div>

            )
        });
    }, [listValues])

    return useMemo(() => {
        return (
            <div className={`DividedList ${className}-DividedList ${instanceName}-DividedList ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}>
                {ListArray}
            </div>
        )
    }, [listValues])

};

export default ListBodyDivided;