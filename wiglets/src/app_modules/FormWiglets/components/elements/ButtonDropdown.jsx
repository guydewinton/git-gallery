import React from 'react';
import SVGDownArrow from "./SVGDownArrow";

const ButtonDropdown = ({className, instanceName, elementName, contextObject}) => {

    const {DropdownButtonActions} = contextObject;

    const {clickHandler} = DropdownButtonActions();

    const handleClick = () => {
        clickHandler();
    };

    return (
        <div
            className={`DropdownButton ${className}-DropdownButton ${instanceName}-DropdownButton ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
            onClick={handleClick}
        >
            <SVGDownArrow/>
        </div>
    )

};

export default ButtonDropdown;