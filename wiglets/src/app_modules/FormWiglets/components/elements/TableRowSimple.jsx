import React, {memo, useContext, useEffect, useMemo, useRef, useState} from 'react';
import SVGDownArrow from "./SVGDownArrow";
import TableItemDisplayString from "./TableItemDisplayString";
import TableItemDisplayDate from "./TableItemDisplayDate";



const TableRowSimple = ({className, instanceName, elementName, contextObject, components, listValue, i}) => {

    const {StateContext, FocusContext, TableRowActions} = contextObject;

    const {TableRowTextItem, ButtonElementLeft, ButtonElementRight} = components;

    const {clickHandler, mouseOverHandler, mouseOutHandler, rowFocusHandler} = TableRowActions();

    const {headerValues, listValues, selection, selectionHandler, sortValue, sortDescending, setSelection} = useContext(StateContext);
    const {focus} = useContext(FocusContext);

    const [selected, setSelected] = useState('INACTIVE_OPTION');

    const rowFocus = useRef(null);

    useEffect(() => {
        console.log('focusTime', focus)
        if (focus) {
            if (selection === listValue.i) {
                setSelected('SELECTED_OPTION');
                rowFocus.current.focus()
            } else {
                setSelected('INACTIVE_OPTION');
            }
        } else {
            setSelected('UNFOCUSED_OPTION');
        }
    }, [selection, sortValue, sortDescending, focus]);


    const handleMouseEnter = () => {
        mouseOverHandler(i)
    };

    const handleMouseLeave = () => {
        mouseOutHandler(i)
    };

    const handleClick = () => {
        clickHandler(i)
    };

    const handleRowFocus = () => {
        rowFocusHandler(i)
    }

    const SortableListRowItem = useMemo(() => {
        return headerValues.map((value, i) => {
            if (value.type === 'string') {
                return (
                    <div
                        className={`TableRowTextItem ${instanceName}-TableRowTextItem-${i} TableRowTextItem-${i} ${className} ${instanceName} ${elementName} ${elementName}-TableRowTextItem-${i}`}
                        key={`${i}_${value.value}`}
                    >
                        <TableItemDisplayString
                            className={className}
                            instanceName={instanceName}
                            elementName={'BodyRow'}
                            contextObject={contextObject}
                            value={listValue[value.value]}
                            i={i}
                        />
                    </div>
                )
            } else if (value.type === 'number') {
                return (
                    <div
                        className={`TableRowTextItem ${instanceName}-TableRowTextItem-${i} TableRowTextItem-${i} ${className} ${instanceName} ${elementName} ${elementName}-TableRowTextItem-${i}`}
                        key={`${i}_${value.value}`}
                    >
                        <TableItemDisplayString
                            className={className}
                            instanceName={instanceName}
                            elementName={'BodyRow'}
                            contextObject={contextObject}
                            value={listValue[value.value]}
                            i={i}
                        />
                    </div>
                )
            } else if (value.type === 'date') {
                return (
                    <div
                        className={`TableRowTextItem ${instanceName}-TableRowTextItem-${i} TableRowTextItem-${i} ${className} ${instanceName} ${elementName} ${elementName}-TableRowTextItem-${i}`}
                        key={`${i}_${value.value}`}
                    >
                        <TableItemDisplayDate
                            className={className}
                            instanceName={instanceName}
                            elementName={'BodyRow'}
                            contextObject={contextObject}
                            value={listValue[value.value]}
                            i={i}
                        />
                    </div>
                )
            }

        });
    }, [headerValues, listValue])

    return useMemo(() => {
        return (
            <div
            className={`TableRowSimple ${className}-TableRowSimple TableRowSimple-${selected} ${instanceName}-TableRowSimple ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
            style={{position: 'relative'}}
            id={`TableRow_${i}`}
            onClick={handleClick}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
            >
                <input
                    className={`rowFocus_${i}`}
                    ref={rowFocus}
                    style={{height: '100%', width: '0', outline: 'none', background: 'none', opacity: '0', position: 'absolute'}}
                    onFocus={handleRowFocus}
                    readOnly
                />
                {ButtonElementLeft && <ButtonElementLeft
                    className={`TableRowButtonsLeft TableRowButtonsLeft-ButtonComponent ButtonComponent`}
                    instanceName={instanceName}
                    elementName={'ButtonComponent'}
                    contextObject={contextObject}
                    listValue={listValue}
                    i={i}

                />}
                {SortableListRowItem}
                {ButtonElementRight && <ButtonElementRight
                    className={`TableRowButtonsRight TableRowButtonsRight-ButtonComponent ButtonComponent`}
                    instanceName={instanceName}
                    elementName={'ButtonComponent'}
                    contextObject={contextObject}
                    listValue={listValue}
                    i={i}

                />}
            </div>
    )
}, [headerValues, listValue, selected])

};

export default TableRowSimple;