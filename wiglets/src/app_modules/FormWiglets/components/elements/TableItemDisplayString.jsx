import React, {useEffect, useMemo} from "react";

const TableItemDisplayString = ({className, instanceName, elementName, contextObject, value, i}) => {


    return useMemo(() => {
        return (
            <p
                className={`TextItem ${className}-TextItem ${instanceName}-TextItem-${i} TextItem-${i} ${className}-TextItem-${i} ${instanceName}-TextItem-${i} ${elementName} ${elementName}-TextItem-${i} ${elementName}-TextItem ${className}-${elementName} ${instanceName}-${elementName}`}
            >
                {value}
            </p>
    )
}, [value])

};

export default TableItemDisplayString