import React, {useContext, useEffect, useMemo, useRef, useState} from 'react';

const ListItemSimple = ({className, instanceName, elementName, contextObject, value, i}) => {

    const {StateContext, FocusContext, ListItemSelectActions} = contextObject;

    const {clickHandler, mouseOverHandler, mouseOutHandler, rowFocusHandler} = ListItemSelectActions();

    const {selection, highlighted} = useContext(StateContext);

    const {focus, inputRef} = useContext(FocusContext);

    const [selected, setSelected] = useState('INACTIVE_OPTION');
    const [highlight, setHighlight] = useState('UNHIGHLIGHTED_OPTION');

    const rowFocus = useRef(null);

    useEffect(() => {
        if (highlighted === i) {
            setHighlight('HIGHLIGHTED_OPTION');
            if (focus) {
                rowFocus.current.focus()
            }
        } else {
            setHighlight('UNHIGHLIGHTED_OPTION');
        }
    }, [highlighted, focus, rowFocus]);

    useEffect(() => {
        if (selection === i) {
            setSelected('SELECTED_OPTION');
        } else {
            setSelected('INACTIVE_OPTION');
        }
    }, [selection]);

    const handleMouseEnter = () => {
        mouseOverHandler(i);
    };

    const handleClick = () => {
        clickHandler(value.value, value.label, i)
    };

    const handleMouseOut = () => {
        mouseOutHandler(i)
    }

    const handleFocus = () => {
        rowFocusHandler(i)
    }

    return useMemo(() => {
        return (
            <div
                className={`ListItemSelect ListItemSelect-${selected} ListItemSelect-${highlight} ${className}-ListItemSelect ${instanceName}-ListItemSelect ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
                style={{position: 'relative'}}
                onClick={handleClick}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseOut}
            >
                <input
                    className={`rowFocus_${i}`}
                    ref={rowFocus}
                    style={{height: '100%', width: '0', outline: 'none', background: 'none', opacity: '0', position: 'absolute'}}
                    onFocus={handleFocus}
                    readOnly
                />
                <p
                    className={`ListItemSelectP ${className}-ListItemSelectP ${instanceName}-ListItemSelectP ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
                >{value.label}</p>
            </div>
        )
    }, [value, selected, highlight, inputRef])

};

export default ListItemSimple;