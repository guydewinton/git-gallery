import React, {memo, useContext, useEffect, useMemo, useRef} from 'react';

const TableBody = ({className, instanceName, elementName, contextObject, components}) => {

    const {StateContext} = contextObject;


    const {listValues, headerValues} = useContext(StateContext);
    const {TableRow} = components;



    const ListArray = useMemo(() => {
        return listValues.map((value, i) => {
            return (
                <TableRow
                    key={`${i}_${value.name}`}
                    // id={`TableRow_${i}`}
                    className={className}
                    instanceName={instanceName}
                    elementName={elementName}
                    contextObject={contextObject}
                    listValue={value}
                    components={components}
                    i={i}
                />
            )
        });
    }, [listValues])


    return useMemo(() => {
        return (
            <div
                className={`TableBody ${className}-TableBody ${instanceName}-TableBody ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
            >
                {ListArray}
            </div>
    )
}, [listValues, headerValues])

};

export default TableBody;