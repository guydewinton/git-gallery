import React, {useContext, useMemo} from 'react';

const ListBodySimple = ({className, instanceName, elementName, contextObject, components}) => {

    const {listValues} = useContext(contextObject.StateContext);
    const {ListItem} = components;

    const ListArray = useMemo(() => {
        return listValues.map((value, i) => {
            return (
                <ListItem
                    key={`${i}_${value.value}`}
                    className={className}
                    instanceName={instanceName}
                    elementName={elementName}
                    contextObject={contextObject}
                    value={value}
                    i={i}
                />
            )
        });
    }, [listValues])

    return useMemo(() => {
        return (
            <div className={`SimpleList ${className}-SimpleList ${instanceName}-SimpleList ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}>
                {ListArray}
            </div>
        )
    }, [listValues])

};

export default ListBodySimple;