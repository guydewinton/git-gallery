import React, {useContext, useEffect, useState} from 'react';

const TableHeaderSimple = ({className, instanceName, elementName, contextObject, components}) => {

    const {StateContext} = contextObject;

    const {TableHeaderRowItem, ButtonElementLeft, ButtonElementRight} = components;

    // const {clickHandler} = SortableListRowActions();

    const {headerValues, listValues, sortColumn, sortValue, sortDescending} = useContext(StateContext);

    const [buttonsClasses, setButtonsClasses] = useState('');

    useEffect(() => {
        const startButtonClass = ButtonElementLeft ? 'StartButton' : ''
        const endButtonClass = ButtonElementRight ? 'EndButton' : ''
        setButtonsClasses(`${startButtonClass} ${endButtonClass}`)
    }, [ButtonElementLeft, ButtonElementRight])


    // const handleClick = () => {
    //     clickHandler(value.value, value.label, i)
    // };

    const TableRowTextItemArray = headerValues.map((value, i) => {

        return (
            <TableHeaderRowItem
                    key={`${i}_${value.value}`}
                    className={className}
                    instanceName={instanceName}
                    elementName={`HeaderRow`}
                    buttonClasses={buttonsClasses}
                    contextObject={contextObject}
                    value={value}
                    components={components}
                    i={i}
                />
        )
    });

    return (
        <div
            className={`TableHeaderSimple ${className}-TableHeaderSimple ${instanceName}-TableHeaderSimple ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
            // onClick={handleClick}
        >
            {ButtonElementLeft && <ButtonElementLeft
                className={`TableHeaderButtonsStart TableHeaderButtonsStart-ButtonComponent ButtonComponent`}
                style={{visibility: 'hidden'}}
            />}
            {TableRowTextItemArray}
            {ButtonElementRight && <ButtonElementRight
                className={`TableHeaderButtonsEnd TableHeaderButtonsEnd-ButtonComponent ButtonComponent`}
                style={{visibility: 'hidden'}}
            />}
        </div>
    )

};

export default TableHeaderSimple;