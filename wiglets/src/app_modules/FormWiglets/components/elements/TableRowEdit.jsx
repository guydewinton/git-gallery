import React, {memo, useContext, useEffect, useMemo, useRef, useState} from 'react';
import SVGDownArrow from "./SVGDownArrow";



const TableRowEdit = ({className, instanceName, elementName, contextObject, components, listValue, i}) => {

    const {StateContext, FocusContext, TableRowActions} = contextObject;

    const {TableRowTextItem, TableRowEditItem, ButtonElementLeft, ButtonElementRight} = components;

    const {clickHandler, rowFocusHandler} = TableRowActions();

    const {headerValues, listValues, selection, selectionHandler, sortValue, sortDescending, setSelection} = useContext(StateContext);
    const {focus, cellFocus, rowClick} = useContext(FocusContext);

    const [selected, setSelected] = useState('INACTIVE_OPTION');

    const rowFocus = useRef(null);

    useEffect(() => {
        if (focus) {
            if (selection === listValue.i) {
                setSelected('SELECTED_OPTION');
                rowFocus.current.focus()
            } else {
                setSelected('INACTIVE_OPTION');
            }
        } else {
            setSelected('UNFOCUSED_OPTION');
        }
    }, [selection, sortValue, sortDescending, focus]);


    const handleMouseEnter = () => {
        if (focus) {
            selectionHandler(i);
        }
    };

    const handleClick = () => {
        clickHandler(i)
    };

    const handleRowFocus = () => {
        rowFocusHandler(i)
    }

    const SortableListRowItem = useMemo(() => {
        return headerValues.map((value, j) => {

            if (value.edit) {
                return (
                    <div
                        className={`TableRowTextItem TableRowTextItem-${j} ${instanceName}-TableRowTextItem-${j} ${className} ${instanceName} ${elementName} ${elementName}-TableRowTextItem-${j}`}
                        key={`${j}_${value.value}`}
                    >
                        <TableRowEditItem
                            className={className}
                            instanceName={instanceName}
                            elementName={'BodyRow'}
                            contextObject={contextObject}
                            value={listValue[value.value]}
                            i={[i,j]}
                        />
                    </div>
                )
            } else {
                return (
                    <div
                        className={`TableRowTextItem TableRowTextItem-${j} ${instanceName}-TableRowTextItem-${j} ${className} ${instanceName} ${elementName} ${elementName}-TableRowTextItem-${j}`}
                        key={`${j}_${value.value}`}
                    >
                        <TableRowTextItem
                            className={className}
                            instanceName={instanceName}
                            elementName={'BodyRow'}
                            contextObject={contextObject}
                            value={listValue[value.value]}
                            i={[i,j]}
                        />
                    </div>
                )
            }

        });
    }, [headerValues, listValue])

    return useMemo(() => {
        return (

            <div
            className={`TableRowSimple ${className}-TableRowSimple TableRowSimple-${selected} ${instanceName}-TableRowSimple ${elementName} ${className}-${elementName} ${instanceName}-${elementName}`}
            id={`TableRow_${i}`}
            style={{position: 'relative'}}
            onClick={handleClick}
            onMouseEnter={handleMouseEnter}
            >
                <input
                    className={`rowFocus_${i}`}
                    ref={rowFocus}
                    style={{height: '100%', width: '0', outline: 'none', background: 'none', opacity: '0', position: 'absolute'}}
                    onFocus={handleRowFocus}
                    readOnly
                />
                {ButtonElementLeft && <ButtonElementLeft
                    className={`TableRowButtonsLeft TableRowButtonsLeft-ButtonComponent ButtonComponent`}
                    instanceName={instanceName}
                    elementName={'ButtonComponent'}
                    contextObject={contextObject}
                    listValue={listValue}
                    i={i}

                />}
                {SortableListRowItem}
                {ButtonElementRight && <ButtonElementRight
                    className={`TableRowButtonsRight TableRowButtonsRight-ButtonComponent ButtonComponent`}
                    instanceName={instanceName}
                    elementName={'ButtonComponent'}
                    contextObject={contextObject}
                    listValue={listValue}
                    i={i}

                />}
            </div>
    )
}, [headerValues, listValue, selected, cellFocus])

};

export default TableRowEdit;