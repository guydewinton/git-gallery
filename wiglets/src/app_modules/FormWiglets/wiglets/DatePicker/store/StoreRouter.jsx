import React from 'react';

import {StateProvider} from "./contexts/StateContext";
import {FocusProvider} from "./contexts/FocusContext";

import ClearableInputActions from "./actions/ClearableInputActions";
import DropdownButtonActions from "./actions/CalendarButtonActions";
import CalendarPickerActions from "./actions/CalendarPickerActions";

export const StoreRouter = (props) => {

    return (
        <StateProvider>
            <FocusProvider>
                <ClearableInputActions/>
                <DropdownButtonActions/>
                <CalendarPickerActions/>
            </FocusProvider>
        </StateProvider>
    )
};