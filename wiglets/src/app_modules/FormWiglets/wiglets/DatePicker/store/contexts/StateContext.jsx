import React, {createContext, useEffect, useState} from 'react';


import dateFormatter from "../../../../library/helpers/functions/dateFormatter";

export const StateContext = createContext();

export const StateProvider = (props) => {

    const [input, setInput] = useState('');
    const startDateIndex = null




    useEffect(() => {
        handleChange(props.socketIn);
    }, [props.socketIn]);

    const handleChange = (inputValue) => {
        if (inputValue.length > input.length) {
            let formattedInput = dateFormatter(inputValue);
            setInput(formattedInput);
            props.socketOut(formattedInput);

        } else {
            setInput(inputValue);
            props.socketOut(inputValue);
        }
    };

    const handleOutput = (values) => {
        props.outputValue(values);
    };

    const keyOut = (keyCode) => {
        console.log('keyOut, eh?')
        props.keyOut(keyCode);
    };

    const valueObject = {
        input,
        setInput,
        handleOutput,
        handleChange,
        keyOut,
        startDateIndex
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};
