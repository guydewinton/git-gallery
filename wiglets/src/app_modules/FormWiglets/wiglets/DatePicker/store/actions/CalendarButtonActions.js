import {useContext, useEffect} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';


export default () => {

    const {showDropdown, setShowDropdown, inputRef} = useContext(FocusContext);
    const stateValue = useContext(StateContext);

    const clickHandler = () => {
        setShowDropdown(!showDropdown)
        inputRef.focus()
    };

    return {clickHandler}

}