import {useContext, useEffect, useRef} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';
import {keyPress, setKeyPress} from "../variables";
import {checkWigletFocus, setWigletFocus} from "../../../../library/variables/focusVariable";


export default () => {

    const {
        focus,
        setFocus,
        mouseOverBanner,
        setMouseOverBanner,
        mouseOverDropdown,
        setMouseOverDropdown,
        inputRef,
        setInputRef,
        showDropdown,
        setShowDropdown,
        updateFocus,
        setCurrentFocus
    } = useContext(FocusContext);

    const {
        input,
        handleChange,
        keyOut
    } = useContext(StateContext);

    const blurTimeoutRef = useRef(NaN)

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, [])


    const changeHandler = (event) => {
        handleChange(event.target.value);
    };

    const clickHandler = () => {
        setShowDropdown(true)
    };

    const focusHandler = () => {
        setWigletFocus(inputRef);
        setFocus(true);
        setKeyPress(false);
        setCurrentFocus()
        // inputRef.select();
    };

    const blurHandler = () => {
        setWigletFocus(null)
        if (keyPress) {
            setFocus(false);
            setShowDropdown(false);
            if (input.length === 8) {
                let prefixDate = input.slice(0, 6)
                let suffixDate = input.slice(-2)
                handleChange(prefixDate + '20' + suffixDate)
            }
            blurTimeoutRef.current = checkWigletFocus(inputRef, inputRef.selectionStart, inputRef.selectionEnd, [setFocus])
        } else if (!mouseOverDropdown && !mouseOverBanner) {
            setShowDropdown(false);
            if (input.length === 8) {
                let prefixDate = input.slice(0, 6)
                let suffixDate = input.slice(-2)
                handleChange(prefixDate + '20' + suffixDate)
            }
            blurTimeoutRef.current = checkWigletFocus(inputRef, inputRef.selectionStart, inputRef.selectionEnd, [setFocus])
        } else {
            inputRef.focus()
        }
    };

    const keyDownHandler = (event) => {
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            setShowDropdown(false);
            updateFocus(event.keyCode);
            setKeyPress(true);
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            setShowDropdown(false);
            updateFocus(event.keyCode)
            setKeyPress(true);
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            if (showDropdown) {
                setShowDropdown(false);
            } else {
                updateFocus(event.keyCode);
                setKeyPress(true);
            }
        }
    };

    const clearHandler = () => {
        handleChange('');
        inputRef.focus();
        setShowDropdown(true);
    };

    return {
        changeHandler,
        keyDownHandler,
        clickHandler,
        focusHandler,
        blurHandler,
        clearHandler,
    };

};
