import React from 'react';

import InputClearable from "../../components/elements/InputClearable";
import BannerDropdownWrapper from "../../components/wrappers/BannerDropdownWrapper";
import ButtonCalendar from "../../components/elements/ButtonCalendar";
import CalendarPicker from "../../components/elements/CalendarPicker";


import {FocusContext, FocusProvider} from "./store/contexts/FocusContext";
import {StateContext, StateProvider} from "./store/contexts/StateContext";

import ClearableInputActions from "./store/actions/ClearableInputActions";
import CalendarButtonActions from "./store/actions/CalendarButtonActions";
import CalendarPickerActions from "./store/actions/CalendarPickerActions";
import useFocus from "../../library/helpers/hooks/useFocus";


const DatePicker = ({

    title = '',
    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    initValue = '',

                    }) => {

    // socketHook
    // refHook

    const {registerRef} = useFocus(focusIndex, 'DatePicker')



    const regRef = (inputRef) => {
        // props.refOut(inputRef, props.tabID)
        if (focusIndex) {
            return registerRef()
        }
    };

    const contextObject = {
        ClearableInputActions,
        CalendarButtonActions,
        CalendarPickerActions,
        FocusContext,
        StateContext,
    };

    const components = {
        InnerElement: InputClearable,
        DropdownButton: ButtonCalendar,
        DropdownElement: CalendarPicker,
    };

    return (
        <div className={className}>
            <FocusProvider regRef={regRef} >
                <StateProvider inputValues={inputValues} outputValue={outputValue} keyOut={keyOut} socketIn={socketIn} socketOut={socketOut} >
                    <div className={'MarginSpacer'}/>
                    <div className={`FilterTitleWrapper`}>
                        <p className={`FilterTitleElement`}>{title}</p>
                    </div>
                    <BannerDropdownWrapper
                        className={'DatePicker'}
                        instanceName={instanceName}
                        contextObject={contextObject}
                        components={components}
                    />
                    <div className={'MarginSpacer'}/>
                </StateProvider>
            </FocusProvider>
        </div>
    )
};

export default DatePicker;