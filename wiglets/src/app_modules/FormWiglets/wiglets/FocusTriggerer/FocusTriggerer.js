import React, {useEffect, useRef} from 'react';

import useFocus from "../../library/helpers/hooks/useFocus";
import {checkWigletFocus, setWigletFocus} from "../../library/variables/focusVariable";

const FocusTriggerer = ({

    className = '',
    keyOut = null,
    focusIndex = [0,0,0],

                        }) => {

    const {registerRef} = useFocus(focusIndex, 'FocusTriggerer')

    const inputField = useRef(null);

    const {
        updateFocus,
        regRef,
        setCurrentFocus,
    } = registerRef();

    const blurTimeoutRef = useRef(NaN)

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, [])

    useEffect(() => {
        if (inputField) {
            regRef(inputField.current)
        }
    }, [inputField.current]);

    const focusUpdate = (keyCode) => {
        updateFocus(keyCode)
        keyOut(keyCode)
    };

    const focusHandler = () => {
        setWigletFocus(inputField.current)
        setCurrentFocus();
    };

    const blurHandler = () => {
        setWigletFocus(null)
        blurTimeoutRef.current = checkWigletFocus(inputField.current)
    };

    return (
        <div className={`FocusTriggererWrapper ${className}`}>
            <input
                className={'FocusTriggerer'}
                ref={inputField}
                style={{opacity: 0, outline: 0, position: 'absolute'}}
                onKeyDown={event => focusUpdate(event.keyCode)}
                onFocus={focusHandler}
                onBlur={blurHandler}
            />
        </div>
    )
};

export default FocusTriggerer;