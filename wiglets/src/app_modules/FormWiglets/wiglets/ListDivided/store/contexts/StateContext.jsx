import React, {createContext, useEffect, useState} from 'react';
import sortObjectArray from "../../../../library/helpers/functions/sortObjectArray";
import {startsWithFilterCaseInsensitive} from "../../../../library/helpers/functions/filterObjectArray";

export const StateContext = createContext();

export const StateProvider = (props) => {

    const [listValues, setListValues] = useState(props.inputValues, 'label');
    const [selection, setSelection] = useState(`${props.initIndex[0]}_${props.initIndex[1]}`);
    const [highlighted, setHighlighted] = useState(null);

    const handleOutput = (value) => {
        props.outputValue && props.outputValue(value);
    };

    const handleSelection = (selection) => {
        handleOutput(listValues[parseInt(selection[0])][parseInt(selection[2])]);

    };

    const valueObject = {
        listValues,
        setListValues,
        selection,
        setSelection,
        handleOutput,
        handleSelection,
        highlighted,
        setHighlighted
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};
