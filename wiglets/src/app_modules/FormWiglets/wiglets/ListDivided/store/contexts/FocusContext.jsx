import React, {createContext, useEffect, useState} from 'react';

export const FocusContext = createContext()

export const FocusProvider = (props) => {

    const [inputRef, setInputRef] = useState(null);
    const [focus, setFocus] = useState(false);


    let {
        updateFocus,
        regRef,
        setCurrentFocus,
    } = props.regRef();

    useEffect(() => {
        if (inputRef) {
            console.log(inputRef)
            regRef(inputRef)
        }
    }, [inputRef]);

    const valueObject = {
        inputRef,
        setInputRef,
        focus,
        setFocus,
        updateFocus,
        setCurrentFocus
    };

    return <FocusContext.Provider value={valueObject}>{props.children}</FocusContext.Provider>

};