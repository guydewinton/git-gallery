import {useContext, useEffect, useRef, useState} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';
import {checkWigletFocus, setWigletFocus} from "../../../../library/variables/focusVariable";


export default () => {

    const {setFocus, updateFocus, inputRef, setCurrentFocus, focus} = useContext(FocusContext);
    const {listValues, selection, setSelection, handleSelection, highlighted, setHighlighted, handleOutput} = useContext(StateContext);

    const keyPress = useRef(false);

    const blurTimeoutRef = useRef(NaN);

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, []);

    const focusHandler = () => {
        if (!focus) {
            setHighlighted(selection)
        }
        setWigletFocus(inputRef);
        setFocus(true);
        setCurrentFocus()

    };

    const blurHandler = () => {
        setWigletFocus(null);
        blurTimeoutRef.current = checkWigletFocus(inputRef, 0, 0, [setFocus, setHighlighted])
    };

    const keyDownHandler = (event) => {
        let highlightedArray;
        if (highlighted) {
            highlightedArray = highlighted.split('_');
        } else {
            highlightedArray = selection.split('_');
        }

        if (event.keyCode === 38) { // UPARROW
            event.preventDefault();
            let newHighlighted;
            if (parseInt(highlightedArray[1]) > 0) {
                newHighlighted = `${highlightedArray[0]}_${parseInt(highlightedArray[1]) - 1}`
            } else {
                if (parseInt(highlightedArray[0]) > 0) {
                    newHighlighted = `${parseInt(highlightedArray[0]) - 1}_${listValues[parseInt(highlightedArray[0]) - 1].length - 1}`
                } else {
                    newHighlighted = `${listValues.length - 1}_${listValues[listValues.length - 1].length - 1}`
                }
            }
            setHighlighted(newHighlighted);
            keyPress.current = true
        }
        if (event.keyCode === 40) { // DOWNARROW
            event.preventDefault();
            let newHighlighted;
            if (parseInt(highlightedArray[1]) < listValues[parseInt(highlightedArray[0])].length - 1) {
                newHighlighted = `${highlightedArray[0]}_${parseInt(highlightedArray[1]) + 1}`
            } else {
                if (parseInt(highlightedArray[0]) < listValues.length - 1) {
                    newHighlighted = `${parseInt(highlightedArray[0]) + 1}_0`
                } else {
                    newHighlighted = `0_0`
                }
            }
            setHighlighted(newHighlighted)
            keyPress.current = true
        }
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            handleSelection(highlighted);
            setSelection(highlighted);
            updateFocus(event.keyCode)
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            handleSelection(highlighted);
            setSelection(highlighted);
            updateFocus(event.keyCode)
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            updateFocus(event.keyCode);
        }
    };

    return {
        keyDownHandler,
        focusHandler,
        blurHandler
    }

}