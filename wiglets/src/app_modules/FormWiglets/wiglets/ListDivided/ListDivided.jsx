import React, {useEffect} from 'react';

import ListItemSelectActions from "./store/actions/ListItemSelectActions";
import {FocusContext, FocusProvider} from "./store/contexts/FocusContext";
import {StateContext, StateProvider} from "./store/contexts/StateContext";


import useFocus from "../../library/helpers/hooks/useFocus";
import HiddenElementWrapper from "../../components/wrappers/HiddenElementWrapper";
import InputHiddenActions from "./store/actions/InputHiddenActions";
import InputHidden from "../../components/elements/InputHidden";
import ListItemSelect from "../../components/elements/ListItemSelect";
import ListBodyDivided from "../../components/elements/ListBodyDivided";


const ListDivided = ({

    title = '',
    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    initIndex = [0,0],
    filterValue = null,

                             }) => {


    const {registerRef} = useFocus(focusIndex, 'ListDivided')

    const regRef = () => {
        // if (focusIndex) {
            return registerRef()
        // }
    };

    const contextObject = {
        FocusContext,
        StateContext,
        ListItemSelectActions,
        InputHiddenActions,
    };

    const components = {
        HiddenElement: InputHidden,
        InnerElement: ListBodyDivided,
        ListItem: ListItemSelect,
    };

    return (
        <div className={`ListDividedWrapper ${className}`}>
            <StateProvider
                inputValues={inputValues}
                filterValue={filterValue}
                initIndex={initIndex}
                outputValue={outputValue}
            >
                <FocusProvider regRef={regRef}>
                    {title && (
                        <div className={`FilterTitleWrapper`}>
                            <p className={`FilterTitleElement`}>{title}</p>
                        </div>
                    )}
                    <HiddenElementWrapper
                        className={'ListDivided'}
                        instanceName={instanceName}
                        contextObject={contextObject}
                        components={components}
                    />
                </FocusProvider>
            </StateProvider>
        </div>
    )
};

export default ListDivided;