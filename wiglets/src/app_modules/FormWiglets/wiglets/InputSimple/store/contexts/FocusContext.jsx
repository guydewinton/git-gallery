import React, {createContext, useState, useEffect} from 'react';

export const FocusContext = createContext();

export const FocusProvider = (props) => {

    const [focus, setFocus] = useState(false);
    const [inputRef, setInputRef] = useState(null);
    const [mouseOverBanner, setMouseOverBanner] = useState(false);
    // const {regRef, updateFocus, setCurrentFocus, update} = props.regRef()


    const {
        updateFocus,
        regRef,
        setCurrentFocus,
    } = props.regRef();



    // const regRef = (inputRef) => {
    //     // props.refOut(inputRef, props.tabID)
    //     if (props.indexArray) {
    //         return registerRef()
    //     }
    // };

    useEffect(() => {
        if (inputRef) {
            regRef(inputRef)
        }
    }, [inputRef]);

    // const keyOut = (keyCode) => {
    //     props.keyOut && props.keyOut(keyCode);
    // };

    const focusUpdate = (keyCode) => {
        updateFocus(keyCode)
    };


    const valueObject = {
        focus,
        setFocus,
        inputRef,
        setInputRef,
        mouseOverBanner,
        setMouseOverBanner,
        focusUpdate,
        setCurrentFocus,
    };


    return <FocusContext.Provider value={valueObject}>{props.children}</FocusContext.Provider>

};