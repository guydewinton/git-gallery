import React, {createContext, useEffect, useState} from 'react';
import sortObjectArray from "../../../../library/helpers/functions/sortObjectArray";
import {startsWithFilterCaseInsensitive} from "../../../../library/helpers/functions/filterObjectArray";

export const StateContext = createContext();

export const StateProvider = (props) => {

    const [input, setInput] = useState('');

    useEffect(() => {

        setInput(props.initValue)

    }, [props.initValue])

    const handleChange = (value) => {
        setInput(value);
        props.outputValue(value);
    };

    const handleOutput = (value) => {
        props.outputValue && props.outputValue(value);
    };

    const keyOut = (keyCode) => {
        props.keyOut && props.keyOut(keyCode);
    };

    const valueObject = {
        input,
        setInput,
        handleChange,
        handleOutput,
        keyOut
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};
