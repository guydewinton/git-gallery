import React from 'react';

import {StateProvider} from "./contexts/StateContext";
import {FocusProvider} from "./contexts/FocusContext";

import ClearableInputActions from "./actions/ClearableInputActions";

export const StoreRouter = (props) => {

    return (
        <StateProvider>
        <FocusProvider>
            <ClearableInputActions/>
        </FocusProvider>
        </StateProvider>
    )
};