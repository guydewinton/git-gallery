import {useContext, useEffect, useRef} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';
import {checkWigletFocus, setWigletFocus} from "../../../../library/variables/focusVariable";


export default () => {

    const {
        focus,
        setFocus,
        inputRef,
        setInputRef,
        focusUpdate,
        setCurrentFocus
    } = useContext(FocusContext);

    const {
        input,
        setInput,
        handleChange,
        handleOutput,
        keyOut
    } = useContext(StateContext);

    const blurTimeoutRef = useRef(NaN)

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, [])

    const changeHandler = (event) => {
        handleChange(event.target.value);
    };

    const clickHandler = (event) => {

    };

    const focusHandler = () => {
        setWigletFocus(inputRef)
        setFocus(true);
        setCurrentFocus();
        // inputRef.select();
    };

    const blurHandler = () => {
        setWigletFocus(null)
        blurTimeoutRef.current = checkWigletFocus(inputRef, inputRef.selectionStart, inputRef.selectionEnd, [setFocus])
    };

    const keyDownHandler = (event) => {
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            // keyOut(event.keyCode)
            focusUpdate(event.keyCode)
            keyOut(event.keyCode)
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            focusUpdate(event.keyCode)
            keyOut(event.keyCode)
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            focusUpdate(event.keyCode)
            keyOut(event.keyCode)
        }
    };

    const clearHandler = () => {
        handleChange('');
        inputRef.focus();
    };

    return {
        changeHandler,
        keyDownHandler,
        focusHandler,
        blurHandler,
        clearHandler,
        clickHandler,
    };

};
