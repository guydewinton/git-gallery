import React from 'react';

import InputClearable from "../../components/elements/InputClearable";
import MouseOverBanner from "../../components/wrappers/MouseOverWrapper";

import {FocusContext, FocusProvider} from "./store/contexts/FocusContext";
import {StateContext, StateProvider} from "./store/contexts/StateContext";

import ClearableInputActions from "./store/actions/ClearableInputActions";
import useFocus from "../../library/helpers/hooks/useFocus";


const InputSimple = ({

    title = '',
    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    initValue = '',

}) => {

    const {registerRef} = useFocus(focusIndex, 'InputSimple')



    const regRef = () => {
        if (focusIndex) {
            return registerRef()
        }
    };

    // socketHook
    // refHook

    const contextObject = {
        ClearableInputActions,
        FocusContext,
        StateContext,
    };

    const components = {
        InnerElement: InputClearable,
    };

    return (
        <div className={`InputClearableWrapper ${className}`}>
            <FocusProvider regRef={regRef} keyOut={keyOut}>
            <StateProvider inputValues={inputValues} outputValue={outputValue} keyOut={keyOut} socketIn={socketIn} socketOut={socketOut} initValue={initValue}>
                <div className={'MarginSpacer'}/>
                <div className={`FilterTitleWrapper`}>
                    <p className={`FilterTitleElement`}>{title}</p>
                </div>
                <MouseOverBanner
                    className={'InputSimple'}
                    instanceName={instanceName}
                    contextObject={contextObject}
                    components={components}
                />
                <div className={'MarginSpacer'}/>
            </StateProvider>
            </FocusProvider>
        </div>
    )
};

export default InputSimple;