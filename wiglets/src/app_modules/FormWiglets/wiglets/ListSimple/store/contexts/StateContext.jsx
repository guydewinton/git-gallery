import React, {createContext, useEffect, useState} from 'react';

export const StateContext = createContext();

export const StateProvider = (props) => {

    const [listValues, setListValues] = useState(props.inputValues);
    const [selection, setSelection] = useState(0);
    const [highlighted, setHighlighted] = useState(null);

    const handleOutput = (value) => {
        props.outputValue && props.outputValue(value);
    };

    const handleSelection = (selection) => {
        if (selection) {
            let newValue = listValues[selection];
            handleOutput(newValue);
        }
    };

    const valueObject = {
        listValues,
        setListValues,
        selection,
        setSelection,
        handleOutput,
        handleSelection,
        highlighted,
        setHighlighted
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};
