import {useContext, useEffect, useRef, useState} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';
import {checkWigletFocus, setWigletFocus} from "../../../../library/variables/focusVariable";


export default () => {

    const {focus, setFocus, updateFocus, inputRef, setCurrentFocus} = useContext(FocusContext)
    const {listValues, selection, setSelection, handleSelection, highlighted, setHighlighted} = useContext(StateContext);

    const keyPress = useRef(false)

    const blurTimeoutRef = useRef(NaN);

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, []);

    const focusHandler = () => {
        if (!focus) {
            setHighlighted(selection)
        }
        setWigletFocus(inputRef);
        setFocus(true);
        setCurrentFocus()
    };

    const blurHandler = () => {
        setWigletFocus(null);
        blurTimeoutRef.current = checkWigletFocus(inputRef, 0, 0, [setFocus, setHighlighted])
    };

    const keyDownHandler = (event) => {
        if (event.keyCode === 38) { // UPARROW
            event.preventDefault();
            let newHighlighted;
            if (highlighted > 0) {
                newHighlighted = highlighted - 1
            } else {
                newHighlighted = listValues.length - 1
            }
            setHighlighted(newHighlighted)
        }
        if (event.keyCode === 40) { // DOWNARROW
            event.preventDefault();
            let newHighlighted;
            if (highlighted < listValues.length-1) {
                newHighlighted = highlighted + 1
            } else {
                newHighlighted = 0
            }
            setHighlighted(newHighlighted)

        }
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            handleSelection(highlighted);
            setSelection(highlighted)
            updateFocus(event.keyCode)
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            handleSelection(highlighted);
            setSelection(highlighted)
            updateFocus(event.keyCode)
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            updateFocus(event.keyCode);
        }
    };

    return {
        keyDownHandler,
        focusHandler,
        blurHandler
    }

}