import React, {useContext} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';


export default () => {

    const {setMouseOverDropdown, inputRef, setFocus, setShowDropdown, updateFocus} = useContext(FocusContext);
    const {handleOutput, keyOut, setSelection, setHighlighted} = useContext(StateContext);

    const clickHandler = (value, label, index) => {
        handleOutput({value, label});
        setSelection(index);
    };

    const mouseOverHandler = (index) => {
        setHighlighted(index)
    };

    const mouseOutHandler = () => {
        setHighlighted(null)
    }

    const rowFocusHandler = () => {
        inputRef.focus()
    }

    return {clickHandler, mouseOverHandler, mouseOutHandler, rowFocusHandler}

}