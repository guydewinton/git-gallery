// import React from 'react';
//
// import {StateProvider} from "./contexts/StateContext";
// import {FocusProvider} from "./contexts/MasterFocusContext";
//
// import ClearableInputActions from "./actions/ClearableInputActions";
// import DropdownButtonActions from "./actions/DropdownButtonActions";
// import ListItemSimpleActions from "./actions/ListItemSimpleActions";
//
// export const StoreRouter = (props) => {
//
//     return (
//         <StateProvider>
//         <FocusProvider>
//             <ClearableInputActions/>
//             <DropdownButtonActions/>
//             <ListItemSimpleActions/>
//         </FocusProvider>
//         </StateProvider>
//     )
// };