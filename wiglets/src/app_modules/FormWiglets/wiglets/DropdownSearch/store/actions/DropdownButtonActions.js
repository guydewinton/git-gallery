import {useContext, useEffect} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';


export default () => {

    const {showDropdown, setShowDropdown, inputRef} = useContext(FocusContext);
    const {setListValues, baseValues, selection} = useContext(StateContext)

    const clickHandler = () => {
        inputRef.focus()
        setShowDropdown(!showDropdown)
        if (!showDropdown) {
            setListValues(baseValues)
        }
    };

    return {clickHandler}

}