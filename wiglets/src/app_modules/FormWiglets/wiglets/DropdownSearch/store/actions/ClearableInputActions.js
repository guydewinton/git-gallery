import {useContext, useEffect, useRef, useState} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';
import {checkWigletFocus, setWigletFocus} from "../../../../library/variables/focusVariable";


export default () => {

    const {
        focus,
        setFocus,
        mouseOverBanner,
        setMouseOverBanner,
        mouseOverDropdown,
        setMouseOverDropdown,
        inputRef,
        setInputRef,
        showDropdown,
        setShowDropdown,
        updateFocus,
        setCurrentFocus,
    } = useContext(FocusContext);

    const {
        listValues,
        selection,
        setSelection,
        handleChange,
        handleSelection,
    } = useContext(StateContext);

    const keyPress = useRef(false)

    const blurTimeoutRef = useRef(NaN)

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, [])

    const changeHandler = (event) => {
        setShowDropdown(true);
        handleChange(event.target.value);
    };

    const clickHandler = () => {
        setShowDropdown(true)
    };

    const focusHandler = () => {
        setWigletFocus(inputRef)
        setFocus(true);
        setCurrentFocus();
        keyPress.current = false
    };

    const blurHandler = () => {
        setWigletFocus(null)
        if (keyPress.current) {
        } else if (!mouseOverDropdown) {
            if (!mouseOverBanner) {
                setShowDropdown(false);
            }

        }
        blurTimeoutRef.current = checkWigletFocus(inputRef, 0, 0, [setFocus])
    };


    const keyDownHandler = (event) => {
        if (event.keyCode === 38) { // UPARROW
            event.preventDefault();
            if (showDropdown) {
                let newSelection
                if (selection > 0) {
                    newSelection = selection-1
                } else {
                    newSelection = listValues.length-1
                }
                setSelection(newSelection)
                keyPress.current = true
            }
        }
        if (event.keyCode === 40) { // DOWNARROW
            event.preventDefault();
            if (showDropdown) {
                let newSelection
                if (selection < listValues.length - 1) {
                    newSelection = selection + 1
                } else {
                    newSelection = 0
                }
                setSelection(newSelection)

                keyPress.current = true
            } else {
                setShowDropdown(true)
                keyPress.current = true
            }
        }
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            setShowDropdown(false);
            handleSelection(selection);
            updateFocus(event.keyCode)
            // setKeyPress(true)
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            setShowDropdown(false);
            handleSelection(selection);
            updateFocus(event.keyCode)
            // setKeyPress(true)
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            if (showDropdown) {
                setShowDropdown(false);
            } else {
                updateFocus(event.keyCode);
                // setKeyPress(true);
            }
        }
    };

    const clearHandler = () => {
        handleChange('');
        inputRef.focus();
        setShowDropdown(false);
    };

    return {
        changeHandler,
        keyDownHandler,
        clickHandler,
        focusHandler,
        blurHandler,
        clearHandler,
    };

};
