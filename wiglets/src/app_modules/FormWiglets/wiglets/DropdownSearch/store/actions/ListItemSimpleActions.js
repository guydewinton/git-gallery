import React, {useContext, useEffect} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';


export default () => {

    const {setMouseOverDropdown, inputRef, setFocus, setShowDropdown, updateFocus, showDropdown} = useContext(FocusContext);
    const {handleChange, handleOutput, setSelection, selection, setListValues, baseValues} = useContext(StateContext);

    const rowFocusHandler = () => {
        inputRef.focus()
    };

    const clickHandler = (value, label, i) => {
        handleChange(label);
        handleOutput({value, label});
        setMouseOverDropdown(false);
        // inputRef.setSelectionRange(-1, -1)
        // inputRef.blur();
        setFocus(false);
        setShowDropdown(false);
        updateFocus(9)
    };

    return {clickHandler, rowFocusHandler}

}