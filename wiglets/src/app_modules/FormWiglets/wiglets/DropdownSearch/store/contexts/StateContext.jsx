import React, {createContext, useEffect, useState} from 'react';
import {sortObjectArrayString} from "../../../../library/helpers/functions/sortObjectArray";
import {startsWithFilterCaseInsensitive} from "../../../../library/helpers/functions/filterObjectArray";

export const StateContext = createContext();

export const StateProvider = (props) => {

    const [input, setInput] = useState('');
    const [baseValues] = useState(sortObjectArrayString(props.inputValues, 'label'));
    const [listValues, setListValues] = useState(baseValues);
    const [selection, setSelection] = useState(0);

    useEffect(() => {

        if (props.initValue !== null) {
            let initIndex;
            listValues.forEach((value, i) => {
                if (value.value === props.initValue) {
                    initIndex = i;
                }
            });
            handleSelection(initIndex);
        }

    }, [props.initValue]);

    const handleChange = (value) => {
        setSelection(0);
        setInput(value);
        let filteredOptions = startsWithFilterCaseInsensitive(baseValues, 'label', value);
        setListValues(filteredOptions);
    };

    const handleOutput = (value) => {
        props.outputValue && props.outputValue(value);
    };

    const handleSelection = (selection) => {
        let newValue = listValues[selection];
        handleChange(newValue.label);
        handleOutput(newValue);

    };

    const keyOut = (keyCode) => {
        props.keyOut && props.keyOut(keyCode);
    };

    const valueObject = {
        input,
        setInput,
        listValues,
        setListValues,
        selection,
        setSelection,
        handleChange,
        handleOutput,
        handleSelection,
        keyOut,
        baseValues
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};
