import React, {useEffect, useMemo} from 'react';

import InputClearable from "../../components/elements/InputClearable";
import BannerDropdownWrapper from "../../components/wrappers/BannerDropdownWrapper";
import ButtonDropdown from "../../components/elements/ButtonDropdown";
import ListBodySimple from "../../components/elements/ListBodySimple";
import ListItemSimple from "../../components/elements/ListItemSimple";

import {FocusContext, FocusProvider} from "./store/contexts/FocusContext";
import {StateContext, StateProvider} from "./store/contexts/StateContext";

import ClearableInputActions from "./store/actions/ClearableInputActions";
import DropdownButtonActions from "./store/actions/DropdownButtonActions";
import ListItemSimpleActions from "./store/actions/ListItemSimpleActions";
import useFocus from "../../library/helpers/hooks/useFocus";

const DropdownSearch = ({

    title = '',
    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    initValue = null,

}) => {

    // socketHook
    // refHook

    const {registerRef} = useFocus(focusIndex, 'DropdownSearch')

    const regRef = () => {
        if (focusIndex) {
            return registerRef()
        }
    };

    const contextObject = useMemo(() => {
        return {
            ClearableInputActions,
            DropdownButtonActions,
            ListItemSimpleActions,
            FocusContext,
            StateContext,
        }
    }, []);

    const components = useMemo (() => {
        return {
            InnerElement: InputClearable,
            DropdownButton: ButtonDropdown,
            DropdownElement: ListBodySimple,
            ListItem: ListItemSimple,
        }
    }, []);

    return (
        <div className={`SearchDropdownWrapper ${className}`}>
            <FocusProvider regRef={regRef}>
            <StateProvider inputValues={inputValues} outputValue={outputValue} keyOut={keyOut} socketIn={socketIn} socketOut={socketOut} initValue={initValue}>
                <div className={'MarginSpacer'}/>
                <div className={`FilterTitleWrapper`}>
                    <p className={`FilterTitleElement`}>{title}</p>
                </div>
                <BannerDropdownWrapper
                    className={'DropSearch'}
                    instanceName={instanceName}
                    contextObject={contextObject}
                    components={components}
                />
                <div className={'MarginSpacer'}/>
            </StateProvider>
            </FocusProvider>
        </div>
    )
};

export default DropdownSearch;