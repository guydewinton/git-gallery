import {useContext, useState} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';
import {checkWigletFocus, setWigletFocus} from "../../../../library/variables/focusVariable";


export default () => {

    const {setFocus, focus, rowClick, cellClick, inputRef, cellFocus, setCurrentFocus, setFocusArray, setActiveFocus, focusArray, activeFocus, updateActiveFocus, cellBlur, setCellFocus, keyPress} = useContext(FocusContext)
    const {setSelection, selectionHandler, listValues, selectionIndex, handleOutput} = useContext(StateContext)

    const registerInput = (focusArray) => {
        setFocusArray(prev => {
            let returnArray = prev;
            if (returnArray[focusArray[0][0]]) {
                returnArray = returnArray[focusArray[0][0]];
                returnArray[focusArray[0][1]] = focusArray[1];
            } else {
                returnArray[focusArray[0][0]] = [];
                returnArray = returnArray[focusArray[0][0]];
                returnArray[focusArray[0][1]] = focusArray[1];
            }
            return prev
        })
    };

    const clickHandler = () => {
        cellClick.current = true
    }

    const focusHandler = (i) => {
        if (keyPress.current) {
            focusArray[activeFocus[0]][activeFocus[1]].setSelectionRange(-1, -1)
        }
        setWigletFocus(inputRef)
        keyPress.current = false
        setCellFocus(true)
        cellBlur.current = false
        if (!focus) {
            setCurrentFocus()
            setActiveFocus(i)

        } else {
            setFocus(false)
        }
    };

    const blurHandler = () => {
        if (!keyPress.current) {
            cellBlur.current = true
            setWigletFocus(null)
            // selectionHandler([0, 0])
            setFocus(false)
            setCellFocus(false)
            checkWigletFocus(inputRef, focusArray[activeFocus[0]][activeFocus[1]].selectionStart, focusArray[activeFocus[0]][activeFocus[1]].selectionEnd)
        } else {
            focusArray[activeFocus[0]][activeFocus[1]].setSelectionRange(-1, -1)
        }
    };

    const keyDownHandler = (event) => {
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            keyPress.current = true
            if (focusArray[activeFocus[0]][activeFocus[1] + 1]) {
                setActiveFocus([activeFocus[0]] [activeFocus[1] + 1])
                focusArray[activeFocus[0]][activeFocus[1] + 1].focus()
            } else if (focusArray[activeFocus[0] + 1]) {
                setActiveFocus([activeFocus[0] + 1, 0])
                focusArray[activeFocus[0] + 1][0].focus()
            } else {
                setActiveFocus([0, 0])
                focusArray[0][0].focus()
            }
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            keyPress.current = true
            setCellFocus(false)
            selectionHandler(activeFocus[0])
            inputRef.focus()
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            keyPress.current = true
            setCellFocus(false)
            selectionHandler(activeFocus[0])
            inputRef.focus()
        }
    }

    return {
        keyDownHandler,
        focusHandler,
        blurHandler,
        registerInput,
        clickHandler
    }

}