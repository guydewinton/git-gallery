import {useContext, useEffect, useRef, useState} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';
import {checkWigletFocus, setWigletFocus} from "../../../../library/variables/focusVariable";


export default () => {

    const {setFocus, focus, setCellFocus, updateFocus, inputRef, setActiveFocus, activeFocus, cellClick, cellFocus, rowClick, focusArray, keyPress, setCurrentFocus, cellBlur, keyOut} = useContext(FocusContext)
    const {setSelection, selectionHandler, listValues, selectionIndex, handleOutput} = useContext(StateContext)

    const blurTimeoutRef = useRef(NaN);

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, []);

    const focusHandler = () => {
        setWigletFocus(inputRef)
        setFocus(true);
        setCurrentFocus();
        if (rowClick.current && cellBlur.current && !cellClick.current) {
            setCellFocus(false);
            rowClick.current = false;
            cellBlur.current = false
        } else if (cellBlur.current) {
            focusArray[activeFocus[0]][activeFocus[1]].focus()
        } else if (keyPress.current) {
            keyPress.current = false
        } else if (cellFocus) {
            focusArray[activeFocus[0]][activeFocus[1]].focus()
        } else {
            // selectionHandler(0)

        }
        cellClick.current = false
        rowClick.current = false

    };

    const blurHandler = () => {
        setWigletFocus(null)
        blurTimeoutRef.current = checkWigletFocus(inputRef, 0, 0, [setFocus])



    };

    const keyDownHandler = (event) => {
        if (event.keyCode === 38) { // UPARROW
            event.preventDefault();
            let newSelection;
            if (selectionIndex > 0) {
                newSelection = selectionIndex - 1
                // setSelectionIndex(() => selectionIndex - 1)
            } else {
                newSelection = listValues.length - 1
                // setSelectionIndex(() => listValues.length - 1)
            }
            selectionHandler(newSelection)
            keyPress.current = true

        }
        if (event.keyCode === 40) { // DOWNARROW
            event.preventDefault();
            let newSelection;
            if (selectionIndex < listValues.length-1) {
                newSelection = selectionIndex + 1
                // setSelectionIndex(() => selectionIndex + 1)
            } else {
                newSelection = 0
                // setSelectionIndex(() => 0)
            }
            selectionHandler(newSelection)
            keyPress.current = true

        }
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            handleOutput(selectionIndex);
            if (keyOut['9']) {
                keyOut['9'](event.keyCode)
            } else {
                updateFocus(event.keyCode);
            }
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            handleOutput(selectionIndex);
            setActiveFocus([selectionIndex, 0])
            keyPress.current = true
            focusArray[selectionIndex][0].focus()
            if (keyOut['13']) {
                keyOut['13'](event.keyCode)
            } else {
                updateFocus(event.keyCode);
            }
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            if (keyOut['27']) {
                keyOut['27'](event.keyCode)
            } else {
                updateFocus(event.keyCode);
            }

        }
    }

    return {
        keyDownHandler,
        focusHandler,
        blurHandler
    }

}