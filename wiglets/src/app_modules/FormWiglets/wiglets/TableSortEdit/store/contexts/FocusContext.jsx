import React, {createContext, useEffect, useRef, useState} from 'react';
import {setWigletFocus} from "../../../../library/variables/focusVariable";

export const FocusContext = createContext()

export const FocusProvider = (props) => {

    const [inputRef, setInputRef] = useState(null);
    const [focus, setFocus] = useState(false);
    const [focusArray, setFocusArray] = useState([]);
    const [activeFocus, setActiveFocus] = useState([0,0]);
    const keyPress = useRef(false);
    const [cellFocus, setCellFocus] = useState(false);
    const cellBlur = useRef(false)
    const rowClick = useRef(false)
    const cellClick = useRef(false)


    const {
        updateFocus,
        regRef,
        setCurrentFocus,
    } = props.regRef();

    useEffect(() => {
        if (inputRef) {
            regRef(inputRef)
        }
    }, [inputRef]);

    const handleKeyOut = (keyCode) => {
        props.keyOut(keyCode)
    }


    const valueObject = {
        inputRef,
        setInputRef,
        focus,
        setFocus,
        updateFocus,
        activeFocus,
        setActiveFocus,
        keyPress,
        cellFocus,
        focusArray,
        setCellFocus,
        setCurrentFocus,
        cellBlur,
        setFocusArray,
        rowClick,
        cellClick,
        keyOut: props.keyOut,
        handleKeyOut
    };

    return <FocusContext.Provider value={valueObject}>{props.children}</FocusContext.Provider>

};