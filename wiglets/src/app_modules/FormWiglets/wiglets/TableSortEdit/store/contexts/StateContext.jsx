import React, {createContext, useEffect, useState} from 'react';

import {
    sortObjectArrayNumber,
    sortObjectArrayNumberReverse,
    sortObjectArrayString,
    sortObjectArrayStringReverse
} from "../../../../library/helpers/functions/sortObjectArray";
import {
    includesFilterCaseInsensitive,
    includesFilterNumber
} from "../../../../library/helpers/functions/filterObjectArray";
import jobsInitial from "../../../../../../applications/InSign/library/initials/data/primaryLists/jobInitial";

export const StateContext = createContext()

export const StateProvider = (props) => {

    const [inputValues, setInputValues] = useState([]);
    const [listValues, setListValues] = useState([]);
    const [headerValues, setHeaderValues] = useState(props.headerValues);
    const [sortDescending, setSortDescending] = useState(true);
    const [sortIndex, setSortIndex] = useState(props.sortIndex);
    const [selection, setSelection] = useState(0)
    const [selectionIndex, setSelectionIndex] = useState(0);

    useEffect(() => {
        setInputValues(() => {
            return props.inputValues.map((object, i) => {
                return {
                    i,
                    ...object
                }
            })
        })
    }, [props.inputValues])

    useEffect(() => {
        setHeaderValues(props.headerValues)
    }, [props.headerValues])

    useEffect(() => {
    if (inputValues) {
        setListValues(() => {
            if (props.headerValues[sortIndex].type === 'string') {
                return [...sortObjectArrayString(inputValues, headerValues[sortIndex].value)]
            } else {
                return [...sortObjectArrayNumber(inputValues, headerValues[sortIndex].value)]
            }
        })
    }

    }, [inputValues, headerValues]);

    useEffect(() => {
        selectionHandler(0)
    }, [listValues]);

    useEffect(() => {
        if (props.filterValue) {
            if (props.headerValues[props.filterIndex].type === 'string') {
                setListValues(() => {
                    return includesFilterCaseInsensitive(inputValues, props.headerValues[props.filterIndex].value, props.filterValue)
                })
            } else {
                throw 'Table filter row must be a string.'
            }
        } else {
            setListValues(inputValues)
        }
    }, [props.filterValue])

    const selectionHandler = (index) => {
        setSelectionIndex(index)
        if (index === null) {
            setSelection(null)
            setSelectionIndex(null)
        } else {
            setSelection(() => {
                if (listValues[index]) {
                    return listValues[index].i
                }
            })
        }
    };

    const sortColumn = (value) => {
        if (value === sortIndex) {
            if (sortDescending) {
                setSortDescending(false);
                setListValues(() => {
                    if (props.headerValues[value].type === 'string') {
                        return [...sortObjectArrayStringReverse(inputValues, headerValues[value].value)]
                    } else {
                        return [...sortObjectArrayNumberReverse(inputValues, headerValues[value].value)]
                    }
                })
            } else {
                setSortDescending(true);
                setListValues(() => {
                    if (props.headerValues[value].type === 'string') {
                        return [...sortObjectArrayString(inputValues, headerValues[value].value)]
                    } else {
                        return [...sortObjectArrayNumber(inputValues, headerValues[value].value)]
                    }
                })
            }
        } else {
            setSortIndex(value);
            setSortDescending(true);
            setListValues(() => {
                if (props.headerValues[value].type === 'string') {
                    return [...sortObjectArrayString(inputValues, headerValues[value].value)]
                } else {
                    return [...sortObjectArrayNumber(inputValues, headerValues[value].value)]
                }
            })
        }
    };

    const handleOutput = (index) => {
        props.outputValue(listValues[index])
    };

    const valueObject = {
        listValues,
        setListValues,
        headerValues,
        sortColumn,
        sortIndex,
        sortDescending,
        selection,
        setSelection,
        selectionHandler,
        selectionIndex,
        setSelectionIndex,
        handleOutput
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};