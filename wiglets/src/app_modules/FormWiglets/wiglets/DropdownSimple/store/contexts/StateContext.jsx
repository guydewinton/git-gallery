import React, {createContext, useState} from 'react';

export const StateContext = createContext();

export const StateProvider = (props) => {

    const [input, setInput] = useState('');
    const [listValues, setListValues] = useState(props.inputValues);
    const [selection, setSelection] = useState(0)

    const handleChange = (value) => {
        setInput(value);
    };

    const handleOutput = (value) => {
        props.outputValue && props.outputValue(value);
    };

    const handleSelection = (selection) => {
        if (selection !== null) {
            let newValue = listValues[selection];
            handleChange(newValue.label);
            handleOutput(newValue);
        }
    };

    const valueObject = {
        input,
        setInput,
        listValues,
        setListValues,
        selection,
        setSelection,
        handleChange,
        handleOutput,
        handleSelection
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};
