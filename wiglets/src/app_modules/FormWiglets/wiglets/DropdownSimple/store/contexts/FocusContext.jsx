import React, {createContext, useState, useEffect} from 'react';

export const FocusContext = createContext();

export const FocusProvider = (props) => {

    const [focus, setFocus] = useState(false)
    const [inputRef, setInputRef] = useState(null);
    const [mouseOverBanner, setMouseOverBanner] = useState(false);
    const [mouseOverDropdown, setMouseOverDropdown] = useState(false);
    const [showDropdown, setShowDropdown] = useState(false);

    const {
        updateFocus,
        regRef,
        setCurrentFocus,
    } = props.regRef();

    useEffect(() => {
        if (inputRef) {
            regRef(inputRef)
        }
    }, [inputRef]);


    const valueObject = {
        focus,
        setFocus,
        mouseOverBanner,
        setMouseOverBanner,
        mouseOverDropdown,
        setMouseOverDropdown,
        inputRef,
        setInputRef,
        showDropdown,
        setShowDropdown,
        updateFocus,
        setCurrentFocus,
    };


    return <FocusContext.Provider value={valueObject}>{props.children}</FocusContext.Provider>

};