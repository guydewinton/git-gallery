import {useContext, useEffect} from 'react';

import {FocusContext} from '../contexts/FocusContext';




export default () => {

    const {showDropdown, setShowDropdown, inputRef, setFocus, focus} = useContext(FocusContext);

    const clickHandler = () => {
        inputRef.focus()
        setShowDropdown(!showDropdown)



    };

    return {clickHandler}

}