import React, {useContext, useEffect} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';


export default () => {

    const {setMouseOverDropdown, inputRef, setFocus, setShowDropdown, updateFocus, showDropdown} = useContext(FocusContext);
    const {handleChange, handleOutput, setSelection, selection} = useContext(StateContext);

    const clickHandler = (value, label, i) => {
        handleChange(label);
        handleOutput({value, label});
        setMouseOverDropdown(false);
        inputRef.blur();
        setFocus(false);
        setShowDropdown(false);
        updateFocus(9);
    };

    useEffect(() => {

        setSelection(null)


    }, [showDropdown])


    const rowFocusHandler = () => {
        inputRef.focus()
    }

    return {clickHandler, rowFocusHandler}

}