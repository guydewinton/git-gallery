import {useContext, useEffect, useRef, useState} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';
import {checkWigletFocus, setWigletFocus} from "../../../../library/variables/focusVariable";

export default () => {

    const {
        focus,
        setFocus,
        mouseOverBanner,
        setMouseOverBanner,
        mouseOverDropdown,
        setMouseOverDropdown,
        inputRef,
        setInputRef,
        showDropdown,
        setShowDropdown,
        updateFocus,
        setCurrentFocus
    } = useContext(FocusContext);

    const {
        input,
        setInput,
        listValues,
        setDropdownValues,
        selection,
        setSelection,
        handleChange,
        handleOutput,
        handleSelection,
    } = useContext(StateContext);

    const keyPress = useRef(false);

    const [blur, setBlur] = useState(false)

    const blurTimeoutRef = useRef(NaN)

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, [])

    const clickHandler = () => {
        if (!focus) {
            setShowDropdown(!showDropdown)
        }
        inputRef.focus()
    };

    const focusHandler = () => {
        setWigletFocus(inputRef);
        setFocus(true);
        setCurrentFocus()
        keyPress.current = false
        if (!blur) {
            setShowDropdown(true)
        }
        setBlur(false)
    };

    const blurHandler = () => {
        setBlur(true)
        if (keyPress.current) {
            setWigletFocus(null);
            blurTimeoutRef.current = checkWigletFocus(inputRef, 0, 0, [setFocus, setBlur])
        } else {
            setWigletFocus(null);
            if (!mouseOverDropdown) {
                if (!mouseOverBanner) {
                    setShowDropdown(false);
                }
            }
            blurTimeoutRef.current = checkWigletFocus(inputRef, 0, 0, [setFocus, setBlur])
        }



    };

    const keyDownHandler = (event) => {
        if (event.keyCode === 38) { // UPARROW
            event.preventDefault();
            if (showDropdown) {
                if (selection !== null) {
                    let newSelection
                    if (selection > 0) {
                        newSelection = selection - 1
                    } else {
                        newSelection = listValues.length - 1
                    }
                    setSelection(newSelection)
                    keyPress.current = true
                } else {
                    setSelection(listValues.length - 1)
                }
            }
        }
        if (event.keyCode === 40) { // DOWNARROW
            event.preventDefault();
            if (showDropdown) {
                if (selection !== null) {
                    let newSelection
                    if (selection < listValues.length - 1) {
                        newSelection = selection + 1
                    } else {
                        newSelection = 0
                    }
                    setSelection(newSelection)
                    keyPress.current = true
                } else {
                    setSelection(0)
                }
            } else {
                setShowDropdown(true)
            }
        }
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            setShowDropdown(false);
            handleSelection(selection);
            updateFocus(event.keyCode)
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            setShowDropdown(false);
            handleSelection(selection);
            updateFocus(event.keyCode)
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            if (showDropdown) {
                setShowDropdown(false);
            } else {
                updateFocus(event.keyCode);
            }
        }
    };

    const clearHandler = () => {
        handleChange('');
        inputRef.focus();
        setShowDropdown(true);
    };

    return {
        keyDownHandler,
        clickHandler,
        focusHandler,
        blurHandler,
        clearHandler,
    };

};
