import React from 'react';

import InputReadOnly from "../../components/elements/InputReadOnly";
import BannerDropdownWrapper from "../../components/wrappers/BannerDropdownWrapper";
import ButtonDropdown from "../../components/elements/ButtonDropdown";
import ListBodySimple from "../../components/elements/ListBodySimple";
import ListItemSimple from "../../components/elements/ListItemSimple";

import {FocusContext, FocusProvider} from "./store/contexts/FocusContext";
import {StateContext, StateProvider} from "./store/contexts/StateContext";

import ReadOnlyInputActions from "./store/actions/ReadOnlyInputActions";
import DropdownButtonActions from "./store/actions/DropdownButtonActions";
import ListItemSimpleActions from "./store/actions/ListItemSimpleActions";
import useFocus from "../../library/helpers/hooks/useFocus";

const DropdownSimple = ({

    title = '',
    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    initIndex = null

                        }) => {

    // socketHook
    // refHook

    const {registerRef} = useFocus(focusIndex, 'DropdownSimple')



    const regRef = (inputRef) => {
        if (focusIndex) {
            return registerRef()
        }
    };

    const contextObject = {
        ReadOnlyInputActions,
        DropdownButtonActions,
        ListItemSimpleActions,
        FocusContext,
        StateContext,
    };

    const components = {
        InnerElement: InputReadOnly,
        DropdownButton: ButtonDropdown,
        DropdownElement: ListBodySimple,
        ListItem: ListItemSimple,
    };

    return (
        <div className={`SimpleDropdownWrapper ${className}`}>
            <FocusProvider regRef={regRef} >
            <StateProvider inputValues={inputValues} outputValue={outputValue} keyOut={keyOut} socketIn={socketIn} socketOut={socketOut} >
                <div className={'MarginSpacer'}/>
                <div className={`FilterTitleWrapper`}>
                    <p className={`FilterTitleElement`}>{title}</p>
                </div>
                <BannerDropdownWrapper
                    className={'Dropdown'}
                    instanceName={instanceName}
                    contextObject={contextObject}
                    components={components}
                />
                <div className={'MarginSpacer'}/>
            </StateProvider>
            </FocusProvider>
        </div>
    )
};

export default DropdownSimple;