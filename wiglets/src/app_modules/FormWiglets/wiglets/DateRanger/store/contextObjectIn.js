import ClearableInputActions from "./actions/actionsIn/ClearableInputActions";
import CalendarButtonActions from "./actions/actionsIn/CalendarButtonActions";
import CalendarPickerActions from "./actions/actionsIn/CalendarPickerActions";
import {FocusContext} from "./contexts/contextsIn/FocusContextIn";
import {StateContext} from "./contexts/contextsIn/StateContextIn";

const contextObjectIn = {
    ClearableInputActions,
    CalendarButtonActions,
    CalendarPickerActions,
    FocusContext,
    StateContext,
};

export default contextObjectIn;