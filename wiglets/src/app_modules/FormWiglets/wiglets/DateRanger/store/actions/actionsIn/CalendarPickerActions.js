import {useContext, useEffect, useState} from 'react';

import {FocusContext} from "../../contexts/contextsIn/FocusContextIn";
import {StateContext} from "../../contexts/contextsIn/StateContextIn";

import {CombinedContext} from "../../CombinedContext";

import {setKeyPressIn} from "../../variables";


export default () => {

    const {
        focus,
        setFocus,
        mouseOverBanner,
        setMouseOverBanner,
        mouseOverDropdown,
        setMouseOverDropdown,
        inputRef,
        setInputRef,
        showDropdown,
        setShowDropdown,
        setKeyOutCode,
        keyOut
    } = useContext(FocusContext);

    const {
        input,
        setInput,
        handleOutput,
        handleChange,
    } = useContext(StateContext);

    const {dateRange, setDateRange} = useContext(CombinedContext);

    const initDate = new Date()

    const [dateValue, setDateValue] = useState([initDate, initDate]);


    const changeHandler = (date) => {
        if (date.length === 0) {
            // let newDate = new Date();
            // handleChange(newDate);
        } else {
            let dayDate = date.getDate();
            if (dayDate.toString().length === 1) {
                dayDate = `0${dayDate}`;
            }
            let monthDate = date.getMonth() + 1;
            if (monthDate.toString().length === 1) {
                monthDate = `0${monthDate}`;
            }
            handleChange(`${dayDate}/${monthDate}/${date.getFullYear()}`);
            const returnDateAssemblyIn = date;
            const returnDateAssemblyOut = dateRange[1] ? dateRange[1] : returnDateAssemblyIn;
            setDateValue([returnDateAssemblyIn, returnDateAssemblyOut]);
            setDateRange([returnDateAssemblyIn, returnDateAssemblyOut])
        }
        setMouseOverDropdown(false);
        setKeyPressIn(true)
        setShowDropdown(false);
        keyOut(9);
    };

    useEffect(() => {
        const cleanedArray = input.split('/');
        const returnDate = Array(3);
        const todayDate = new Date();
        if (cleanedArray[0]) {
            returnDate[0] = parseInt(cleanedArray[0])
        } else {
            returnDate[0] = todayDate.getDate()
        }
        if (cleanedArray[1]) {
            returnDate[1] = parseInt(cleanedArray[1]) - 1;
        } else {
            returnDate[1] = todayDate.getMonth()
        }
        if (cleanedArray[2]) {
            if (cleanedArray[2].length <= 2) {
                returnDate[2] = 2000 + parseInt(cleanedArray[2])
            }
            if (cleanedArray[2].length > 2) {
                returnDate[2] = parseInt(cleanedArray[2])
            }
        } else {
            returnDate[2] = todayDate.getFullYear()
        }
        const returnDateAssemblyIn = new Date(returnDate[2], returnDate[1], returnDate[0])
        const returnDateAssemblyOut = dateRange[1] ? dateRange[1] : returnDateAssemblyIn;
        setDateValue([returnDateAssemblyIn, returnDateAssemblyOut]);
        setDateRange([returnDateAssemblyIn, returnDateAssemblyOut])
    }, [input]);


    return {
        dateValue,
        setDateValue,
        changeHandler,
    };

};
