import {useContext, useEffect} from 'react';

import {FocusContext} from "../../contexts/contextsOut/FocusContextOut";
import {StateContext} from "../../contexts/contextsOut/StateContextOut";


export default () => {

    const {showDropdown, setShowDropdown, inputRef} = useContext(FocusContext);
    const stateValue = useContext(StateContext);

    const clickHandler = () => {
        setShowDropdown(!showDropdown)
        inputRef.focus()
    };

    return {clickHandler}

}