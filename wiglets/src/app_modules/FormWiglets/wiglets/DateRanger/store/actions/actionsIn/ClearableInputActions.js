import {useContext, useEffect, useRef} from 'react';

import {FocusContext} from "../../contexts/contextsIn/FocusContextIn";
import {StateContext} from "../../contexts/contextsIn/StateContextIn";
import {keyPressIn, setKeyPressIn} from "../../variables";
import {CombinedContext} from "../../CombinedContext";
import {checkWigletFocus, setWigletFocus} from "../../../../../library/variables/focusVariable";


export default () => {

    const {
        focus,
        setFocus,
        mouseOverBanner,
        setMouseOverBanner,
        mouseOverDropdown,
        setMouseOverDropdown,
        inputRef,
        setInputRef,
        showDropdown,
        setShowDropdown,
        setKeyOutCode,
        keyOutCode,
        updateFocus,
        setCurrentFocus,
        keyOut
    } = useContext(FocusContext);

    const {
        input,
        handleChange,
    } = useContext(StateContext);

    const {
        setSaveDateIn,
        setSaveDateOut,
    } = useContext(CombinedContext);

    const blurTimeoutRef = useRef(NaN)

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, [])

    const changeHandler = (event) => {
        handleChange(event.target.value);
    };

    const clickHandler = () => {
        setShowDropdown(true)
    };

    const focusHandler = () => {
        setWigletFocus(inputRef)
        setFocus(true);
        setCurrentFocus();
        setKeyPressIn(false);
        // inputRef.select();
    };

    const blurHandler = () => {
        setWigletFocus(null)
        const mouseOver = !mouseOverDropdown && !mouseOverBanner;
        if (mouseOver || keyPressIn) {
            setFocus(false);
            setShowDropdown(false);
            if (input.length === 8) {
                let prefixDate = input.slice(0, 6)
                let suffixDate = input.slice(-2)
                handleChange(prefixDate + '20' + suffixDate)
            }
            setSaveDateIn(true)
            blurTimeoutRef.current = checkWigletFocus(inputRef, inputRef.selectionStart, inputRef.selectionEnd, [setFocus])

        } else {
            inputRef.focus()
        }
    };

    const keyDownHandler = (event) => {
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            setKeyPressIn(true);
            setShowDropdown(false);
            keyOut();
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            setShowDropdown(false);
            setKeyPressIn(true);
            updateFocus(event.keyCode)
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            if (showDropdown) {
                setShowDropdown(false);
            } else {
                setKeyPressIn(true);
                updateFocus(event.keyCode);
            }
        }
    };

    const clearHandler = () => {
        handleChange('');
        inputRef.focus();
        setShowDropdown(true);
    };

    return {
        changeHandler,
        keyDownHandler,
        clickHandler,
        focusHandler,
        blurHandler,
        clearHandler,
    };

};
