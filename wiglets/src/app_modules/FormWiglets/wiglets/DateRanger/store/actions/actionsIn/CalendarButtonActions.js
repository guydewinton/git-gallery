import {useContext, useEffect} from 'react';

import {FocusContext} from "../../contexts/contextsIn/FocusContextIn";
import {StateContext} from "../../contexts/contextsIn/StateContextIn";


export default () => {

    const {showDropdown, setShowDropdown, inputRef} = useContext(FocusContext);
    const stateValue = useContext(StateContext);

    const clickHandler = () => {
        setShowDropdown(!showDropdown)
        inputRef.focus()
    };

    return {clickHandler}

}