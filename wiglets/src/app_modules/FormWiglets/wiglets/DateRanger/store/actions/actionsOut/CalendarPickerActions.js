import {useContext, useEffect, useState} from 'react';

import {FocusContext} from "../../contexts/contextsOut/FocusContextOut";
import {StateContext} from "../../contexts/contextsOut/StateContextOut";

import {CombinedContext} from "../../CombinedContext";
import {setKeyPressOut} from "../../variables";

export default () => {

    const {
        focus,
        setFocus,
        mouseOverBanner,
        setMouseOverBanner,
        mouseOverDropdown,
        setMouseOverDropdown,
        inputRef,
        setInputRef,
        showDropdown,
        setShowDropdown,
        updateFocus
    } = useContext(FocusContext);

    const {
        input,
        setInput,
        handleOutput,
        handleChange,
        keyOut
    } = useContext(StateContext);

    const {dateRange, setDateRange} = useContext(CombinedContext);

    const initDate = new Date()

    const [dateValue, setDateValue] = useState([initDate, initDate]);

    // useEffect(() => {
    //     if (dateValue[0] !== dateRange[0]) {
    //         console.log(dateValue, dateRange)
    //         setDateRange(dateValue)
    //     }
    // }, [dateValue]);
    //
    useEffect(() => {

            // console.log(dateValue, dateRange)
            setDateValue(dateRange)

    }, [dateRange]);

    const changeHandler = (date) => {
        if (date.length === 0) {
        } else {
            let dayDate = date.getDate();
            if (dayDate.toString().length === 1) {
                dayDate = `0${dayDate}`;
            }
            let monthDate = date.getMonth() + 1;
            if (monthDate.toString().length === 1) {
                monthDate = `0${monthDate}`;
            }
            handleChange(`${dayDate}/${monthDate}/${date.getFullYear()}`);
            // const returnDateAssemblyOut = date;
            // const returnDateAssemblyIn = dateRange[0] ? dateRange[0] : returnDateAssemblyOut;
            // setDateValue([returnDateAssemblyIn, returnDateAssemblyOut]);
            // setDateRange([returnDateAssemblyIn, returnDateAssemblyOut])
        }
        setMouseOverDropdown(false);
        setKeyPressOut(true)
        setShowDropdown(false);
        updateFocus(9);
    };

    useEffect(() => {
        const cleanedArray = input.split('/');
        const returnDate = Array(3);
        const todayDate = new Date();
        if (cleanedArray[0]) {
            returnDate[0] = parseInt(cleanedArray[0])
        } else {
            returnDate[0] = todayDate.getDate()
        }
        if (cleanedArray[1]) {
            returnDate[1] = parseInt(cleanedArray[1]) - 1;
        } else {
            returnDate[1] = todayDate.getMonth()
        }
        if (cleanedArray[2]) {
            if (cleanedArray[2].length <= 2) {
                returnDate[2] = 2000 + parseInt(cleanedArray[2])
            }
            if (cleanedArray[2].length > 2) {
                returnDate[2] = parseInt(cleanedArray[2])
            }
        } else {
            returnDate[2] = todayDate.getFullYear()
        }
        const returnDateAssemblyOut = new Date(returnDate[2], returnDate[1], returnDate[0])
        const returnDateAssemblyIn = dateRange[0] ? dateRange[0] : returnDateAssemblyOut;
        setDateValue([returnDateAssemblyIn, returnDateAssemblyOut]);
        setDateRange([returnDateAssemblyIn, returnDateAssemblyOut])
    }, [input]);


    return {
        dateValue,
        setDateValue,
        changeHandler,
    };

};
