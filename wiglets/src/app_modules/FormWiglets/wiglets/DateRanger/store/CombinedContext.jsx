import React, {createContext, useEffect, useState} from 'react';

export const CombinedContext = createContext();

export const CombinedProvider = (props) => {

    const initDate = new Date()

    const [dateRange, setDateRange] = useState([initDate, initDate]);
    const [inputOutRef, setInputOutRef] = useState()

    const [saveDateIn, setSaveDateIn] = useState(false)
    const [saveDateOut, setSaveDateOut] = useState(false)

    const [toggleOutput, setToggleOutput] = useState([false, false])

    useEffect(() => {
        if (toggleOutput[0] && toggleOutput[1]) {
            props.outputValue(dateRange)
        } else {
            props.outputValue([null, null])
        }
    }, [dateRange, toggleOutput])


    const valueObject = {
        dateRange,
        setDateRange,
        inputOutRef,
        setInputOutRef,
        saveDateIn,
        setSaveDateIn,
        saveDateOut,
        setSaveDateOut,
        setToggleOutput
    };

    return <CombinedContext.Provider value={valueObject}>{props.children}</CombinedContext.Provider>

};
