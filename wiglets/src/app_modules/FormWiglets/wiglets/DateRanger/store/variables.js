export let keyPressIn = false;

export const setKeyPressIn = (bool) => {
    keyPressIn = bool;
};

export let keyPressOut = false;

export const setKeyPressOut = (bool) => {
    keyPressOut = bool;
};