import React, {createContext, useState, useEffect, useContext} from 'react';
import {CombinedContext} from "../../CombinedContext";

export const FocusContext = createContext();

export const FocusProviderOut = (props) => {

    const {setInputOutRef} = useContext(CombinedContext)

    const [focus, setFocus] = useState(false)
    const [inputRef, setInputRef] = useState(null);
    const [mouseOverBanner, setMouseOverBanner] = useState(false);
    const [mouseOverDropdown, setMouseOverDropdown] = useState(false);
    const [showDropdown, setShowDropdown] = useState(false);

    const {
        updateFocus,
        regRef,
        setCurrentFocus,
    } = props.regRef();

    useEffect(() => {
        setInputOutRef(inputRef)
    }, [inputRef]);

    const valueObject = {
        focus,
        setFocus,
        mouseOverBanner,
        setMouseOverBanner,
        mouseOverDropdown,
        setMouseOverDropdown,
        inputRef,
        setInputRef,
        showDropdown,
        setShowDropdown,
        updateFocus,
        setCurrentFocus,
    };


    return <FocusContext.Provider value={valueObject}>{props.children}</FocusContext.Provider>

};