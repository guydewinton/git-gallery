import React, {createContext, useState, useEffect, useContext} from 'react';
import {CombinedContext} from "../../CombinedContext";

export const FocusContext = createContext();

export const FocusProviderIn = (props) => {

    const [focus, setFocus] = useState(false)
    const [inputRef, setInputRef] = useState(null);
    const [mouseOverBanner, setMouseOverBanner] = useState(false);
    const [mouseOverDropdown, setMouseOverDropdown] = useState(false);
    const [showDropdown, setShowDropdown] = useState(false);
    const {dateRange, setDateRange, inputOutRef} = useContext(CombinedContext);


    const {
        updateFocus,
        regRef,
        setCurrentFocus,
    } = props.regRef();

    useEffect(() => {
        if (inputRef) {
            regRef(inputRef)
        }
    }, [inputRef]);

    let keyOutCode = undefined;

    const setKeyOutCode = (keyCode) => {
        keyOutCode = keyCode
    }


    const keyOut = (keyCode) => {
        inputOutRef.focus()
    };

    const valueObject = {
        focus,
        setFocus,
        mouseOverBanner,
        setMouseOverBanner,
        mouseOverDropdown,
        setMouseOverDropdown,
        inputRef,
        setInputRef,
        showDropdown,
        setShowDropdown,
        keyOutCode,
        setKeyOutCode,
        updateFocus,
        setCurrentFocus,
        keyOut,
    };


    return <FocusContext.Provider value={valueObject}>{props.children}</FocusContext.Provider>

};