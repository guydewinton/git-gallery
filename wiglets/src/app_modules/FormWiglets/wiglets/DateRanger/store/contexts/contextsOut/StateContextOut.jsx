import React, {createContext, useContext, useEffect, useState} from 'react';

import {CombinedContext} from "../../CombinedContext";

import dateFormatter from "../../../../../library/helpers/functions/dateFormatter";

export const StateContext = createContext();

export const StateProviderOut = (props) => {

    const {dateRange, setDateRange, inputOutRef, saveDateIn, saveDateOut, setSaveDateIn, setToggleOutput} = useContext(CombinedContext);


    const [input, setInput] = useState('');
    const startDateIndex = 1

    useEffect(() => {
        handleChange(props.socketIn);
    }, [props.socketIn]);

    useEffect(() => {
        if (saveDateIn) {
            if (dateRange[0] > dateRange[1]) {
                console.log('NOOOOOOOOOOOOOOOO!!!!!!!')
                let dayDate = dateRange[0].getDate();
                if (dayDate.toString().length === 1) {
                    dayDate = `0${dayDate}`;
                }
                let monthDate = dateRange[0].getMonth() + 1;
                if (monthDate.toString().length === 1) {
                    monthDate = `0${monthDate}`;
                }
                setInput(`${dayDate}/${monthDate}/${dateRange[0].getFullYear()}`);
                setDateRange([dateRange[0], dateRange[0]])
            }
            setSaveDateIn(false)
        }

    }, [saveDateIn, dateRange]);

    useEffect(() => {
        if (input === '') {
            setToggleOutput(prev => {return [prev[0], false]})
        } else {
            setToggleOutput(prev => {return [prev[0], true]})
        }
    }, [input])

    const handleChange = (inputValue) => {

        if (inputValue.length > input.length) {
            let formattedInput = dateFormatter(inputValue);
            setInput(formattedInput);
            props.socketOut(formattedInput);

        } else {
            setInput(inputValue);
            props.socketOut(inputValue);

        }
    };

    const keyOut = (keyCode) => {
        props.keyOut(keyCode);
    };

    const valueObject = {
        input,
        setInput,
        handleChange,
        keyOut,
        startDateIndex
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};
