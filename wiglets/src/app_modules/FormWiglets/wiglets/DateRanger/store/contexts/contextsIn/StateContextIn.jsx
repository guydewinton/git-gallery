import React, {createContext, useContext, useEffect, useState} from 'react';

import {CombinedContext} from "../../CombinedContext";

import dateFormatter from "../../../../../library/helpers/functions/dateFormatter";
import {FocusContext} from "../contextsIn/FocusContextIn";


export const StateContext = createContext();

export const StateProviderIn = (props) => {

    const {dateRange, setDateRange, inputOutRef, setSaveDateOut, saveDateOut, setToggleOutput} = useContext(CombinedContext);


    const [input, setInput] = useState('');
    const startDateIndex = 0

    useEffect(() => {
        handleChange(props.socketIn);
    }, [props.socketIn]);

    useEffect(() => {
        if (saveDateOut) {
            if (dateRange[0] > dateRange[1]) {
                console.log('NOOOOOOOOOOOOOOOO!!!!!!!')
                let dayDate = dateRange[1].getDate();
                if (dayDate.toString().length === 1) {
                    dayDate = `0${dayDate}`;
                }
                let monthDate = dateRange[1].getMonth() + 1;
                if (monthDate.toString().length === 1) {
                    monthDate = `0${monthDate}`;
                }
                setInput(`${dayDate}/${monthDate}/${dateRange[1].getFullYear()}`);
                setDateRange([dateRange[1], dateRange[1]])
            }
            setSaveDateOut(false)
        }

    }, [saveDateOut, dateRange]);

    useEffect(() => {
        if (input === '') {
            setToggleOutput(prev => {return [false, prev[1]]})
        } else {
            setToggleOutput(prev => {return [true, prev[1]]})
        }
    }, [input])


    const handleChange = (inputValue) => {
        if (inputValue.length > input.length) {
            let formattedInput = dateFormatter(inputValue);
            setInput(formattedInput);
            props.socketOut(formattedInput);
            console.log('handle date range change 1', new Date(inputValue))

        } else {
            setInput(inputValue);
            props.socketOut(inputValue);
            console.log('handle date range change 2', inputValue)
        }
    };

    const keyOut = (keyCode) => {
        props.keyOut(keyCode);
    };

    const valueObject = {
        input,
        setInput,
        handleChange,
        keyOut,
        startDateIndex
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};
