import ClearableInputActions from "./actions/actionsOut/ClearableInputActions";
import CalendarButtonActions from "./actions/actionsOut/CalendarButtonActions";
import CalendarPickerActions from "./actions/actionsOut/CalendarPickerActions";
import {FocusContext} from "./contexts/contextsOut/FocusContextOut";
import {StateContext} from "./contexts/contextsOut/StateContextOut";

const contextObjectOut = {
    ClearableInputActions,
    CalendarButtonActions,
    CalendarPickerActions,
    FocusContext,
    StateContext,
};

export default contextObjectOut;