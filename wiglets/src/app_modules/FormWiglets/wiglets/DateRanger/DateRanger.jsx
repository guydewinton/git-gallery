import React from 'react';

import InputClearable from "../../components/elements/InputClearable";
import BannerDropdownWrapper from "../../components/wrappers/BannerDropdownWrapper";
import ButtonCalendar from "../../components/elements/ButtonCalendar";
import CalendarPicker from "../../components/elements/CalendarPicker";

import {FocusProviderIn} from "./store/contexts/contextsIn/FocusContextIn";
import {FocusProviderOut} from "./store/contexts/contextsOut/FocusContextOut";

import {StateProviderIn} from "./store/contexts/contextsIn/StateContextIn";
import {StateProviderOut} from "./store/contexts/contextsOut/StateContextOut";

import {CombinedProvider} from "./store/CombinedContext";

import contextObjectIn from "./store/contextObjectIn";
import contextObjectOut from "./store/contextObjectOut";
import useFocus from "../../library/helpers/hooks/useFocus";


const DateRanger = ({

    titleIn = '',
    titleOut = '',
    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    initValue = '',

                    }) => {

    // socketHook
    // refHook
    const {registerRef} = useFocus(focusIndex, 'DateRanger')



    const regRef = () => {
        if (focusIndex) {
            return registerRef()
        }
    };


    const componentsIn = {
        InnerElement: InputClearable,
        DropdownButton: ButtonCalendar,
        DropdownElement: CalendarPicker,
    };

    const componentsOut = {
        InnerElement: InputClearable,
        DropdownButton: ButtonCalendar,
        DropdownElement: CalendarPicker,
    };

    return (
        <div className={className}>
            <CombinedProvider outputValue={outputValue}>
                <FocusProviderIn regRef={regRef}>
                    <StateProviderIn inputValues={inputValues} keyOut={keyOut} socketIn={socketIn} socketOut={socketOut} >
                        <div className={'MarginSpacer'}/>
                        <div className={`FilterTitleWrapper`}>
                            <p className={`FilterTitleElement`}>{titleIn}</p>
                        </div>
                        <BannerDropdownWrapper
                            className={'DateRanger'}
                            instanceName={instanceName}
                            contextObject={contextObjectIn}
                            components={componentsIn}
                        />
                    </StateProviderIn>
                </FocusProviderIn>
                <FocusProviderOut regRef={regRef}>
                    <StateProviderOut inputValues={inputValues} keyOut={keyOut} socketIn={socketIn} socketOut={socketOut} >
                        <div className={'MarginSpacer'}/>
                        <div className={'MarginSpacer'}/>
                        <div className={`FilterTitleWrapper`}>
                            <p className={`FilterTitleElement`}>{titleOut}</p>
                        </div>
                        <BannerDropdownWrapper
                            className={'DateRanger'}
                            instanceName={instanceName}
                            contextObject={contextObjectOut}
                            components={componentsOut}
                        />
                        <div className={'MarginSpacer'}/>
                    </StateProviderOut>
                </FocusProviderOut>
            </CombinedProvider>
        </div>
    )
};

export default DateRanger;