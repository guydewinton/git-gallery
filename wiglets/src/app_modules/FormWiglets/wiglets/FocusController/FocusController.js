import React, {useContext, useEffect, useRef} from 'react';

import {MasterFocusContext} from "../../store/MasterFocusContext";

const FocusController = ({
                            defaultArray = [0,0,0],
                            focusUpdate = null
                        }) => {

    const {setCurrentFocus, pageLoadSetter, setUpdate} = useContext(MasterFocusContext);

    useEffect(() => {
        setCurrentFocus(defaultArray)

    }, []);

    useEffect(() => {

        return () => {
            pageLoadSetter(false)
            setUpdate([0,0])
        }
    }, []);

    useEffect(() => {
       if (focusUpdate) {
           setCurrentFocus(focusUpdate)
        }
    }, [focusUpdate]);


    return (
        <></>
    )
};

export default FocusController;