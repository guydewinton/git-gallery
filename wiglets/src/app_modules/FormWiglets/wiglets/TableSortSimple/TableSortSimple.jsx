import React, {useEffect} from 'react';

import TableHeaderActions from "./store/actions/TableHeaderActions";
import TableRowActions from "./store/actions/TableRowActions";
import {FocusContext, FocusProvider} from "./store/contexts/FocusContext";
import {StateContext, StateProvider} from "./store/contexts/StateContext";

import TableHeaderSimple from "../../components/elements/TableHeaderSimple";
import TableBody from "../../components/elements/TableBody";

import HeaderBodyWrapper from "../../components/wrappers/HeaderBodyWrapper";
import TableRowSimple from "../../components/elements/TableRowSimple";
import TableItemDisplayString from "../../components/elements/TableItemDisplayString";
import useFocus from "../../library/helpers/hooks/useFocus";
import InputHiddenActions from "./store/actions/InputHiddenActions";
import HiddenElementWrapper from "../../components/wrappers/HiddenElementWrapper";
import InputHidden from "../../components/elements/InputHidden";
import TableHeaderItem from "../../components/elements/TableHeaderItem";

const TableSortSimple = ({

    title = '',
    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    filterValue = null,
    filterIndex = 0,
    filterArray = [],
    headerValues = [],
    sortIndex = 0,

    ButtonElementLeft = null,
    ButtonElementRight = null,

                            }) => {


    const {registerRef} = useFocus(focusIndex, 'TableSortSimple')

    const regRef = () => {
        if (focusIndex) {
            return registerRef()
        }
    };

    const contextObject = {
        FocusContext,
        StateContext,
        TableHeaderActions,
        TableRowActions,
        InputHiddenActions,

    };

    const components = {
        HiddenElement: InputHidden,
        InnerElement: HeaderBodyWrapper,
        HeaderElement: TableHeaderSimple,
        TableHeaderRowItem: TableHeaderItem,
        BodyElement: TableBody,
        TableRow: TableRowSimple,
        TableRowTextItem: TableItemDisplayString,
        ButtonElementLeft,
        ButtonElementRight,


    };

    return (
        <div className={`SortableTableSimpleWrapper ${className}`}>
            <StateProvider 
                inputValues={inputValues} 
                headerValues={headerValues} 
                filterValue={filterValue} 
                filterIndex={filterIndex}
                sortIndex={sortIndex}
                outputValue={outputValue}
                filterArray={filterArray}
                >
                <FocusProvider regRef={regRef} >
                    <HiddenElementWrapper
                        className={'TableSortSimple'}
                        instanceName={instanceName}
                        contextObject={contextObject}
                        components={components}
                    />
                </FocusProvider>
            </StateProvider>
        </div>
    )
};

export default TableSortSimple;