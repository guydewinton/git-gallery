import {useContext, useEffect, useState} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';

export default () => {

    const {setFocus, updateFocus, focus, inputRef, rowClick} = useContext(FocusContext)
    const {setSelection, selectionHandler, listValues, selectionIndex, handleOutput} = useContext(StateContext)


    const clickHandler = (i) => {
        rowClick.current = true
        selectionHandler(i)
        inputRef.focus()
        handleOutput(i)
    };

    const mouseOverHandler = (i) => {
        if (focus) {
            selectionHandler(i)
        }
    };

    const mouseOutHandler = (i) => {
        // if (focus) {
        //     selectionHandler(null)
        // }
    };


    const rowFocusHandler = () => {
        inputRef.focus()
    };



    return {
        clickHandler,
        mouseOverHandler,
        mouseOutHandler,
        rowFocusHandler
    }

}