import {useContext, useEffect, useRef, useState} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';
import {checkWigletFocus, setWigletFocus} from "../../../../library/variables/focusVariable";


export default () => {

    const {setFocus, updateFocus, inputRef, rowClick, setCurrentFocus, focus} = useContext(FocusContext)
    const {setSelection, selectionHandler, listValues, selectionIndex, handleOutput} = useContext(StateContext)

    const blurTimeoutRef = useRef(NaN)

    useEffect(() => {
        return () => {
            clearTimeout(blurTimeoutRef.current)
        }
    }, []);

    const focusHandler = () => {
        if (!rowClick.current && !focus) {
            setSelection(0)
        }
        setWigletFocus(inputRef);
        setFocus(true);
        setCurrentFocus();
        rowClick.current = false;

    };

    const blurHandler = () => {
        setWigletFocus(null);
        blurTimeoutRef.current = checkWigletFocus(inputRef, 0, 0, [setFocus])
    };

    const keyDownHandler = (event) => {
        if (event.keyCode === 38) { // UPARROW
            event.preventDefault();
            let newSelection;
            if (selectionIndex > 0) {
                newSelection = selectionIndex - 1
            } else {
                newSelection = listValues.length - 1
            }
            selectionHandler(newSelection)
        }
        if (event.keyCode === 40) { // DOWNARROW
            event.preventDefault();
            let newSelection;
            if (selectionIndex < listValues.length-1) {
                newSelection = selectionIndex + 1
            } else {
                newSelection = 0
            }
            selectionHandler(newSelection)

        }
        if (event.keyCode === 9) { // TAB
            event.preventDefault();
            handleOutput(selectionIndex);
            updateFocus(event.keyCode)
        }
        if (event.keyCode === 13) { // ENTER
            event.preventDefault();
            handleOutput(selectionIndex);
            updateFocus(event.keyCode)
        }
        if (event.keyCode === 27) { // ESC
            event.preventDefault();
            updateFocus(event.keyCode);
        }
    }

    return {
        keyDownHandler,
        focusHandler,
        blurHandler
    }

}