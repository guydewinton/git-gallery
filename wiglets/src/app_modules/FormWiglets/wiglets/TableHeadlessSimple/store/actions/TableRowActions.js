import {useContext, useEffect, useState} from 'react';

import {FocusContext} from '../contexts/FocusContext';
import {StateContext} from '../contexts/StateContext';


export default () => {

    const {setFocus, rowClick, focus, inputRef} = useContext(FocusContext)
    const {setSelection, selectionHandler, listValues, selectionIndex, handleOutput} = useContext(StateContext)

    const clickHandler = (value) => {
        rowClick.current = true
        selectionHandler(value)
        inputRef.focus()
        handleOutput(value)
    };

    const mouseOverHandler = (i) => {
        if (focus) {
            selectionHandler(i)
        }
    }

    const mouseOutHandler = (i) => {
        // if (focus) {
        //     selectionHandler(null)
        // }
    }


    const rowFocusHandler = () => {
        inputRef.focus()
    }



    return {
        clickHandler,
        mouseOverHandler,
        mouseOutHandler,
        rowFocusHandler
    }

}