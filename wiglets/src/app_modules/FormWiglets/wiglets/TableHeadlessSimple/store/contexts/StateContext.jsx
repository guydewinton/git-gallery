import React, {createContext, useEffect, useState} from 'react';

import {
    sortObjectArrayDate,
    sortObjectArrayNumber,
    sortObjectArrayNumberReverse,
    sortObjectArrayString,
    sortObjectArrayStringReverse
} from "../../../../library/helpers/functions/sortObjectArray";
import {
    includesFilterCaseInsensitive, includesFilterNumber, rangeFilterDate
} from "../../../../library/helpers/functions/filterObjectArray";

export const StateContext = createContext()

export const StateProvider = (props) => {

    const [inputValues, setInputValues] = useState([]);
    const [listValues, setListValues] = useState([]);
    const [headerValues, setHeaderValues] = useState(props.headerValues);
    const [sortDescending, setSortDescending] = useState(true);
    const [sortIndex, setSortIndex] = useState(props.sortIndex);
    const [selection, setSelection] = useState(0)
    const [selectionIndex, setSelectionIndex] = useState(0);

    useEffect(() => {
        console.log(props.inputValues)
        setInputValues(() => {
            return props.inputValues.map((object, i) => {
                return {
                    i,
                    ...object
                }
            })
        })
    }, [props.inputValues])

    useEffect(() => {
        setHeaderValues(props.headerValues)
    }, [props.headerValues])

    useEffect(() => {
        setListValues(() => {
            if (props.headerValues[sortIndex].type === 'string') {
                return [...sortObjectArrayString(inputValues, headerValues[sortIndex].value)]
            } else if (props.headerValues[sortIndex].type === 'number') {
                return [...sortObjectArrayNumber(inputValues, headerValues[sortIndex].value)]
            } else if (props.headerValues[sortIndex].type === 'date') {
                return [...sortObjectArrayDate(inputValues, headerValues[sortIndex].value)]
            }
        })
    }, [inputValues]);

    useEffect(() => {
        selectionHandler(0)
    }, [listValues]);

    useEffect(() => {
        let returnValues = inputValues;
        props.filterArray.forEach((filterSet) => {
            if (typeof filterSet.index === 'object') {
                if (props.headerValues[filterSet.index[0]].type === 'date' && props.headerValues[filterSet.index[1]].type === 'date') {
                    returnValues = rangeFilterDate(returnValues, props.headerValues[filterSet.index[0]].value, props.headerValues[filterSet.index[1]].value, filterSet.value)
                } else {
                    throw 'Invalid filter type'
                }
            } else {
                if (props.headerValues[filterSet.index].type === 'string') {
                    returnValues = includesFilterCaseInsensitive(returnValues, props.headerValues[filterSet.index].value, filterSet.value)
                } else if (props.headerValues[filterSet.index].type === 'number') {
                    returnValues = includesFilterNumber(returnValues, props.headerValues[filterSet.index].value, filterSet.value)
                } else {
                    throw 'Invalid filter type'
                }
            }

        })
        setListValues(returnValues)

    }, [props.filterArray])

    const selectionHandler = (index) => {
        setSelectionIndex(index)
        if (index === null) {
            setSelection(null)
            setSelectionIndex(null)
        } else {
            setSelection(() => {
                if (listValues[index]) {
                    return listValues[index].i
                }
            })
        }
    };


    const handleOutput = (index) => {
        props.outputValue(listValues[index])
    };

    const valueObject = {
        listValues,
        setListValues,
        headerValues,
        sortIndex,
        sortDescending,
        selection,
        setSelection,
        selectionHandler,
        selectionIndex,
        setSelectionIndex,
        handleOutput
    };

    return <StateContext.Provider value={valueObject}>{props.children}</StateContext.Provider>

};