import React, {createContext, useEffect, useRef, useState} from 'react';

export const FocusContext = createContext()

export const FocusProvider = (props) => {

    const [inputRef, setInputRef] = useState(null);
    const [focus, setFocus] = useState(false);
    const rowClick = useRef(false)



    const {
        updateFocus,
        regRef,
        setCurrentFocus,
    } = props.regRef();

    useEffect(() => {
        if (inputRef) {
            regRef(inputRef)
        }
    }, [inputRef]);


    const valueObject = {
        inputRef,
        setInputRef,
        focus,
        setFocus,
        updateFocus,
        setCurrentFocus,
        rowClick
    };

    return <FocusContext.Provider value={valueObject}>{props.children}</FocusContext.Provider>

};