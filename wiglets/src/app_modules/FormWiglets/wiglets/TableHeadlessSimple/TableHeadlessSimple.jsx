import React, {useEffect} from 'react';

import TableRowActions from "./store/actions/TableRowActions";
import {FocusContext, FocusProvider} from "./store/contexts/FocusContext";
import {StateContext, StateProvider} from "./store/contexts/StateContext";

import TableHeaderSimple from "../../components/elements/TableHeaderSimple";
import TableBody from "../../components/elements/TableBody";

import HeaderBodyWrapper from "../../components/wrappers/HeaderBodyWrapper";
import TableRowSimple from "../../components/elements/TableRowSimple";
import TableItemDisplayString from "../../components/elements/TableItemDisplayString";
import useFocus from "../../library/helpers/hooks/useFocus";
import InputHiddenActions from "./store/actions/InputHiddenActions";
import HiddenElementWrapper from "../../components/wrappers/HiddenElementWrapper";
import InputHidden from "../../components/elements/InputHidden";
import TableHeaderItem from "../../components/elements/TableHeaderItem";
import BodyWrapper from "../../components/wrappers/BodyWrapper";

const TableHeadlessSimple = ({

    title = '',
    className = '',
    instanceName = '',
    inputValues,
    outputValue = (output) => console.log('output not rigged', output),
    keyOut = (keyCode) => console.log('keyOut not rigged', keyCode),
    socketIn = '',
    socketOut = (socketOutput) => console.log('socketOutput not rigged', socketOutput),
    focusIndex = null,
    focusOut = null,
    blurOut = null,
    defaultKeyNull = {},

    filterArray = [],
    filterValue = null,
    filterIndex = 0,
    headerValues = [],
    sortIndex = 0,

    ButtonElementLeft = null,
    ButtonElementRight = null,

                            }) => {


    const {registerRef} = useFocus(focusIndex, 'TableSortableSimple')

    const regRef = () => {
        if (focusIndex) {
            return registerRef()
        }
    };

    const contextObject = {
        FocusContext,
        StateContext,
        TableRowActions,
        InputHiddenActions,

    };

    const components = {
        HiddenElement: InputHidden,
        InnerElement: BodyWrapper,
        BodyElement: TableBody,
        TableRow: TableRowSimple,
        TableRowTextItem: TableItemDisplayString,
        ButtonElementLeft,
        ButtonElementRight,


    };

    return (
        <div className={`ListTableWrapper ${className}`}>
            <StateProvider 
                inputValues={inputValues} 
                headerValues={headerValues}
                filterArray={filterArray}
                filterValue={filterValue} 
                filterIndex={filterIndex}
                sortIndex={sortIndex}
                outputValue={outputValue}
                >
                <FocusProvider regRef={regRef} >
                    <HiddenElementWrapper
                        className={'TableHeadlessSimple'}
                        instanceName={instanceName}
                        contextObject={contextObject}
                        components={components}
                    />
                </FocusProvider>
            </StateProvider>
        </div>
    )
};

export default TableHeadlessSimple;