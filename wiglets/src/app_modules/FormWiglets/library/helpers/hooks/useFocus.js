import React, {useEffect, useContext, useState} from "react";

import {MasterFocusContext} from "../../../store/MasterFocusContext";


const useFocus = (focusArray, componentName) => {

    const {addFocusRef, removeFocusRef, updateFocus, update, setCurrentFocus, setUpdate, pageLoad} = useContext(MasterFocusContext);
    const [inputRef, setInputRef] = useState(null);

    useEffect(() => {
        if (inputRef !== null) {
            setUpdate(prev => {
                return [prev[0], prev[1] + 1]
            })
        }
    }, [inputRef]);

    useEffect(() => {
        setUpdate(prev => {
            return [prev[0] + 1, prev[1]]
        })
        return () => {
            removeFocusRef(focusArray)
        }
    }, []);

    const addRef = (focusArray, inputRef) => {
        if (inputRef) {
            addFocusRef(focusArray, inputRef);
        }
    };

    const registerRef = () => {
        return {
            updateFocus,
            regRef: (inputRef) => {
                setInputRef(inputRef);
                addRef(focusArray, inputRef);
            },
            setCurrentFocus: () => setCurrentFocus(focusArray),
        }
    };

    return {
        registerRef
    };

};

export default useFocus;