export default (date) => {

    const getDay = () => {
        if (date.getDate() < 10) {
            return `0${date.getDate()}`
        } else {
            return date.getDate()
        }
    };

    const getMonth = () => {
        if (date.getMonth() < 9) {
            return `0${date.getMonth() + 1}`
        } else {
            return date.getMonth() + 1
        }
    };

    return `${getDay()}/${getMonth()}/${date.getFullYear()}`
};