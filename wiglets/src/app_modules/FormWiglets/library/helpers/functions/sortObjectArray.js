export const sortObjectArrayString = (objectArray, index) => {
    objectArray.sort((a, b) => {
        let higher = a[index].toLowerCase();
        let lower = b[index].toLowerCase();
        if (lower > higher) return -1;
        if (higher > lower) return 1;
        return 0;
    });
    return objectArray;
};

export const sortObjectArrayStringReverse = (objectArray, index) => {
    objectArray.sort((a, b) => {
        let higher = b[index].toLowerCase();
        let lower = a[index].toLowerCase();
        if (lower > higher) return -1;
        if (higher > lower) return 1;
        return 0;
    });
    return objectArray;
};

export const sortObjectArrayNumber = (objectArray, index) => {
    objectArray.sort((a, b) => {
        let higher = a[index];
        let lower = b[index];
        if (lower > higher) return -1;
        if (higher > lower) return 1;
        return 0;
    });
    return objectArray;
};

export const sortObjectArrayNumberReverse = (objectArray, index) => {
    objectArray.sort((a, b) => {
        let higher = b[index];
        let lower = a[index];
        if (lower > higher) return -1;
        if (higher > lower) return 1;
        return 0;
    });
    return objectArray;
};

export const sortObjectArrayDate = (objectArray, index) => {
    objectArray.sort((a, b) => {
        let higher = new Date(a[index]);
        let lower = new Date(b[index]);
        if (lower > higher) return -1;
        if (higher > lower) return 1;
        return 0;
    });
    return objectArray;
};

export const sortObjectArrayDateReverse = (objectArray, index) => {
    objectArray.sort((a, b) => {
        let higher = new Date(b[index]);
        let lower = new Date(a[index]);
        if (lower > higher) return -1;
        if (higher > lower) return 1;
        return 0;
    });
    return objectArray;
};
