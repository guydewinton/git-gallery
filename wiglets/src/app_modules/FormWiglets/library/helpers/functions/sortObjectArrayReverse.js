export default (objectArray, index) => {
    objectArray.sort((a, b) => {
        let higher = b[index].toLowerCase();
        let lower = a[index].toLowerCase();
        if (lower > higher) return -1;
        if (higher > lower) return 1;
        return 0;
    });
    return objectArray;
};