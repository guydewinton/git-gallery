export default (input) => {

    input = input.replace('.', '/')

    const cleanedArray = input.split('/');

    const returnArray = [cleanedArray.join('')];

    if (returnArray[0]) {
        if (returnArray[0].length === 2) {
            if (parseInt(returnArray[0]) > 31 || parseInt(returnArray[0]) < 1) {
                returnArray[0] = ''
            } else {
                returnArray[1] = ''
            }
        }
    }

    if (returnArray[0]) {
        if (returnArray[0].length > 2) {
            returnArray[1] = returnArray[0].slice(2);
            returnArray[0] = returnArray[0].slice(0, 2);
        }
        if(/[^0-9]/.test(returnArray[0])) {
            returnArray[0] = ''
            returnArray.splice(1, 1)
        }
    }

    if (returnArray[1]) {
        if (returnArray[1].length === 2) {
            if (parseInt(returnArray[1]) > 12 || parseInt(returnArray[0]) < 1) {
                returnArray[1] = ''
            } else {
                returnArray[2] = ''
            }
        }
    }

    if (returnArray[1]) {
        if (returnArray[1].length > 2) {
            returnArray[2] = returnArray[1].slice(2);
            returnArray[1] = returnArray[1].slice(0, 2);
        }
        if(/[^0-9]/.test(returnArray[1])) {
            returnArray[1] = ''
            returnArray.splice(2, 1)
        }
    }

    if (returnArray[2]) {
        if (returnArray[2].length > 4) {
            returnArray[2] = returnArray[2].slice(0, 4);
        }
        if(/[^0-9]/.test(returnArray[2])) {
            returnArray[2] = ''
        }
    }

    return returnArray.join('/')

}


// enter two digits
// display four
// allow