export const startsWithFilterCaseSensitive = (objectArray, index, input) => {
    return objectArray.filter((option) => {
        return option[index].startsWith(input)
    });
};

export const startsWithFilterCaseInsensitive = (objectArray, index, input) => {
    return objectArray.filter((option) => {
        return option[index].toLowerCase().startsWith(input.toLowerCase())
    });
};

export const includesFilterCaseSensitive = (objectArray, index, input) => {
    return objectArray.filter((option) => {
        return option[index].includes(input)
    });
};

export const includesFilterCaseInsensitive = (objectArray, index, input) => {
    return objectArray.filter((option) => {
        return option[index].toLowerCase().includes(input.toLowerCase())
    });
};

export const startsWithFilterNumber = (objectArray, index, input) => {
    return objectArray.filter((option) => {
        return option[index].toString().startsWith(input.toString())
    });
};

export const includesFilterNumber = (objectArray, index, input) => {
    return objectArray.filter((option) => {
        return option[index].toString().includes(input.toString())
    });
};

// export const startsWithFilterDate = (objectArray, index, input) => {
//     return objectArray.filter((option) => {
//         return option[index].toString().startsWith(input.toString())
//     });
// };
//
// export const includesFilterDate = (objectArray, index, input) => {
//     return objectArray.filter((option) => {
//         return option[index].toString().includes(input.toString())
//     });
// };

export const rangeFilterDate = (objectArray, fromIndex, toIndex, input) => {
    return objectArray.filter((option) => {
        if (input && input[0] && input[1]) {
            const startDate = option[fromIndex] > input[0] && option[fromIndex] < input[1]
            const endDate = option[toIndex] > input[0] && option[toIndex] < input[1]
            const wholeDate = option[fromIndex] < input[0] && option[toIndex] > input[1]
            console.log('startDate', startDate, input[0], option[fromIndex], input[1], 'endDate', endDate, wholeDate)
            return (startDate || endDate) || wholeDate
        } else {
            return true
        }
    });
};