let wigletFocus = false;

export const setWigletFocus = (update) => {
    wigletFocus = update;
};

export const checkWigletFocus = (input, caretIn = 0, caretOut = 0, negFunctions = []) => {
    return (setTimeout(() => {
        if (!wigletFocus) {
            input.focus()
            input.setSelectionRange(caretIn, caretOut)
        }
        if (wigletFocus !== input) {
            negFunctions.forEach((negFunction) => {
                negFunction(false)
            })
        }
    }, 200))
}