import React, { useEffect, useState } from 'react';

const PageController = (props) => {

    const [refArray, setRefArray] = useState({});

    useEffect(() => {
        console.log('useEffect', props.linkIn)
        if (Object.keys(refArray).length === props.children.length) {
            if (props.linkIn) {
                if (props.linkIn.cluster === props.clusterID) {
                    console.log('useEffect', props.linkIn.cluster, props.clusterID)
                    refArray[0].focus()
                }
            } else {
                refArray[0].focus()
            }
        }
        console.log(refArray)
    }, [props.linkIn, refArray]);

    const registerRef = (ref, regID) => {
        if (ref !== null) {
            setRefArray(prev => {return {...prev, [regID]: ref}})
        }
    };

    const handlekeyOut = (tab) => {
        console.log(tab, Object.keys(refArray).length - 1);
        if (tab < Object.keys(refArray).length - 1) {
            console.log('haha')
            refArray[tab + 1].focus()
        } else {
            console.log('lala')
            refArray[tab].blur()
            props.linkOut(props.clusterID + 1)
        }
    };

    return props.children.map((child, i) => {
        return React.cloneElement(child, {
            tabID: i,
            refOut: registerRef,
            keyOut: handlekeyOut,
            key: `test_${i}`,
            className: `test_${i}`
        })
    });
};

export default PageController;



// malarky pile:

// let properties = {
//     refOut: registerRef,
//     keyOut: handlekeyOut,
//     key: `test_${componentRegisterIndex}`,
//     className: `test_${componentRegisterIndex}`
// };

// const addNewComponent = () => {
//     let clonedComponent = React.cloneElement(props.children[componentRegisterIndex], properties)
//     setReturnComponents([...returnComponents, clonedComponent]);
// }
//
// useEffect(() => {
//     if (componentRegisterIndex < props.children.length) {
//         addNewComponent()
//     }
// }, [componentRegisterIndex]);