import React, {useContext} from 'react'

import appObject from "../library/constants/appObject";

import {AppContext} from "../store/contexts/AppContext";

const SwitchBoard = () => {

    const {activeApp} = useContext(AppContext)

    const Component = appObject[activeApp][1];

    return (
        <Component />
        )

};

export default SwitchBoard