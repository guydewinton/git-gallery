import React, {useRef, useEffect} from 'react';
import styled from "styled-components";

const StyledInput = styled.input`
   position: absolute !important;
   top: -9999px !important;
   left: -9999px !important;
  
`;

const MasterFocusInput = (props) => {

    const inputRef = useRef(null);

    useEffect(() => {
        inputRef.current.focus();
    }, []);

    const changeHandler = (event) => {
        console.log(event.target.value)
    };

    const focusHandler = () => {
        console.log('focus!')
    };

    const keyDownHandler = () => {
        console.log('keyDown')
    };

    return <StyledInput ref={inputRef} onKeyDown={keyDownHandler} onChange={changeHandler} onFocus={focusHandler}/>

};

export default MasterFocusInput