import React from 'react';

import TopNavContainer from "../containers/TopNavContainer";

const TopNavFrame = () => {
    return (
        <div >
            <TopNavContainer/>
        </div>
    )
};

export default TopNavFrame;