import React from "react";
import styled from "styled-components";

import TopNavFrame from "../frames/TopNavFrame";
import BodyFrame from "../frames/BodyFrame";

import sizes from "../../library/theme/definitions/sizes";
import InSign from "../../../applications/InSign/InSign";

const topNavHeight = sizes.frames[1].topNavHeight[0]

const TopNavWrapper = styled.div`
  height: ${topNavHeight};
  width: 100vw;
  background-color: red;
`;

const BodyWrapper = styled.div`
  height: calc(100vh - ${topNavHeight});
  width: 100vw;
  background-color: blue;
`;

const MainPane = (props) => {

    return (
        <>
            <TopNavWrapper>
                <TopNavFrame/>
            </TopNavWrapper>
            <BodyWrapper>
                {props.children}
            </BodyWrapper>
        </>
    )
};

export default MainPane


// const BodyDiv = styled.div`
//   height: 100vh;
//   width: 100vw;
//   display: flex;
//   flex-direction: row;
//   overflow: hidden;
//   background-color: aqua;
// `;
//
// const ShadowDiv = styled.div`
//   box-shadow: 2px 0 8px 0 rgba(0, 0, 0, 0.1), 4px 0 20px 0 rgba(0, 0, 0, 0.1);
//   z-index: 5;
//
// `;

//
// const MainPane = (props) => {
//
//     return (
//         <>
//             <BodyDiv>
//                 <ShadowDiv>
//                     <SideBar/>
//                 </ShadowDiv>
//                 <ContentRoutes/>
//             </BodyDiv>
//         </>
//     )
// };
//
// export default MainPane