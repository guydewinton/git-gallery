import React from 'react';
import {MemoryRouter, Switch, Route} from 'react-router-dom'
import {ThemeProvider} from "styled-components";

import theme from "../library/theme/theme";

import SwitchBoard from "./SwitchBoard";


import MasterFocusInput from "./components/elements/MasterFocusInput";
import InSign from "../applications/InSign/InSign";
import CombinedContext from "../store/CombinedContext";
import MainPane from "./components/pages/MainPane";


const Application = () => {


    return (
        <CombinedContext>
            <ThemeProvider theme={theme}>
                <MemoryRouter>
                    <MasterFocusInput/>
                    <Switch>
                        <MainPane>
                            <SwitchBoard />
                        </MainPane>
                    </Switch>
                </MemoryRouter>
            </ThemeProvider>
        </CombinedContext>

    );
};

export default Application;
