export default {
    frames: [
        'Frames',
        {
            topNavHeight: ['0rem'],
        }
    ],
    scroll: [
        'Scroll',
        {
            bigScrollWidth: ['10rem'],
            bigScrollBorderRadius: ['5rem'],
        }
    ],

}