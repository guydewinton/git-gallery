from src.library.helpers.console.print_list import watch_console, trigger_console, log_console
from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import read_watch_items, read_trigger_items, read_log, read_watch_item


def watchtrigger_list_cli_router(args):

    process_user_options_server(args)

    watch_items = []

    if args.watchtrigger_id:
        if args.sudo:
            watch_items = read_watch_item(args.watchtrigger_id)
        else:
            watch_items = filter_list_by_user(read_watch_item(args.watchtrigger_id), args.user)
    else:
        if args.sudo:
            watch_items = read_watch_items()
        else:
            watch_items = filter_list_by_user(read_watch_items(), args.user)

    if len(watch_items) == 0:
        args.raise_error('Invalid watchtrigger id.')

    if args.sudo:
        trigger_items = read_trigger_items()
    else:
        trigger_items = filter_list_by_user(read_trigger_items(), args.user)

    log = []

    if args.log:
        log = read_log()

    watchtrigger_count = 0

    for i, watch_item in enumerate(watch_items):
        for trigger_item in trigger_items:
            if watch_item['id'] == trigger_item['id']:
                if not args.watchtrigger_id:
                    args.compile_print(f"#{watchtrigger_count}:")
                watch_console(watch_item, 3, args)
                args.compile_print('   Trigger:')
                trigger_console(trigger_item, 6, args)
                if args.log:
                    log_iteration = 0
                    args.compile_print(f"{' ' * 6}Log:")
                    for log_item in log:
                        if log_item['trigger_id'] == trigger_item['id']:
                            args.compile_print(f"{' ' * 9}#{log_iteration}")
                            log_console(log_item, 12, args)
                            log_iteration += 1
                    if log_iteration == 0:
                        args.compile_print(f"{' ' * 6}No log entries recorded.")
                watchtrigger_count += 1
                args.compile_print('')
    if watchtrigger_count == 0:
        args.compile_print('No watchtriggers registered.\n')
