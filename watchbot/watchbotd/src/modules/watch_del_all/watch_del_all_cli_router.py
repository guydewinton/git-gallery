from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import delete_watch_items, read_watch_items


def watch_del_all_cli_router(args):

    process_user_options_server(args)

    watch_items = filter_list_by_user(read_watch_items(), args.user)

    watch_count = 0

    for watch_item in watch_items:
        delete_watch_items(args, watch_item['id'])
        watch_count += 1

    if watch_count == 0:
        args.compile_print(f"Watch id {args.watch_id} not found for user {args.user}.")
    else:
        args.compile_print(f"\n{watch_count} watches deleted\n")

    delete_watch_items(args)


