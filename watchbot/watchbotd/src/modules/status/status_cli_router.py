from os_tools.generic import execute_command

from src.library.helpers.sqlite.sqlite import read_watch_items, read_trigger_items, read_log, introspect_watch_db


def status_cli_router(args):

    watch_db_status = introspect_watch_db()

    watches = read_watch_items()
    triggers = read_trigger_items()
    log = read_log()

    if len(watches) == 1:
        watch_message = f"{len(watches)} watch registered."
    else:
        watch_message = f"{len(watches)} watches registered."

    if len(triggers) == 1:
        trigger_message = f"{len(triggers)} trigger registered."
    else:
        trigger_message = f"{len(triggers)} triggers registered."

    if len(log) == 1:
        log_message = f"{len(log)} log entry recorded."
    else:
        log_message = f"{len(log)} log entries recorded."

    stdout, stderr, code = execute_command('systemctl status watchbot.service | grep active')

    args.compile_print(f"""
Daemon Service:
{stdout.replace('   Active: ', '   ')}
Watch DB:
    {watch_db_status['tables']['watch']['status']}
    {watch_message}

Trigger DB:
    {watch_db_status['tables']['trigger']['status']}
    {trigger_message}

Log DB:
    {watch_db_status['tables']['log']['status']}
    {log_message}
    """
    )

    """
    =================
     WATCHBOT STATUS
    =================
    
    Daemon Service:
       active (running) since ...
    Watch DB:
       OK: [All required fields present and correct]
       23 watches registered
    Trigger DB:
       OK: [All required fields present and correct]
       12 triggers registered
    Log DB:
       OK: [All required fields present and correct]
       WARNING: [Additional unnecessary fields also present]
       782 log entries
    Other DB:
       ERROR: [Missing required fields - try reinstalling watchbot]
    
    
    
    
    """