import argparse
import uuid


from src.library.helpers.sqlite.sqlite import create_watch_item, create_trigger_item


def watchtrigger_cli_router(args):

    if not args.name:
        args.name = str(uuid.uuid4())
    args.watch_id = args.name

    # TODO: things to do:
        # check if directory is a file or a directory
        # parse ext and event to convert to list
        # sprt out infinite loop if watcher watches directory which affected by watch trigger

    create_watch_item(args)
    create_trigger_item(args)

    # TODO: combine watch and trigger permission check so that watches are not created if trigger doesn't have permissions

    args.compile_print('')
