from src.library.helpers.console.print_list import log_console
from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import read_log


def log_cli_router(args):

    process_user_options_server(args)

    if args.name:
        log = read_log(args.name)
    else:
        log = read_log()

    if args.sudo:
        user_log = log
    else:
        user_log = filter_list_by_user(log, args.user)

    if args.name:
        iteration = 0
        for log_item in user_log:
            if log_item['trigger_id'] == args.name:
                args.compile_print(f"#{iteration}")
                log_console(log_item, 3, args)
                iteration += 1
        if iteration == 0:
            args.compile_print('No log entries recorded for this trigger id.')
    else:
        if len(user_log) > 0:
            for i, log_item in enumerate(user_log):
                if log_item['user'] == args.user:
                    args.compile_print(f"#{i}")
                    log_console(log_item, 3, args)
        else:
            args.compile_print('No log entries recorded.')
    #
    #

    args.compile_print('')
