import sys

from cryptography.fernet import Fernet


class ArgClass:

    def __init__(self, args, connection, aes_key):
        for key, value in args.items():
            self.__setattr__(key, value)
        self.server_connection = connection
        self.server_response = ''
        self.aes_key = aes_key

    def raise_error(self, warning):
        self.server_response = f"{self.server_response}\nError: {warning}\nOperation cancelled\n"
        f = Fernet(self.aes_key)
        encrypted_response = f.encrypt(bytes(self.server_response, 'utf-8'))
        self.server_connection.send(encrypted_response)
        self.server_connection.close()
        sys.exit()

    def compile_print(self, message):
        self.server_response = f"{self.server_response}\n{message}"

    def close_connection(self):
        self.server_connection.close()
