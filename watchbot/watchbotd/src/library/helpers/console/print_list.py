import json


def watch_console(watch_item, leading_space, args):
    spacer = ' ' * leading_space
    args.compile_print(f"{spacer}Watch ID:     {watch_item['id']}")
    args.compile_print(f"{spacer}Path:         {watch_item['path']}")
    args.compile_print(f"{spacer}Type:         {watch_item['type']}")
    if watch_item['type'] == 'dir':
        args.compile_print(f"{spacer}Extensions:   {watch_item['ext']}")
        args.compile_print(f"{spacer}Recursive:    {watch_item['recursive']}")
    args.compile_print(f"{spacer}User:         {watch_item['user']}")
    args.compile_print(f"{spacer}Created:      {watch_item['timestamp']}")


def trigger_console(trigger_item, leading_space, args):
    spacer = ' ' * leading_space
    args.compile_print(f"{spacer}Trigger ID:     {trigger_item['id']}")
    args.compile_print(f"{spacer}Watch ID:       {trigger_item['watch_id']}")
    args.compile_print(f"{spacer}Event Type:     {trigger_item['event']}")
    args.compile_print(f"{spacer}Working Dir:    {trigger_item['work_dir']}")
    args.compile_print(f"{spacer}Command:        {' '.join(json.loads(trigger_item['command']))}")
    args.compile_print(f"{spacer}User:           {trigger_item['user']}")
    args.compile_print(f"{spacer}Created:        {trigger_item['timestamp']}")


def log_console(log_item, leading_space, args):

    spacer = ' ' * leading_space

    if len(log_item['stdout']) > 0:
        if '\n' in log_item['stdout']:
            if log_item['stdout'][-1:] == '\n':
                log_item['stdout'] = log_item['stdout'][:-1]
            if '\n' in log_item['stdout']:
                log_item['stdout'] = f"\n{log_item['stdout']}"
                log_item['stdout'] = log_item['stdout'].replace('\n', f"\n{spacer}   ")

    if len(log_item['stderr']) > 0:
        if '\n' in log_item['stderr']:
            if log_item['stderr'][-1:] == '\n':
                log_item['stderr'] = log_item['stderr'][:-1]
            if '\n' in log_item['stderr']:
                log_item['stderr'] = f"\n{log_item['stderr']}"
                log_item['stderr'] = log_item['stderr'].replace('\n', f"\n{spacer}   ")

    args.compile_print(f"{spacer}Trigger ID:    {log_item['trigger_id']}")
    args.compile_print(f"{spacer}Timestamp:     {log_item['timestamp']}")
    args.compile_print(f"{spacer}STDOUT:        {log_item['stdout']}")
    args.compile_print(f"{spacer}STDERR:        {log_item['stderr']}")
    args.compile_print(f"{spacer}Exit Code:     {log_item['exit_code']}")
    args.compile_print(f"{spacer}User:          {log_item['user']}")
