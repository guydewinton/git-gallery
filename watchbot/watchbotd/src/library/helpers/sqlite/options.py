import json
import os
import uuid
from os.path import exists, isdir, isfile

from os_tools.generic import check_user_path_permissions

from src.library.helpers.data.user_process import process_user_options_server
from src.library.helpers.data.data import string_or_list, format_string_for_sql, comma_seperated_string_to_list, \
    remove_spaces_from_list_of_strings
from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.sqlite import sqlite


def watch_options(args):
    watch_options = {}
    watch_type = ''
    process_user_options_server(args)
    watch_options['user'] = args.user
    args.check_password = True
    if not args.name:
        args.name = str(uuid.uuid4())
    if exists(args.src_path):
        if isdir(args.src_path):
            watch_type = 'dir'
            if args.recursive:
                watch_options['recursive'] = 'True'
            else:
                watch_options['recursive'] = 'False'
        elif isfile(args.src_path):
            if args.recursive:
                args.raise_error('Files cannot be watched recursively.')
            else:
                watch_type = 'file'
                watch_options['recursive'] = 'False'
        if args.sudo:
            watch_options['sudo'] = 'True'
        else:
            if check_user_path_permissions(args.src_path, args.user, ['r', 'w', 'x'], True, args.recursive):
                watch_options['sudo'] = 'False'
            else:
                args.raise_error('User does not have permission to perform this watch.')
    else:
        args.raise_error('Invalid path.')
    if args.ext:
        if watch_type == 'file':
            args.raise_error('Cannot set extensions when watching a file.')
        else:
            value_list = comma_seperated_string_to_list(args.ext)
            ext_list = remove_spaces_from_list_of_strings(value_list)
            watch_options['ext'] = format_string_for_sql(string_or_list(ext_list, args))
    else:
        watch_options['ext'] = json.dumps(['*'])
    return args.name, args.src_path, watch_type, watch_options


def trigger_options(args):
    if not args.check_password:
        process_user_options_server(args)
    trigger_options = {
        'event': None,
        'work_dir': None,
    }
    if not args.name:
        args.name = str(uuid.uuid4())
    if args.event:
        trigger_options['event'] = string_or_list(args.event, args)
    else:
        trigger_options['event'] = 'modified'
    watch = filter_list_by_user(sqlite.read_watch_item(args.watch_id), args.user)
    if len(watch) == 0:
        args.raise_error('Invalid watch id.')
    elif len(watch) == 1:
        args.watch = watch[0]
    if args.work_dir:
        if isdir(os.path.abspath(args.work_dir)):
            trigger_options['work_dir'] = os.path.abspath(args.work_dir)
        else:
            args.raise_error('Invalid working directory.')
    else:
        if isfile(watch[0]['path']):
            trigger_options['work_dir'] = os.path.dirname(os.path.abspath(watch[0]['path']))
        else:
            trigger_options['work_dir'] = watch[0]['path']
    if args.sudo:
        trigger_options['sudo'] = 'True'
    else:
        if check_user_path_permissions(trigger_options['work_dir'], args.user, ['x'], False, False):
            trigger_options['sudo'] = 'False'
        else:
            args.raise_error('User does not have permission to register this trigger.')
    command = json.dumps(args.command)
    return args.name, args.watch_id, command, trigger_options
