import os

from os_tools.generic import check_user_password, check_user_sudo, get_file_dir_info


def process_user_options_server(args):
    password_check = False
    if args.user:
        if args.auth == 'pass':
            if check_user_password(args.user, args.password):
                password_check = True
            else:
                args.raise_error('Incorrect password.')
        elif args.auth == 'cert':
            valid_cert = False
            path = f"/usr/local/lib/watchbot/perms/{args.password[0:8]}{args.user}"
            if get_file_dir_info(path)[2] == args.user:
                with open(path, 'r') as file:
                    unique_string = file.read()
                    file.close()
                    os.remove(path)
                    if unique_string == args.password:
                        valid_cert = True
            if not valid_cert:
                args.raise_error('Woah! Weird... maybe a malicious actor present.')
    if args.sudo:
        if check_user_sudo(args.user):
            if not password_check:
                if not check_user_password(args.user, args.password):
                    args.raise_error('Incorrect password.')
        else:
            args.raise_error('This user does not have sudo privileges.')
