# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['/home/guydewinton/Documents/DevStuff/projects/watchbot/watchbot/watchbot.py'],
             pathex=['/home/guydewinton/.local/bin/envs/full_django_v2/lib/python3.7/site-packages/sqlite3_api', '/home/guydewinton/Documents/DevStuff/projects/watchbot/watchbot/', '/usr/local/etc/watchbot/', '/usr/local/lib/watchbot/', '/home/guydewinton/Documents/DevStuff/projects/watchbot/watchbot/package/spec/'],
             binaries=[],
             datas=[],
             hiddenimports=['sqlite3_api'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='watchbot',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True )
