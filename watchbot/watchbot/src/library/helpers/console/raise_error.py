import sys


def raise_error(warning):
    print(f"\nError: {warning}")
    print('Operation cancelled\n')
    sys.exit(1)