from src.library.helpers.routing.routing import cli_route_terminator, socket_router


def import_cli_router(argparser, subparser, router_args):
    subparser.add_argument(metavar='<PATH>', dest='file_path', type=str, help='Path of JSON file to import.')

    args = cli_route_terminator(argparser)

    args.sudo = True

    socket_router(args, router_args[1][0])
