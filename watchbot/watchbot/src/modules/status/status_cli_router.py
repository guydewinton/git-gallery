from src.library.helpers.routing.routing import cli_route_terminator, socket_router


def status_cli_router(argparser, subparser, router_args):

    args = cli_route_terminator(argparser)

    socket_router(args, router_args[1][0])
