import os
import sys
import shutil

from sqlite3_api.base_class import SqliteManager

from uninstaller import uninstaller
from lib import config, db_schema


os.path.dirname(os.path.abspath(__file__))


def installer(current_user):

    def reinstall_input():
        print('Would you like to reinstall this software?')
        user_input = input('(y/n): ')
        if user_input == 'y':
            uninstaller()
            installer(current_user)
        elif user_input == 'n':
            print('\nInstallation aborted.\n')
        else:
            print('\nInvalid input. Installation aborted.\n')

    if os.name == 'posix':

        if os.path.exists('/usr/local/bin/watchbot') and os.path.exists('/usr/local/bin/watchbotd') and os.path.exists('/usr/local/lib/watchbot/watchbot.db'):
            print('\nIt appears that this software has already been installed.')
            reinstall_input()

        elif os.path.exists('/usr/local/bin/watchbot') or os.path.exists('/usr/local/bin/watchbotd') or os.path.exists('/usr/local/lib/watchbot/watchbot.db'):
            print('\nIt appears that this software has an incomplete pre-existing installation.')
            reinstall_input()
        else:
            os.system('sudo groupadd watchbot')
            if not os.path.exists('/usr/local/lib/watchbot'):
                os.mkdir('/usr/local/lib/watchbot')
                os.chmod('/usr/local/lib/watchbot', 0o730)
                shutil.chown(path='/usr/local/lib/watchbot', user='root', group='watchbot')
                print('Directory /usr/local/lib/watchbot created.')
            else:
                print('Directory /usr/local/lib/watchbot already exists')

            if not os.path.exists('/usr/local/lib/watchbot/perms'):
                os.mkdir('/usr/local/lib/watchbot/perms')
                os.chmod('/usr/local/lib/watchbot/perms', 0o730)
                shutil.chown(path='/usr/local/lib/watchbot/perms', user='root', group='watchbot')
                print('Directory /usr/local/lib/watchbot/perms created.')
            else:
                print('Directory /usr/local/lib/watchbot/perm already exists')

            watchbot_db_instance = SqliteManager(db_schema.watchbot_schema, f'{config.data_dir}{config.watchbot_db}')
            print('DB file /usr/local/lib/watchbot/watchbot.db created.')
            if not watchbot_db_instance.validate_db_schema()['code'] == 0:
                watchbot_db_instance.deploy_schema()
            print('Watchbot DB initialized.')

            os.chmod('/usr/local/lib/watchbot/watchbot.db', 0o700)
            shutil.chown(path='/usr/local/lib/watchbot/watchbot.db', user='root', group='root')

            os.chdir(sys._MEIPASS)

            if not os.path.exists('/usr/local/bin/watchbot'):
                try:
                    shutil.move('./binaries/watchbot', '/usr/local/bin/')
                    os.chmod('/usr/local/bin/watchbot', 0o755)
                    shutil.chown(path='/usr/local/bin/watchbot', user='root', group='watchbot')
                    print('Copied watchbot binary to /usr/local/bin/')
                except shutil.Error:
                    print(shutil.Error)
            else:
                print('File /usr/local/bin/watchbot already exists.')

            if not os.path.exists('/usr/local/bin/watchbotd'):
                try:
                    shutil.move('./binaries/watchbotd', '/usr/local/bin/')
                    os.chmod('/usr/local/bin/watchbotd', 0o700)
                    shutil.chown(path='/usr/local/bin/watchbotd', user='root', group='root')
                    print('Copied watchbotd binary to /usr/local/bin/')

                except shutil.Error:
                    print(shutil.Error)
            else:
                print('File /usr/local/bin/watchbotd already exists.')

            if not os.path.exists('/etc/systemd/system/watchbot.service'):
                try:
                    shutil.move('./etc/watchbot.service', '/etc/systemd/system/')
                    os.chmod('/etc/systemd/system/watchbot.service', 0o700)
                    shutil.chown(path='/etc/systemd/system/watchbot.service', user='root', group='root')
                    print('Copied watchbot.service to /etc/systemd/system/')
                except shutil.Error:
                    print(shutil.Error)
            else:
                print('File /etc/systemd/system/watchbot.service already exists.')

            os.system('systemctl daemon-reload')
            os.system('systemctl enable watchbot')
            os.system('systemctl start watchbot')

            print('Watchbot service added to systemd.')

            print('\nInstallation complete.\n')

    else:

        print('This software currently only supports POSIX systems')


active_user = sys.argv[1]

installer(active_user)
