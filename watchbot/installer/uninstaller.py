import os
import shutil


def uninstaller():
    if os.path.exists('/usr/local/bin/watchbot'): 
        os.remove('/usr/local/bin/watchbot')
    if os.path.exists('/usr/local/bin/watchbotd'):
        os.remove('/usr/local/bin/watchbotd')
    if os.path.exists('/usr/local/lib/watchbot/'):
        shutil.rmtree('/usr/local/lib/watchbot/')
    print('\nAll watchbot files removed from system.')
    if os.path.exists('/etc/systemd/system/watchbot.service'):
        os.system('sudo systemctl stop watchbot')
        os.system('sudo systemctl disable watchbot')
        os.system('sudo systemctl daemon-reload')
        os.remove('/etc/systemd/system/watchbot.service')
    print('Watchbot service removed from systemd.\n')
