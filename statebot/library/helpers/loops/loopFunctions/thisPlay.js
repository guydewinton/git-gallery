// const huh = {thing: 'consfusion'}
//
//
// const nestedObject = {
//     thisArrowFunc: {
//         function: (blah) => {
//             const thing = blah
//             const number = 6
//             // this.that = blah

//     },
//         object: {},
//         string: 'lala'
//     },
//     thisRegularFunc: function(blah) {
//         const thing = blah
//         this.that = blah
//     },
//     innerNest: {
//         thisArrowFunc: () => {
//         },
//         thisRegularFunc: function() {
//         },
//     },
// };
//
//
//
// nestedObject.thisArrowFunc.function(8)
// nestedObject.thisRegularFunc('blah')
// nestedObject.innerNest.thisArrowFunc()
// nestedObject.innerNest.thisRegularFunc()
//
// const one = 1
// const two = 2
//
// const abstractedIf = (this.two === this.three);
// // an array of matching conditions, either localised address on the object or with this
//
// if (abstractedIf) {
// } else {
// }
//
// const d1 = [
//     {
//         two: 2,
//         three: 3,
//     }
// ]
//
// d1.forEach((thing) => {
//     (function () {
//         if (abstractedIf) {
//         } else {
//         }
//     }).call(thing)
// })

class SomeClass {

    static staticFunction = () => {
    }

    instanceFunction = () => {
    }
}

// SomeClass.staticFunction()
// SomeClass.instanceFunction()

const someObject = new SomeClass()

// someObject.staticFunction()
someObject.instanceFunction()