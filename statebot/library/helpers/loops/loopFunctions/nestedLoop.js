const nestForLoop = (forLoopObject) => {


    const innerLooper = (innerForLoopObject, dataObject = []) => {

        innerForLoopObject.forEach((forLoopArray) => {


            if (Array.isArray(forLoopArray[2])) {
                innerLooper(forLoopArray[2])
            }

            if (Array.isArray(forLoopArray[1])) {
                forLoopArray[1].forEach((functionArray) => {

                    functionArray[0]()

                    /// would need to use named arguements!!! ({blah: blah}) d'ahhh!!!!


                });
            }

        })

    }

    innerLooper(forLoopObject)


}

export default nestForLoop







// const nestedLoop = (forLoopObject) => {
//
//     if (forLoopObject.foreCall) {
//         forLoopObject.foreCall(forLoopObject)
//     }
//
//     /// does it return anything or just execute functions?
//     /// so the objectProcessMap bundles a function to be executed here
//
//     /// maybe this just executes a single funtion... (or funtion array... hmm...
//     /// whether the function araay loop is here centrally or scatreed... better all here
//
//     forLoopObject.helper = {
//         i: 0,
//         funcArgs: {},
//         loopArray: [],
//         tick: false
//     }
//
//     const forLooper = () => {
//
//
//         let loopArray = forLoopObject.helper.loopArray
//         let loopI = forLoopObject.helper.i
//
//         while (forLoopObject.helper.i < loopArray.length) {
//
//             let i = forLoopObject.helper.i
//
//             let dataArray = loopArray[i][0]
//             let funcArray = loopArray[i][1]
//
//             forLoopObject.helper.i++
//
//             for (let j = 0; j < dataArray.length; j++) {
//
//                 forLoopObject.helper.funcArgs[`d${i}`] = dataArray[j]
//
//
//                 for (let k = 0; k < funcArray.length; k++) {
//
//
//                     funcArray[k](forLoopObject.helper.funcArgs)
//
//                     forLoopObject.helper.tick = true
//
//
//                 }
//
//                 forLoopObject.callBack(forLoopObject)
//             }
//
//
//         }
//
//
//
//     }
//
//     const loopObjectBuilder = () => {
//
//
//         forLoopObject.dataArray.forEach((dataArray, i) => {
//
//             if (i === forLoopObject.dataArray.length - 1) {
//
//                 let loopArray = [dataArray, forLoopObject.innerFunctionArray]
//                 forLoopObject.helper.loopArray.push(loopArray)
//
//             } else {
//
//                 let loopArray = [dataArray, forLooper]
//                 forLoopObject.helper.loopArray.push(loopArray)
//                 /// needs to be i for each dataArray
//                 // nned to remove this logic from inner core
//             }
//             // in order to remove ifs from inner space
//         })
//     }
//
//     loopObjectBuilder()
//
//     forLooper()
//
//     return forLoopObject.newArray
//
// }
//
//
//
// /// don't forget to clear the loopObject