import getFuncArrayParams from "../objects/getFuncArrayParams";
import nestArrayValues from "../objects/nestArrayValues";
import embedFunctionsIntoNestedArray from "../objects/embedFunctionsIntoNestedArray";
import nestedLoop from "../loops/loopFunctions/nestedLoop";


const buildNestedLoopObject = {
    inputData: {
        city: 'initCities',
        startups1: 'initStartups1',
        startups2: 'initStartups2',
        startups3: 'initStartups3',
    },
    outputMount: 'citiesAll',
    // method ?
    processMap: [
        // extracts values from each array object and sets them as variables: so instead of city.cities -> cities
        [
            'cities',
            (city, startups1) => {
                console.log(city.whatever)
            },
        ],
        [
            'cities',
            (city, startups2) => {
                console.log('two')
            },
        ],
        [
            'cities',
            (city, startups2, startups3) => {
                console.log('three')
            },
        ],
        [
            'cities',
            (city, startups1, startups3, startups4) => {
                console.log('three')
            },
        ],
        [
            'cities',
            (startups1, startups2) => {
                console.log('four')
            },
        ],
        [
            'cities',
            (startups2, startups3) => {
                console.log('five')
            },
        ],
        [
            'cities',
            () => {
                console.log('six')
            },
        ],
        [
            'cities',
            (bob, startups1) => {
                console.log('one')
            },
        ],
        [
            'cities',
            (city, bob) => {
                console.log('two')
            },
        ],
        [
            'cities',
            (city, startups2, startups3, startups4, startups5, startups6) => {
                console.log('three')
            },
        ],
        [
            'cities',
            (city, startups1, startups3, mouse) => {
                console.log('three')
            },
        ],
        [
            'cities',
            (startups1, startups2) => {
                console.log('four')
            },
        ],
        [
            'cities',
            (startups2, startups3) => {
                console.log('five')
            },
        ],
        [
            'cities',
            () => {
                console.log('six')
            },
        ]

    ]
}






export default () => {

    const processMap = buildNestedLoopObject.processMap.map((process) => {
        return [process[1], process[2]]
    });

    const funcArrayParams = getFuncArrayParams(processMap);

    const nestedArray = nestArrayValues(funcArrayParams);

    const nestFuncArray = embedFunctionsIntoNestedArray(nestedArray, processMap, funcArrayParams);

    nestedLoop(nestFuncArray)
    
    return nestFuncArray

};