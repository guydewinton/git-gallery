
export default (arrayLengthSets) => {

// extract all of the individual parameters
//      (out of object of arrays - double nested)

    const arrayLengthSetKeys = Object.keys(arrayLengthSets)

    let individualParams = {}

    arrayLengthSetKeys.forEach((key) => {
        arrayLengthSets[key].forEach((paramArray) => {
            paramArray.forEach((param) => {
                if (individualParams[param]) {
                    individualParams[param].push(param)
                } else {
                    individualParams[param] = [param]
                }
            })
        })
    })

/// takes in an object with  ...  and counts repetitions returning an object with the array values

// counts repetition of each parameter and assigns the number to a key of the value

    let individualParamKeys = Object.keys(individualParams)

    let paramCounter = {}

    individualParamKeys.forEach((key) => {
        let ticker = 0
        individualParams[key].forEach((paramArray) => {
            ticker++
        })
        paramCounter[key] = ticker
    })

    return paramCounter

}

// input: {
//     '1': [ [ 'd0' ] ],
//     '2': [ [ 'd0', 'd1' ] ],
//     '3': [ [ 'd0', 'd1', 'd2' ] ]
// }
// output: { d0: 3, d1: 2, d2: 1 }