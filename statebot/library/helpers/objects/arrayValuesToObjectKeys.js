
export default (arrays, nesting) => {

// extract all of the individual parameters
//      (out of object of arrays - double nested)

    let individualParams = {};

    const loopNester = (array, nest) => {
        array.forEach((element) => {
            if (nest > 0) {
                const innerNest = nest - 1;
                loopNester(element, innerNest)
            } else {
                if (individualParams[element]) {
                    individualParams[element].push(element)
                } else {
                    individualParams[element] = [element]
                }

            }
        })
    };

    loopNester(arrays, nesting);

    // const arrayLengthSetKeys = Object.keys(arrayLengthSets)
    //
    // let individualParams = {}
    //
    // arrayLengthSetKeys.forEach((key) => {
    //     arrayLengthSets[key].forEach((paramArray) => {
    //         paramArray.forEach((param) => {
    //             if (individualParams[param]) {
    //                 individualParams[param].push(param)
    //             } else {
    //                 individualParams[param] = [param]
    //             }
    //         })
    //     })
    // })

/// takes in an object with  ...  and counts repetitions returning an object with the array values

// counts repetition of each parameter and assigns the number to a key of the value


    let individualParamKeys = Object.keys(individualParams)

    let paramCounter = {}

    individualParamKeys.forEach((key) => {
        let ticker = 0
        individualParams[key].forEach(() => {
            ticker++
        })
        paramCounter[key] = ticker
    })



    return paramCounter

}

// input: [ 'd1', 'd0' , 'd0' , 'd0', 'd2', 'd1' ] (nesting = 0)
// input: [ [ 'd1', 'd0' ], [ 'd0' ], [ 'd0', 'd2', 'd1' ] ] (nesting = 1)
// input: [ [ [ 'd1', 'd0' ] ], [ [ 'd0' ], [ 'd0', 'd2', 'd1' ] ] ] (nesting = 2)
// output: { d0: 3, d1: 2, d2: 1 }