
// arrays the parameters by order of their repetitions

// useful with previous array... return an array from an object... the keys of the object become the values of the array


const objectOrderNumberByKey = (numberObject, biggestNumberArray = []) => {

    let biggestNumber = -1
    let biggestNumberIndex = 0

    const counterKeys = Object.keys(numberObject)

    counterKeys.forEach((key, i) => {
        if (numberObject[key] > biggestNumber) {
            biggestNumber = numberObject[key]
            biggestNumberIndex = i
        }
    })

    delete numberObject[counterKeys[biggestNumberIndex]]
    biggestNumberArray.push(counterKeys.splice(biggestNumberIndex, 1)[0])



    if (counterKeys.length > 0) {
        return objectOrderNumberByKey(numberObject, biggestNumberArray)
    }

    if (counterKeys.length === 0) {
        if (biggestNumberArray[0] === undefined) {
            return []

        } else {
            return biggestNumberArray

        }
    }

}
export default objectOrderNumberByKey

// input: { d0: 3, d1: 2, d2: 1 }
// output: [ 'd0', 'd1', 'd2' ]