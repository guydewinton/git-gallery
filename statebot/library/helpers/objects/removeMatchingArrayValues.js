export default (inputArrays, matchArray) => {

    const matchIndex = [];

    const returnArray = [];

    inputArrays.forEach((array, i) => {
        returnArray.push([])
        array.forEach((element, j) => {
            returnArray[returnArray.length -1].push(element)
            matchArray.forEach((match) => {
                if (element === match) {
                    matchIndex.push([i, j])
                }
            })
        })
    });

    for (let i = matchIndex.length -1; i >= 0; i--) {

        returnArray[matchIndex[i][0]].splice(matchIndex[i][1], 1)
    }

    // remove empty arrays from inputArrays
    for (let i = returnArray.length -1; i >= 0; i--) {

        if (returnArray[i].length === 0) {
            returnArray.splice(i, 1)
        }
    }

    return returnArray

}

// input:

    // inputArrays: [[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]]
    // matchArray: [0, 1]

// output:

    // inputArrays: [[2, 3], [2, 3], [2, 3]]
