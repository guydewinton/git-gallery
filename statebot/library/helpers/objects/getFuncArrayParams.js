// extract the parameters from the functions

// takes an array of functions (funcArray), returns an array of arrays containing the functions parameters (paramArrays)

import getFuncParams from "./getFuncParams";

export default (processMap) => {

    let paramArray = [];

    processMap.forEach((functionArray) => {

        paramArray.push(getFuncParams(functionArray[0]))

    });

    return paramArray

};

//input: [
//     [
//         'a',
//         (d1, d0) => {
//             return d0.a + d1.a
//         }],
//     // mount string if just one
//     [['b'], (d0) => {
//         return d0.b + d2.c.d
//     }, {}],
//     // mount array if multiple
//     [
//         ['c', 'd'],
//         (d0, d2, d1) => {
//             if (true) {
//                 return d0.a + d1.c.d + d2.c.e
//             }
//         },
//         {
//             overwrite: true,
//             merge: true,
//             createNew: false,
//             init: '',
//             condition: '',
//             callBack: '',
//         }
//     ],
// ]
// output: [ [ 'd1', 'd0' ], [ 'd0' ], [ 'd0', 'd2', 'd1' ] ]