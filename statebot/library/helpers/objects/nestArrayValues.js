import objectOrderNumberByKey from "./objectOrderNumberByKey";
import arrayValuesToObjectKeys from "./arrayValuesToObjectKeys";
import arrayNester from "./arrayNester";
import extractMatchingArrays from "./extractMatchingArrays";
import removeMatchingArrayValues from "./removeMatchingArrayValues";

const nestArrayValues = (inputArray, returnArray = [], arrayObject = [], nestLevel = -1, activeElements = []) => {

    if (nestLevel === -1) {
        // load initial arrays

        const arrays = [...inputArray];

        nestLevel = 0;

        arrayObject[nestLevel] = {
            arrays,
        };

        return nestArrayValues(inputArray, returnArray, arrayObject, nestLevel, activeElements)

    } else if (nestLevel === 0 && arrayObject[nestLevel].arrays.length === 0) {
        // RETURN ARRAY

        return returnArray

    } else if (arrayObject[nestLevel].arrays.length === 0) {
        // GO DOWN A LEVEL

        activeElements.splice(activeElements.length -1, 1);

        arrayObject.splice(nestLevel, 1);

        nestLevel--;

        return nestArrayValues(inputArray, returnArray, arrayObject, nestLevel, activeElements)

    } else if (arrayObject[nestLevel].arrays.length > 0) {
        // ADD -> GO UP, DOWN OR SAME LEVEL

        if (true) {

            const cleanedArrays = removeMatchingArrayValues(arrayObject[nestLevel].arrays, activeElements)

            const activeArrays = objectOrderNumberByKey(arrayValuesToObjectKeys(cleanedArrays, 1));

            activeElements.push(activeArrays.splice(0, 1)[0]);

        }

        const activeElement = activeElements[activeElements.length -1];

        returnArray = arrayNester(returnArray, nestLevel, activeElement);

        const [nestLevelArrays, higherLevelArrays] = extractMatchingArrays(arrayObject[nestLevel].arrays, activeElements);

        const cleanedNestedArrays = removeMatchingArrayValues(nestLevelArrays, activeElements);

        const sortedArrays = objectOrderNumberByKey(arrayValuesToObjectKeys(cleanedNestedArrays, 1));

        const cleanHigherArrays = higherLevelArrays.filter((array) => {
            return array.length > activeElements.length
        });

        arrayObject[nestLevel] = {
            arrays: [...nestLevelArrays],
            sortedArrays,
        };

        if (nestLevelArrays.length === 0 && higherLevelArrays.length === 1 && higherLevelArrays[0].length === activeElements.length) {

            activeElements.splice(activeElements.length -1, 1);

            arrayObject[nestLevel] = {
                ...arrayObject[nestLevel],
                arrays: [],
            };

            return nestArrayValues(inputArray, returnArray, arrayObject, nestLevel, activeElements)

        } else {

            nestLevel++;

            arrayObject[nestLevel] = {
                arrays: [...cleanHigherArrays],
            };

            return nestArrayValues(inputArray, returnArray, arrayObject, nestLevel, activeElements)

        }
    }
};

export default nestArrayValues