export default (array) => {

    const arrayLengthSets = {}

    array.forEach((arr) => {

        let existingArrayLengthSetsValue = []

        let arrLength = 0

        if (arr.length === 1 && arr[0] === '') {
            arrLength = 0
        } else {
            arrLength = arr.length
        }

        arrayLengthSets[arrLength] ? existingArrayLengthSetsValue = arrayLengthSets[arrLength] : existingArrayLengthSetsValue = []

        arrayLengthSets[arrLength] = [arr, ...existingArrayLengthSetsValue]

    })

    return arrayLengthSets

}


// maps the arrays of parameters to an object, keyed by the number of parameters in each array


// receives an array of array, returns an object with the inner array length as key

// input: [ [ 'd1', 'd0' ], [ 'd0' ], [ 'd0', 'd2', 'd1' ] ]
// output: {
//   '1': [ [ 'd0' ] ],
//   '2': [ [ 'd1', 'd0' ] ],
//   '3': [ [ 'd0', 'd2', 'd1' ] ]
// }