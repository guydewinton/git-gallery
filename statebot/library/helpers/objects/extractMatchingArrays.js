// extract arrayObject[nestLevel].arrays which match activeElements -> levelArrays

export default (inputArray, matchArray) => {


    let elementMatches = 0;

    const extractedArrays = [];
    const extractedIndex = [];

    const filteredArray = [];

    inputArray.forEach((array, i) => {
        elementMatches = 0;
        filteredArray.push([]);
        array.forEach((element) => {
            filteredArray[filteredArray.length -1].push(element)
            matchArray.forEach((activeElement) => {
                if (activeElement === element) {
                    elementMatches++;
                }
            })
        });

        if (elementMatches === matchArray.length) {

            extractedIndex.push(i);
            extractedArrays.push(array);

        }
    });

    // clear out inputArray of extractedArrays
    for (let i = extractedIndex.length - 1; i >= 0; i--) {
        inputArray.splice(extractedIndex[i], 1)
    }

    return [inputArray, extractedArrays]

}

// extracts arrays from an array of arrays which contain all of the elements of an array of values
// returns an array of arrays

// input:

    // inputArray: [[0, 1, 2, 3], [0, 1, 2, 4], [0, 1, 4, 5]] (array of arrays)
    // matchArray: [0, 1, 2] (array)

// output:
    // inputArray: [[0, 1, 4, 5]] (array of arrays)
    // extractedArrays: [[0, 1, 2, 3], [0, 1, 2, 4]] (array of arrays)