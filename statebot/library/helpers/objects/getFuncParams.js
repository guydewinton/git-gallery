export default (func) => {

    // String representaation of the function code
    let str = func.toString();

    // Remove comments of the form /* ... */
    // Removing comments of the form //
    // Remove body of the function { ... }
    // removing '=>' if func is arrow function
    str = str.replace(/\/\*[\s\S]*?\*\//g, '')
    str = str.replace(/\/\/(.)*/g, '')
    str = str.replace(/{[\s\S]*}/, '')
    str = str.replace(/=>/g, '')
    str = str.trim();

    if (str.indexOf("(") === 0) {

        // Start parameter names after first '('
        const start = str.indexOf("(") + 1;

        // End parameter names is just before last ')'
        const end = str.length - 1;

        str = str.substring(start, end).split(", ");


    } else {
        str = str.substring(0, str.length).split(", ");
    }

    const params = [];

    str.forEach(element => {

        // Removing any default value
        element = element.replace(/=[\s\S]*/g, '').trim();

        // if(element.length > 0)
            params.push(element);
    });

    return params;
}