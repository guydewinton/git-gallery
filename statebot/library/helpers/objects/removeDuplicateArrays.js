// could this instead be a duplicate array counter (maybe with options)

// returns index of duplicates... it is passed two arrays and it counts duplicate elements

// this function:
// goes through an object of arrays,
// iterates over the arrays (which are the same length),
// sorts them by first value
// iterates over each pair
// sorts the contents of each pair
// compares them
// counts same elements
// compares count with length
// if equal marks as duplicate (add index to array of duplicates)
// after iteration of primary array, deletes duplicates




// removes duplicates

// removes duplicates from an array of arrays of the same length

// receives an object of arrays with keys of of the nested array lengths

// removes duplicate arrays from each key

export default (arrayLengthSets) => {

    const arrayLengthSetKeys = Object.keys(arrayLengthSets)


// sort parameters in each set
    arrayLengthSetKeys.forEach((key) => {
        arrayLengthSets[key].forEach((paramSet) => {
            paramSet.sort()
        })

        // sort parameterSet array
        arrayLengthSets[key].sort((a, b) => a[0] - b[0])

        let duplicates = []
        let duplicate = true

        // check if two adjacent arrays of the same length contain the same set of values
        if (arrayLengthSets[key].length > 1) {
            for (let i = 1; i < arrayLengthSets[key].length; i++) {

                let uniqueArray = []

                arrayLengthSets[key][i].forEach((param, j) => {
                    if (param === arrayLengthSets[key][i - 1][j]) {
                        uniqueArray.push(false)
                    } else {
                        uniqueArray.push(true)
                    }
                })

                uniqueArray.forEach((bool) => {
                    if (bool) {
                        duplicate = false
                    }
                })
                duplicate && duplicates.push(i)

            }
            duplicates.forEach((index) => {
                arrayLengthSets[key].splice(index, 1)
            })
        }

    })

    return arrayLengthSets

}

// input: {
//          '2': [ [ 'd0', 'd1' ], [ 'd1', 'd0' ] ],
//          '3': [ [ 'd0', 'd2', 'd1' ] ]
//        }
// output: { '2': [ [ 'd0', 'd1' ] ], '3': [ [ 'd0', 'd1', 'd2' ] ] }
