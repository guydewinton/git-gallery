import getFuncArrayParams from "./getFuncArrayParams";

export default (nestedArray, processMap, funcArrayParams = []) => {

    if (funcArrayParams.length === 0) {
        funcArrayParams = getFuncArrayParams(processMap);
    }

    const insertArrays = (array, returnArray = []) => {
        // input: [ ['xxx', ['xxx', ['xxx']]], ['xxx', ['xxx', ['xxx']]] ]
        if (Array.isArray(array) && array.length > 0) {
            array.forEach((innerArray) => {
                if (Array.isArray(innerArray) && innerArray.length > 1) {
                    const arrayNesting = []
                    if (Array.isArray(innerArray)) {
                        for (let i = 1; i < innerArray.length; i++) {
                            arrayNesting.push(innerArray[i])
                        }
                    }
                    returnArray.push([innerArray[0], null, insertArrays(arrayNesting)])
                } else {
                    returnArray.push([innerArray[0], null, null])
                }
            })
        } else {
            returnArray.push([array[0], null, null])
        }

        return returnArray

    };

    const newArray = insertArrays(nestedArray)

    const funcEmbed = (array, activeElements = []) => {

        if (Array.isArray(array)) {

            array.forEach((innerArray) => {

                const elements = [...activeElements]

                elements.push(innerArray[0])

                if (Array.isArray(innerArray[2])) {
                    funcEmbed(innerArray[2], elements)
                }

                funcArrayParams.forEach((paramArray, i) => {

                    let matches = 0;

                    paramArray.forEach((param) => {
                        elements.forEach((element) => {
                            if (param === element) {
                                matches++
                            }
                        })
                    });

                    if (matches === elements.length && matches === paramArray.length) {
                        if (innerArray[1] === null) {
                            innerArray[1] = [processMap[i]]
                        } else {
                            innerArray[1].push(processMap[i])
                        }
                    }
                })
            })
        }
    };

    funcEmbed(newArray)


    return newArray

}

