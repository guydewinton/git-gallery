import buildNestedLoopObject from "./library/helpers/translators/buildNestedLoopObject";

const nestedLoopObject = buildNestedLoopObject();

console.dir(nestedLoopObject, { depth: null });

