from __future__ import absolute_import, unicode_literals
from celery import Celery
from motion.celery import app
from django.core import mail


class EmailDispatch(object):

    @staticmethod
    def mail_confirm(instance):
        with mail.get_connection() as connection:
            mail.EmailMessage(
                f'New post created by: {instance.request.user.first_name} {instance.request.user.last_name}',
                instance.request.data['title'] + '\n' + instance.request.data['text'], 'guydewinton@gmail.com',
                ['guydewinton@gmail.com'],
                connection=connection,
            ).send()

        return True


@app.task
def dispatch_email(source):
    EmailDispatch().mail_confirm(source)
