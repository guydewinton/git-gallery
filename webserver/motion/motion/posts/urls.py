from django.urls import path
from motion.posts import views

urlpatterns = [
    path('', views.PostListCreate.as_view()),
    path('<int:pk>/', views.PostGetPatchDelete.as_view()),
    path('likes/', views.UserPostsLiked),
    path('toggle-like/<int:pk>', views.LikesToggler.as_view()),
    path('following/', views.FollowedUsersPosts.as_view()),
    path('user/<int:pk>', views.GetUserPosts.as_view()),
    path('friends/', views.FriendsPosts.as_view()),
    path('share/', views.SharePost.as_view()),
]
