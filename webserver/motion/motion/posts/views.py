from __future__ import absolute_import, unicode_literals
from django.db.models import Q
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, DestroyAPIView, ListAPIView, \
    CreateAPIView
from rest_framework.permissions import IsAuthenticated
from dry_rest_permissions.generics import DRYPermissionFiltersBase
from motion.posts.permissions import IsOwnerOfPostOrReadOnly
from motion.posts.serializers import PostSerializer
from motion.joins.serializers import LikeSerializer, ShareSerializer
from rest_framework.response import Response
from motion.posts.models import Post
from motion.posts.tasks import dispatch_email
from motion.users.models import MotionUser
from motion.joins.models import Likes, Follows, Friends, Share
from rest_framework import filters

# Create your views here.


class ProjectFilterBackend(DRYPermissionFiltersBase):

    def filter_list_queryset(self, request, queryset, view):
        """
        Limits all list requests to only be seen by the owners or creators.
        """
        return queryset.filter(is_public=True)


class PostListCreate(ListCreateAPIView):
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated, IsOwnerOfPostOrReadOnly]
    filter_backends = (ProjectFilterBackend, filters.SearchFilter)
    search_fields = ['user__first_name', 'user__last_name', 'title', 'text']
    queryset = Post.objects.all().order_by('date_created')

    def perform_create(self, serializer):

        serializer.save(user=self.request.user)
        dispatch_email(self)


class PostGetPatchDelete(RetrieveUpdateDestroyAPIView):
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated, IsOwnerOfPostOrReadOnly]
    queryset = Post.objects.filter()


class GetUserPosts(ListAPIView):
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated, IsOwnerOfPostOrReadOnly]

    def get_queryset(self, **kwargs):
        return Post.objects.filter(user=MotionUser(id=self.kwargs['pk']))


class LikesToggler(CreateAPIView):
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated, IsOwnerOfPostOrReadOnly]

    def post(self, request, **kwargs):
        if len(Likes.objects.filter(user=self.request.user, post=Post(id=self.kwargs['pk']))) == 0:
            Likes.add_like(Likes, self.request.user, Post(id=self.kwargs['pk']))
            serializer = PostSerializer(Post.objects.get(id=self.kwargs['pk']))
            return Response(serializer.data)
        elif len(Likes.objects.filter(user=self.request.user, post=Post(id=self.kwargs['pk']))) > 0:
            Likes.remove_like(Likes, self.request.user, Post(id=self.kwargs['pk']))
            serializer = PostSerializer(Post.objects.get(id=self.kwargs['pk']))
            return Response(serializer.data)


class UserPostsLiked(ListAPIView):

    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated, IsOwnerOfPostOrReadOnly]

    def get_queryset(self):
        queryset = Post.objects.filter(likes_post__user=self.request.user)
        return queryset


class FollowedUsersPosts(ListAPIView):

    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated, IsOwnerOfPostOrReadOnly]

    def get_queryset(self):

        return Post.objects\
            .filter(user__follower_user__in=Follows.list_followed(self.request.user))\
            .order_by('date_created')

# TODO: apply follow method


class FriendsPosts(ListAPIView):

    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated, IsOwnerOfPostOrReadOnly]

    def get_queryset(self):
        user_friends = Friends.list_user_friends(self.request.user)
        return Post.objects.filter(Q(user__in=user_friends) | Q(user__in=user_friends))


class SharePost(CreateAPIView):
    serializer_class = ShareSerializer
    permission_classes = [IsAuthenticated, IsOwnerOfPostOrReadOnly]

    def perform_create(self, serializer):
        Share.share_post(Post(id=self.request.data['post']), MotionUser(id=self.request.data['user']))


