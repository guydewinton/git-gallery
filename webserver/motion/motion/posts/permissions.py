from rest_framework import permissions

from motion.posts.models import Post


class IsOwnerOfPostOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET' and Post.is_public:
            return True
        if request.user == obj.user:
            return True
        else:
            return False
