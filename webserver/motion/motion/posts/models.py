from django.db import models
from motion.users.models import MotionUser
from motion import joins

# Create your models here.


class Post(models.Model):
    user = models.ForeignKey(
        verbose_name='User',
        to=MotionUser,
        on_delete=models.CASCADE,
        related_name='post',
        blank=True,
        null=True,
    )
    title = models.CharField(
        verbose_name='Title',
        max_length=100,
        blank=False,
    )
    text = models.TextField(
        verbose_name='Text'
    )
    date_created = models.DateTimeField(
        verbose_name='Date Created',
        auto_now_add=True,
        editable=False,
    )
    date_modified = models.DateTimeField(
        verbose_name='Date Modified',
        auto_now=True,
        editable=False,
    )
    is_public = models.BooleanField(
        verbose_name='Public',
        default=False,
    )
    blah = models.CharField(
        verbose_name='blah',
        blank=False,
        max_length=100,
    )

    def __str__(self):
        return self.title

    def likes(self):
        return len(joins.models.Likes.objects.filter(post=self))
