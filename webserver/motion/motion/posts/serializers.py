from rest_framework import serializers
from motion.comments.serializers import CommentSerializer
from motion.joins.serializers import ShareSerializer
from motion.posts.models import Post
from motion.users.serializers import UserSerializer


class PostSerializer(serializers.ModelSerializer):

    user = UserSerializer(required=False)
    comments = CommentSerializer(required=False, many=True)
    shares = ShareSerializer(required=False, many=True)

    class Meta:
        model = Post
        fields = ['id', 'user', 'title', 'text', 'date_created', 'likes', 'comments', 'shares', 'is_public']
        read_only_fields = ['user']
