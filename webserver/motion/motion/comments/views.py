from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated

from motion.comments.models import Comment
from motion.posts.models import Post
from motion.comments.serializers import CommentSerializer

# Create your views here.


class CreateListComments(ListCreateAPIView):
    serializer_class = CommentSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Comment.objects.filter(post=Post(id=self.kwargs['pk']))

    def perform_create(self, serializer_class):
        return Comment.objects.create(user=self.request.user, post=Post(id=self.kwargs['pk']), text=self.request.data['text'])
