from rest_framework import serializers
from motion.comments.models import Comment


class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ['user', 'post', 'comment_text', 'date_created']
