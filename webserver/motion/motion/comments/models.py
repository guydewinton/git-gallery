from django.db import models

# Create your models here.
from motion.posts.models import Post
from motion.users.models import MotionUser


class Comment(models.Model):

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'

    user = models.ForeignKey(
        verbose_name='User',
        to=MotionUser,
        on_delete=models.CASCADE,
        related_name='comments'
    )
    post = models.ForeignKey(
        verbose_name='Post',
        to=Post,
        on_delete=models.CASCADE,
        related_name='comments',
    )
    comment_text = models.TextField(
        verbose_name='Comment',
        editable=True
    )
    date_created = models.DateTimeField(
        auto_now_add=True,
        editable=False
    )
    date_modified = models.DateTimeField(
        auto_now=True,
        editable=True
    )
