from django.urls import path
from motion.comments import views


urlpatterns = [
    path('<int:pk>', views.CreateListComments.as_view())
]
