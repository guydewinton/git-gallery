"""motion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView
from motion.joins.urls import follows, friends
from motion.register.views import ResetPassword, ValidateReset, RegisterInit

auth = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_refresh'),
    path('password-reset/', ResetPassword.as_view()),
    path('password-reset/validation/', ValidateReset.as_view()),
    path('registration/', RegisterInit.as_view())
]

social = [
    path('posts/', include('motion.posts.urls')),
    path('followers/', include(follows)),
    path('comments/', include('motion.comments.urls')),
    path('friends/', include(friends)),
]

api = [
    path('', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
    path('users/', include('motion.users.urls')),
    path('social/', include(social)),
    path('auth/', include(auth)),
    path('register/', include('motion.register.urls'))
]


urlpatterns = [
    path('api/', include(api)),
]
