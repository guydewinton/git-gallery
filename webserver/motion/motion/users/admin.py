from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from motion.users.models import MotionUser

# Register your models here.

admin.site.register(MotionUser, UserAdmin)
