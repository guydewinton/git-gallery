from rest_framework.generics import ListAPIView, RetrieveAPIView, RetrieveUpdateAPIView
from rest_framework import filters
from motion.users.models import MotionUser
from rest_framework.permissions import IsAuthenticated
from motion.users.permissions import IsOwnerOrReadOnly, IsMe
from motion.users.serializers import UserSerializer, MeSerializer


class ListUsers(ListAPIView):
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]
    queryset = MotionUser.objects.all()

    filter_backends = [filters.SearchFilter]
    search_fields = ['first_name', 'last_name']


class RetrieveUser(RetrieveAPIView):
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def get_queryset(self):
        return MotionUser.objects.filter(id=self.kwargs['pk'])


class GetMe(RetrieveUpdateAPIView):
    serializer_class = MeSerializer
    permission_classes = [IsAuthenticated, IsMe]
    queryset = MotionUser.objects.all()

    def get_queryset(self):
        self.kwargs['pk'] = self.request.user.pk
        return self.queryset

    def perform_update(self, serializer):
        user = MotionUser.objects.filter(id=self.kwargs['pk'])
        user.update(**self.request.data)
        return
