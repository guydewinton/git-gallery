from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from motion import joins

# Create your models here.


class MotionUser(AbstractUser):

    is_public = models.BooleanField(default=False)

    validation = models.CharField(max_length=50)

    objects = UserManager()

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def followers(self):
        return len(joins.models.Follows.objects.filter(followed=self))

    def friends(self):
        return len(joins.models.Friends.list_user_friends(self))
