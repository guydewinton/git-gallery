from rest_framework.serializers import ModelSerializer
from motion.users.models import MotionUser


class UserSerializer(ModelSerializer):

    class Meta:
        model = MotionUser
        fields = ['first_name', 'last_name', 'followers', 'friends', 'id']


class MeSerializer(ModelSerializer):

    class Meta:
        model = MotionUser
        fields = ['first_name', 'last_name', 'email', 'followers', 'friends', 'id']
