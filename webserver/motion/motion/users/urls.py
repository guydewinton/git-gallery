
from django.urls import path
from motion.users import views

urlpatterns = [
    path('', views.ListUsers.as_view()),
    path('<int:pk>', views.RetrieveUser.as_view()),
    path('me/', views.GetMe.as_view())
]
