from django.contrib import admin
from motion.joins.models import Likes, Follows, Friends, Share

# Register your models here.

admin.site.register(Likes)
admin.site.register(Follows)
admin.site.register(Friends)
admin.site.register(Share)
