from rest_framework import serializers
from motion.joins.models import Likes, Follows, Friends, Share
from motion.users.models import MotionUser
from motion.posts.models import Post
from motion.users.serializers import UserSerializer


class LikeSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=MotionUser.objects.all(), required=False)
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all(), required=False)

    class Meta:
        model = Likes
        fields = ['user', 'post']


class FollowsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Follows
        fields = '__all__'


class FriendSerializer(serializers.ModelSerializer):

    friend_a = UserSerializer(read_only=True)
    friend_b = UserSerializer(read_only=True)

    class Meta:
        model = Friends
        fields = ['friend_a', 'friend_b', 'status']


class ShareSerializer(serializers.ModelSerializer):

    user = UserSerializer(read_only=True)

    class Meta:
        model = Share
        fields = ['user']
