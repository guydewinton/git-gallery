from django.urls import path, include
from motion.joins import views


friends = [
    path('', views.FriendList.as_view()),
    path('request/<int:pk>', views.FriendRequest.as_view()),
    path('requests/<int:pk>', views.FriendRequestResponse.as_view()),
]

follows = [
    path('toggle-follow/<int:pk>', views.ToggleFollow.as_view()),
    path('followers/', views.ListFollowers.as_view()),
    path('following/', views.ListFollowing.as_view()),
]

urlpatterns = [
    path('followers/', include(follows))
]
