from itertools import chain

# Create your models here.


from django.db import models
from django.db.models import Q
from motion.users.models import MotionUser
from motion.posts.models import Post
from django.db.models.signals import pre_save

# Create your models here.


class Likes(models.Model):

    class Meta:
        unique_together = [['user', 'post']]
        verbose_name = 'Like'
        verbose_name_plural = 'Likes'

    user = models.ForeignKey(
        verbose_name='User Liker',
        to=MotionUser,
        related_name='likes_user',
        on_delete=models.CASCADE,
        unique=False,
    )
    post = models.ForeignKey(
        verbose_name='Post Liked',
        to=Post,
        related_name='likes_post',
        on_delete=models.CASCADE,
        unique=False,
    )
    date_created = models.DateTimeField(
        verbose_name='Date Created',
        auto_now_add=True,
        editable=False,
    )

    def add_like(self, liker_user, liked_post):
        self.objects.create(user=liker_user, post=liked_post)

    def remove_like(self, liker_user, liked_post):
        self.objects.filter(user=liker_user, post=liked_post).delete()

    def list_user_likes(self, liking_user):
        pass


class Follows(models.Model):
    class Meta:
        unique_together = [['follower', 'followed']]
        verbose_name = 'Follow'
        verbose_name_plural = 'Follows'

    follower = models.ForeignKey(
        verbose_name='Follower',
        to=MotionUser,
        related_name='followed_user',
        on_delete=models.CASCADE,
        unique=False
    )
    followed = models.ForeignKey(
        verbose_name='Followed',
        to=MotionUser,
        related_name='follower_user',
        on_delete=models.CASCADE,
        unique=False
    )
    date_created = models.DateTimeField(
        verbose_name='Date Created',
        auto_now_add=True,
        editable=False,
    )

    @staticmethod
    def add_follow(user_follower, user_followed):
        Follows.objects.create(follower=user_follower, followed=user_followed)

    @staticmethod
    def remove_follow(user_follower, user_followed):
        Follows.objects.filter(follower=user_follower, followed=user_followed).delete()

    @staticmethod
    def list_followers(followed_user):
        return Follows.objects.filter(followed=followed_user)

    @staticmethod
    def list_followed(follower_user):
        return Follows.objects.filter(follower=follower_user)


class Friends(models.Model):

    class Meta:
        unique_together = [['friend_a', 'friend_b']]
        verbose_name = 'Friend'
        verbose_name_plural = 'Friends'

    friend_a = models.ForeignKey(
        to=MotionUser,
        on_delete=models.CASCADE,
        related_name='friend_a'
    )
    friend_b = models.ForeignKey(
        to=MotionUser,
        on_delete=models.CASCADE,
        related_name='friend_b'
    )
    status = models.CharField(
        max_length=25,
        default='request',
        choices=[
            ('request', 'requested'),
            ('accept', 'accepted'),
            ('reject', 'rejected')
        ],
    )
    date_created = models.DateTimeField(
        auto_now_add=True,
        editable=False
    )

    @staticmethod
    def friend_request_init(friend_a, friend_b):
        Friends.objects.create(friend_a=friend_a, friend_b=friend_b)

    @staticmethod
    def friend_request_info(friend_a, friend_b):
        return Friends.objects.get(friend_a=friend_a, friend_b=friend_b)

    @staticmethod
    def friend_request_response(friend_a, friend_b, response):
        Friends.objects.filter(friend_a=friend_a, friend_b=friend_b).update(status=response)

    @staticmethod
    def list_user_friends(friend):
        user_friends = list(
            chain(MotionUser.objects.filter(Q(friend_b__friend_a=friend)),
                  MotionUser.objects.filter(Q(friend_a__friend_b=friend))))
        return user_friends

    def list_user_friends_posts(self):
        pass


class Share(models.Model):

    class Meta:
        unique_together = [['user', 'post']]
        verbose_name = 'Share'
        verbose_name_plural = 'Shares'

    user = models.ForeignKey(
        verbose_name='User Shared',
        to=MotionUser,
        related_name='shared_user',
        on_delete=models.CASCADE,
        unique=False,
    )
    post = models.ForeignKey(
        verbose_name='Post Shared',
        to=Post,
        related_name='shares',
        on_delete=models.CASCADE,
        unique=False,
    )
    date_created = models.DateTimeField(
        auto_now_add=True,
        editable=False
    )

    @staticmethod
    def share_post(post, user):
        Share.objects.create(post=post, user=user)
