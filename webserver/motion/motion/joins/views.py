from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView, ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from motion.joins.serializers import FollowsSerializer, FriendSerializer
from motion.users.serializers import UserSerializer
from motion.joins.models import Follows, Friends
from motion.users.models import MotionUser
from rest_framework.response import Response


class ToggleFollow(CreateAPIView):
    serializer_class = FollowsSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request, **kwargs):
        if len(Follows.objects.filter(follower=self.request.user, followed=MotionUser(id=self.kwargs['pk']))) == 0:
            Follows.add_follow(self.request.user, MotionUser(id=self.kwargs['pk']))
            return Response("Followed!")
        elif len(Follows.objects.filter(follower=self.request.user, followed=MotionUser(id=self.kwargs['pk']))) > 0:
            Follows.remove_follow(self.request.user, MotionUser(id=self.kwargs['pk']))
            return Response('Follow destroyed!!!!')


class ListFollowers(ListAPIView):

    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return MotionUser.objects.filter(followed_user__in=Follows.list_followers(self.request.user))


class ListFollowing(ListAPIView):

    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return MotionUser.objects.filter(follower_user__in=Follows.list_followed(self.request.user))


class FriendRequest(CreateAPIView):

    serializer_class = FriendSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, request, **kwargs):
        return Friends.friend_request_init(self.request.user, MotionUser(id=self.kwargs['pk']))


class FriendRequestResponse(RetrieveUpdateDestroyAPIView):

    serializer_class = FriendSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return Friends.friend_request_info(self.request.user, MotionUser(id=self.kwargs['pk']))

    def perform_update(self, serializer):
        Friends.friend_request_response(self.request.user, self.kwargs['pk'], self.request.data['response'])

    def perform_destroy(self, instance):
        Friends.objects.get(friend_a=self.request.user, friend_b=self.kwargs['pk']).delete()
        return 'Friend Deleted!!!'


class FriendList(ListAPIView):

    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Friends.list_user_friends(self.request.user)
